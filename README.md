# mFormApp


## Install node 

Install node version [5.11.1](https://nodejs.org/en/blog/release/v5.11.1/)

Run `node --version` to check install success.

## Init project

Use terminal to project workspace. 

Run `npm install` to get all dependencies.

Use `git checkout origin` for reverse code custom dependencies.

## Config project

File path `modules/Util/Config.js` set path api.

## Local development

### For Android
 
Open folder `android` with Android Studio

Install NDK version [r10e](https://developer.android.com/ndk/downloads/older_releases.html)

Set NDK path in local.properties file or menu "File > Project Structure > SDK Location > Android NDK location"

Download [Boost C++ 1.57.0](https://drive.google.com/open?id=1CKExaKrCO0hM8yER09tXx8QVi1s2_fDy)
and place it in "[Path to your project]/node_modules/react-native/ReactAndroid/build/downloads"

Run `rect-native start` to run server on local.

Run app to device with Android Studio

You can find the IP address in System Preferences → Network.
1. Make sure your laptop and your phone are on the same Wi-Fi network.
2. Open your React Native app on your device.
3. You'll see a red screen with an error. This is OK. The following steps will fix that.
4. Open the in-app [Developer menu](https://facebook.github.io/react-native/docs/0.38/debugging.html#accessing-the-in-app-developer-menu).
5. Go to Dev Settings → Debug server host for device.
6. Type in your machine's IP address and the port of the local dev server (e.g. 10.0.1.1:8081).
7. Go back to the Developer menu and select Reload JS.

You can now enable Live reloading from the Developer menu. Your app will reload whenever your JavaScript code has changed.

### For iOS

[Running On Device iOS](https://facebook.github.io/react-native/docs/0.38/running-on-device.html)
or just open DigitalForm.xcworkspace and run.

## Generate Release Build

### For Android
  
Customer Server: `npm run-script build-apk-customer`  
Internal Server: `npm run-script build-apk-internal`  
Staging Server: `npm run-script build-apk-staging`  
  
Note : Use key at "[Path to your project]/android/app/mForm.jks" if you want to generate APK from Android Studio

### For iOS
#### Archive with following scheme

#### AppStore
Customer Server: `DigitalForm Customer`  

#### Enterprise Distribution (For Tester & Internal User)
Customer Server: `DigitalForm Customer - Enterprise`  
Internal Server: `DigitalForm Internal - Enterprise`   
Staging Server: `DigitalForm Staging - Enterprise`  

#### Generate Temporary URL for Enterprise Distribution
- Install [antenna-ota](https://github.com/soulchild/antenna) with command `sudo gem install antenna-ota`
- Navigate to directory that contain your IPA file
- run command `antenna s3 -a [S3_ACCESS_KEY_ID] -s [S3_SECRET_ACCESS_KEY] --region [S3_REGION] --bucket [S3_BUCKET_NAME] --expires [EXPIRATION_TIME_IN_SECONDS] --file [IPA_FILE_NAME]`

Note:
- Maximium value for `EXPIRATION_TIME_IN_SECONDS` is 604800 (7 days)
- Please contact AIS staff for information related to S3
