/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Navigator,
  AsyncStorage,
  TouchableHighlight,
  Dimensions,
  BackAndroid,
  AppState,
  ActivityIndicator,
  NativeModules
} from 'react-native';

import FCM from 'react-native-fcm';

import AlertConfirmDialog from './modules/Util/AlertConfirmDialog';

import {MenuContext} from 'react-native-popup-menu';
import LoginPage from './modules/LoginPage'
import SignUpPage from './modules/SignUpPage'
import Topbar from './modules/Topbar'
import WorkingList from './modules/WorkingList'
import ImageViewer from './modules/CustomViews/ImageViewer'
import ProgressSpinner from './modules/Animation/ProgressSpinner'
import DFService from './modules/Service/DFService';
import apiConfig from './modules/Util/Config';
import DeviceInfo from 'react-native-device-info';

import STRINGS from './modules/Util/strings.js'

let NativeAwareAPI = NativeModules.RNAwareModule;

let STORAGE_KEY = '@DigitialFromIsUserAuthenticated:key';

let _navigator;
let routeProp;

let CONFIRM_CALLBACK = null;
let ALERT_CALLBACK = null;

let self;

var {height, width} = Dimensions.get('window');

class DigitalForm extends Component {

  constructor(props) {
    super(props);
    self = this;
    this.state = {
      isLoading: true,
      isAuthenticated: false,
      isSignUp: false,
      messages: '',
      firebasetoken: '',
      appState: AppState.currentState,
      previousAppState: "active",
      mainAppAlertMessage: STRINGS.COMMON.UNEXPECTED_ERROR_TRY_AGAIN,
      showMainAppAlert: false,
      mainAppConfirmMessage: STRINGS.POPUP_MSG.ARE_YOU_SURE,
      showMainAppConfrim: false,
      spinnerLoadingMsg: STRINGS.COMMON.LOADING,
      showMainSpinner: false,
      showUpdateApp: false,
      showImageViewer: false,
      mainImageViewHeight: null,
      mainImageViewWidth: null,
      mainImageViewUrl: null,
      mainImageViewTitle: null
    };

    this.isShowMenu = false;
    this.isShowForgot = false;
    this.isPreViewImage = false;
    this.appVersion = null;
    //Strings.setLanguage('th');

    AsyncStorage.getItem('LANGUAGE', (err, result) => {
      if (err || result === null) {
        console.log('Unable to get LANGUAGE so setting to default english');
        AsyncStorage.setItem('LANGUAGE', 'en', (err, result) => {
          if (err) {
            console.log('Unable to set the LANGUAGE into local storage')
          }
        });
        STRINGS.setLanguage('en');
      } else {
        console.log('Language: ', result);
        STRINGS.setLanguage(result);
      }
    });


  }

  _appendMessage(message) {
    this.setState({messages: this.state.messages.concat(message)});
  }

  componentDidMount() {
    //AppState.addEventListener('change', this._handleAppStateChange);
    FCM.getFCMToken().then(token => {
      // console.log('Android Token!!!', token)
      self.setState({firebasetoken: token})
      // store fcm token in your server
    });
    // this.notificationUnsubscribe = FCM.on('notification', (notif) => {
    //     // there are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
    //     console.log('Received Notification', notif.data)
    //     if(notif.local_notification){
    //       //this is a local notification
    //       console.log('Local Notification')
    //     }
    //     if(notif.opened_from_tray){
    //       //app is open/resumed because user clicked banner
    //       console.log('Opened notification from tray')
    //       console.log(notif.data)
    //     }
    // });
    // this.refreshUnsubscribe = FCM.on('refreshToken', (token) => {
    //     console.log(token)
    //     // fcm token may not be available on first load, catch it here
    // });
    this._loadInitialState().done();

    BackAndroid.addEventListener('hardwareBackPress', () => {
      let popToTop = false;
      let customBackFunction = false;
      if (this.isPreViewImage || this.isShowForgot) {
        return true;
      }

      if (this.state.isSignUp) {
        this.setState({
          isSignUp: false
        });
        return true;
      }


      if (_navigator && _navigator.getCurrentRoutes().length > 1) {

        if ("refreshWorkingList" in routeProp) {
          // console.log("getCurrentRoutes", routeProp.navigator.getCurrentRoutes().length);
          // console.log("getCurrentRoutes 2 : ", routeProp.navigator.getCurrentRoutes())

          if (_navigator.getCurrentRoutes().length <= 2) {
            routeProp.refreshWorkingList();
          }

          popToTop = true;
          customBackFunction = true
        }

        if ("draft_form_id" in routeProp) {
          routeProp.refreshWorkingList();
          popToTop = true;
          customBackFunction = true
        }

        if ("refreshNoti" in this.props) {
          this.props.refreshNoti();
        }

        if ("clearAll" in routeProp) {
          routeProp.clearAll();
          customBackFunction = false
        }

        if ('fromNotification' in routeProp) {
          customBackFunction = false
        }

        if ('fromProfile' in this.props) {
          this.props.navigator.refs['dash'].refreshList();
        }

        if (customBackFunction) {
          routeProp.onBack(popToTop);
        } else {
          _navigator.pop();
        }

        return true;
      }

      return this.isShowMenu;

    });
  }

  componentWillUnmount() {
    // prevent leaking
    //AppState.removeEventListener('change', this._handleAppStateChange);
    // this.refreshUnsubscribe();
    // this.notificationUnsubscribe();
  }

  async _loadInitialState() {
    try {
      let value = await AsyncStorage.getItem(STORAGE_KEY);
      if (value !== null) {
        const newValue = (value === 'true');
        this.setState({
          isAuthenticated: newValue,
          isLoading: false
        });
      } else {
        this.setState({isLoading: false});
      }
    } catch (error) {
      console.log(error);
      this._appendMessage('AsyncStorage error: ' + error.message);
      this.setState({isLoading: false});
    }
    this.getVersion();
  }

  async _onAuthenticationSuccess() {
    try {
      await AsyncStorage.setItem(STORAGE_KEY, 'true');
    } catch (error) {
      this._appendMessage('AsyncStorage error: ' + error.message);
    }
  }

  async _onLogout() {
    try {
      await AsyncStorage.setItem(STORAGE_KEY, 'false');
      this.setState({isAuthenticated: false});
    } catch (error) {
      this._appendMessage('AsyncStorage error: ' + error.message);
    }
  }

  showMenu(value) {
    console.log("isShowMenu", value);
    this.isShowMenu = value
  }

  render() {

    var loadView = [];

    if (this.state.isLoading) {
      loadView.push(<View key={'LoadingView'} style={styles.spinnerContainer}>
        <ActivityIndicator
          hidden='true'
          size='large'/>
        <Text>{STRINGS.COMMON.LOADING}</Text>
      </View>)
    } else {
      if (this.state.isAuthenticated) {
        loadView.push(<Navigator
          key={'Navigator'}
          style={styles.container}
          initialRoute={{
            title: 'WorkingList',
            name: 'dash',
            component: WorkingList,
            logout: this._onLogout.bind(this),
            passProps: {
              name: 'WorkingList',
              logout: this._onLogout.bind(this),
              showMenu: this.showMenu.bind(this)
            }
          }}
          configureScene={() => Navigator.SceneConfigs.FadeAndroid}
          renderScene={this._routeMapper}/>)
      } else if (this.state.isSignUp) {
        loadView.push(<SignUpPage
          toggleMainSpinner={(toggle = true, loadingMsg = null) => self._toggleMainSpinner(toggle, loadingMsg)}
          openMainModal={(message = null, callback = null) => self._openMainModal(message, callback)}
          userHasSignUp={this.userHasSignUp.bind(this)}/>)
      } else {
        loadView.push(<LoginPage
          key={'LoginPage'} errorMessage={this.state.messages}
          deviceToken={this.state.firebasetoken}
          deviceType='ANDROID'
          toggleMainSpinner={(toggle = true, loadingMsg = null) => self._toggleMainSpinner(toggle, loadingMsg)}
          openMainModal={(message = null, callback = null) => self._openMainModal(message, callback)}
          userHasAuthenticated={this.userHasAuthenticated.bind(this)}
          userHasShowForgot={this.userHasShowForgot.bind(this)}
          userHasSignUp={this.userHasSignUp.bind(this)}/>)

      }
    }

    if (this.state.showMainAppAlert) {
      loadView.push(<AlertConfirmDialog
        key={'MainAppAlert'} alertMessage={this.state.mainAppAlertMessage}
        closeModalFail={this._alertOkPressed.bind(this)}/>)
    }

    if (this.state.showMainAppConfirm) {
      loadView.push(<AlertConfirmDialog
        key={'MainAppConfirm'} confrimMessage={this.state.mainAppConfirmMessage}
        closeModalFail={() => this.setState({showMainAppConfirm: false})}
        closeModalSuccess={this._mainConfrimTrue.bind(this)}/>)
    }

    if (this.state.showUpdateApp) {
      loadView.push(<AlertConfirmDialog
        key={'UpdateAppAlert'}
        alertMessage={STRINGS.COMMON.NEW_VERSION}
        closeModalFail={this.toAppVersion.bind(this)}/>)
    }

    if (this.state.showMainSpinner) {
      loadView.push(
        <ProgressSpinner
          key={'SpinnerIndex'} spinnerText={this.state.spinnerLoadingMsg}/>)
    }

    if (this.state.showImageViewer) {
      loadView.push(<ImageViewer
        key={'MainImageViewer'}
        mainImageViewTitle={this.state.mainImageViewTitle}
        mainImageViewUrl={this.state.mainImageViewUrl}
        mainImageViewHeight={this.state.mainImageViewHeight}
        mainImageViewWidth={this.state.mainImageViewWidth}
        closeImageViewer={this._closeImageViewer.bind(this)}
      />)
    }

    return (
      <MenuContext>
        <View style={styles.container}>
          {loadView}
        </View>
      </MenuContext>
    )
  }

  _openImageViewer(title = null, source = null) {
    self.isPreViewImage = true;
    self.setState({
      mainImageViewTitle: title,
      mainImageViewUrl: source,
      mainImageViewHeight: height,
      mainImageViewWidth: width,
      showImageViewer: true,
      showMainSpinner: false
    })
  }

  _closeImageViewer() {
    self.isPreViewImage = false;
    this.setState({
      mainImageViewTitle: null,
      mainImageViewUrl: null,
      mainImageViewHeight: null,
      mainImageViewWidth: null,
      showImageViewer: false
    })
  }

  userHasAuthenticated() {
    this._onAuthenticationSuccess();
    this.setState({
      messages: '',
      isAuthenticated: true
    });
  }

  userHasShowForgot(isShowForgot) {
    this.isShowForgot = isShowForgot;
  }

  userHasSignUp(value) {
    this.setState({
      messages: '',
      isSignUp: value
    });
  }

  _handleAppStateChange(appState) {
    console.log(appState);
    previousAppState = self.state.appState;
    self.setState({
      appState,
      previousAppState,
    });

    if (appState === 'active') {
      console.log('AutoSave: App is active and in foreground')
      if (previousAppState === "background") {
        console.log('AutoSave: Just came back from a background or inactive state')
        // update the users notification count
      }
    }
  }

  _routeMapper(route, navigationOperations, onComponentRef) {
    routeProp = route.passProps;
    _navigator = navigationOperations;
    if (route.name === 'dash' || route.name === 'noti' || route.name === 'GPS' || route.name === 'contact') {
      return (
        <View style={{flex: 1}}>
          <route.component
            ref={route.name}
            style={{flex: 1}}
            {...route.passProps}
            navigator={navigationOperations}
            toggleMainSpinner={(toggle = true, loadingMsg = null) => self._toggleMainSpinner(toggle, loadingMsg)}
            openMainModal={(message = null, callback = null) => self._openMainModal(message, callback)}
            openMainConfirm={(message = null, callback) => self._openMainConfirm(message, callback)}
            openImageViewer={(title = '', source) => self._openImageViewer(title, source)}
          />
        </View>
      );
    } else {
      return (
        <View style={{flex: 1}}>
          <Topbar {...route.passProps} navigator={navigationOperations} title={route.title}/>
          <route.component
            ref={route.name}
            style={{flex: 1}}
            {...route.passProps}
            navigator={navigationOperations}
            toggleMainSpinner={(toggle = true, loadingMsg = null) => self._toggleMainSpinner(toggle, loadingMsg)}
            openMainModal={(message = null, callback = null) => self._openMainModal(message, callback)}
            openMainConfirm={(message = null, callback) => self._openMainConfirm(message, callback)}
            openImageViewer={(title = '', source) => self._openImageViewer(title, source)}
          />
        </View>
      );
    }
  }

  _toggleMainSpinner(toggle, loadingMsg) {
    if (loadingMsg === null || loadingMsg === "") {
      loadingMsg = STRINGS.COMMON.LOADING
    }
    self.setState({
      spinnerLoadingMsg: loadingMsg,
      showMainSpinner: toggle
    })
  }

  _openMainModal(message = null, callback) {
    ALERT_CALLBACK = callback;

    if (message === null || message === "") {
      message = STRINGS.COMMON.UNEXPECTED_ERROR_2
    }
    self.setState({
      mainAppAlertMessage: message,
      showMainAppAlert: true
    })
  }

  _alertOkPressed() {
    self.setState({showMainAppAlert: false});
    console.log('Main app alert shown');
    if (ALERT_CALLBACK !== null) {
      console.log('Alert Callback Executed');
      ALERT_CALLBACK()
    }
    ALERT_CALLBACK = null
  }

  _openMainConfirm(message, callback) {
    if (message === null || message === "") {
      message = STRINGS.POPUP_MSG.ARE_YOU_SURE
    }
    self.setState({
      mainAppConfirmMessage: message,
      showMainAppConfirm: true
    });

    CONFIRM_CALLBACK = callback
  }

  _mainConfrimTrue() {
    self.setState({
      showMainAppConfirm: false
    });

    if (CONFIRM_CALLBACK !== null) {
      CONFIRM_CALLBACK();
    }
    CONFIRM_CALLBACK = null;
  }

  toAppVersion() {
    console.log('toAppVersion');
    //this.setState({showUpdateApp: false});
    let fullPath = apiConfig.download_url + '/' + this.appVersion.file_path;
    let encodedURI = encodeURI(fullPath);
    NativeAwareAPI.downloadAPKFile(encodedURI, this.appVersion.file_path);
  }

  getVersion() {
    console.log('getVersion');
    let apiData = {};
    DFService.callApiNoHeader(apiData, apiConfig.getAppVersion, function (error, response) {
      if (response) {
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          console.log(response);
          for (let i = 0; i < response.data.length; i++) {
            if (response.data[i].device_type === 'android') {
              self.appVersion = response.data[i];
              //self.checkVersion();
            }
          }
        }
      } else {
        console.log(error);
      }
    });
  }

  checkVersion() {

    if (parseFloat(DeviceInfo.getVersion()) < parseFloat(this.appVersion.version)) {
      // console.log(this.appVersion);
      this._onLogout();
      this.setState({showUpdateApp: true});
    }
  }
}

const styles =
  StyleSheet.create({
    container: {
      flex: 1
    },
    messageText: {
      marginTop: 10,
      fontSize: 14,
      textAlign: 'center',
      color: 'red'
    },
    leftNavButtonText: {
      fontSize: 18,
      padding: 5
    },
    rightNavButtonText: {
      fontSize: 18,
      padding: 5
    },
    navBtnImage: {
      width: 30,
      height: 30
    },
    nav: {
      height: 60,
      flex: 1,
      backgroundColor: '#779e2f'
    },
    navBarBtn: {
      height: 40,
      marginTop: 10,
      marginRight: 10,
      justifyContent: 'center',
      alignItems: 'center'
    },
    title: {
      fontSize: 16,
      width: 250,
      left: 0,
      top: 15,
      color: 'white',
      position: 'absolute',
      textAlign: 'center',
      justifyContent: 'center',
      alignSelf: 'center'
    },
    buttonText: {
      fontSize: 18,
      padding: 5
    },
    toolbar: {
      backgroundColor: '#779e2f',
      height: 56,
    },
  });

AppRegistry.registerComponent('DigitalForm', () => DigitalForm);
