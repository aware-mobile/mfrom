/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Navigator,
  ActivityIndicator,
  Dimensions,
  AsyncStorage,
  StatusBar,
  TouchableHighlight
  // AppState
} from 'react-native';

var {height, width} = Dimensions.get('window');

import FCM from 'react-native-fcm';
import { MenuContext } from 'react-native-popup-menu';
import LoginPage from './modules/LoginPage'
import Topbar from './modules/Topbar'
import WorkingList from './modules/WorkingList'
import SignUpPage from './modules/SignUpPage'

import ImageViewer from './modules/CustomViews/ImageViewer'

import AlertConfirmDialog from './modules/Util/AlertConfirmDialog';

import ProgressSpinner from './modules/Animation/ProgressSpinner'

// var Ionicons = require('react-native-vector-icons/Ionicons');

var STORAGE_KEY = '@DigitialFromIsUserAuthenticated:key';
var NavigationBarRouteMapper;
var _navigator;

var self;

import STRINGS from './modules/Util/strings.js'

var CONFIRM_CALLBACK = null
var ALERT_CALLBACK = null
var IMAGE_VIEW_CALLBACK = null

class DigitalForm extends Component {

  constructor(props) {
    super(props);
    self = this

    this.state = {
      isLoading: true,
      isAuthenticated: false,
      isSignUp: false,
      messages: '',
      firebasetoken: '',
      mainAppAlertMessage: STRINGS.COMMON.UNEXPECTED_ERROR_TRY_AGAIN,
      showMainAppAlert: false,
      mainAppConfirmMessage: STRINGS.POPUP_MSG.ARE_YOU_SURE,
      showMainAppConfrim: false,
      spinnerLoadingMsg: STRINGS.COMMON.LOADING,
      showMainSpinner: false,
      showImageViewer: false,
      mainImageViewHeight: null,
      mainImageViewWidth: null,
      mainImageViewUrl: null,
      mainImageViewTitle: null,
      // appState: "active",
      // previousAppState: "active"
    };


    AsyncStorage.getItem('LANGUAGE', (err, result) => {
      if (err || result == null) {
        console.log('Unable to get LANGUAGE so setting to default english');
        AsyncStorage.setItem('LANGUAGE', 'en', (err, result) => {
          if(err){
            console.log('Unable to set the LANGUAGE into local storage')
          }
        });
        STRINGS.setLanguage('en');
      }else{
        console.log('Language: ', result);
        STRINGS.setLanguage(result);
      }
    });
  }

  _appendMessage(message) {
    this.setState({ messages: this.state.messages.concat(message) });
  }

  componentDidMount() {
    // AppState.addEventListener('change', this._handleAppStateChange(this));

    this._loadInitialState().done();

    FCM.requestPermissions(); // for iOS
    FCM.getFCMToken().then(token => {
      // console.log('Android Token!!!', token)
      self.setState({ firebasetoken: token })
      // store fcm token in your server
    });
    this.refreshUnsubscribe = FCM.on('refreshToken', (token) => {
      console.log('IOS Refreshed Firebase Token: ', token)
      self.setState({ firebasetoken: token })
      // fcm token may not be available on first load, catch it here
    });
  }

  componentWillUnmount() {
    // prevent leaking
    AppState.removeEventListener('change', this._handleAppStateChange);
    this.refreshUnsubscribe();
    // this.notificationUnsubscribe();
  }

  async _loadInitialState() {
    try {
      var value = await AsyncStorage.getItem(STORAGE_KEY);
      if (value !== null) {
        const newValue = (value == 'true') ? true : false;
        this.setState({
          isAuthenticated: newValue,
          isLoading: false
        });
        // this._appendMessage('Recovered selection from disk: ' + value);
      } else {
        this.setState({ isLoading: false });
      }
    } catch (error) {
      console.log(error)
      this._appendMessage('AsyncStorage error: ' + error.message);
      this.setState({ isLoading: false });
    }
  }

  async _onAuthenticationSuccess() {
    try {
      await AsyncStorage.setItem(STORAGE_KEY, 'true');
    } catch (error) {
      this._appendMessage('AsyncStorage error: ' + error.message);
    }
  }

  async _onLogout() {
    try {
      await AsyncStorage.setItem(STORAGE_KEY, 'false');
      this.setState({ isAuthenticated: false });
    } catch (error) {
      this._appendMessage('AsyncStorage error: ' + error.message);
    }
  }

  _onSetLanguage(newLanguage) {
    STRINGS.setLanguage(newLanguage);
    this.setState({});
  }

  showMenu(value) {

  }

  render() {
    var loadView = [];

    if (this.state.isLoading) {
      this.setStatusBarColor(false)
      loadView.push(<View key={"LoadingView"} style={styles.spinnerContainer}>
        <ActivityIndicator
          hidden='true'
          size='large' />

        <Text>{STRINGS.COMMON.LOADING}</Text>
      </View>)
    } else {
      if (this.state.isAuthenticated) {
        this.setStatusBarColor(true)
        loadView.push(<Navigator
          key={"Nav"}
          style={styles.container}
          initialRoute={{
            title: 'WorkingList',
            name: 'dash',
            component: WorkingList,
            logout: this._onLogout.bind(this),
            passProps: {
              name: 'WorkingList',
              logout: this._onLogout.bind(this),
              openMainModal: this._openMainModal.bind(this),
              showMenu: this.showMenu.bind(this)
            }
          }}
          configureScene={() => Navigator.SceneConfigs.FadeAndroid}
          renderScene={this._routeMapper}
        />)
      } else if (this.state.isSignUp) {
        this.setStatusBarColor(false)
        loadView.push(<SignUpPage
          toggleMainSpinner={(toggle = true, loadingMsg = null) => self._toggleMainSpinner(toggle, loadingMsg)}
          openMainModal={(message = null, callback = null) => self._openMainModal(message, callback)}
          userHasSignUp={this.userHasSignUp.bind(this)}
          />)
      } else {
        this.setStatusBarColor(false)
        loadView.push(<LoginPage
          errorMessage={this.state.messages}
          key={"LoginPage"}
          deviceToken={this.state.firebasetoken}
          deviceType='IOS'
          openMainModal={(message = null, callback = null) => self._openMainModal(message, callback)}
          toggleMainSpinner={(toggle = true, loadingMsg = null) => self._toggleMainSpinner(toggle, loadingMsg)}
          userHasAuthenticated={this.userHasAuthenticated.bind(this)}
          userHasShowForgot={this.userHasShowForgot.bind(this)}
          userHasSignUp={this.userHasSignUp.bind(this)}
          />)

      }
    }

    if (this.state.showMainAppAlert) {
      loadView.push(<AlertConfirmDialog key={'MainAppAlert'} alertMessage={this.state.mainAppAlertMessage}
        closeModalFail={this._alertOkPressed.bind(this)} />)
    }

    if (this.state.showMainAppConfirm) {
      loadView.push(<AlertConfirmDialog key={'MainAppConfirm'} confrimMessage={this.state.mainAppConfirmMessage}
        closeModalFail={() => this.setState({ showMainAppConfirm: false })}
        closeModalSuccess={this._mainConfrimTrue.bind(this)} />)
    }

    if (this.state.showMainSpinner) {
      loadView.push(
        <ProgressSpinner key={'SpinnerIndex'} spinnerText={this.state.spinnerLoadingMsg} />
      )
    }

    if(this.state.showImageViewer){
      loadView.push(<ImageViewer
                      key={'MainImageViewer'}
                      mainImageViewTitle={this.state.mainImageViewTitle}
                      mainImageViewUrl={this.state.mainImageViewUrl}
                      mainImageViewHeight={this.state.mainImageViewHeight}
                      mainImageViewWidth={this.state.mainImageViewWidth}
                      closeImageViewer={this._closeImageViewer.bind(this)}
                    />)
    }

    return (
      <MenuContext>
        <View style={styles.container}>
          {loadView}
        </View>
      </MenuContext>
    )

  }

  setStatusBarColor(isLightContent = true) {
    if (isLightContent) {
      ['default', 'light-content'].map((style) => StatusBar.setBarStyle(style))
    } else {
      ['default'].map((style) => StatusBar.setBarStyle(style))
    }
  }

  userHasAuthenticated() {
    this._onAuthenticationSuccess();
    this.setState({
      messages: '',
      isAuthenticated: true
    });
  }

  userHasShowForgot(isShowForgot) {
    this.isShowForgot = isShowForgot;
  }

  userHasSignUp(value) {
    this.setState({
      messages: '',
      isSignUp: value
    });
  }

  _routeMapper(route, navigationOperations, onComponentRef) {
    // const routes = navigationOperations.getCurrentRoutes();
    // if(routes.length > 1){
    //   let routeName = routes[routes.length-2].name;
    //   if (typeof navigationOperations.refs[routeName]._removeKeyboardListeners !== "undefined") {
    //       // safe to use the function
    //       navigationOperations.refs[routeName]._removeKeyboardListeners();
    //   }
    // }
    // console.log('route ---> ', route.name);
    // console.log('routeName ---> ', routeName);

    _navigator = navigationOperations;
    if (route.name === 'dash' || route.name === 'noti' || route.name === 'GPS' || route.name === 'contact') {
      // DONT USE NORMAL TOPBAR FOR THESE PAGES
      return (
        <View style={{ flex: 1 }}>
          <route.component
            ref={route.name}
            style={{ flex: 1, paddingTop: 20 }}
            {...route.passProps}
            navigator={navigationOperations}
            toggleMainSpinner={(toggle = true, loadingMsg = null) => self._toggleMainSpinner(toggle, loadingMsg)}
            openMainModal={(message = null, callback = null) => self._openMainModal(message, callback)}
            openMainConfirm={(message = null, callback) => self._openMainConfirm(message, callback)}
            openImageViewer={(title = '', source) => self._openImageViewer(title, source)}
          />
        </View>
      );
    } else {
      // USE THE DEFAULT NAV BAR FOR ALL OTHER PAGES
      return (
        <View style={{ flex: 1 }}>
          <Topbar  {...route.passProps} navigator={navigationOperations} title={route.title} />
          <route.component
            ref={route.name}
            style={{ flex: 1 }}
            {...route.passProps}
            navigator={navigationOperations}
            toggleMainSpinner={(toggle = true, loadingMsg = null) => self._toggleMainSpinner(toggle, loadingMsg)}
            openMainModal={(message = null, callback = null) => self._openMainModal(message, callback)}
            openMainConfirm={(message = null, callback) => self._openMainConfirm(message, callback)}
            openImageViewer={(title = '', source) => self._openImageViewer(title, source)}
          />
        </View>
      );
    }

  };

  _toggleMainSpinner(toggle, loadingMsg) {
    if (loadingMsg == null || loadingMsg == "") {
      loadingMsg = STRINGS.COMMON.LOADING
    }
    self.setState({
      spinnerLoadingMsg: loadingMsg,
      showMainSpinner: toggle
    })
  }

  _openMainModal(message = null, callback = null) {
    if (message == null || message == "") {
      message = STRINGS.COMMON.UNEXPECTED_ERROR_2
    }
    self.setState({
      mainAppAlertMessage: message,
      showMainAppAlert: true
    })

    ALERT_CALLBACK = callback
  }

  _openImageViewer(title = null, source = null) {
    this._toggleMainSpinner(true, '');
    Image.getSize(source.uri, (imgWidth, imgHeight) => {
      // original height * new width / original width = new height;
      let newWidth = width;
      let newHeight = imgHeight * width / imgWidth;

      // if(width > imgWidth){
      //   newWidth = imgWidth;
      //   newHeight = imgHeight;
      //   if(imgHeight > newHeight){
      //     // original width * new height / original height = new width;
      //     newWidth = imgWidth * height / imgHeight;
      //     newHeight = height;
      //   }
      // }


      self.setState({
        mainImageViewTitle: title,
        mainImageViewUrl: source,
        mainImageViewHeight: newHeight,
        mainImageViewWidth: newWidth,
        showImageViewer: true,
        showMainSpinner: false
      })

    });
  }

  _closeImageViewer(){
    this.setState({
      mainImageViewTitle: null,
      mainImageViewUrl: null,
      mainImageViewHeight: null,
      mainImageViewWidth: null,
      showImageViewer: false
    })
  }

  _alertOkPressed() {
    self.setState({ showMainAppAlert: false })
    if (ALERT_CALLBACK !== null) {
      ALERT_CALLBACK()
    }
    ALERT_CALLBACK = null
  }

  _openMainConfirm(message, callback) {
    if (message == null || message == "") {
      message = "Are you sure?"
    }
    self.setState({
      mainAppConfirmMessage: message,
      showMainAppConfirm: true
    })

    CONFIRM_CALLBACK = callback
  }

  _mainConfrimTrue() {
    self.setState({
      showMainAppConfirm: false
    })

    if (CONFIRM_CALLBACK !== null) {
      CONFIRM_CALLBACK()
    }
    CONFIRM_CALLBACK = null
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  spinnerContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  messageText: {
    marginTop: 10,
    fontSize: 14,
    textAlign: 'center',
    color: 'red'
  }
});

AppRegistry.registerComponent('DigitalForm', () => DigitalForm);
