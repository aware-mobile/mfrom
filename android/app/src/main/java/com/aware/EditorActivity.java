package com.aware;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.aware.dialogs.DrawAttribsDialog;
import com.aware.dialogs.SelectChoiceDialog;
import com.byox.drawview.enums.DrawingCapture;
import com.byox.drawview.enums.DrawingTool;
import com.byox.drawview.views.DrawView;
import com.digitalform.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class EditorActivity extends AppCompatActivity implements View.OnClickListener {

    CanvasView cvImage;
    //Button choosePicture;
    Button savePicture;
    private String path;
    private LinearLayout lyImage;
    private DrawView mDrawView;
    private Button optionButton;
    private Button clearDrawing;
    private Button unDo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        path = getIntent().getExtras().getString("path", "");
//        lyImage = (LinearLayout) this.findViewById(R.id.lyImage);
//        cvImage = (CanvasView) this.findViewById(R.id.cvImage);
        mDrawView = (DrawView) findViewById(R.id.draw_view);
        //choosePicture = (Button) this.findViewById(R.id.ChoosePictureButton);
        savePicture = (Button) this.findViewById(R.id.SavePictureButton);
        optionButton = (Button) this.findViewById(R.id.optionButton);
        clearDrawing = (Button) this.findViewById(R.id.clearDrawing);
        unDo = (Button) this.findViewById(R.id.unDo);
        unDo.setOnClickListener(this);
        savePicture.setOnClickListener(this);
        optionButton.setOnClickListener(this);
        clearDrawing.setOnClickListener(this);

        mDrawView.setOnDrawViewListener(new DrawView.OnDrawViewListener() {
            @Override
            public void onStartDrawing() {
            }

            @Override
            public void onEndDrawing() {
            }

            @Override
            public void onClearDrawing() {
            }

            @Override
            public void onRequestText() {

            }
        });

        show(path);
    }

    @Override
    public void onClick(View v) {
        if (v == savePicture) {
            finishWithResult(path);
        } else if (v == optionButton) {
            changeDrawAttribs();
        } else if (v == clearDrawing) {
            clearDrawing();
        } else if (v == unDo) {
            undo();
        }
    }

    private void changeDrawAttribs() {
        DrawAttribsDialog drawAttribsDialog = DrawAttribsDialog.newInstance();
        drawAttribsDialog.setPaint(mDrawView.getCurrentPaintParams());
        drawAttribsDialog.setOnCustomViewDialogListener(new DrawAttribsDialog.OnCustomViewDialogListener() {
            @Override
            public void onRefreshPaint(Paint newPaint) {
                mDrawView.setDrawColor(newPaint.getColor())
                        .setPaintStyle(newPaint.getStyle())
                        .setDither(newPaint.isDither())
                        .setDrawWidth((int) newPaint.getStrokeWidth())
                        .setDrawAlpha(newPaint.getAlpha())
                        .setAntiAlias(newPaint.isAntiAlias())
                        .setLineCap(newPaint.getStrokeCap())
                        .setFontFamily(newPaint.getTypeface())
                        .setFontSize(newPaint.getTextSize());
//                If you prefer, you can easily refresh new attributes using this method
//                mDrawView.refreshAttributes(newPaint);
            }
        });
        drawAttribsDialog.show(getSupportFragmentManager(), "drawAttribs");
    }

    private void changeDrawTool() {
        SelectChoiceDialog selectChoiceDialog = SelectChoiceDialog.newInstance("Select a draw tool", "PEN", "LINE", "RECTANGLE", "CIRCLE", "ELLIPSE");
        selectChoiceDialog.setOnChoiceDialogListener(new SelectChoiceDialog.OnChoiceDialogListener() {
            @Override
            public void onChoiceSelected(int position) {
                mDrawView.setDrawingTool(DrawingTool.values()[position]);
            }
        });
        selectChoiceDialog.show(getSupportFragmentManager(), "choiceDialog");
    }

    private void clearDrawing() {
        mDrawView.restartDrawing();
        mDrawView.setBackgroundImage(new File(path));
    }

    private void undo() {
        if (mDrawView.canUndo()) {
            mDrawView.undo();
            if (mDrawView.getDrawIndex() == -1) {
                mDrawView.setBackgroundImage(new File(path));
            }
        }

    }

    private void finishWithResult(String path) {
        Bitmap myBitmap = (Bitmap) mDrawView.createCapture(DrawingCapture.BITMAP);
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(new File(path));
            myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Log.i("finishWithResult", "path: " + path);
        Intent intent = new Intent();
        intent.putExtra("path", path);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void show(String path) {
        Bitmap myBitmap = BitmapFactory.decodeFile(path);
        if (myBitmap != null) {
            Log.i("EditorActivity", "getHeight: " + myBitmap.getHeight());
            Log.i("EditorActivity", "getWidth: " + myBitmap.getWidth());

        }
        //mDrawView.setBackground(Drawable.createFromPath(path));
        mDrawView.setBackgroundImage(new File(path));
    }
}
