package com.aware;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

//import com.adobe.creativesdk.aviary.AdobeImageIntent;
import com.digitalform.R;
import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class AwareModule extends ReactContextBaseJavaModule implements ActivityEventListener {

    public ReactApplicationContext mContext;
    public static final String REACT_CLASS = "RNAwareModule";
    private Callback call;
    private WritableMap response;
    private boolean isPhone = false;


    @Override
    public String getName() {
        return REACT_CLASS;
    }

    public AwareModule(ReactApplicationContext reactContext) {
        super(reactContext);
        mContext = reactContext;
        mContext.addActivityEventListener(this);

        String screen = mContext.getString(R.string.screen);
        if (screen.equals("phone")) {
            isPhone = true;
        }
    }

    @ReactMethod
    public void editImage(String path, Callback callback) {
        Log.i("AwareModule", "editImage: " + path);
        call = callback;
        Intent intent = new Intent(mContext, EditorActivity.class);
        intent.putExtra("path", path);
        mContext.startActivityForResult(intent, 300, null);
    }


    @ReactMethod
    public void browseFile(Callback callback) {
        Log.i("AwareModule", "browseFile()");
        call = callback;
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        // Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()); // a directory
        intent.setType("*/*");//setDataAndType(uri, "*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        mContext.startActivityForResult(Intent.createChooser(intent, "Open folder"), 500, null);
    }

    @ReactMethod
    public void editImageLib(String path, Callback callback) {
        Log.i("AwareModule", "editImage: " + path);
        call = callback;
        File file = new File(path);
        Uri imageUri = Uri.fromFile(file);
//        Intent imageEditorIntent = new AdobeImageIntent.Builder(mContext)
//                .setData(imageUri)
//                .build();
//        mContext.startActivityForResult(imageEditorIntent, 400, null);
    }

    @ReactMethod
    public void getDeviceScreen(Callback callback) {
        String screen = mContext.getString(R.string.screen);
        Log.i("AwareModule", "getDeviceScreen: " + screen);
        callback.invoke(screen);
    }

    @ReactMethod
    public boolean isPhone() {
        Log.i("AwareModule", "isPhone: " + isPhone);
        return isPhone;
    }

    @ReactMethod
    public void downloadFile(String url, String name) {

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription("Download file content");
        request.setTitle("Download " + name);
        // in order for this if to run, you must use the android 3.2 to
        // compile your app
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, name);

        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    @ReactMethod
    public void downloadAPKFile(String url, String name) {
        name = name.replace("android/", "");
        UpdateApp atualizaApp = new UpdateApp(mContext, name);
        atualizaApp.execute(url);
    }

    private String getRealPathFromURI(Uri uri) {
        String result;
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = mContext.getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    @Override
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
        Log.i("onActivityResult", "RESULT: " + activity.RESULT_OK);
        response = Arguments.createMap();
        if (resultCode == activity.RESULT_OK && requestCode == 300) {
            String path = data.getStringExtra("path");
            File file = new File(path);
            Uri uri = Uri.fromFile(file);
            response.putString("path", path);
            response.putString("uri", uri.toString());
            Log.i("onActivityResult", "path: " + path);
            call.invoke(response);
        }
        if (resultCode == activity.RESULT_OK && requestCode == 400) {
            Uri uri = data.getData();
            String path = uri.getPath();
            response.putString("path", path);
            response.putString("uri", "file://" + uri.toString());
            call.invoke(response);

        }
        if (resultCode == activity.RESULT_OK && requestCode == 500) {
            Uri uri = data.getData();
            String path = FileUtils.getPath(mContext, uri);

            // Alternatively, use FileUtils.getFile(Context, Uri)
            if (path != null && FileUtils.isLocal(path)) {
                try {
                    File f = new File(path);
                    response.putDouble("fileSize", f.length());
                    response.putString("fileName", f.getName());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // type
                String extension = MimeTypeMap.getFileExtensionFromUrl(path);
                if (extension != null) {
                    response.putString("type", MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension));
                }
            }

            response.putString("path", "file://" + path);
            response.putString("uri", data.getData().toString());
            Log.i("onActivityResult", "uri: " + data.getData().toString());
            call.invoke(response);
        }

    }

    @Override
    public void onNewIntent(Intent intent) {

    }

    private class UpdateApp extends AsyncTask<String, String, Void> {
        private Context context;
        private ProgressDialog pd;
        private String fileName;

        UpdateApp(Context context, String name) {
            this.context = context;
            this.fileName = name;
            Log.i("UpdateApp", "path: " + fileName);
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(context);
            pd.setMessage("Loading...");
            pd.setCanceledOnTouchOutside(false);
            pd.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            pd.show();
        }

        @Override
        protected Void doInBackground(String... arg0) {
            try {
                URL url = new URL(arg0[0]);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();
                long lenghtOfFile = c.getContentLength();
                String PATH = "/mnt/sdcard/Download/";
                File file = new File(PATH);
                file.mkdirs();
                File outputFile = new File(file, fileName);
                if (outputFile.exists()) {
                    outputFile.delete();
                }
                FileOutputStream fos = new FileOutputStream(outputFile);

                InputStream is = new BufferedInputStream(url.openStream(), 8192);

                byte[] buffer = new byte[1024];
                long total = 0;
                int len1 = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    total += len1;
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    fos.write(buffer, 0, len1);
                }
                fos.close();
                is.close();

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(new File("/mnt/sdcard/Download/" + fileName)), "application/vnd.android.package-archive");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            //Log.d("ANDRO_ASYNC", values[0]);
            //pd.setProgress(Integer.parseInt(values[0]));
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            pd.dismiss();
        }
    }
}
