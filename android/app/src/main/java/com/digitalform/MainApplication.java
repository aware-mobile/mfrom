package com.digitalform;

import android.app.Application;

import com.RNFetchBlob.RNFetchBlobPackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.arttitude360.reactnative.rngoogleplaces.RNGooglePlacesPackage;
import com.aware.AwarePackage;
import com.babisoft.ReactNativeLocalization.ReactNativeLocalizationPackage;
import com.evollu.react.fcm.FIRMessagingPackage;
import com.facebook.react.ReactApplication;
import com.lugg.ReactNativeConfig.ReactNativeConfigPackage;
import com.joshblour.reactnativepermissions.ReactNativePermissionsPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.imagepicker.ImagePickerPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.lwansbrough.RCTCamera.RCTCameraPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.reactnative.photoview.PhotoViewPackage;
import com.rssignaturecapture.RSSignatureCapturePackage;

import java.util.Arrays;
import java.util.List;


public class MainApplication extends Application implements ReactApplication {

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(
                    new MainReactPackage(),
            new ReactNativeConfigPackage(),
            new ReactNativePermissionsPackage(),
            new RNGooglePlacesPackage(),
                    new ReactNativeLocalizationPackage(),
                    new RCTCameraPackage(),
                    new MapsPackage(),
                    new RNFetchBlobPackage(),
                    new FIRMessagingPackage(),
                    new RNDeviceInfo(),
                    new ImagePickerPackage(),
                    new VectorIconsPackage(),
                    new MapsPackage(),
                    new AwarePackage(),
                    new RSSignatureCapturePackage(),
                    new PhotoViewPackage()
            );
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SoLoader.init(this, /* native exopackage */ false);
    }
}
