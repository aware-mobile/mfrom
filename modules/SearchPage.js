import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableHighlight,
  ListView,
  ActivityIndicator,
  AsyncStorage,
  Platform,
  NativeModules
} from 'react-native';

import FormPage from './FormPage';
import HeaderMain from './HeaderMain';
import DFService from './Service/DFService';
import apiConfig from './Util/Config';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import Util from './Util/Util';
import AppText from './Util/AppText';
//import SelectFormModal from './SelectFormModal';

var Device = NativeModules.RNAwareModule;
import CustomModal from './Util/CustomModalContent'

var dataSource = new ListView.DataSource(
  { rowHasChanged: (r1, r2) => r1._id !== r2._id }
);

var rows;
var self;
var response;


class SearchPage extends Component {
  constructor(props) {
    super(props);
    console.log("WorkingList", props);
    this.state = {
      isLoading: true,
      message: '',
      lastUpdate: '',
      tabObj: [{ color: 'black' }, { color: '#B2D234' }, { color: '#B2D234' }],
      dataSource: dataSource.cloneWithRows([{}]),
      workSize: 0,
      draftSize: 0,
      hisSize: 0,
      activeModal: false
    };
    self = this;
    this._loadInitial();
    this.getWorkinglist();



  }

  modalActive(isActive) {
    this.setState({ activeModal: isActive })
  }

  keywordPressed(keyword) {

  }

  rowPressed(formData, sectionID, rowID) {
    console.log("rowPressed");
    this.props.navigator.push({
      name: 'submit_form',
      title: formData.template_name,
      component: FormPage,
      passProps: {

        formData: formData
      }
    });
  }

  logout() {
    console.log("logout");
    this.props.logout();
  }

  async _loadInitial() {
    try {

      if (!(Platform.OS === 'ios')) {
        Device.getDeviceScreen((response) => {
          console.log('DeviceScreen = ' + response);
          if (response === 'phone') {
            Util.setPhone(true);
          } else {
            Util.setPhone(false);
          }
        });
      }
    } catch (error) {
      console.log(error)
    }
  }

  renderRow(rowData, sectionID, rowID) {

    return (
      <View style={styles.box}>
        <TouchableHighlight
          onPress={() => this.rowPressed(rowData, sectionID, rowID) }
          underlayColor='#dddddd'
          style={{ backgroundColor: '#ffffff' }}>
          <View>
            <View style={styles.rowContainer}>
              <View style={[styles.circle, { backgroundColor: rowData.user_form_id.category_id.color }]}>
                <Image source={require('./img/form_icon.png') } style={styles.listImage}/>
              </View>
              <View style={{ flexDirection: 'column', paddingLeft: 10, flexGrow: 0.6 }}>
                <AppText style={{ fontSize: 14, color: 'black' }}>{rowData.user_form_id.template_name}</AppText>
                <AppText style={{ fontSize: 14, color: 'grey' }}>{rowData.user_form_id.description}</AppText>
                <AppText style={{ fontSize: 12, color: 'blue' }}>{rowData.createdBy.displayName}
                  / { new Date(rowData.createdDate).toLocaleString() }</AppText>
              </View>
              <Image source={this.getImgStatus(rowData.status) } style={styles.status}/>
            </View>
            <View style={styles.separator}/>
            <View style={styles.rowContain}>
              <View style={{ flexDirection: 'row', flexGrow: 0.3, padding: 5 }}>
                <Image source={require('./img/workID.png') } style={styles.img}/>
                <View style={{ flexDirection: 'column', flexGrow: 1 }}>
                  <AppText style={{ fontSize: 12, color: 'grey' }}>WORK ID: </AppText>
                  <AppText style={{ fontSize: 12, color: 'grey' }}>{rowData.form_job_id._id}</AppText>
                </View>
              </View>
              <View style={styles.pipe}/>
              <View style={{ flexDirection: 'row', flexGrow: 0.3, padding: 5 }}>
                <Image source={require('./img/workDead.png') } style={styles.img}/>
                <View style={{ flexDirection: 'column', flexGrow: 1 }}>
                  <AppText style={{ fontSize: 12, color: 'grey' }}>DUE DATE: </AppText>
                  <AppText style={{
                    fontSize: 12,
                    color: 'grey'
                  }}>{this.dateExpire(rowData.createdDate, rowData.expired_hours) }</AppText>
                </View>
              </View>
              <View style={styles.pipe}/>
              <View style={{ flexDirection: 'row', flexGrow: 0.3, padding: 5 }}>
                <Image source={require('./img/workStep.png') } style={styles.img}/>
                <View style={{ flexDirection: 'column', flexGrow: 1 }}>
                  <AppText style={{
                    fontSize: 12,
                    color: 'grey'
                  }}>STEP: {rowData.form_job_id.current_step}/{rowData.form_job_id.form_flow_id.steps.length}</AppText>
                  <AppText style={{ fontSize: 12, color: 'grey' }}>{this.getUpdate(rowData.form_job_id.updatedBy) }</AppText>
                </View>
              </View>
            </View>
          </View>
        </TouchableHighlight>
      </View>
    );
  }




  render() {



    var loadView
    if (this.state.isLoading) {
      loadView =
        <View>
          <View style={styles.spinner}>
            <ActivityIndicator
              hidden='true'
              size='large'/>
            <AppText>Loading Form List...</AppText>
          </View>
        </View>
    } else {
      if (this.state.message != '') {
        loadView =
          <View>
            <View style={styles.container}>
              <AppText>{this.state.message}</AppText>
            </View>
          </View>
      } else {
        if (Platform.OS === 'ios') {
          loadView =
            <View style={{ flexGrow: 1 }}>
              <ListView
                dataSource={this.state.dataSource}
                renderRow={this.renderRow.bind(this) }/>
            </View>

        } else {
          loadView =
            <View style={{ flexGrow: 1 }}>
              <ListView
                dataSource={this.state.dataSource}
                renderRow={this.renderRow.bind(this) }/>

            </View>
        }
      }
    }

    return (
      <View style={{ flexGrow: 1 }}>
        {loadView}
        // {this.state.activeModal ? <SelectFormModal closeModal={this.closeModal.bind(this, false) }/> : null }
      </View>
    )



  }

  closeModal() {
    this.setState({ activeModal: false })
  }

  tabPress(position) {
    if (position == 0) {
      this.setState({
        dataSource: dataSource.cloneWithRows([]),
        tabObj: [{ color: 'black' }, { color: '#B2D234' }, { color: '#B2D234' }],
      });
      this.getWorkinglist();
    } else if (position == 1) {
      this.setState({
        dataSource: dataSource.cloneWithRows([]),
        tabObj: [{ color: '#B2D234' }, { color: 'black' }, { color: '#B2D234' }],
      });
    } else {
      this.setState({
        dataSource: dataSource.cloneWithRows([]),
        tabObj: [{ color: '#B2D234' }, { color: '#B2D234' }, { color: 'black' }],
      });
      this.getHistorylist();
    }
  }

  getWorkinglist() {
    self = this;
    var apiData = {};
    DFService.callApiWithHeader(apiData, apiConfig.getWorkinglist, function (error, response) {

      if(response){
        if (response.resCode == 'DF2000000') {
          // Successfull Response
          self.successWorking(response);
        }else{
          // Failed Response
          self.props.openMainModal(response.resMessage)
        }
      }else{
        // Error from application calling service
        console.log("error = ", error);
      }

    });
  }

  getHistorylist() {
    self = this;
    var apiData = {};
    DFService.callApiWithHeader(apiData, apiConfig.getHistorylist, function (error, response) {

      if(response){
        if (response.resCode == 'DF2000000') {
          // Successfull Response
          self.successHistory(response);
        }else{
          // Failed Response
          self.props.openMainModal(response.resMessage)
        }
      }else{
        // Error from application calling service
        console.log("error = ", error);
      }

    });
  }

  dateExpire(date, expire) {
    if (expire > 0) {
      var d = new Date(date);
      d.setHours(d.getHours() + expire);
      return d.toLocaleString('en-GB')
    } else {
      return "None"
    }
  }

  isExpire(date, expire) {
    if (expire > 0) {
      var d = new Date(date);
      d.setHours(d.getHours() + expire);
      var dateNow = new Date();
      return dateNow > d;
    } else {
      return false;
    }
  }

  getUpdate(name) {
    if (name != null) {
      return name.firstName;
    } else {
      return '-';
    }
  }

  getImgStatus(status) {
    console.log("Status = ", status);
    if (status == "NEW") {
      return require('./img/iconNew.png')
    } else if (status == "REJECTED") {
      return require('./img/wl_rejected_flag.png')
    } else if (status == "INPROGRESS") {
      return require('./img/wl_inprogress_flag.png')
    } else if (status == "EXPIRED") {
      return require('./img/wl_expired_flag.png')
    } else if (status == "COMPLETED") {
      return require('./img/wl_completed_flag.png')
    }
  }

  successWorking(response) {
    if (response.resCode == 'DF2000000') {
      var workingList = [];
      for (var i = 0; i < response.data.length; i++) {

        if (response.data[i].user_form_id !== null && response.data[i].user_form_id.status === 'ACTIVE') {
          var item = response.data[i];
          if (this.isExpire(item.createdDate, item.expired_hours)) {
            item.status = "EXPIRED"
          }
          item.template_name = item.user_form_id.template_name;
          item.description = item.user_form_id.description;
          workingList.push(item)
        }
      }
      var last = '';
      if (workingList.length > 0) {
        last = new Date(workingList[0].createdDate).toLocaleString('en-GB')
      }

      this.setState({
        isLoading: false,
        lastUpdate: last,
        message: '',
        dataSource: dataSource.cloneWithRows(workingList),
        workSize: workingList.length
      });
    } else {
      this.setState({ message: response.resMessage });
    }
  }

  successHistory(response) {
    if (response.resCode == 'DF2000000') {
      var historyList = [];
      for (var i = 0; i < response.data.length; i++) {

        if (response.data[i].user_form_id !== null && response.data[i].user_form_id.status === 'ACTIVE') {
          var item = response.data[i];
          item.template_name = item.user_form_id.template_name;
          item.description = item.user_form_id.description;
          historyList.push(item)
        }
      }
      var last = '';
      if (historyList.length > 0) {
        last = new Date(historyList[0].createdDate).toLocaleString('en-GB')
      }
      this.setState({
        isLoading: false,
        lastUpdate: last,
        message: '',
        dataSource: dataSource.cloneWithRows(historyList),
        hisSize: historyList.length
      });
    } else {
      this.setState({ message: response.resMessage });
    }
  }
}

const styles = StyleSheet.create({
  spinner: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  tabbar: {
    height: 45,
    flexDirection: 'row',
    backgroundColor: '#B2D234'
  },
  tabView: {
    flexDirection: 'row',
    flexGrow: 0.3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    flexGrow: 1,
    marginTop: 70,
  },
  rowContainer: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center'
  },
  rowContain: {
    flexDirection: 'row'
  },
  colContainer: {
    flexDirection: 'column',
    padding: 10
  },
  separator: {
    height: 1,
    backgroundColor: '#dddddd'
  },
  indicator: {
    height: 2,
    backgroundColor: 'black',
    flexDirection: 'row',
    flexGrow: 0.3
  },
  pipe: {
    width: 1,
    backgroundColor: '#dddddd'
  },
  listView: {
    marginTop: 65
  },
  listImage: {
    width: 40,
    height: 40
  },
  status: {
    alignItems: 'flex-end',
    width: 100,
  },
  img: {
    width: 35,
    height: 35
  },
  box: {
    padding: 8,
    shadowColor: "#000000",
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 0
    }
  },
  circle: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center'
  },
  search: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  navBtnImage: {
    width: 30,
    height: 30
  },
  navBarBtn: {
    height: 35,
    marginTop: 10,
    marginRight: 15,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  rowSearch: {
    marginTop: 5,
    flexDirection: 'row',
    alignItems: 'center'
  },
});

export default SearchPage;
