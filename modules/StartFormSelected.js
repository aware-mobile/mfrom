import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableHighlight,
  ListView,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  Platform
} from 'react-native';

import DFService from './Service/DFService';
import apiConfig from './Util/Config';
import Util from './Util/Util'
import STRINGS from './Util/strings';
import AppText from './Util/AppText';

import ProgressSpinner from './Animation/ProgressSpinner'

var Ionicons = require('react-native-vector-icons/Ionicons');
var {height, width} = Dimensions.get('window');


class StartFormSelected extends Component {
  constructor(props) {
    super(props);

    this.state = {
      formData: this.props.getForm,
      createWorkData: {},
      isLoading: false
    };
    console.log('formDataformDataformData: ', this.state.formData)
  }


  start() {
    //     if (this.selectCategory == null || this.selectForm == null) {
    //         return;
    //     }
    //     var user_form_id = formData.form_flow_id;
    //     var id = formData.id;
    //     var body = { "form_flow_id": user_form_id, "form_id": id };
    //     let apiConfig = {}
    //     DFService.callApiWithHeader(apiData, apiConfig.getWorkinglistPublished, function (error, response)
    //     angular.copy(config.createWork, apiConfig)
    //     this.UTILS_SERVICE.callApiWithHeaders(apiConfig, body).then((response, status) => {
    //         this.$scope.cancel();
    //     }, (args) => {
    //         this.UTILS_SERVICE.checkErrorResponse(args)
    //     })
    this.setState({
      isLoading: true
    })
    var user_form_id = this.state.formData.form_flow_id;
    var id = this.state.formData.id;
    var self = this;
    var apiData = {
      form_flow_id: user_form_id,
      form_id: id
    };
    DFService.callApiWithHeader(apiData, apiConfig.createWork, function (error, response) {

      if (response) {
        if (response.resCode == 'DF2000000') {
          // Successfull Response
          console.log("responseCreateWork = ", response);
          let arrVariable = response.data.user_form_id.template_name.match(/\$\{(.*?)\}/g);
          if (arrVariable) {
            for (let x = 0; x < arrVariable.length; x++) {
              let strFormNameVar = arrVariable[x].replace(/\$\{(.*?)\}/g, "$1")
              response.data.user_form_id.template_name = response.data.user_form_id.template_name.replace("${" + strFormNameVar + "}", '');
            }
          }
          self.setState({
            createWorkData: response.data,
            isLoading: false
          })
          self.props.createWorkModal(self.state.createWorkData, 'creatework')
          self.props.closeModal
        } else {
          // Failed Response
          self.setState({
            isLoading: false
          })
          self.props.openMainModal(response.resMessage)
        }
      } else {
        // Error from application calling service
        self.setState({
          isLoading: false
        })
        console.log("error = ", error);
      }

    });
  }


  render() {
    var mainSpinner = this.state.isLoading ?
      <ProgressSpinner
        setSuccess={this.state.doneSaving}
        isSuccessfull={this.state.saveSuccess}
        spinnerText={this.state.spinnerText}
        /> : <View />;

    var viewModal =
      <View style={styles.container}>
        <View style={{ flexGrow: 12 }}>
          <AppText style={styles.headerText}>
            {STRINGS.SUBMIT_ASSIGN.SUB_HEADING}
          </AppText>
          <AppText style={styles.bodyText}>
            {STRINGS.SUBMIT_ASSIGN.SUB_HEADING_2}
          </AppText>
          <View style={styles.containerChoice}>

            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <TouchableHighlight>
                <View style={[styles.circleLeft, { backgroundColor: this.state.formData.color }]}>
                  <Image source={require('./img/wl_transaction_icon.png')}
                    style={{ alignItems: 'center', justifyContent: 'center', height: 30, width: 25 }}
                    />
                </View>
              </TouchableHighlight>
              <AppText style={{ paddingTop: 15, fontWeight: 'bold' }}>
                {this.state.formData.template_name}
              </AppText>
            </View>
          </View>
        </View>
        <View style={styles.bottomContainer}>
          <AppText style={styles.stepCounter}>
            {STRINGS.formatString(STRINGS.COMMON.STEP_COUNTER, 3, 3)}
          </AppText>
          <View style={{ alignSelf: "flex-end", flexGrow: undefined }}>
            <TouchableHighlight
              onPress={() => this.start()}
              underlayColor='#dddddd'
              style={{
                width: 80,
                height: 36,
                backgroundColor: '#B2D234',
                justifyContent: 'center',
                alignItems: 'center',
                borderColor: '#B2D234',
                borderWidth: 1,
                borderRadius: 4,
              }}>
              <View>
                <AppText style={{ color: '#ffffff' }}>
                  {STRINGS.COMMON.BTN_START}
                </AppText>
              </View>
            </TouchableHighlight>
          </View>
        </View>

      </View>


    return (
      <View>
        {viewModal}
        {mainSpinner}
      </View>
    )
  }

}

var styles = StyleSheet.create({
  container: {
    height: height - 180,
    flexGrow: 1,
    flexDirection: 'column',
    paddingTop: 13,
    paddingLeft: 13,
    paddingRight: 13,
  },
  headerText: {
    marginTop: 18,
    fontWeight: 'bold',
    fontSize: 15,
  },
  bodyText: {
    marginTop: 9,
    fontSize: 14,
    color: 'grey'
  },
  containerChoice: {
    marginTop: 80,
    height: 130,
    flexDirection: 'column'
  },
  circleLeft: {
    width: 70,
    height: 70,
    borderRadius: 70 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ee7155',
  },
  circleRight: {
    width: 85,
    height: 85,
    borderRadius: 85 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000000',
    alignSelf: 'center',
    marginLeft: 30
  },
  footer: {
    backgroundColor: '#FFFFFF',
    height: 30,
    position: 'absolute',
    flexDirection: 'row',
    bottom: 0,
  },
  stepCounter: {
    justifyContent: 'center',
  },
  bottomContainer: {
    flexGrow: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'transparent'
  },

});

export default StartFormSelected;
