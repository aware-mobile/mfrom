import React, {Component} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Image,
  TouchableHighlight,
  ListView,
  ActivityIndicator,
  AsyncStorage,
  Platform,
  NativeModules,
  Alert,
  AppState,
  NetInfo,
  Keyboard,
  LayoutAnimation,
  UIManager,
  TouchableOpacity,
  Dimensions,
  Text
} from 'react-native';

var {height, width} = Dimensions.get('window');

// import { MenuContext } from 'react-native-popup-menu';
const Timer = require('react-native-timer');
import apiConfig from './Util/Config';
import STRINGS from './Util/strings';
import AppText from './Util/AppText';
import DFService from './Service/DFService';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import WorkingListTopSearchBar from './CustomViews/WorkingListTopSearchBar';
import UserDetailPage from './UserDetailPage';
import _ from 'lodash'
import Communications from 'react-native-communications';
import Menu, {
  MenuContext,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from 'react-native-popup-menu';

var dataSource = new ListView.DataSource(
  {rowHasChanged: (r1, r2) => r1._id !== r2._id}
);

var resData

export default class UserContact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      message: '',
      dataSource: dataSource.cloneWithRows([{}]),
      isConnected: true,
      keyboardHeight: 0,
      username: '',
      dataLocalStorage: '',
      tempData: [],
      dataFilter: [],
      dataSearch: [],
      isSearching: false,
      arrFilter: [{
        name: STRINGS.COMMON.FILTER_ALL,
        value: 0,
        active: true
      },
        {
          name: STRINGS.COMMON.FILTER_FAV,
          value: 1,
          active: false
        }],
      dataRender: [],
    };

    this.selectTel = '';
    this.selectEmail = '';
    this.selectName = '';
    //this.getUserFilter();
    this.initUserContact(0, false)
  }

  async initUserContact(type, check = true) {
    try {
      let value = await AsyncStorage.getItem('userFilter');
      console.log('getUserFilter: ' + value);
      if (value) {
        this.state.arrFilter = JSON.parse(value);
      }
    } catch (error) {
      console.log(error)
    }

    let newFilter = [];
    if (check) {
      for (let i = 0; i < this.state.arrFilter.length; i++) {
        let newObjFilter = this.state.arrFilter[i];
        if (newObjFilter.value === type) {
          newObjFilter.active = true;
          newFilter.push(newObjFilter)
        } else {
          newObjFilter.active = false;
          newFilter.push(newObjFilter)
        }
      }
      this.saveUserFilter(JSON.stringify(newFilter));
    } else {
      for (let i = 0; i < this.state.arrFilter.length; i++) {
        let newObjFilter = this.state.arrFilter[i];
        if (newObjFilter.active) {
          console.log(newObjFilter);
          type = newObjFilter.value;
        }
      }
      newFilter = this.state.arrFilter;
    }

    this.getFavUser();
    this.setState({
      isLoading: true,
      dataSource: dataSource.cloneWithRows([{}]),
      arrFilter: newFilter
    });

    let self = this;
    let apiData = {};
    DFService.callApiWithHeader(apiData, apiConfig.getUserContact, function (error, response) {
      if (response) {
        if (response.resCode === 'DF2000000') {
          resData = [];
          for (let i = 0; i < response.data.length; i++) {
            let obj = response.data[i];
            obj.isFav = false;
            if (!obj.updatedDate) {
              obj.updatedDate = obj.createdDate;
            }
            resData.push(obj)
          }

          resData.sort(function (a, b) {
            return new Date(b.updatedDate).getTime() - new Date(a.updatedDate).getTime()
          });

          self.initFavData(resData, type)
        } else {
          // Failed Response
          self.setState({
            isLoading: false,
            dataSource: dataSource.cloneWithRows([{}])
          });
          self.props.openMainModal(response.resMessage)
        }
      } else {
        // Error from application calling service
        self.setState({
          isLoading: false,
          dataSource: dataSource.cloneWithRows([]),
        });
        console.log("initUserContact error = ", error);
      }

    });
  }

  initFavData(data, type) {
    let favLocalStore = this.state.dataLocalStorage === null ? '' : this.state.dataLocalStorage.split(',');
    let dataFav = [];
    Loop1:
      for (let i = 0; i < data.length; i++) {
        Loop2:
          for (let x = 0; x < favLocalStore.length; x++) {
            if (data[i]._id === favLocalStore[x]) {
              data[i].isFav = true;
              dataFav.push(data[i]);
              console.log('assign new valaaaa');
              continue Loop1
            }
          }
      }
    console.log('dataFav : ', dataFav);
    this.setState({
      isLoading: false,
      message: '',
      isFavoriteSelected: type, // 0 == Not Selected, 1 == selected
      dataSource: type === 0 ? dataSource.cloneWithRows(data) : dataSource.cloneWithRows(dataFav),
      dataRender: type === 0 ? data : dataFav,
      dataSearch: [],
      isSearching: false,
      tempData: data,
      dataFilter: type === 0 ? data : dataFav
    });
  }

  render() {
    let removeClippedSubviews = Platform.OS !== 'android';
    let ListViewData = null;
    let empty_list = null;
    let emptyListText;
    if (this.state.tempData.length === 0) {
      emptyListText = STRINGS.COMMON.EMPTY_LIST
    } else {
      emptyListText = STRINGS.COMMON.NO_MATCH;
    }

    if (!this.state.isLoading) {
      empty_list = <View style={{flexGrow: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Image source={require('./img/empty_list_logo.png')} style={{width: 160, resizeMode: 'contain'}}/>
        <Text style={{
          fontSize: 28,
          marginTop: 5,
          fontFamily: 'DB Heavent',
          textAlign: 'center',
          paddingLeft: 10,
          paddingRight: 10
        }}>{emptyListText}</Text>
      </View>;

      if (this.state.dataSource.getRowCount() > 0) {
        ListViewData = <ListView
          keyboardShouldPersistTaps={true}
          dataSource={this.state.dataSource}
          renderRow={this.renderRow.bind(this)}
          enableEmptySections={true}
          removeClippedSubviews={removeClippedSubviews}
          contentContainerStyle={{paddingBottom: 150}}
        />
      } else {
        ListViewData = empty_list
      }
    } else {
      ListViewData =
        <View style={styles.spinner}>
          <ActivityIndicator
            hidden='true'
            size='large'/>
          <AppText>{STRINGS.COMMON.LOADING}</AppText>
        </View>
    }
    return (
      <View style={{flexGrow: 1, backgroundColor: '#F7F6F9'}}>
        <WorkingListTopSearchBar pageName="contact" arrFilter={this.state.arrFilter}
                                 selectFilter={this.selectFilter.bind(this)}
                                 searchTransactions={this.searchTransactions.bind(this)}/>
        {ListViewData}
      </View>
    )
  }

  renderRow(rowData, sectionID, rowID) {
    let options_menu_ic = <TouchableHighlight
      onPress={() => this.pressFav(rowData, sectionID, rowID, true)}
      underlayColor='transparent'
      style={styles.option_menu_container}>
      <Ionicons name="md-star-outline" size={27} color="#ED9D34"/>
    </TouchableHighlight>;

    let options_menu_ic_fav = <TouchableHighlight
      onPress={() => this.pressFav(rowData, sectionID, rowID, false)}
      underlayColor='transparent'
      style={styles.option_menu_container}>
      <Ionicons name="md-star" size={27} color="#ED9D34"/>
    </TouchableHighlight>;

    var options = [];
    // if (rowData.telephone.length > 1) {
    for (let i = 0; i < rowData.phone.length; i++) {
      options.push(<MenuOption
        key={rowID + 'optionIndex' + i} value={rowData.phone[i]}
        style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
        <Text style={{
          fontFamily: 'DB Heavent',
          fontSize: 19,
          color: 'grey',
          fontWeight: 'bold',
          flex: 0.4
        }}>{rowData.phone[i]}</Text>
        <Entypo name="old-phone" size={23} color="#7F7F7F"
                style={styles.img}/>
      </MenuOption>)
    }

    let strRole = [];
    for (let i = 0; i < rowData.roles.length; i++) {
      strRole.push(rowData.roles[i].role_name)
    }
    let txtphone = (rowData.phone.length > 0) ? rowData.phone[0] : '-';
    var bottomRowContent =
      <View style={[styles.rowContain, {borderWidth: 1, borderColor: '#dddddd'}]}>
        <Menu onSelect={value => this.callTelephone(value, rowData.displayName)}
              style={{flexDirection: 'row', flexGrow: 0.4, padding: 5, backgroundColor: '#EEEEF0'}}>
          <MenuTrigger
            key={'MenuTriggerIndex' + rowID}>
            <View style={{flexDirection: 'row', flexGrow: 0.5, padding: 5, backgroundColor: '#EEEEF0'}}>
              <Image source={require('./img/mobile-phone.png')} style={styles.img}/>
              <View style={{flexDirection: 'column', width: width / 2 - 50}}>
                <AppText style={{fontSize: 10, color: 'grey'}}>{STRINGS.USER_DATA.TELEPHONE_FORMAT_2}</AppText>
                <Text numberOfLines={1}
                      style={{
                        width: 100,
                        flexGrow: 0.4,
                        fontSize: 16,
                        color: 'grey',
                        fontWeight: 'bold',
                        fontFamily: 'DB Heavent'
                      }}>{txtphone}</Text>
              </View>
            </View>
          </MenuTrigger>
          <MenuOptions optionsContainerStyle={{}}>
            {options}
          </MenuOptions>
        </Menu>
        <View style={styles.pipe}/>
        <TouchableOpacity
          key={'pressSendMailIndex' + rowID}
          style={{flexDirection: 'row', flexGrow: 0.5, padding: 5, backgroundColor: '#EEEEF0'}}
          onPress={() => this.pressSendMail(rowData.email)}>
          <View style={{flexDirection: 'row', padding: 5, backgroundColor: '#EEEEF0'}}>
            <Image source={require('./img/email-icon.png')} style={styles.img}/>
            <View style={{flexDirection: 'column', width: width / 2 - 65}}>
              <AppText style={{fontSize: 10, color: 'grey'}}>{STRINGS.USER_DATA.EMAIL_FORMAT_2}</AppText>
              <Text numberOfLines={1} style={{
                fontSize: 16,
                color: 'grey',
                fontWeight: 'bold',
                fontFamily: 'DB Heavent'
              }}>{rowData.email}</Text>
            </View>
          </View>
        </TouchableOpacity>

      </View>;

    var departmentVal = rowData.department && rowData.department.trim() !== '' ? rowData.department.trim() : '-';
    departmentVal = STRINGS.USER_DATA.DEPARTMENT + ":" + departmentVal;
    return (
      <View style={styles.box}>
        <TouchableHighlight
          onPress={() => this.rowPressed(rowData, sectionID, rowID)}
          underlayColor='#dddddd'
          style={[{backgroundColor: '#ffffff', zIndex: -1}]}>
          <View style={styles.rowContainer}>
            <View style={{flexDirection: 'column', paddingLeft: 10, width: width - 70}}>
              <Text style={{
                fontSize: 19,
                color: '#1C1D21',
                fontWeight: 'bold',
                fontFamily: 'DB Heavent'
              }} numberOfLines={1}>{rowData.displayName}</Text>
              <Text style={{
                fontSize: 16,
                color: '#4E9AA9',
                fontFamily: 'DB Heavent'
              }} numberOfLines={1}>{departmentVal}</Text>

              <Text numberOfLines={1}
                    style={{
                      fontSize: 16,
                      color: '#4E9AA9',
                      fontFamily: 'DB Heavent'
                    }}>{STRINGS.USER_DATA.ROLE}: {strRole.length !== 0 ? strRole.join(',') : '-'}</Text>
            </View>
          </View>
        </TouchableHighlight>
        {rowData.isFav ? options_menu_ic_fav : options_menu_ic}
        {bottomRowContent}
      </View>
    );
  }

  rowPressed(rowData, sectionID, rowID) {
    console.log("Press row user contact")
    this.props.navigator.push({
      name: 'userDetail',
      title: 'USER DETAIL',
      component: UserDetailPage,
      passProps: {
        refreshUser: this.props.refreshUser.bind(this),
        data: rowData,
        openMainModal: this.props.openMainModal.bind(this),
        openMainConfirm: this.props.openMainConfirm.bind(this)
      }
    });
  }

  pressFav(data, sectionID, rowID, statusFav) {
    let tmpData = this.state.dataRender;
    if (this.state.isSearching) {
      tmpData = this.state.dataSearch;
    }

    tmpData[rowID].isFav = statusFav;

    this.setState({
      dataSource: dataSource.cloneWithRows(tmpData),
      // dataRender: tmpData
    });

    var dataSave = [];
    for (let i = 0; i < resData.length; i++) {
      if (resData[i].isFav) {
        dataSave.push(resData[i]._id)
      }
    }
    this.saveFavUser(dataSave.join())
  }

  selectFilter(obj) {
    this.initUserContact(obj.value)
  }

  callTelephone(telNo, name) {
    console.log(telNo);
    this.selectTel = telNo;
    Timer.setTimeout(this, 'showDailog', () => {
      console.log('callTelephone');
      this.props.openMainConfirm(STRINGS.formatString(STRINGS.USER_CONTACT_PAGE.CONFIRM_CALL, name), this.confirmCallPhone)
    }, 500);
  }

  confirmCallPhone = () => {
    Communications.phonecall(this.selectTel, false)
  };

  pressSendMail(email) {
    this.selectEmail = email;
    this.props.openMainConfirm(STRINGS.formatString(STRINGS.USER_CONTACT_PAGE.CONFIRM_EMAIL, email), this.confirmPressSendMail)
  }

  confirmPressSendMail = () => {
    Communications.email([this.selectEmail], null, null, null, null)
  };

  searchTransactions(query) {

    this.setState({
      isLoading: true,
      dataSource: dataSource.cloneWithRows([])
    });

    let activeList = [];
    activeList = this.state.dataRender;

    if (query !== undefined && query !== null && query !== '') {
      query = query.toLowerCase();
      if (activeList !== null && activeList.length !== 0) {
        let tmp_activeList = _.filter(activeList, (v) => {
          if ('displayName' in v || 'email' in v) {
            let formName = v.displayName.toString()
            let description = (v.email !== undefined) ? v.email.toString() : '';

            return (formName.toLowerCase().indexOf(query) !== -1 || description.toLowerCase().indexOf(query) !== -1)
          } else {
            return true
          }
        });

        Timer.setTimeout(this, 'Load Data', () => {
          this.setState({
            isLoading: false,
            dataSource: dataSource.cloneWithRows(tmp_activeList),
            dataSearch: tmp_activeList,
            isSearching: true
          })
        }, 500);
      } else {
        Timer.setTimeout(this, 'Load Data', () => {
          this.setState({
            isLoading: false,
            dataSource: dataSource.cloneWithRows([]),
            dataSearch: [],
            isSearching: true
          });
        }, 500);
      }
    } else {
      this.setState({
        isLoading: false,
        dataSource: dataSource.cloneWithRows(activeList),
        dataSearch: [],
        isSearching: false
      });
    }
  }

  async saveFavUser(data) {
    console.log('saveFavUser: ', data);
    try {
      let value = await AsyncStorage.getItem('Username');
      await AsyncStorage.setItem('UserContact_fav_' + value, data);
    } catch (error) {
      console.log(error)
    }
  }

  async getFavUser() {
    console.log('getFavUser');
    try {
      let username = await AsyncStorage.getItem('Username');
      let value = await AsyncStorage.getItem('UserContact_fav_' + username);
      console.log('getFavUser value', value)
      this.setState({
        dataLocalStorage: value
      })
    } catch (error) {
      console.log(error)
    }
  }

  async saveUserFilter(data) {
    console.log('saveUserFilter: ', data);
    try {
      await AsyncStorage.setItem('userFilter', data);
    } catch (error) {
      console.log(error)
    }
  }

  async getUserFilter() {

    try {
      let value = await AsyncStorage.getItem('userFilter');
      console.log('getUserFilter: ' + value);
      if (value) {
        this.state.arrFilter = JSON.parse(value);
      }
    } catch (error) {
      console.log(error)
    }
  }
}

const styles = StyleSheet.create({
  spinner: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  tabbar: {
    height: 45,
    flexDirection: 'row',
    backgroundColor: '#B2D234'
  },
  tabView: {
    flexDirection: 'row',
    flexGrow: 0.3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    flexGrow: 1,
    marginTop: 70,
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowContainer: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center'
  },
  rowContain: {
    flexDirection: 'row'
  },
  colContainer: {
    flexDirection: 'column',
    padding: 10
  },
  separator: {
    height: 1,
    backgroundColor: '#dddddd'
  },
  indicator: {
    height: 2,
    backgroundColor: 'black',
    flexDirection: 'row',
    flexGrow: 0.3
  },
  pipe: {
    width: 1,
    backgroundColor: '#dddddd'
  },
  listView: {
    marginTop: 65
  },
  listImage: {
    width: 16,
    height: 20
  },
  status: {
    alignItems: 'flex-end',
    width: 60,
    height: 16
  },
  backdropView: {
    height: 16,
    width: 60,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0)',
  },
  headline: {
    fontSize: 8,
    textAlign: 'center',
    backgroundColor: 'rgba(0,0,0,0)',
    color: 'white'
  },
  img: {
    width: 25,
    height: 25,
    marginRight: 5
  },
  box: {
    padding: 8,
    // shadowColor: "#000000",
    // shadowOpacity: 0.8,
    // shadowRadius: 2,
    // shadowOffset: {
    //   height: 1,
    //   width: 0
    // }
  },
  circle: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center'
  },
  search: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  navBtnImage: {
    width: 25,
    height: 25
  },
  navBarBtn: {
    height: 35,
    marginTop: 10,
    marginRight: 15,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  rowSearch: {
    marginTop: 5,
    flexDirection: 'row',
    alignItems: 'center'
  },
  containerError: {
    flexGrow: 1,
    marginTop: 70,
    justifyContent: 'center',
    alignItems: 'center'
  },
  option_menu_icon: {
    width: 3,
    height: 13
  },
  option_menu_container: {
    // backgroundColor: '#43D5FE',
    alignItems: 'center',
    justifyContent: 'center',
    width: 45,
    height: 45,
    right: 10,
    // paddingTop: 7,
    // paddingRight: 20,
    top: 10,
    zIndex: 99,
    position: 'absolute'
  },
  actionButton: {
    flexGrow: 1,
    height: 50,
    justifyContent: 'center'
  },
  actionIcon: {
    width: 15,
    height: 15,
    marginBottom: 2
  },
  text: {
    color: '#FFFFFF',
    fontSize: 10
  },
  cancel_draft_icon: {
    width: 15,
    height: 16,
    marginBottom: 2
  },
  textArea: {
    height: 30,
    fontSize: 12,
    paddingTop: 2,
    paddingBottom: 2,
    color: '#000000',
    backgroundColor: '#ffffff'
  },
  borderView: {
    flexGrow: 1,
    marginRight: 2,
    marginLeft: 2,
    marginBottom: 4,
    marginTop: 4,
    padding: 1,
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 5,
  },

  okBtn: {
    width: 60,
    height: 30,
    backgroundColor: '#B2D234',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#B2D234',
    borderWidth: 1,
    borderRadius: 4,
    marginRight: 5
  },
  inactiveOkBtn: {
    width: 60,
    height: 30,
    backgroundColor: '#90A3AE',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#90A3AE',
    borderWidth: 1,
    borderRadius: 4,
    marginRight: 5
  },
  noInternetText: {
    marginTop: 20,
    fontSize: 22,
    textAlign: 'center'
  }
});
