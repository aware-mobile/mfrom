import React, { Component } from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Image,
  TouchableHighlight,
  ListView,
  ActivityIndicator,
  AsyncStorage,
  Platform,
  NativeModules,
  Text,
  Alert,
  AppState,
  NetInfo,
  Keyboard,
  LayoutAnimation,
  UIManager,
  Dimensions
} from 'react-native';
import FilterModel from "./CustomViews/FilterModel";

const Timer = require('react-native-timer');

let { height, width } = Dimensions.get('window');

import _ from 'lodash'
import WorkingDetail from './WorkingDetail'
import FormPage from './FormPage'
import NotiPage from './NotiPage'
import mainContact from './MainContact'
import HeaderMain from './HeaderMain'
import DFService from './Service/DFService';
import apiConfig from './Util/Config';
import ActionButton from 'react-native-action-button';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Util from './Util/Util';
import Color from './Util/Color';
import WorkingListTopSearchBar from './CustomViews/WorkingListTopSearchBar';
import AlertConfirmDialog from './Util/AlertConfirmDialog';
import FCM from 'react-native-fcm';
import STRINGS from './Util/strings';
import AppText from './Util/AppText';

import dismissKeyboard from 'react-native-dismiss-keyboard';

let Icon = require('react-native-vector-icons/FontAwesome');

let Device = NativeModules.RNAwareModule;
import CustomModal from './Util/CustomModalContent'

let dataSource = new ListView.DataSource(
  { rowHasChanged: (r1, r2) => r1._id !== r2._id });

let self;

class WorkingList extends Component {
  constructor(props) {
    super(props);

    this.original_workingList = [];
    this.original_draftList = [];
    this.original_historyList = [];

    this.workingList = [];
    this.draftList = [];
    this.historyList = [];

    this.workingFilter = {};
    this.draftFilter = {};
    this.historyFilter = {};

    this.userId = '';
    this.formQueue = null;
    this.index = 0;
    this.profile = {};
    this.lastRecievedNotiDate = new Date();

    this.keyboardIsShowing = false;

    this.state = {
      isShowAction: true,
      isLoading: true,
      message: '',
      lastUpdate: '',
      tabObj: [{ color: 'black' }, { color: '#B2D234' }, { color: '#B2D234' }],
      dataSource: dataSource.cloneWithRows([]),
      isMatch: true,
      workSize: 0,
      draftSize: 0,
      hisSize: -1,
      notiCount: 0,
      activeModal: false,
      submitModal: false,
      filterModal: false,
      createWorkData: {},
      optionMenuFormJobId: null,
      optionMenuTransId: null,
      optionMenuCurrentStep: null,
      optionMenuFlowId: null,
      optionMenuSelectIndex: -1,
      optionMenuRejectText: '',
      optionMenuDeleteText: '',
      optionMenu_showRejectText: false,
      optionMenu_showDeleteText: false,
      appState: AppState.currentState,
      previousAppState: "active",
      isConnected: true,
      keyboardHeight: 0,
      arrFilter: [],
    };
    self = this;
    this._loadInitial();
    this.activeTab = 'workingList';

    this.getNotificationBadge();

    this.confirmLogout = () => {
      this.setState({ isLoading: true });
      if (self.state.isConnected) {
        DFService.callApiWithHeader(null, apiConfig.logout, function (error, response) {
          if (response) {
            self.logoutApp();
          } else {
            console.log(error);
            self._storeLogoutOffline();
          }
        });
      } else {
        // In offline mode
        self._storeLogoutOffline()
      }
    };

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental &&
        UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  _storeLogoutOffline() {
    AsyncStorage.getItem('Profile', (err, result) => {
      if (err) {
        self.logoutApp();
      } else {
        let profile = JSON.parse(result);
        if (profile !== null) {
          AsyncStorage.setItem('LOGOUT_QUEUE', JSON.stringify(profile), () => {
            self.logoutApp();
          });
        } else {
          console.log('Profile: value is null!!');
          self.logoutApp();
        }
      }
    });
  }

  modalActive(isActive) {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({ activeModal: isActive })
  }

  componentDidMount() {
    this.notificationUnsubscribe = FCM.on('notification', (notif) => {
      // there are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
      // console.log('Received Local Notification', notif);

      let now = new Date();
      if (now > this.lastRecievedNotiDate) {
        this.lastRecievedNotiDate = now;
        if ('noti_type' in notif) {
          if (notif.noti_type === 'EXPIRED') {
            self.refreshList()
          }
        } else {
          if (notif.status === 'NEW' || notif.status === 'REJECTED') {
            if (self.activeTab === 'workingList') {
              if (!self.state.isLoading) {
                self.refreshList()
              }
            } else {
              self.setState({ workSize: self.state.workSize + 1 });
            }
          } else if (notif.status === 'COMPLETED' || notif.status === 'CANCELED' || notif.status === 'EXPIRED') {
            if (self.activeTab === 'historyList') {
              self.refreshList()
            } else {
              if (self.state.hisSize !== -1) {
                self.setState({ hisSize: self.state.hisSize + 1 });
              }
            }
          }
        }
      }

      self.setState({ notiCount: self.state.notiCount + 1 });
    });

    AppState.addEventListener('change', this._handleAppStateChange);

    NetInfo.isConnected.addEventListener('change', this._handleConnectivityChange);
  }

  componentWillMount() {
    // console.log('Working List componentWillMount!');
    this._addKeyBoardListeners();
  }

  componentWillUnmount() {
    // console.log('Working List componentWillUnmount!');
    Timer.clearTimeout(self);
    this.notificationUnsubscribe();

    AppState.removeEventListener('change', this._handleAppStateChange);

    NetInfo.isConnected.removeEventListener('change', this._handleConnectivityChange);

    this._removeKeyboardListeners();
  }

  _addKeyBoardListeners() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  _removeKeyboardListeners() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _handleConnectivityChange = (isConnected) => {
    if (this.state.isConnected !== isConnected) {
      this.setState({
        isConnected,
      });

      if (isConnected) {
        this.getSaveForm();
      }
    }
  };

  _keyboardDidShow = (e) => {
    this.keyboardIsShowing = false;
    let activeList = [];
    if (this.activeTab === 'workingList') activeList = this.workingList;
    else if (this.activeTab === 'draftList') activeList = this.draftList;
    else activeList = this.historyList;

    activeList.push({ keyboardRow: true });

    self.setState({
      keyboardHeight: e.endCoordinates.height,
      dataSource: dataSource.cloneWithRows(activeList)
    })
  };

  _keyboardDidHide = (e) => {
    this.keyboardIsShowing = false;
    self.setState({
      keyboardHeight: 0
    })
  };

  _handleAppStateChange(appState) {
    let routes = self.props.navigator.getCurrentRoutes();
    let currentRouteName = routes[routes.length - 1].name;
    if (currentRouteName !== 'dash') {
      return;
    }

    previousAppState = self.state.appState;

    self.setState({
      appState,
      previousAppState,
    });

    if (appState === 'active') {
      if (previousAppState === "background" || previousAppState === "inactive") {
        if (this.activeTab === 'workingList') {
          self.getNotificationBadge(true);
        } else {
          self.getNotificationBadge(false);
        }
      }
    }
  }

  rowPressed(formData, sectionID, rowID) {
    dismissKeyboard();
    this.props.navigator.push({
      name: 'WorkingDetail',
      title: formData.template_name,
      component: WorkingDetail,
      passProps: {
        onBack: this._onBack.bind(this),
        status: formData.status,
        step: formData.form_job_id.current_step,
        formData: formData,
        refreshWorkingList: this.refreshList.bind(this),
        logout: this.logout.bind(this),
        openMainModal: this.props.openMainModal.bind(this)
      }
    });
  }

  refreshList = () => {
    this.setState({
      isShowAction: true,
      isLoading: true,
      dataSource: dataSource.cloneWithRows([{}])
    });

    if (this.activeTab === 'historyList') {
      this.getHistoryList();
    } else {
      this.getWorkingList(true);
    }
  };

  logout() {
    self.props.openMainConfirm(STRINGS.POPUP_MSG.LOGOUT, this.confirmLogout)
  }

  logoutApp() {
    this.setState({ isLoading: false });
    FCM.cancelAllLocalNotifications();
    self.props.logout();
  }

  async _loadInitial() {
    try {
      if (!(Platform.OS === 'ios')) {
        Device.getDeviceScreen((response) => {
          if (response === 'phone') {
            Util.setPhone(true);
          } else {
            Util.setPhone(false);
          }
        });
      }
    } catch (error) {
      console.log(error)
    }
  }

  renderRow(rowData, sectionID, rowID) {

    let keyboardRow = ('keyboardRow' in rowData);
    if (keyboardRow && !this.keyboardIsShowing) {
      this.keyboardIsShowing = true;
      return (<View style={{ height: this.state.keyboardHeight }} />)
    }
    if (keyboardRow && this.keyboardIsShowing) {
      return (<View style={{ height: 0 }} />)
    }

    let showOptionsMenu = ('showOptionsMenu' in rowData && rowData.showOptionsMenu);

    let workingIDTemp = rowData.form_job_id !== undefined ? rowData.form_job_id._id : 'UNDEFINED';
    let workingID1 = workingIDTemp.slice(0, rowData.form_job_id._id.length / 2);
    let workingID2 = workingIDTemp.slice(rowData.form_job_id._id.length / 2);
    let createdDate = new Date(rowData.createdDate);

    let createdDateStr = Util.formatDate(createdDate);

    let displayName = '';
    if (rowData.form_job_id.createdBy) {
      displayName = rowData.form_job_id.createdBy.displayName;
    }

    let options_menu_ic = null;
    if (showOptionsMenu) {
      options_menu_ic = <TouchableHighlight
        underlayColor='transparent'
        style={styles.option_menu_container}
        onPress={() => this.closeOptionMenu(rowData, sectionID, rowID)}>
        <Ionicons name="md-close" size={20} color="#B2B3B7" />
      </TouchableHighlight>
    } else {
      if (this.activeTab === 'workingList' && rowData.status !== "INPROGRESS") {
        options_menu_ic = <TouchableHighlight
          underlayColor='transparent'
          style={styles.option_menu_container}
          onPress={() => this.showOptionMenu(rowData, sectionID, rowID)}>
          <Ionicons name="md-more" size={20} color="#B2B3B7" />
        </TouchableHighlight>
      }

      if (this.activeTab === 'workingList' && rowData.status === "INPROGRESS" && displayName === this.profile.displayName) {
        options_menu_ic = <TouchableHighlight
          underlayColor='transparent'
          style={styles.option_menu_container}
          onPress={() => this.showOptionMenu(rowData, sectionID, rowID)}>
          <Ionicons name="md-more" size={20} color="#B2B3B7" />
        </TouchableHighlight>
      }

      if (this.activeTab === 'draftList') {
        options_menu_ic = <TouchableHighlight
          underlayColor='transparent'
          style={styles.option_menu_container}
          onPress={() => this.showOptionMenu(rowData, sectionID, rowID)}>
          <Image source={require('./img/option_menu.png')} style={styles.option_menu_icon} />
        </TouchableHighlight>
      }
    }

    let rejectBtn = <TouchableHighlight
      key={'RejectBtn'}
      underlayColor='#4E949C'
      style={[styles.actionButton, { backgroundColor: '#5AAAB3' }]}
      onPress={() => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({ optionMenu_showRejectText: true });
      }}>
      <View style={{ alignItems: 'center', flexDirection: 'column' }}>
        <Image source={require('./img/reject_icon.png')} style={styles.actionIcon} />
        <AppText style={styles.text}>
          {STRINGS.POPUP_MSG.BTN_REJECT}
        </AppText>
      </View>
    </TouchableHighlight>;

    let deleteBtn = <TouchableHighlight
      key={'DeleteBtn'}
      underlayColor='#B04146'
      style={[styles.actionButton, { backgroundColor: '#D14E53' }]}
      onPress={() => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({ optionMenu_showDeleteText: true });
      }}>
      <View style={{ alignItems: 'center', flexDirection: 'column' }}>
        <Image source={require('./img/delete_transaction_icon.png')} style={styles.actionIcon} />
        <AppText style={styles.text}>
          {STRINGS.POPUP_MSG.BTN_DELETE}
        </AppText>
      </View>
    </TouchableHighlight>;

    let cancelDraftBtn = <TouchableHighlight
      key={'CancelDraftBtn'}
      underlayColor='#3F7F89'
      style={[styles.actionButton, { backgroundColor: '#4D99A6' }]}
      onPress={() => this.props.openMainConfirm(STRINGS.POPUP_MSG.CANCEL_DRAFT, this.cancelDraftOptionMenu)}>
      <View style={{ alignItems: 'center', flexDirection: 'column' }}>
        <Image source={require('./img/cancel_draft_icon.png')} style={styles.cancel_draft_icon} />
        <AppText style={styles.text}>
          {STRINGS.POPUP_MSG.BTN_CANCEL}
        </AppText>
      </View>
    </TouchableHighlight>;

    let rejectMsgText = <View style={styles.borderView} key={'rejectMsgText'}>
      <TextInput
        multiline={false}
        numberOfLines={1}
        style={styles.textArea}
        maxLength={500}
        underlineColorAndroid='rgba(0,0,0,0)'
        placeholder={STRINGS.WORKING_LIST_PAGE.ENTER_REASON_REJECT}
        onChangeText={(text) => {
          this.setState({ optionMenuRejectText: text });
          this.keyboardIsShowing = false;
        }}
      />
    </View>;

    let stylesBtnReject = styles.inactiveOkBtn;
    if (this.state.optionMenuRejectText.trim() !== '') {
      stylesBtnReject = styles.okBtn
    }

    let rejectSubmitBtn =
      <TouchableHighlight
        key={'rejectSubmitBtn'}
        onPress={() => {
          if (this.state.optionMenuRejectText.trim() !== '') {
            dismissKeyboard();
            this.props.openMainConfirm(STRINGS.WORKING_LIST_PAGE.REJECT_FORM, this.rejectFormOptionMenu)
          }
        }}
        underlayColor='#dddddd'
        style={stylesBtnReject}>
        <View>
          <AppText style={{ color: '#ffffff', fontSize: 12 }}>
            {STRINGS.POPUP_MSG.BTN_REJECT}
          </AppText>
        </View>
      </TouchableHighlight>;

    let deleteMsgText =
      <View style={styles.borderView} key={'deleteMsgText'}>
        <TextInput
          multiline={false}
          numberOfLines={1}
          style={styles.textArea}
          maxLength={500}
          underlineColorAndroid='rgba(0,0,0,0)'
          placeholder={STRINGS.WORKING_LIST_PAGE.ENTER_REASON_DELETE}
          onChangeText={(text) => {
            this.setState({ optionMenuDeleteText: text });
            this.keyboardIsShowing = false;
          }}
        />
      </View>;

    let stylesBtnDelete = styles.inactiveOkBtn;
    if (this.state.optionMenuDeleteText.trim() !== '') {
      stylesBtnDelete = styles.okBtn
    }

    let deleteSubmitBtn =
      <TouchableHighlight
        key={'deleteSubmitBtn'}
        onPress={() => {
          if (this.state.optionMenuDeleteText.trim() !== '') {
            dismissKeyboard();
            this.props.openMainConfirm(STRINGS.WORKING_LIST_PAGE.DELETE_TRANS, this.deleteTransOptionMenu);
          }
        }}
        underlayColor='#dddddd'
        style={stylesBtnDelete}>
        <View>
          <AppText style={{ color: '#ffffff', fontSize: 12 }}>
            {STRINGS.POPUP_MSG.BTN_DELETE}
          </AppText>
        </View>
      </TouchableHighlight>;

    let optionMenuContent = [];

    if (this.state.optionMenu_showRejectText || this.state.optionMenu_showDeleteText) {
      if (this.state.optionMenu_showRejectText) {
        optionMenuContent.push(rejectMsgText);
        optionMenuContent.push(rejectSubmitBtn)
      } else {
        optionMenuContent.push(deleteMsgText);
        optionMenuContent.push(deleteSubmitBtn)
      }
    } else {
      if (this.activeTab === 'workingList') {
        if (rowData.form_job_id.current_step > 1 && rowData.status !== "INPROGRESS") {
          optionMenuContent.push(rejectBtn)
        }
        optionMenuContent.push(deleteBtn)
      } else {
        if (rowData.form_job_id.current_step > 1) {
          optionMenuContent.push(rejectBtn)
        }
        optionMenuContent.push(cancelDraftBtn);
        optionMenuContent.push(deleteBtn)
      }
    }

    let optionMenuContainer =
      <View style={[styles.rowContain, { alignItems: 'center', zIndex: 10, left: 0, width: width - 15 }]}>
        <View style={[styles.rowContain, { flexGrow: 1, justifyContent: 'center', alignItems: 'center' }]}>
          {optionMenuContent}
        </View>
      </View>;

    let bottomRowContent =
      <View style={[styles.rowContain, { borderWidth: 1, borderColor: '#dddddd' }]}>

        <View style={{ flexDirection: 'row', flexGrow: 0.3, padding: 5, backgroundColor: '#EEEEF0' }}>
          <Image source={require('./img/workID.png')} style={styles.img} />
          <View style={{ flexDirection: 'column', flex: 0.3 }}>
            <AppText style={{ fontSize: 10, color: 'grey' }}>WORK ID: </AppText>
            <AppText style={{ fontSize: 8, color: 'grey', fontWeight: 'bold' }}>{workingID1}</AppText>
            <AppText style={{ fontSize: 8, color: 'grey', fontWeight: 'bold' }}>{workingID2}</AppText>
          </View>
        </View>

        <View style={styles.pipe} />

        <View style={{ flexDirection: 'row', flexGrow: 0.3, padding: 5, backgroundColor: '#EEEEF0' }}>
          <Image source={require('./img/workDead.png')} style={styles.img} />
          <View style={{ flexDirection: 'column', flex: 0.3 }}>
            <AppText style={{ fontSize: 10, color: 'grey' }}>DUE DATE: </AppText>
            <AppText style={{
              fontSize: 10,
              color: '#EF775B',
            }}>{this.dateDeadline(rowData.deadline, rowData.status)}</AppText>
          </View>
        </View>

        <View style={styles.pipe} />

        <View style={{ flexDirection: 'row', flexGrow: 0.3, padding: 5, backgroundColor: '#EEEEF0' }}>
          <Image source={require('./img/workStep.png')} style={styles.img} />
          <View style={{ flexDirection: 'column', flex: 0.3 }}>
            <View style={{ flexDirection: 'row' }}>
              <AppText style={{
                fontSize: 10,
                color: 'grey'
              }}>STEP:
              </AppText>
              <AppText style={{
                fontSize: 10,
                color: 'grey',
                fontWeight: 'bold'
              }}>{this.stepIndex(rowData.form_job_id.form_flow_id, rowData.form_job_id.current_step)}</AppText>
            </View>
            <AppText style={{ fontSize: 10, color: 'grey' }}>From: </AppText>
            <Text numberOfLines={1} style={{
              fontSize: 16,
              color: 'grey',
              fontWeight: 'bold',
              fontFamily: 'DB Heavent'
            }}>{this.getUpdate(rowData)}</Text>
          </View>
        </View>

      </View>;

    let centerWidth = width - 26 - 60 - 50; // subtracts Category circle and status flad and padding
    if (this.activeTab === 'draftList') {
      centerWidth = centerWidth + 50; // No status flag on draft list
    }

    return (
      <View style={[styles.box, { height: 180 }]}>
        <View style={{ backgroundColor: '#ffffff' }}>
          <View style={styles.rowContainer}>
          <View style={[styles.circle, { backgroundColor: rowData.user_form_id.category_id === null ? '#41516b': rowData.user_form_id.category_id.color }]}>
              <Image source={require('./img/wl_transaction_icon.png')} style={styles.listImage} />
            </View>
            <View style={{ flexDirection: 'column', paddingLeft: 10, width: centerWidth }}>
              <Text numberOfLines={1}
                style={{
                  fontSize: 19,
                  color: '#1C1D21',
                  fontWeight: 'bold',
                  fontFamily: 'DB Heavent'
                }}>{rowData.user_form_id.template_name}</Text>
              <Text numberOfLines={1}
                style={{
                  fontSize: 16,
                  color: '#666666',
                  fontFamily: 'DB Heavent'
                }}>{rowData.user_form_id.description}</Text>
              <Text numberOfLines={1}
                style={{
                  fontSize: 16,
                  color: '#B2D234',
                  fontFamily: 'DB Heavent'
                }}>{displayName}</Text>
              <Text numberOfLines={1}
                style={{ fontSize: 16, color: '#B2D234', fontFamily: 'DB Heavent' }}>{createdDateStr}</Text>
            </View>
            <Image source={this.getImgStatus(rowData.status)}
              style={[styles.status, { tintColor: this.getColorStatus(rowData.status) }]}>
              <View style={styles.backdropView}>
                <AppText style={styles.headline}>{this.getStatus(rowData.status)}</AppText>
              </View>
            </Image>
          </View>

          {(showOptionsMenu) ? optionMenuContainer : bottomRowContent}

          {options_menu_ic}

          <TouchableHighlight
            onPress={() => this.rowPressed(rowData, sectionID, rowID)}
            underlayColor='transparent'
            style={[styles.rowButtonOverlay]}>
            <View />
          </TouchableHighlight>

        </View>
      </View>
    );
  }

  render() {
    let activeList = [];

    if (this.activeTab === 'workingList') activeList = this.workingList;
    else if (this.activeTab === 'draftList') activeList = this.draftList;
    else activeList = this.historyList;

    let removeClippedSubviews = Platform.OS !== 'android';

    let mainHeader = <HeaderMain
      logout={this.logout.bind(this)}
      navigator={this.props.navigator}
      toggleMainSpinner={this.props.toggleMainSpinner.bind(this)}
      showMenu={this.props.showMenu.bind(this)}
      openMainModal={this.props.openMainModal}
      openMainConfirm={this.props.openMainConfirm}
      notiCount={this.state.notiCount}
      toMainContact={this.toMainContact.bind(this)}
      toNotiPage={this.toNotiPage.bind(this)} />;

    let emptyListText;
    if (this.activeTab === 'workingList') {
      emptyListText = STRINGS.WORKING_LIST_PAGE.NO_TRANS
    } else if (this.activeTab === 'draftList') {
      emptyListText = STRINGS.WORKING_LIST_PAGE.NO_DRAFTS
    } else {
      emptyListText = STRINGS.WORKING_LIST_PAGE.NO_HISTORY
    }

    let empty_list = <View style={{ flexGrow: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Image source={require('./img/empty_list_logo.png')} style={{ width: 160, resizeMode: 'contain' }} />
      <AppText style={styles.noInternetText}>{emptyListText}</AppText>
    </View>;

    let ListViewData = null;    
    if (activeList.length > 0) {
      if (this.state.isMatch) {
        ListViewData = <ListView
          keyboardShouldPersistTaps={true}
          dataSource={this.state.dataSource}
          renderRow={this.renderRow.bind(this)}
          enableEmptySections={true}
          removeClippedSubviews={removeClippedSubviews}
        />
      } else {
        ListViewData = <View style={{ flexGrow: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Image source={require('./img/empty_list_logo.png')} style={{ width: 160, resizeMode: 'contain' }} />
          <AppText style={styles.noInternetText}>{STRINGS.COMMON.NO_MATCH}</AppText>
        </View>
      }
    } else {
      ListViewData = empty_list
    }

    let loadView;

    if (!this.state.isConnected) {
      loadView =
        <View>
          {mainHeader}
          <View style={{ flexGrow: 1, justifyContent: 'center', alignItems: 'center' }}>
            <View style={{ width: 150, height: 150, marginTop: 50, justifyContent: 'center', alignItems: 'center' }}>
              <Icon
                name='exclamation-triangle'
                size={80}
                color='black'
              />
            </View>
            <AppText style={styles.noInternetText}>{STRINGS.COMMON.NO_INTERNET}</AppText>
          </View>
        </View>
    } else if (this.state.isLoading) {
      loadView =
        <View>
          {mainHeader}
          <View style={styles.spinner}>
            <ActivityIndicator
              hidden='true'
              size='large' />
            <AppText>{STRINGS.WORKING_LIST_PAGE.LOADING}</AppText>
          </View>
        </View>
    } else {
      if (this.state.message !== '') {
        loadView =
          <View>
            {mainHeader}
            <View style={styles.container}>
              <AppText>{this.state.message}</AppText>
            </View>
          </View>
      } else {
        loadView =
          <View style={{ flexGrow: 1 }}>
            {mainHeader}
            <View style={styles.tabbar}>
              <TouchableHighlight
                onPress={() => this.tabPress(0)}
                style={styles.tabView}
                underlayColor='#dddddd'>
                <View style={{ flexGrow: 1 }}>
                  <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                    <AppText style={{ fontSize: 14, fontWeight: 'bold', color: 'white' }}>{this.state.workSize}</AppText>
                    <AppText style={{ fontSize: 14, color: 'white' }}>{STRINGS.WORKING_LIST_PAGE.WORKING_TAB}</AppText>
                  </View>
                </View>
              </TouchableHighlight>
              <TouchableHighlight
                onPress={() => this.tabPress(1)}
                style={styles.tabView}
                underlayColor='#dddddd'>
                <View style={{ flexGrow: 1 }}>
                  <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                    <AppText style={{ fontSize: 14, fontWeight: 'bold', color: 'white' }}>{this.state.draftSize}</AppText>
                    <AppText style={{ fontSize: 14, color: 'white' }}>{STRINGS.WORKING_LIST_PAGE.DRAFT_TAB}</AppText>
                  </View>
                </View>
              </TouchableHighlight>
              <TouchableHighlight
                onPress={() => this.tabPress(2)}
                style={styles.tabView}
                underlayColor='#dddddd'>
                <View style={{ flexGrow: 1 }}>
                  <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                    <AppText style={{
                      fontSize: 14,
                      fontWeight: 'bold',
                      color: 'white'
                    }}>{this.state.hisSize !== -1 ? this.state.hisSize : ' '}</AppText>
                    <AppText style={{ fontSize: 14, color: 'white' }}>{STRINGS.WORKING_LIST_PAGE.HISTORY_TAB}</AppText>
                  </View>
                </View>
              </TouchableHighlight>
            </View>
            <View style={styles.rowContain}>
              <View style={[styles.indicator, { backgroundColor: this.state.tabObj[0].color }]} />
              <View style={[styles.indicator, { backgroundColor: this.state.tabObj[1].color }]} />
              <View style={[styles.indicator, { backgroundColor: this.state.tabObj[2].color }]} />
            </View>
            <WorkingListTopSearchBar pageName="workinglist" lastUpdate={this.state.lastUpdate}
              searchTransactions={this.searchTransactions.bind(this)}
              openFilter={this.openFilter.bind(this)} />
            {ListViewData}
            
            {(this.state.isShowAction) ? <ActionButton
              key={'btnAction'}
              offsetX={10}
              offsetY={10}
              buttonColor="#ee7155"
              onPress={() => {
                this.modalActive(true)
              }}
              icon={<Image source={require('./img/create_work.png')} style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: 25,
                width: 25
              }} />}>

            </ActionButton> : null}

          </View>
      }
    }

    return (
      <View style={{ flexGrow: 1, backgroundColor: '#F7F6F9' }}>
        {loadView}
        {this.state.activeModal ?
          <CustomModal createWork={this.createWork.bind(this)}
            stepPage={1}
            closeModal={this.closeModal.bind(this)}
            openMainModal={(message) => this.props.openMainModal(message)}
            toggleMainSpinner={this.props.toggleMainSpinner.bind(this)}
          /> : null}
        {this.state.filterModal ?
          <FilterModel arrFilter={this.state.arrFilter} closeFilter={this.closeFilter.bind(this)}
            saveFilter={this.saveFilter.bind(this)}
            openMainModal={(message) => this.props.openMainModal(message)} /> : null}
        {this.state.submitModal ?
          <AlertConfirmDialog confrimMessage={STRINGS.WORKING_LIST_PAGE.FORM_OFFLINE}
            closeModalFail={this.closeModalFail.bind(this)}
            closeModalSuccess={this.closeModalSuccess.bind(this)} /> : null}
      </View>
    )
  }

  closeModalFail() {
    this.setState({ submitModal: false });
    this.removeSaveForm();
    this.refreshList()
  }

  closeModalSuccess() {
    this.setState({ submitModal: false });
    this.uploadFileSaveForm(this.index);
  }

  uploadFileSaveForm(index) {
    if (index < this.formQueue.uploads.length) {
      let upload = this.formQueue.uploads[index];
      this.uploadFile(upload.field_key, upload, this.formQueue.form_data.form_job_id._id);
    } else {
      this.submitSaveForm();
    }
  }

  submitSaveForm() {
    if (this.formQueue.request_type === 'end') {
      this.submitEndForm(this.formQueue.form_save, this.formQueue.url);
    } else if (this.formQueue.request_type === 'submit') {
      this.submitForm(this.formQueue.form_save, this.formQueue.url);
    } else if (this.formQueue.request_type === 'reject') {
      this.rejectForm(this.formQueue.form_save);
    } else if (this.formQueue.request_type === 'cancel') {
      this.cancelForm(this.formQueue.form_save);
    } else if (this.formQueue.request_type === 'cancel_draft') {
      this.cancelDraftForm(this.formQueue.url);
    }
  }

  createWork(data, type) {
    if (type === 'creatework') {
      this.setState({ createWorkData: data, activeModal: false });
      if (this.state.createWorkData._id !== undefined) {
        this.props.navigator.push({
          name: 'submit_form',
          title: this.state.createWorkData.user_form_id.template_name,
          component: FormPage,
          passProps: {
            onBack: this._onBack.bind(this),
            formData: this.state.createWorkData,
            refreshWorkingList: this.refreshList.bind(this),
            logout: this.logout.bind(this),
            openMainModal: this.props.openMainModal.bind(this)
          }
        });
      }
    } else {
      this.closeModal();
      this.refreshList()
    }
  }

  _onBack = (popToTop = false) => {

    let routes = this.props.navigator.getCurrentRoutes();
    let currentRouteName = routes[routes.length - 1].name;

    try {
      this.props.navigator.refs[currentRouteName].onBackPressed(popToTop)
    } catch (error) {
      if (popToTop) {
        this.props.navigator.popToTop(0)
      } else {
        this.props.navigator.pop()
      }
    }
  };

  closeModal() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({ activeModal: false, filterModal: false })
  }

  openFilter() {
    this.setState({ filterModal: true })
  }

  closeFilter(filter) {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);

    this.setState({
      filterModal: false,
      arrFilter: filter,
      dataSource: dataSource.cloneWithRows([]),
    });
    this.filter(filter);
  }

  saveFilter(key, value) {
    this.setState({
      dataSource: dataSource.cloneWithRows([])
    });

    if (this.activeTab === 'workingList') {
      if (value !== null) {
        this.workingFilter[key] = value;
      } else {
        this.workingFilter = {};
      }
      this.saveWorkingFilter('workingList', JSON.stringify(this.workingFilter))
    } else if (this.activeTab === 'draftList') {
      if (value !== null) {
        this.draftFilter[key] = value;
      } else {
        this.draftFilter = {};
      }
      this.saveWorkingFilter('draftList', JSON.stringify(this.draftFilter))
    } else if (this.activeTab === 'historyList') {
      if (value !== null) {
        this.historyFilter[key] = value;
      } else {
        this.historyFilter = {};
      }
      this.saveWorkingFilter('historyList', JSON.stringify(this.historyFilter))
    }
  }

  filter(filter) {
    let filterList = [];
    if (this.activeTab === 'workingList') {
      if (!filter['All'][0].active) {
        for (let i = 0; i < this.workingList.length; i++) {
          let item = this.workingList[i];
          if (this.findFilter(filter, 'By Status', item.status) && this.findFilter(filter, 'By Category', item.user_form_id.category_id.name)) {
            filterList.push(item);
          }
        }
        // console.log(filterList);
        this.setState({
          isMatch: filterList.length > 0,
          dataSource: dataSource.cloneWithRows(filterList)
        });
      } else {
        this.setState({
          isMatch: true,
          dataSource: dataSource.cloneWithRows(this.workingList)
        });
      }
    } else if (this.activeTab === 'draftList') {
      if (!filter['All'][0].active) {
        for (let i = 0; i < this.draftList.length; i++) {
          let item = this.draftList[i];
          if (this.findFilter(filter, 'By Category', item.user_form_id.category_id.name)) {
            filterList.push(item);
          }
        }
        // console.log(filterList);
        this.setState({
          isMatch: filterList.length > 0,
          dataSource: dataSource.cloneWithRows(filterList)
        });
      } else {
        this.setState({
          isMatch: true,
          dataSource: dataSource.cloneWithRows(this.draftList)
        });
      }
    } else if (this.activeTab === 'historyList') {
      // console.log("on history list");
      if (!filter['All'][0].active) {
        for (let i = 0; i < this.historyList.length; i++) {
          let item = this.historyList[i];

          if (this.findFilter(filter, 'By Status', item.status) && this.findFilter(filter, 'By Category', item.user_form_id.category_id.name)) {
            filterList.push(item);
          }
        }
        // console.log(filterList);
        this.setState({
          isMatch: filterList.length > 0,
          dataSource: dataSource.cloneWithRows(filterList)
        });
      } else {
        this.setState({
          isMatch: true,
          dataSource: dataSource.cloneWithRows(this.historyList)
        });
      }
    }
  }

  findFilter(filter, key, value) {
    let list = filter[key];
    let isNotCheck = true;
    for (let i = 0; i < list.length; i++) {
      let item = list[i];
      if (item.name === value && item.active) {
        return true;
      }
      if (item.active) {
        isNotCheck = false
      }
    }
    return isNotCheck;
  }

  tabPress(position) {
    if (position === 0) {
      if (this.activeTab !== 'workingList') {
        this.activeTab = 'workingList';
        this.setState({
          isLoading: true,
          // dataSource: dataSource.cloneWithRows([{}]),
          isMatch: true,
          tabObj: [{ color: 'black' }, { color: '#B2D234' }, { color: '#B2D234' }],
        });
        this.refreshList()
      }
    } else if (position === 1) {
      if (this.activeTab !== 'draftList') {
        this.activeTab = 'draftList';
        this.setState({
          isLoading: true,
          // dataSource: dataSource.cloneWithRows([{}]),
          isMatch: true,
          tabObj: [{ color: '#B2D234' }, { color: 'black' }, { color: '#B2D234' }],
        });
        this.refreshList()
      }
    } else {
      if (this.activeTab !== 'historyList') {
        this.activeTab = 'historyList';
        this.setState({
          isLoading: true,
          // dataSource: dataSource.cloneWithRows([{}]),
          isMatch: true,
          tabObj: [{ color: '#B2D234' }, { color: '#B2D234' }, { color: 'black' }],
        });
        this.refreshList()
      }
    }
  }

  stepIndex(flow, index) {
    if (flow === undefined) {
      return index;
    } else {
      return index + '/' + flow.steps.length;
    }
  }

  getWorkingList(next = false) {
    self = this;

    FCM.cancelAllLocalNotifications();

    let apiData = {};
    DFService.callApiWithHeader(apiData, apiConfig.getWorkinglist, function (error, response) {
      if (response) {
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          self.successWorking(response, next);
          if (next) {
            self.getDraftList();
          } else {
            self.setState({ isLoading: false });
          }
        } else {
          // Failed Response
          self.setState({ isLoading: false });
          // INVALID CREDENTIALS
          if (response.httpCode === 401) {
            self.props.openMainConfirm(response.resMessage, () => {
              self.logoutApp();
            });
          } else if (response.httpCode === 403) {
            self.props.openMainModal(response.resMessage, () => {
              self.logoutApp();
            });
          } else {
            self.props.openMainModal(response.resMessage);
          }
        }
      } else {
        // Error from application calling service
        self.setState({
          isLoading: false,
          dataSource: dataSource.cloneWithRows([]),
        });
      }
    });
  }

  getDraftList(next = false) {
    self = this;
    let apiData = {};
    DFService.callApiWithHeader(apiData, apiConfig.listFormDraft, function (error, response) {
      if (response) {
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          self.successDraft(response, next);
        } else {
          // Failed Response
          self.setState({ isLoading: false });
          // INVALID CREDENTIALS
          if (response.httpCode === 401) {
            self.props.openMainConfirm(response.resMessage, () => {
              self.logoutApp();
            });
          } else if (response.httpCode === 403) {
            self.props.openMainModal(response.resMessage, () => {
              self.logoutApp();
            });
          } else {
            self.props.openMainModal(response.resMessage);
          }
        }
      } else {
        // Error from application calling service
        self.setState({ isLoading: false });
        console.log("error = ", error);
      }
    });
  }

  getHistoryList() {
    self = this;
    let apiData = {};
    DFService.callApiWithHeader(apiData, apiConfig.getHistorylist, function (error, response) {
      if (response) {
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          self.successHistory(response);
        } else {
          // Failed Response
          self.setState({ isLoading: false });
          // INVALID CREDENTIALS
          if (response.httpCode === 401) {
            self.props.openMainConfirm(response.resMessage, () => {
              self.logoutApp();
            });
          } else if (response.httpCode === 403) {
            self.props.openMainModal(response.resMessage, () => {
              self.logoutApp();
            });
          } else {
            self.props.openMainModal(response.resMessage);
          }
        }
      } else {
        // Error from application calling service
        self.setState({ isLoading: false });
        console.log("error = ", error);
      }
    });
  }

  dateExpire(date, expire, status) {
    if (status === "COMPLETED" || status === "CANCELED") {
      return "None"
    }
    if (expire > 0) {
      let d = new Date(date);
      d.setHours(d.getHours() + expire);
      return Util.format(d)
    } else {
      return "None"
    }
  }

  dateDeadline(date, status) {
    //console.log('dateDeadline: ' + date);
    if (status === "COMPLETED" || status === "CANCELED") {
      return "None"
    }
    if (date) {
      let d = new Date(date);
      return Util.format(d)
    } else {
      return "None"
    }
  }

  isExpire(date, expire) {
    if (expire > 0) {
      let d = new Date(date);
      d.setHours(d.getHours() + expire);
      let dateNow = new Date();
      return dateNow > d;
    } else {
      return false;
    }
  }

  getUpdate(form_item) {
    if ('updatedBy' in form_item && form_item.updatedBy !== null) {
      return form_item.updatedBy.firstName;
    } else if ('updatedByCustomerToken' in form_item && form_item.updatedByCustomerToken !== null) {
      //console.log('form_item.updatedByCustomerToken: ', form_item.updatedByCustomerToken)
      if ('customer_email' in form_item.updatedByCustomerToken) {
        return form_item.updatedByCustomerToken.customer_email
      } else {
        return form_item.updatedByCustomerToken.customer_id.name
      }
    } else {
      return '-';
    }
  }

  getImgStatus(status) {
    if (status === "NEW" && this.activeTab !== 'draftList') {
      return require('./img/iconNew.png')
    } else if (status === "REJECTED" && this.activeTab !== 'draftList') {
      return require('./img/wl_rejected_flag.png')
    } else if (status === "INPROGRESS" && this.activeTab !== 'draftList') {
      return require('./img/wl_inprogress_flag.png')
    } else if (status === "EXPIRED" && this.activeTab !== 'draftList') {
      return require('./img/wl_expired_flag.png')
    } else if (status === "COMPLETED" && this.activeTab !== 'draftList') {
      return require('./img/wl_completed_flag.png')
    } else if (status === "CANCELED" && this.activeTab !== 'draftList') {
      return require('./img/wl_cancled_flag.png')
    }
  }

  getColorStatus(status) {
    if (status === "NEW" && this.activeTab !== 'draftList') {
      return Color.ribbon_new;
    } else if (status === "REJECTED" && this.activeTab !== 'draftList') {
      return Color.ribbon_reject;
    } else if (status === "INPROGRESS" && this.activeTab !== 'draftList') {
      return Color.ribbon_inprogress;
    } else if (status === "EXPIRED" && this.activeTab !== 'draftList') {
      return Color.ribbon_expire;
    } else if (status === "COMPLETED" && this.activeTab !== 'draftList') {
      return Color.ribbon_complete;
    } else if (status === "CANCELED" && this.activeTab !== 'draftList') {
      return Color.ribbon_cancel;
    } else {
      return null;
    }
  }

  getStatus(status) {
    // console.log("Status = ", status);
    if (status === "NEW" && this.activeTab !== 'draftList') {
      return 'New';
    } else if (status === "REJECTED" && this.activeTab !== 'draftList') {
      return 'Reject';
    } else if (status === "INPROGRESS" && this.activeTab !== 'draftList') {
      return 'Inprogress';
    } else if (status === "EXPIRED" && this.activeTab !== 'draftList') {
      return 'Expire';
    } else if (status === "COMPLETED" && this.activeTab !== 'draftList') {
      return 'Complete';
    } else if (status === "CANCELED" && this.activeTab !== 'draftList') {
      return 'Cancel';
    }
  }

  async successWorking(response, isLoading = false) {
    this.workingList = [];
    if (response.resCode === 'DF2000000') {

      let arrStatus = [];
      let arrCat = [];
      let listStatus = [];
      let listCat = [];
      let isAll = true;
      try {
        let value = await AsyncStorage.getItem('filterworkingList');
        // console.log('getFilter: ' + value);
        if (value) {
          this.workingFilter = JSON.parse(value);
        }
      } catch (error) {

      }

      for (let i = 0; i < response.data.length; i++) {
        if (response.data[i].user_form_id !== null) {
          let item = response.data[i];
          let arrVariable = item.user_form_id.template_name.match(/\$\{(.*?)\}/g);
          // console.log('item.form_job_id.form_data_id', item.form_job_id.form_data_id);
          if (arrVariable) {
            for (let x = 0; x < arrVariable.length; x++) {
              let strFormNameVar = arrVariable[x].replace(/\$\{(.*?)\}/g, "$1")
              if (item.form_job_id.form_data_id) {
                console.log('item.form_job_id.form_data_id', item.form_job_id.form_name_variable);
                if (!this.isObject(item.form_job_id.form_data_id.form_name_variable[strFormNameVar])) {
                  item.user_form_id.template_name = item.user_form_id.template_name.replace("${" + strFormNameVar + "}", item.form_job_id.form_data_id.form_name_variable[strFormNameVar]);
                } else {
                  item.user_form_id.template_name = item.user_form_id.template_name.replace("${" + strFormNameVar + "}", '');
                }
              } else {
                item.user_form_id.template_name = item.user_form_id.template_name.replace("${" + strFormNameVar + "}", '');
              }
            }
          }
          item.template_name = item.user_form_id.template_name;
          item.description = item.user_form_id.description;
          item.showOptionsMenu = false;
          this.workingList.push(item);

          // Add local notification for any deadlines
          if (item.deadline !== null) {
            this._createLocalNoti(item)
          }

          if (!arrStatus[item.status]) {
            let value = false;
            if (this.workingFilter[item.status]) {
              value = this.workingFilter[item.status];
              if (value === true) {
                isAll = false;
              }
            }
            arrStatus[item.status] = { name: item.status, active: value };
            listStatus.push(arrStatus[item.status]);
          }

          if (item.user_form_id.category_id !== null &&
            !arrCat[item.user_form_id.category_id.name]) {
            let value = false;
            if (this.workingFilter[item.user_form_id.category_id.name]) {
              value = this.workingFilter[item.user_form_id.category_id.name];
              if (value === true) {
                isAll = false;
              }
            }
            arrCat[item.user_form_id.category_id.name] = { name: item.user_form_id.category_id.name, active: value };
            listCat.push(arrCat[item.user_form_id.category_id.name]);
          }

        }
      }

      let listFilter = {};

      listFilter['All'] = [{ name: 'All Working List', active: isAll }];

      if (isAll) {
        this.saveWorkingFilter('workingList', '{}')
      }

      if (listStatus.length > 0) {
        listFilter['By Status'] = listStatus
      }

      if (listCat.length > 0) {
        listFilter['By Category'] = listCat
      }

      let last = Util.formatDate(new Date());// new Date(workingList[0].createdDate).toLocaleString('en-GB')

      //this.original_workingList = this.workingList.splice()

      if (this.activeTab === 'workingList') {
        this.setState({
          isLoading: isLoading,
          lastUpdate: last,
          arrFilter: listFilter,
          message: '',
          dataSource: dataSource.cloneWithRows([]),
          workSize: this.workingList.length
        });
        this.filter(this.state.arrFilter);
      } else {
        this.setState({
          isLoading: isLoading,
          workSize: this.workingList.length

        });
      }

    } else {
      this.setState({ isLoading: false, message: response.resMessage });
    }
  }

  async successDraft(response, isLoading = false) {
    this.draftList = [];

    if (response.resCode === 'DF2000000') {
      let arrStatus = [];
      let arrCat = [];
      let listStatus = [];
      let listCat = [];
      let isAll = true;
      try {
        let value = await AsyncStorage.getItem('filterdraftList');
        // console.log('getFilter: ' + value);
        if (value) {
          this.draftFilter = JSON.parse(value);
        }
      } catch (error) {

      }

      for (let i = 0; i < response.data.length; i++) {
        if (response.data[i].form_transaction_id !== null) {
          let item = response.data[i].form_transaction_id;
          let arrVariable = item.user_form_id.template_name.match(/\$\{(.*?)\}/g);
          // console.log('item.form_job_id', item.form_job_id.form_data_id);
          if (arrVariable) {
            for (let x = 0; x < arrVariable.length; x++) {
              let strFormNameVar = arrVariable[x].replace(/\$\{(.*?)\}/g, "$1")
              if (item.form_job_id.form_data_id) {
                if (!this.isObject(item.form_job_id.form_data_id.form_name_variable[strFormNameVar])) {
                  item.user_form_id.template_name = item.user_form_id.template_name.replace("${" + strFormNameVar + "}", item.form_job_id.form_data_id.form_name_variable[strFormNameVar]);
                } else {
                  item.user_form_id.template_name = item.user_form_id.template_name.replace("${" + strFormNameVar + "}", '');
                }
              } else {
                item.user_form_id.template_name = item.user_form_id.template_name.replace("${" + strFormNameVar + "}", '');
              }
            }
          }
          item.template_name = item.user_form_id.template_name;
          item.description = item.user_form_id.description;
          item.showOptionsMenu = false;
          this.draftList.push(item);

          if (item.deadline !== null) {
            this._createLocalNoti(item)
          }

          if (!arrCat[item.user_form_id.category_id.name]) {
            let value = false;
            if (this.draftFilter[item.user_form_id.category_id.name]) {
              value = this.draftFilter[item.user_form_id.category_id.name];
              if (value === true) {
                isAll = false;
              }
            }
            arrCat[item.user_form_id.category_id.name] = { name: item.user_form_id.category_id.name, active: value };
            listCat.push(arrCat[item.user_form_id.category_id.name]);
          }

        }
      }

      let listFilter = {};

      listFilter['All'] = [{ name: 'All Draft List', active: isAll }];

      if (isAll) {
        this.saveWorkingFilter('draftList', '{}')
      }

      if (listCat.length > 0) {
        listFilter['By Category'] = listCat
      }

      let last = Util.formatDate(new Date());

      //this.original_draftList = this.draftList.splice()

      if (this.activeTab === 'draftList') {
        this.setState({
          isLoading: isLoading,
          lastUpdate: last,
          arrFilter: listFilter,
          message: '',
          dataSource: dataSource.cloneWithRows([]),
          draftSize: this.draftList.length
        });
        this.filter(this.state.arrFilter);
      } else {
        this.setState({
          isLoading: isLoading,
          draftSize: this.draftList.length
        });
      }

    } else {
      this.setState({ isLoading: false, message: response.resMessage });
    }
  }

  async successHistory(response, isLoading = false) {
    this.historyList = [];
    //console.log("historyList");
    if (response.resCode === 'DF2000000') {
      //console.log("historyList DF2000000");
      let arrStatus = [];
      let arrCat = [];
      let listStatus = [];
      let listCat = [];
      let isAll = true;
      try {
        let value = await AsyncStorage.getItem('filterhistoryList');
        // console.log('getFilter: ' + value);
        if (value) {
          this.historyFilter = JSON.parse(value);
        }
      } catch (error) {

      }

      for (let i = 0; i < response.data.length; i++) {

        if (response.data[i].user_form_id !== null) {
          let item = response.data[i];
          let arrVariable = item.user_form_id.template_name.match(/\$\{(.*?)\}/g);
          //console.log('item.form_job_id', item.form_job_id.form_data_id);
          if (arrVariable) {
            for (let x = 0; x < arrVariable.length; x++) {
              let strFormNameVar = arrVariable[x].replace(/\$\{(.*?)\}/g, "$1");
              if (item.form_job_id.form_data_id) {
                if (!this.isObject(item.form_job_id.form_data_id.form_name_variable[strFormNameVar])) {
                  item.user_form_id.template_name = item.user_form_id.template_name.replace("${" + strFormNameVar + "}", item.form_job_id.form_data_id.form_name_variable[strFormNameVar]);
                } else {
                  item.user_form_id.template_name = item.user_form_id.template_name.replace("${" + strFormNameVar + "}", '');
                }
              } else {
                item.user_form_id.template_name = item.user_form_id.template_name.replace("${" + strFormNameVar + "}", '');
              }
            }
          }
          item.template_name = item.user_form_id.template_name;
          item.description = item.user_form_id.description;
          item.showOptionsMenu = false;
          this.historyList.push(item);

          if (!arrStatus[item.status]) {
            let value = false;
            if (this.historyFilter[item.status]) {
              value = this.historyFilter[item.status];
              if (value === true) {
                isAll = false;
              }
            }
            arrStatus[item.status] = { name: item.status, active: value };
            listStatus.push(arrStatus[item.status]);
          }

          if (!arrCat[item.user_form_id.category_id.name]) {
            let value = false;
            if (this.historyFilter[item.user_form_id.category_id.name]) {
              value = this.historyFilter[item.user_form_id.category_id.name];
              if (value === true) {
                isAll = false;
              }
            }
            arrCat[item.user_form_id.category_id.name] = { name: item.user_form_id.category_id.name, active: value };
            listCat.push(arrCat[item.user_form_id.category_id.name]);
          }
        }
      }

      let listFilter = {};

      listFilter['All'] = [{ name: 'All History List', active: isAll }];

      if (isAll) {
        this.saveWorkingFilter('historyList', '{}')
      }

      if (listStatus.length > 0) {
        listFilter['By Status'] = listStatus
      }

      if (listCat.length > 0) {
        listFilter['By Category'] = listCat
      }

      let last = Util.formatDate(new Date());

      //this.original_historyList = this.historyList.splice();

      if (this.activeTab === 'historyList') {
        // console.log("historyList");
        this.setState({
          isLoading: isLoading,
          lastUpdate: last,
          arrFilter: listFilter,
          message: '',
          dataSource: dataSource.cloneWithRows([]),
          hisSize: this.historyList.length
        });
        this.filter(this.state.arrFilter);
      } else {

        this.setState({
          isLoading: isLoading,
          hisSize: this.historyList.length
        });
      }

    } else {
      this.setState({ isLoading: false, message: response.resMessage });
    }
  }

  async saveWorkingFilter(tap, data) {
    // console.log('saveFilter: ' + data);
    try {
      await AsyncStorage.setItem('filter' + tap, data);
    } catch (error) {
      console.log(error)
    }
  }

  isObject = function (a) {
    return (!!a) && (a.constructor === Object);
  };

  getNotificationBadge(reload = true) {
    let self = this;
    DFService.getNotificationCount(function (error, response) {
      if (response) {
        if (response.resCode === "DF2000000") {
          self.setState({ notiCount: response.data.count });
          if (reload) {
            self.getSaveForm()
          }
        } else {
          self.setState({ isLoading: false });
          if (reload) {
            self.getSaveForm()
          }
        }
      } else {
        console.log("get notification count error = ", error);
      }
    });
  }

  async getSaveForm() {
    // console.log('getSaveForm');
    try {
      let value = await AsyncStorage.getItem("Profile");
      let profile = await JSON.parse(value);
      this.userId = profile.user.id;
      this.profile = profile.user;
      // console.log(profile.user.id);
      value = await AsyncStorage.getItem('processQueue_' + this.userId);
      // console.log(value);
      if (!value) {
        this.refreshList();
        return;
      }
      this.formQueue = await JSON.parse(value);
      if (this.formQueue !== null) {
        this.setState({
          submitModal: true,
        });
      }
    } catch (error) {

    }
  }

  rejectForm(saveData) {

    DFService.callApiWithHeader(saveData, apiConfig.rejectForm, function (error, response) {

      if (response) {
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          self.cancelDraftForm(self.formQueue.form_data._id)
        } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
          self.setState({
            isLoading: false
          });
          let callback = self.refreshList;
          self.props.toggleMainSpinner(false);
          self.props.openMainModal(response.resMessage, callback)
        } else {
          // Failed Response
          self.setState({ isLoading: false });
          // INVALID CREDENTIALS
          if (response.httpCode === 401) {
            self.props.openMainConfirm(response.resMessage, () => {
              self.logoutApp();
            });
          } else {
            self.props.openMainModal(response.resMessage);
          }
        }
      } else {
        // Error from application calling service
        self.setState({ isLoading: false });
        console.log("error = ", error);
      }
    });
  }

  cancelForm(saveData) {

    DFService.callApiWithHeader(saveData, apiConfig.cancelForm, function (error, response) {

      if (response) {
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          self.cancelDraftForm(self.formQueue.form_data._id)
        } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
          self.setState({
            isLoading: false
          });
          let callback = self.refreshList;
          self.props.toggleMainSpinner(false);
          self.props.openMainModal(response.resMessage, callback)
        } else {
          // Failed Response
          self.setState({ isLoading: false });
          // INVALID CREDENTIALS
          if (response.httpCode === 401) {
            self.props.openMainConfirm(response.resMessage, () => {
              self.logoutApp();
            });
          } else {
            self.props.openMainModal(response.resMessage);
          }
        }
      } else {
        // Error from application calling service
        self.setState({ isLoading: false });
        console.log("error = ", error);
      }
    });
  }

  submitForm(saveData, formId) {
    let config = JSON.parse(JSON.stringify(apiConfig.saveFormData));
    config.url += formId;

    DFService.callApiWithHeader(saveData, config, function (error, response) {

      if (response) {
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          self.cancelDraftForm(self.formQueue.form_data._id)
        } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
          self.setState({
            isLoading: false
          });
          self.props.toggleMainSpinner(false);
          let callback = self.refreshList;
          self.props.openMainModal(response.resMessage, callback)
        } else {
          // Failed Response
          self.setState({ isLoading: false });
          // INVALID CREDENTIALS
          if (response.httpCode === 401) {
            self.props.openMainConfirm(response.resMessage, () => {
              self.logoutApp();
            });
          } else {
            self.props.openMainModal(response.resMessage);
          }
        }
      } else {
        // Error from application calling service
        self.setState({ isLoading: false });
        console.log("error = ", error);
      }
    });
  }

  submitEndForm(saveData, formId) {
    let config = JSON.parse(JSON.stringify(apiConfig.submitEndForm));
    config.url += formId;

    DFService.callApiWithHeader(saveData, config, function (error, response) {

      if (response) {
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          self.cancelDraftForm(self.formQueue.form_data._id)
        } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
          self.setState({
            isLoading: false
          });
          let callback = self.refreshList;
          self.props.toggleMainSpinner(false);
          self.props.openMainModal(response.resMessage, callback)
        } else {
          // Failed Response
          self.setState({ isLoading: false });
          // INVALID CREDENTIALS
          if (response.httpCode === 401) {
            self.props.openMainConfirm(response.resMessage, () => {
              self.logoutApp();
            });
          } else {
            self.props.openMainModal(response.resMessage);
          }
        }
      } else {
        // Error from application calling service
        self.setState({ isLoading: false });
        console.log("error = ", error);
      }

    });
  }

  _clearAutoSaveData(draftFormID) {
    let STORAGE_KEY = 'AutoSaveData:' + draftFormID;
    AsyncStorage.removeItem(STORAGE_KEY, (err) => {
      if (err) {
        conole.log(err)
      }
    });
  }

  cancelDraftForm(formId) {
    let config = JSON.parse(JSON.stringify(apiConfig.cancelDraftForm));
    config.url += formId; //self.state.formData._id;
    let apiData = {};

    DFService.callApiWithHeader(apiData, config, function (error, response) {

      if (response) {
        // console.log("Cancel Draft Res Code: ", response.resCode)
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          self._clearAutoSaveData(self.formQueue.form_draft_id);
          self.removeSaveForm();
          self.setState({ isLoading: false });
          self.refreshList()
        } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
          self.setState({
            isLoading: false
          });
          let callback = self.refreshList;
          self.props.toggleMainSpinner(false);
          self.props.openMainModal(response.resMessage, callback)
        } else {
          // Failed Response
          self.setState({ isLoading: false });
          // INVALID CREDENTIALS
          if (response.httpCode === 401) {
            self.props.openMainConfirm(response.resMessage, () => {
              self.logoutApp();
            });
          } else {
            self.props.openMainModal(response.resMessage);
            self.refreshList();
          }
        }
      } else {
        // Error from application calling service
        self.setState({ isLoading: false });
        console.log("error = ", error);
      }

    });
  }

  async removeSaveForm() {
    await AsyncStorage.removeItem('processQueue_' + this.userId);
  }

  async uploadFile(field_key, uploadObj, formJobID) {
    self = this;
    try {
      let type = uploadObj.uploadFileType;

      let formData = new FormData();
      formData.append(type, uploadObj);
      let config = null;

      if (type === 'image') {
        config = JSON.parse(JSON.stringify(apiConfig.uploadFileImage));
      } else {
        config = JSON.parse(JSON.stringify(apiConfig.uploadFileFile));
      }
      config.url += formJobID;

      DFService.callApiWithHeaderObj(formData, config, function (error, response) {

        if (response) {
          if (response.resCode === 'DF2000000') {
            // Successfull Response
            let file_url = response.data;
            self.insertData(field_key, file_url);
            self.index += 1;
            self.uploadFileSaveForm(self.index);
          } else {
            // Failed Response
            self.setState({ isLoading: false });
            // INVALID CREDENTIALS
            if (response.httpCode === 401) {
              self.props.openMainConfirm(response.resMessage, () => {
                self.logoutApp();
              });
            } else {
              self.props.openMainModal(response.resMessage);
            }
          }
        } else {
          // Error from application calling service
          console.log('upload offline in working error :' + error);
          self.setState({ isLoading: false });
          self.props.openMainModal(STRINGS.COMMON.UNEXPECTED_ERROR);
        }

      });
    } catch (error) {
      self.setState({ isLoading: false });
      self.props.openMainModal(STRINGS.COMMON.UNEXPECTED_ERROR_CATCH);
    }
  }

  insertData(field_key, data) {
    // console.log('insertData', data);
    for (let i = 0; i < this.formQueue.form_save.pages.length; i++) {
      for (let j = 0; j < this.formQueue.form_save.pages[i].fields.length; j++) {
        if (this.formQueue.form_save.pages[i].fields[j].field_key === field_key) {
          this.formQueue.form_save.pages[i].fields[j].value = data;
          return;
        }
      }
    }
  }

  toNotiPage() {
    this.props.navigator.push({
      name: "noti",
      title: "NOTIFICATION",
      component: NotiPage,
      passProps: {
        onBack: this._onBack.bind(this),
        logout: this.logout.bind(this),
        refreshWorkingList: this.refreshList.bind(this),
        clearAll: this.getNotificationBadge.bind(this, false),
        openMainModal: this.props.openMainModal.bind(this)
      }
    });
  }

  toMainContact() {
    this.props.navigator.push({
      name: "contact",
      title: "CUSTOMER CONTACT",
      component: mainContact,
      passProps: {
        navigator: this.props.navigator,
        onBack: this._onBack.bind(this),
        // logout: this.logout.bind(this),
        toggleMainSpinner: this.props.toggleMainSpinner.bind(this),
        refreshWorkingList: this.refreshList.bind(this),
        openMainModal: this.props.openMainModal.bind(this),
        openMainConfirm: this.props.openMainConfirm.bind(this)
      }
    });
  }

  showOptionMenu(form_obj, sectionID, rowID) {

    let form_flow_id = null;
    if (form_obj.form_job_id.form_flow_id !== undefined && form_obj.form_job_id.form_flow_id !== null) {
      form_flow_id = form_obj.form_job_id.form_flow_id._id
    }

    let activeList = [];

    if (this.activeTab === 'workingList') activeList = this.workingList;
    else if (this.activeTab === 'draftList') activeList = this.draftList;
    else activeList = this.historyList;

    if (this.state.optionMenuSelectIndex !== -1) {
      if ('showOptionsMenu' in activeList[this.state.optionMenuSelectIndex]) {
        activeList[this.state.optionMenuSelectIndex].showOptionsMenu = false
      }
    }

    // New Row to show options
    if ('showOptionsMenu' in activeList[rowID]) {
      activeList[rowID].showOptionsMenu = true
    }

    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);

    this.setState({
      isShowAction: false,
      optionMenuFormJobId: form_obj.form_job_id._id,
      optionMenuTransId: form_obj._id,
      optionMenuFlowId: form_flow_id,
      optionMenuCurrentStep: form_obj.form_job_id.current_step,
      optionMenuSelectIndex: rowID,
      optionMenuDeleteText: '',
      optionMenuRejectText: '',
      optionMenu_showRejectText: false,
      optionMenu_showDeleteText: false,
      dataSource: dataSource.cloneWithRows(activeList)
    })
  }

  closeOptionMenu(form_obj, sectionID, rowID) {

    let activeList = [];

    if (this.activeTab === 'workingList') activeList = this.workingList;
    else if (this.activeTab === 'draftList') activeList = this.draftList;
    else activeList = this.historyList;

    if (this.state.optionMenuSelectIndex !== -1) {
      if ('showOptionsMenu' in activeList[this.state.optionMenuSelectIndex]) {
        activeList[this.state.optionMenuSelectIndex].showOptionsMenu = false
      }
    }

    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);

    this.setState({
      isShowAction: true,
      optionMenuFormJobId: null,
      optionMenuTransId: null,
      optionMenuFlowId: null,
      optionMenuCurrentStep: null,
      optionMenuSelectIndex: -1,
      optionMenuDeleteText: '',
      optionMenuRejectText: '',
      optionMenu_showRejectText: false,
      optionMenu_showDeleteText: false,
      dataSource: dataSource.cloneWithRows(activeList)
    })

  }

  closeActionMenuWithErr(msg = '') {
    this.setState({ activeModal: false });
    if (msg !== '') {
      this.props.openMainModal(msg)
    }
  }

  searchTransactions(query) {
    //console.log("searchTransactions: ", error);
    this.setState({
      dataSource: dataSource.cloneWithRows([{}])
    });
    let activeList = [];
    let tmp_activeList = [];
    if (this.activeTab === 'workingList') activeList = this.workingList;
    else if (this.activeTab === 'draftList') activeList = this.draftList;
    else activeList = this.historyList;
    
    let countKey = 0;
    if (query !== undefined && query !== null && query !== '') {
      query = query.toLowerCase();
      if (activeList !== null && activeList.length !== 0) {
        tmp_activeList = _.filter(activeList, (v) => {
          if ('template_name' in v) {
            let formName = v.template_name.toString();
            
            let description = (v.description !== undefined) ? v.description.toString() : '';
            let _id = (v.form_job_id._id !== undefined) ? v.form_job_id._id.toString() : '';

            return (_id.toLowerCase().indexOf(query) !== -1 || formName.toLowerCase().indexOf(query) !== -1 || description.toLowerCase().indexOf(query) !== -1)
          } else {
            countKey += 1;
            return true
          }
        });
        console.log('tmp_activeList : ', tmp_activeList);

        this.setState({
          isMatch: tmp_activeList.length - countKey > 0,
          dataSource: dataSource.cloneWithRows(tmp_activeList)
        })
      }
    } else {
      this.setState({
        isMatch: true,
        dataSource: dataSource.cloneWithRows(activeList)
      })
    }

  }

  // OPTION MENU ACTIONS
  cancelDraftOptionMenu = () => {
    self.props.toggleMainSpinner(true);

    let config = JSON.parse(JSON.stringify(apiConfig.cancelDraftFormByUser));
    let apiData = {};
    config.url += self.state.optionMenuTransId;
    DFService.callApiWithHeader(apiData, config, function (error, response) {
      // Stop the loading spinner
      self.props.toggleMainSpinner(false);

      if (response) {
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          self.clearOptionMenuInfo();
          self.refreshList()
        } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
          self.setState({
            isLoading: false
          });
          let callback = self.refreshList;
          self.props.toggleMainSpinner(false);
          self.props.openMainModal(response.resMessage, callback)
        } else {
          // Failed Response
          self.setState({ isLoading: false });
          // INVALID CREDENTIALS
          if (response.httpCode === 401) {
            self.props.openMainConfirm(response.resMessage, () => {
              self.logoutApp();
            });
          } else {
            self.props.openMainModal(response.resMessage);
          }
        }
      } else {
        // Error from application calling service
        console.log("error = ", error);
      }

    });
  };

  cancelDraftNoNeed = () => {
    self.props.toggleMainSpinner(true);

    let config = JSON.parse(JSON.stringify(apiConfig.cancelDraftForm));
    let apiData = {};
    config.url += self.state.optionMenuTransId;
    DFService.callApiWithHeader(apiData, config, function (error, response) {
      // Stop the loading spinner
      self.props.toggleMainSpinner(false);

      if (response) {
        // console.log("cancelDraftOptionMenu Res Code: ", response.resCode)
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          self.clearOptionMenuInfo();
          self.refreshList()
        } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
          self.setState({
            isLoading: false
          });
          let callback = self.refreshList;
          self.props.toggleMainSpinner(false);
          self.props.openMainModal(response.resMessage, callback)
        } else {
          // Failed Response
          self.setState({ isLoading: false });
          // INVALID CREDENTIALS
          if (response.httpCode === 401) {
            self.props.openMainConfirm(response.resMessage, () => {
              self.logoutApp();
            });
          } else {
            self.props.openMainModal(response.resMessage);
          }
        }
      } else {
        // Error from application calling service
        console.log("error = ", error);
      }

    });
  };

  deleteTransOptionMenu = () => {
    self.props.toggleMainSpinner(true);
    let config = JSON.parse(JSON.stringify(apiConfig.cancelForm));
    let cancelBody = {
      "form_job_id": self.state.optionMenuFormJobId,
      "cancel_msg": self.state.optionMenuDeleteText,
      "current_step": self.state.optionMenuCurrentStep
    };
    DFService.callApiWithHeader(cancelBody, config, function (error, response) {
      if (response) {
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          if (self.state.activeTab !== 'workingList') {
            self.cancelDraftNoNeed()
          } else {
            self.clearOptionMenuInfo();
            self.props.toggleMainSpinner(false);
            self.refreshList()
          }
        } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
          self.setState({
            isLoading: false
          });
          self.props.toggleMainSpinner(false);
          let callback = self.refreshList;
          self.props.openMainModal(response.resMessage, callback)
        } else {
          // Failed Response
          self.props.toggleMainSpinner(false);
          // INVALID CREDENTIALS
          if (response.httpCode === 401) {
            self.props.openMainConfirm(response.resMessage, () => {
              self.logoutApp();
            });
          } else {
            self.props.openMainModal(response.resMessage);
          }
        }
      } else {
        // Error from application calling service
        self.props.openMainModal('Unknown error occured');
        console.log("error = ", error);
      }

    });
  };

  rejectFormOptionMenu = () => {
    self.props.toggleMainSpinner(true);
    let rejectBody = {
      "form_job_id": self.state.optionMenuFormJobId,
      "reject_msg": self.state.optionMenuRejectText,
      "current_step": self.state.optionMenuCurrentStep
    };
    DFService.callApiWithHeader(rejectBody, apiConfig.rejectForm, function (error, response) {

      if (response) {
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          if (self.state.activeTab !== 'workingList') {
            self.cancelDraftNoNeed()
          } else {
            self.clearOptionMenuInfo();
            self.props.toggleMainSpinner(false)
          }
        } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
          self.setState({
            isLoading: false
          });
          self.props.toggleMainSpinner(false);
          let callback = self.refreshList;
          self.props.openMainModal(response.resMessage, callback)
        } else {
          // Failed Response
          self.props.toggleMainSpinner(false);
          // INVALID CREDENTIALS
          if (response.httpCode === 401) {
            self.props.openMainConfirm(response.resMessage, () => {
              self.logoutApp();
            });
          } else {
            self.props.openMainModal(response.resMessage);
          }
        }
      } else {
        // Error from application calling service
        self.props.toggleMainSpinner(false);
        self.props.openMainModal('Unknown error occurred');
        console.log("error = ", error);
      }
    });
  };

  clearOptionMenuInfo() {
    this.setState({
      isShowAction: true,
      optionMenuFormJobId: null,
      optionMenuTransId: null,
      optionMenuCurrentStep: null,
      optionMenuFlowId: null,
      optionMenuSelectIndex: -1,
      optionMenuRejectText: '',
      optionMenuDeleteText: '',
      optionMenu_showRejectText: false,
      optionMenu_showDeleteText: false,
    })
  }

  // SCHEDULING LOCAL NOTIFICATIONS
  async _createLocalNoti(item) {
    let notiDate = new Date(item.deadline);
    let now = new Date();
    let sound = "default";
    let value = await AsyncStorage.getItem("SoundNoti");
    if (value) {
      sound = value;
    }
    if (notiDate > now) {
      FCM.scheduleLocalNotification({
        fire_date: notiDate.getTime(),      //RN's converter is used, accept epoch time and whatever that converter supports
        id: item._id,    //REQUIRED! this is what you use to lookup and delete notification. In android notification with same ID will override each other
        title: "Form Expired",                     // as FCM payload
        body: STRINGS.formatString(STRINGS.WORKING_LIST_PAGE.TRANS_EXPIRED, item.user_form_id.template_name),
        big_text: STRINGS.formatString(STRINGS.WORKING_LIST_PAGE.TRANS_EXPIRED, item.user_form_id.template_name),                    // as FCM payload (required)
        sound: sound === "off" ? undefined : "default",                                   // as FCM payload
        priority: "high",                                   // as FCM payload
        click_action: "ACTION",                             // as FCM payload
        noti_type: "EXPIRED",
        // color: "red",                                       // Android only
        vibrate: 300,                                       // Android only default: 300, no vibration if you pass null
        lights: true
      })
    }
  }
}

function addMinutes(date, minutes) {
  return new Date(date.getTime() + minutes * 60000);
}


const styles = StyleSheet.create({
  spinner: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  tabbar: {
    height: 45,
    flexDirection: 'row',
    backgroundColor: '#B2D234'
  },
  tabView: {
    flexDirection: 'row',
    flexGrow: 0.3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    flexGrow: 1,
    marginTop: 70,
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowContainer: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center',
    alignSelf: 'center'
  },
  rowContain: {
    flexDirection: 'row'
  },
  colContainer: {
    flexDirection: 'column',
    padding: 10
  },
  separator: {
    height: 1,
    backgroundColor: '#dddddd'
  },
  indicator: {
    height: 2,
    backgroundColor: 'black',
    flexDirection: 'row',
    flexGrow: 0.3
  },
  pipe: {
    width: 1,
    backgroundColor: '#dddddd'
  },
  listView: {
    marginTop: 65
  },
  listImage: {
    width: 16,
    height: 20
  },
  status: {
    alignItems: 'flex-end',
    width: 60,
    height: 16
  },
  backdropView: {
    height: 16,
    width: 60,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0)',
  },
  headline: {
    fontSize: 8,
    textAlign: 'center',
    backgroundColor: 'rgba(0,0,0,0)',
    color: 'white'
  },
  img: {
    width: 25,
    height: 25
  },
  box: {
    padding: 8,
    // shadowColor: "#000000",
    // shadowOpacity: 0.8,
    // shadowRadius: 2,
    // shadowOffset: {
    //   height: 1,
    //   width: 0
    // }
  },
  circle: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center'
  },
  search: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  navBtnImage: {
    width: 25,
    height: 25
  },
  navBarBtn: {
    height: 35,
    marginTop: 10,
    marginRight: 15,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  rowSearch: {
    marginTop: 5,
    flexDirection: 'row',
    alignItems: 'center'
  },
  containerError: {
    flexGrow: 1,
    marginTop: 70,
    justifyContent: 'center',
    alignItems: 'center'
  },
  option_menu_icon: {
    width: 3,
    height: 13
  },
  option_menu_container: {
    alignItems: 'flex-end',
    width: 50,
    height: 115,
    right: 0,
    paddingTop: 3,
    paddingRight: 10,
    top: 0,
    zIndex: 2,
    position: 'absolute'
  },
  rowButtonOverlay: {
    alignSelf: 'flex-start',
    width: width - 60,
    height: 115,
    left: 0,
    top: 0,
    zIndex: 3,
    position: 'absolute'
  },
  actionButton: {
    flexGrow: 1,
    height: 50,
    justifyContent: 'center'
  },
  actionIcon: {
    width: 15,
    height: 15,
    marginBottom: 2
  },
  text: {
    color: '#FFFFFF',
    fontSize: 10
  },
  cancel_draft_icon: {
    width: 15,
    height: 16,
    marginBottom: 2
  },
  textArea: {
    height: 30,
    fontSize: 18,
    paddingTop: 2,
    paddingBottom: 2,
    borderWidth: 0,
    color: '#000000',
    backgroundColor: '#ffffff'
  },
  borderView: {
    flexGrow: 1,
    backgroundColor: '#ffffff',
    marginRight: 2,
    marginLeft: 2,
    marginBottom: 4,
    marginTop: 4,
    padding: 1,
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 5,
  },

  okBtn: {
    width: 60,
    height: 30,
    backgroundColor: '#B2D234',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#B2D234',
    borderWidth: 1,
    borderRadius: 4,
    marginRight: 5
  },
  inactiveOkBtn: {
    width: 60,
    height: 30,
    backgroundColor: '#90A3AE',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#90A3AE',
    borderWidth: 1,
    borderRadius: 4,
    marginRight: 5
  },
  noInternetText: {
    marginTop: 20,
    fontSize: 22,
    textAlign: 'center'
  }
});

export default WorkingList;
