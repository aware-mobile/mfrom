import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  Platform
} from 'react-native';

var styles

class DFPopup extends Component {
    constructor(props) {
        super(props);

        var title = ('title' in this.props) ? this.props.title : 'AIS Digital Form'

        this.state = {
          modelWidth: Dimensions.get('window').width - 60,
          deviceWidth: Dimensions.get('window').width,
          deviceHeight: Dimensions.get('window').height,
          title: title
        }

        var marginTop = -100

        styles = StyleSheet.create({
          container: {
            flexGrow: 1,
            position: 'absolute',
            top: 0,
            left: 0,
            width: this.state.deviceWidth,
            height: this.state.deviceHeight,
            backgroundColor: '#00000075',
            justifyContent: 'center',
            alignSelf: 'center',
          },
          innerContainer: {
            width: this.state.modelWidth,
            backgroundColor: 'white',
            borderRadius: 7,
            paddingTop: 10,
            justifyContent: 'center',
            alignSelf: 'center',
            marginTop: marginTop
          },
          header: {
            backgroundColor: '#779e2f',
            padding: 10,
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
          },
          headerText: {
            color: 'white',
            textAlign: 'center',
            fontSize: 16,
            fontWeight: 'bold'
          },
          modelContent: {
            padding: 8,
            justifyContent: 'center',
            alignSelf: 'stretch'
          }
        })
    }

    // <View style={styles.header}>
    //   <Text style={styles.headerText}>
    //     {this.state.title}
    //   </Text>
    // </View>

    render() {
        return (
          <View style={styles.container}>
            <View style={styles.innerContainer}>
              <View style={styles.modelContent}>
                {this.props.children}
              </View>
            </View>
          </View>
        )
    }
}

export default DFPopup;
