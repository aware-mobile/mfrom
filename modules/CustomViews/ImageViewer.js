import React, { Component } from 'react';

import {
  StyleSheet,
  TextInput,
  View,
  Image,
  Button,
  TouchableHighlight,
  Platform,
  Dimensions
} from 'react-native';

import PhotoView from 'react-native-photo-view';
import AppText from '../Util/AppText'

var {height, width} = Dimensions.get('window');

class ImageViewer extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <View style={styles.imageViewContainer}>
        <View style={styles.imageViewTopBar}>

          <View style={styles.imageViewtitleContainer}>
            <AppText numberOfLines={1} style={styles.imageViewtitle}>{this.props.mainImageViewTitle}</AppText>
          </View>

          <View style={styles.imageViewRowContain}>
            <TouchableHighlight
              underlayColor='transparent'
              style={styles.navBarBtn}
              onPress={() => { this.props.closeImageViewer() }}>
              <Image source={require('../img/delete-button.png')} style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: 30,
                width: 30
              }}/>
            </TouchableHighlight>

          </View>

        </View>

        <PhotoView
            source={this.props.mainImageViewUrl}
            minimumZoomScale={1}
            maximumZoomScale={3}
            androidScaleType="center"
            onLoad={() => console.log("Image loaded!")}
            style={{width: this.props.mainImageViewWidth, height: this.props.mainImageViewHeight}}
            />
      </View>
    );
  }

}
var styles = StyleSheet.create({
  imageViewContainer: {
    ...StyleSheet.absoluteFillObject,
    zIndex: 9996,
    backgroundColor: '#2d313c',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageViewtitle: {
    fontSize: 18,
    width: width-90,
    color: 'white',
    textAlign: 'center',
    // backgroundColor:'#a4eda4',
  },
  imageViewtitleContainer: {
    width: width,
    // backgroundColor:'#eda4e6',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageViewRowContain: {
    position: 'absolute',
    top: 23,
    right: 5,
    // backgroundColor:'#a4eddc',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageViewTopBar: {
    ...StyleSheet.absoluteFillObject,
    zIndex: 9997,
    height:65,
    backgroundColor: 'rgba(45, 49, 60, 0.5)',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 15
  },
  navBarBtn: {
    height: 35,
    width: 35,
    justifyContent: 'center',
    alignItems: 'center'
  },
});

export default ImageViewer;
