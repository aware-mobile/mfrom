import React, {Component} from 'react';

import {
  StyleSheet,
  TextInput,
  View,
  Image,
  Button,
  TouchableHighlight,
  Platform,
  Dimensions
} from 'react-native';

import DFLabel from '../FormComponents/DFLabel'
import DFButton from '../FormComponents/DFButton'
import DFService from '../Service/DFService';
import apiConfig from '../Util/Config';
import AppText from '../Util/AppText'
import DFErrorLabel from '../FormComponents/DFErrorLabel'
import STRINGS from '../Util/strings.js';
var Ionicons = require('react-native-vector-icons/Ionicons');
var {height, width} = Dimensions.get('window');

class ForgotPasswordModel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headerTitle: STRINGS.FORGOT_PASSWORD_MODAL.HEADER,
      errorEmail: "",
      msg: "",
    };
    this.email = "";
  }

  render() {

    return (<View style={styles.container}>
      <View style={styles.innerContainer}>
        <View style={styles.headerModal}>
          <AppText style={styles.textHeaderModal}>
            {this.state.headerTitle}
          </AppText>
          <TouchableHighlight
            underlayColor='transparent'
            style={{marginRight: 15}}
            onPress={() => this.onPressed(null) }>
            <Ionicons name="md-close" size={30} color="black"
                      style={{justifyContent: 'center', alignItems: 'center'}}/>
          </TouchableHighlight>
        </View>
        <View>
          <AppText style={styles.textContainer}>
            {STRINGS.FORGOT_PASSWORD_MODAL.BODY}
          </AppText>
          <View style={styles.borderView}>
            <TextInput
              keyboardType="email-address"
              style={styles.input}
              placeholder={STRINGS.LOGIN_LABEL.EMAIL}
              maxLength={100}
              editable={true}
              underlineColorAndroid='rgba(0,0,0,0)'
              onChangeText={(text) => {
                this.email = text;
                this.setState({errorEmail: ''});
              }}
            />
          </View>
          <DFErrorLabel
            key={"email-required"}
            isShow={true}
            text={this.state.errorEmail} />
          <DFButton
            text={STRINGS.FORGOT_PASSWORD_MODAL.BTN_RESET}
            onPress={() => this.onPressed(this.email)}
            style={{backgroundColor: "#B2D234", borderColor: "#B2D234"}}
            underlayColor='#B2D234'/>
        </View>
      </View>
    </View>)
  }

  onPressed(email) {
    if (email === null) {
      this.props.closeAction(email)
    } else {
      if(this.validateEmail(email)){
        this.props.closeAction(email)
      }else{
        this.setState({errorEmail: STRINGS.VALIDATION_MESSAGES.EMAIL_INCORRECT_FORMAT });
      }
    }
  }

  validateEmail(email) {
    let emailFilter = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
    return emailFilter;
  }
}

const styles = StyleSheet.create({
  textContainer: {
    marginTop: 20,
    marginBottom: 20,
    paddingLeft: 20,
    paddingRight: 20,
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent: 'center',
    padding: 30
  },
  headerModal: {
    height: 60,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#efeff1'
  },
  textHeaderModal: {
    marginLeft: 20,
    fontWeight: 'bold',
    fontSize: 18,
  },
  input: {
    height: 30,
    fontSize: 20,
    color: '#333435',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 2,
    paddingBottom: 2
  },
  innerContainer: {
    backgroundColor: '#ffffff'
  },
  rowContainer: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center'
  },
  row: {
    alignItems: 'center',
    flexGrow: 1,
    flexDirection: 'row',
    marginBottom: 20,
  },
  rowTitle: {
    flexGrow: 1,
    fontWeight: 'bold',
  },
  button: {
    borderRadius: 5,
    flexGrow: 1,
    height: 44,
    alignSelf: 'stretch',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  buttonText: {
    fontSize: 18,
    margin: 5,
    textAlign: 'center',
  },
  modalButton: {
    marginTop: 10,
  },
  textArea: {
    height: 110,
    padding: 4,
    flexGrow: 4,
    fontSize: 14,
    color: '#49621c',
    backgroundColor: '#ffffff'
  },
  borderView: {
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 8,
  },
  img: {
    width: 30,
    height: 30
  },
});

export default ForgotPasswordModel;
