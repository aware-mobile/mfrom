import React, { Component } from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Image,
  Button,
  TouchableHighlight,
  Platform,
  Dimensions
} from 'react-native';

import DFLabel from '../FormComponents/DFLabel'
import DFButton from '../FormComponents/DFButton'
import DFService from '../Service/DFService';
import apiConfig from '../Util/Config';
import STRINGS from '../Util/strings';
import AppText from '../Util/AppText'
var Ionicons = require('react-native-vector-icons/Ionicons');
var {height, width} = Dimensions.get('window');

class NoFlowModel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headerTitle: this.props.headerTitle,
    };
  }

  render() {
    var styleBtn = {
      height: 36,
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: 1,
      borderRadius: 4, backgroundColor: "#B2D234", borderColor: "#B2D234"
    };

    var modal =
        <View style={styles.container}>
          <View style={styles.innerContainer}>
            <View style={styles.headerModal}>
              <AppText style={styles.textHeaderModal}>
                {this.state.headerTitle}
              </AppText>
              <TouchableHighlight
                underlayColor='transparent'
                style={{marginRight: 15}}
                onPress={this.props.closeDialog}>
                <Ionicons name="md-close" size={30} color="black"
                          style={{justifyContent: 'center', alignItems: 'center'}}/>
              </TouchableHighlight>
            </View>

            <View>
              <TouchableHighlight
                onPress={this.selectAction.bind(this, 'submit') }
                underlayColor='#dddddd'>
                <View style={{flexDirection: 'column', padding: 10, flexGrow: 0.6}}>
                  <AppText style={{fontSize: 18, color: 'black'}}>{STRINGS.POPUP_MSG.BTN_SUBMIT}</AppText>
                </View>
              </TouchableHighlight>
              <TouchableHighlight
                onPress={this.selectAction.bind(this, 'end') }
                underlayColor='#dddddd'>
                <View style={{flexDirection: 'column', padding: 10, flexGrow: 0.6}}>
                  <AppText style={{fontSize: 18, color: 'black'}}>{STRINGS.POPUP_MSG.BTN_NEXT}</AppText>
                </View>
              </TouchableHighlight>

            </View>
          </View>
        </View>;

    return (modal)
  }

  selectAction(action) {
    this.props.noFlowAction(action);
  }

}
var styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent: 'center',
    padding: 30
  },
  headerModal: {
    height: 60,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#efeff1'
  },
  textHeaderModal: {
    marginLeft: 20,
    fontWeight: 'bold',
    fontSize: 18,
  },
  innerContainer: {
    backgroundColor: '#ffffff'
  },
  row: {
    alignItems: 'center',
    flexGrow: 1,
    flexDirection: 'row',
    marginBottom: 20,
  },
  rowTitle: {
    flexGrow: 1,
    fontWeight: 'bold',
  },
  button: {
    borderRadius: 5,
    flexGrow: 1,
    height: 44,
    alignSelf: 'stretch',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  buttonText: {
    fontSize: 18,
    margin: 5,
    textAlign: 'center',
  },
  modalButton: {
    marginTop: 10,
  },
  textArea: {
    height: 110,
    padding: 4,
    flexGrow: 4,
    fontSize: 14,
    color: '#49621c',
    textAlignVertical: 'top',
    backgroundColor: '#ffffff'
  },
  borderView: {
    marginRight: 5,
    marginLeft: 5,
    marginBottom: 10,
    padding: 10,
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 8,
  },
});

export default NoFlowModel;
