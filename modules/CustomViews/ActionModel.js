import React, { Component } from 'react';

import {
  StyleSheet,
  TextInput,
  View,
  Image,
  Button,
  TouchableHighlight,
  Platform,
  Dimensions
} from 'react-native';

import DFLabel from '../FormComponents/DFLabel'
import DFButton from '../FormComponents/DFButton'
import DFService from '../Service/DFService';
import apiConfig from '../Util/Config';
import AppText from '../Util/AppText'

var Ionicons = require('react-native-vector-icons/Ionicons');
var {height, width} = Dimensions.get('window');

import STRINGS from '../Util/strings'

class ActionModel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headerTitle: this.props.headerTitle,
      isReject: this.props.isReject,
      msgError: "",
      isLoading: false,
    };
    this.reason = "";
  }

  render() {

    var reject = this.props.step === 1 ? null :
      <TouchableHighlight
        onPress={() => this.onPressed(1) }
        underlayColor='#dddddd'>
        <View style={styles.rowContainer}>
          <Image resizeMode='contain' source={require('../img/reject_icon_gray.png') } style={styles.reject_icon_gray}/>
          <View style={{ flexDirection: 'column', paddingLeft: 10, flexGrow: 0.6 }}>
            <AppText style={{ fontSize: 18, color: 'black' }}>{STRINGS.CUSTOM_MODALS.ACTION_REJECT}</AppText>
          </View>
        </View>
      </TouchableHighlight>;



    return (<View style={styles.container}>
        <View style={styles.innerContainer}>
          <View style={styles.headerModal}>
            <AppText style={styles.textHeaderModal}>
              {this.state.headerTitle}
            </AppText>
            <TouchableHighlight
              underlayColor='transparent'
              style={{ marginRight: 15 }}
              onPress={() => this.onPressed(99) }>
              <Ionicons name="md-close" size={30} color="black"
                style={{ justifyContent: 'center', alignItems: 'center' }}/>
            </TouchableHighlight>
          </View>
          <View>

            {
              this.props.type !== "FORM" ? <TouchableHighlight
                onPress={() => this.onPressed(0) }
                underlayColor='#dddddd'>
                <View style={styles.rowContainer}>
                  <Image resizeMode='contain' source={require('../img/cancel_draft_gray.png') } style={styles.cancel_draft_gray}/>
                  <View style={{ flexDirection: 'column', paddingLeft: 10, flexGrow: 0.6 }}>
                    <AppText style={{ fontSize: 18, color: 'black' }}>{STRINGS.CUSTOM_MODALS.ACTION_DELETE_TRANS}</AppText>
                  </View>
                </View>
              </TouchableHighlight> : null
            }
            {
              this.props.type === "FORM" ?
                <TouchableHighlight
                  onPress={() => this.onPressed(2) }
                  underlayColor='#dddddd'>
                  <View style={styles.rowContainer}>
                    <Ionicons name="md-refresh" size={30} style={{marginLeft: 4}} color="#626262"/>
                    <View style={{ flexDirection: 'column', paddingLeft: 13, flexGrow: 0.6 }}>
                      <AppText style={{ fontSize: 18, color: 'black' }}>{STRINGS.CUSTOM_MODALS.ACTION_CLEAR_DATA}</AppText>
                    </View>
                  </View>
                </TouchableHighlight> : null
            }

            { reject }

            {
              this.props.type !== "WORK" ?
                <TouchableHighlight
                  onPress={() => this.onPressed(4) }
                  underlayColor='#dddddd'>
                  <View style={styles.rowContainer}>
                    <Image  resizeMode='contain' source={require('../img/cancel_draft_gray.png') } style={styles.cancel_draft_gray}/>
                    <View style={{ flexDirection: 'column', paddingLeft: 10, flexGrow: 0.6 }}>
                      <AppText style={{ fontSize: 18, color: 'black' }}>{STRINGS.CUSTOM_MODALS.ACTION_CANCEL_DRAFT}</AppText>
                    </View>
                  </View>
                </TouchableHighlight> : null
            }

            {
              this.props.type === "FORM" ?
                <TouchableHighlight
                  onPress={() => this.onPressed(3) }
                  underlayColor='#dddddd'>
                  <View style={styles.rowContainer}>
                    <Image resizeMode='contain' source={require('../img/save_draft_icon_gray.png') } style={styles.save_draft_icon_gray}/>
                    <View style={{ flexDirection: 'column', paddingLeft: 10, flexGrow: 0.6 }}>
                      <AppText style={{ fontSize: 18, color: 'black' }}>{STRINGS.CUSTOM_MODALS.ACTION_SAVE_DRAFT}</AppText>
                    </View>
                  </View>
                </TouchableHighlight> : null
            }


          </View>
        </View>
      </View>)
  }

  onPressed(number) {
    this.props.closeAction(number)
  }

}
var styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent: 'center',
    padding: 30
  },
  headerModal: {
    height: 60,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#efeff1'
  },
  textHeaderModal: {
    marginLeft: 20,
    fontWeight: 'bold',
    fontSize: 18,
  },
  innerContainer: {
    backgroundColor: '#ffffff'
  },
  rowContainer: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center'
  },
  row: {
    alignItems: 'center',
    flexGrow: 1,
    flexDirection: 'row',
    marginBottom: 20,
  },
  rowTitle: {
    flexGrow: 1,
    fontWeight: 'bold',
  },
  button: {
    borderRadius: 5,
    flexGrow: 1,
    height: 44,
    alignSelf: 'stretch',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  buttonText: {
    fontSize: 18,
    margin: 5,
    textAlign: 'center',
  },
  modalButton: {
    marginTop: 10,
  },
  textArea: {
    height: 110,
    padding: 4,
    flexGrow: 4,
    fontSize: 14,
    color: '#49621c',
    backgroundColor: '#ffffff'
  },
  borderView: {
    marginRight: 5,
    marginLeft: 5,
    marginBottom: 10,
    padding: 10,
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 8,
  },
  img: {
    width: 30,
    height: 30
  },
  reject_icon_gray: {width: 30, height: 30},
  cancel_draft_gray: {width: 30, height: 30},
  iconClear: {width: 30, height: 30},
  save_draft_icon_gray: {width: 30, height: 30},
});

export default ActionModel;
