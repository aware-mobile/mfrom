import React, {Component} from 'react';

import {
  StyleSheet,
  TextInput,
  View,
  Image,
  Button,
  TouchableHighlight,
  Platform,
  Dimensions
} from 'react-native';

import DFLabel from '../FormComponents/DFLabel'
import DFButton from '../FormComponents/DFButton'
import DFService from '../Service/DFService';
import apiConfig from '../Util/Config';
import DFErrorLabel from '../FormComponents/DFErrorLabel'
var Ionicons = require('react-native-vector-icons/Ionicons');
var {height, width} = Dimensions.get('window');
import AppText from '../Util/AppText'

class SuccessModel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      header: this.props.header,
      msg: this.props.msg,
      btn: this.props.btn,
    };
  }

  render() {
    return (<View style={styles.container}>
      <View style={styles.innerContainer}>
        <View style={styles.headerModal}>
          <AppText style={styles.textHeaderModal}>
            {this.state.header}
          </AppText>
          <TouchableHighlight
            underlayColor='transparent'
            style={{marginRight: 15}}
            onPress={() => this.onPressed(null) }>
            <Ionicons name="md-close" size={30} color="black"
                      style={{justifyContent: 'center', alignItems: 'center'}}/>
          </TouchableHighlight>
        </View>
        <View>
          <AppText style={styles.textContainer}>
            {this.state.msg}
          </AppText>

          <DFButton
            text={this.state.btn}
            onPress={() => this.onPressed(1)}
            style={{backgroundColor: "#B2D234", borderColor: "#B2D234"}}
            underlayColor='#B2D234'/>
        </View>
      </View>
    </View>)
  }

  onPressed(email) {
    this.props.successAction(email);
  }
}

const styles = StyleSheet.create({
  textContainer: {
    marginTop: 20,
    marginBottom: 20,
    paddingLeft: 20,
    paddingRight: 20,
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent: 'center',
    padding: 30
  },
  headerModal: {
    height: 60,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#efeff1'
  },
  textHeaderModal: {
    marginLeft: 20,
    fontWeight: 'bold',
    fontSize: 18,
  },
  input: {
    height: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#333435',
    paddingTop: 2,
    paddingBottom: 2
  },
  innerContainer: {
    backgroundColor: '#ffffff'
  },
  rowContainer: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center'
  },
  row: {
    alignItems: 'center',
    flexGrow: 1,
    flexDirection: 'row',
    marginBottom: 20,
  },
  rowTitle: {
    flexGrow: 1,
    fontWeight: 'bold',
  },
  button: {
    borderRadius: 5,
    flexGrow: 1,
    height: 44,
    alignSelf: 'stretch',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  buttonText: {
    fontSize: 18,
    margin: 5,
    textAlign: 'center',
  },
  modalButton: {
    marginTop: 10,
  },
  textArea: {
    height: 110,
    padding: 4,
    flexGrow: 4,
    fontSize: 14,
    color: '#49621c',
    backgroundColor: '#ffffff'
  },
  borderView: {
    marginRight: 10,
    marginLeft: 10,
    marginBottom: 10,
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 8,
  },
  img: {
    width: 30,
    height: 30
  },
});

export default SuccessModel;
