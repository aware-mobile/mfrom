import React, {Component} from 'react';

import {
  StyleSheet,
  TextInput,
  View,
  Image,
  Button,
  TouchableHighlight,
  Dimensions,
  Keyboard,
  LayoutAnimation
} from 'react-native';

import DFLabel from '../FormComponents/DFLabel'
import DFButton from '../FormComponents/DFButton'
import DFService from '../Service/DFService';
import apiConfig from '../Util/Config';
import AppText from '../Util/AppText'
import dismissKeyboard from 'react-native-dismiss-keyboard';

var Ionicons = require('react-native-vector-icons/Ionicons');
var {height, width} = Dimensions.get('window');

import STRINGS from '../Util/strings'

var self;

class CancelModel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headerTitle: this.props.headerTitle,
      isReject: this.props.isReject,
      msgError: "",
      isLoading: false,
      isSend: false,

      keyboardHeight: 0
    };
    this.reason = '';
    this.isClick = false;
    self = this
  }

  _keyboardDidShow = (e) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    self.setState({
      keyboardHeight: e.endCoordinates.height
    })
  }

  _keyboardDidHide = (e) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    self.setState({
      keyboardHeight: 0
    })
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow)
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide)
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  render() {
    var styleBtn = {};
    if (this.state.isSend) {
      styleBtn = {
        height: 36,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 4, backgroundColor: "#B2D234", borderColor: "#B2D234"
      }
    } else {
      styleBtn = {
        height: 36,
        backgroundColor: "#90A3AE", borderColor: "#90A3AE", justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 4,
      }
    }

    var modal =
      <View style={styles.container}>
        <View style={styles.innerContainer}>
          <View style={{padding: 15}}>
            <AppText style={{marginTop: 5, marginBottom: 10, fontSize: 16}}>{this.state.headerTitle}</AppText>
            <View style={styles.borderView}>
              <TextInput
                multiline={true}
                style={styles.textArea}
                maxLength={100}
                underlineColorAndroid='rgba(0,0,0,0)'
                onChangeText={(text) => {
                  this.insertData(text)
                }}
              />
            </View>
            <View style={{padding: 5, flexDirection: 'row', alignSelf: "flex-end"}}>
              <TouchableHighlight
                onPress={this.props.closeDialog}
                underlayColor='#dddddd'
                style={styles.cancelBtn}>
                <View>
                  <AppText style={{color: '#000000'}}>
                    {STRINGS.POPUP_MSG.BTN_CANCEL}
                  </AppText>
                </View>
              </TouchableHighlight>
              <TouchableHighlight
                onPress={this.saveForm.bind(this)}
                underlayColor='#dddddd'
                style={(this.state.isSend) ? styles.okBtn : styles.inactiveOkBtn}>
                <View>
                  <AppText style={{color: '#ffffff'}}>
                    {STRINGS.POPUP_MSG.BTN_OK}
                  </AppText>
                </View>
              </TouchableHighlight>
            </View>
          </View>

        </View>
        <View style={{height: this.state.keyboardHeight}}/>
      </View>;

    return (modal)
  }

  insertData(data) {

    this.reason = data;
    if (this.reason.trim() != '') {
      this.setState({isSend: true})
    } else {
      this.setState({isSend: false})
    }
  }

  cancelDraftForm() {
    let self = this

    console.log('Prop : ' + JSON.stringify(this.props));
    var config = JSON.parse(JSON.stringify(apiConfig.cancelDraftForm));
    config.url += this.props.TranID;
    var apiData = {};

    DFService.callApiWithHeader(apiData, config, function (error, response) {
      console.log('cancelDraftForm res: ', response)
      if (response) {
        if (response.resCode == 'DF2000000') {
          // Successfull Response
          self.success(response);
        } else if (response.resCode == 'DF5000038' || response.resCode == 'DF5000022') { // Concurrent Action Occured
          self.isClick = false;
          let callback = self.props.backPage();
          self.props.openMainModal(response.resMessage, callback)
        } else {
          self.isClick = false;
          // Failed Response
          self.props.openMainModal(response.resMessage)
        }
      } else {
        // Error from application calling service
        self.isClick = false;
        console.log("error = ", error);
      }

    });

  }

  saveForm() {
    if (this.isClick){
      return;
    }
    this.isClick = true;

    var self = this;
    if (this.reason.trim() == "") {
      this.setState({msgError: STRINGS.VALIDATION_MESSAGES.ENTER_REASON})
    } else {
      this.setState({isLoading: true});
      if (this.state.isReject) {
        var rejectBody = {
          "form_job_id": this.props.formJobId,
          "reject_msg": this.reason,
          "current_step": this.props.step
        };
        DFService.callApiWithHeader(rejectBody, apiConfig.rejectForm, function (error, response) {
          console.log('save form res: ', response)

          if (response) {
            if (response.resCode === 'DF2000000') {
              // Successfull Response
              console.log("before cancelDraftForm");
              self.cancelDraftForm();
              console.log("after cancelDraftForm");
            } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
              self.isClick = false;
              let callback = self.props.backPage();
              self.props.openMainModal(response.resMessage, callback)
            } else {
              self.isClick = false;
              // Failed Response
              self.props.openMainModal(response.resMessage)
            }
          } else {
            // Error from application calling service
            self.isClick = false;
            self.props.errorCancelReject(rejectBody, 'reject');
            console.log("error = ", error);
          }

        });
      } else {
        var cancelBody = {
          "form_job_id": this.props.formJobId,
          "cancel_msg": this.reason,
          "current_step": this.props.step
        };
        DFService.callApiWithHeader(cancelBody, apiConfig.cancelForm, function (error, response) {
          console.log('saveForm res: ', response)
          this.isClick = false;
          if (response) {
            if (response.resCode == 'DF2000000') {
              // Successfull Response
              self.cancelDraftForm();
            } else if (response.resCode == 'DF5000038' || response.resCode == 'DF5000022') { // Concurrent Action Occured
              let callback = self.props.backPage();
              self.props.openMainModal(response.resMessage, callback)
            } else {
              // Failed Response
              self.props.openMainModal(response.resMessage)
            }
          } else {
            // Error from application calling service
            self.props.errorCancelReject(cancelBody, 'cancel');
            console.log("error = ", error);
          }

        });
      }
    }
  }

  success(response) {
    console.log("success = ", response);
    if (response.resCode == 'DF2000000') {
      this.props.backPage();
    } else {
      this.setState({msgError: response.resMessage});
    }
  }
}

var styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent: 'center',
    padding: 30
  },
  headerModal: {
    height: 60,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#efeff1'
  },
  textHeaderModal: {
    marginLeft: 20,
    fontWeight: 'bold',
    fontSize: 18,
  },
  innerContainer: {
    backgroundColor: '#ffffff',
    borderRadius: 8
  },
  row: {
    alignItems: 'center',
    flexGrow: 1,
    flexDirection: 'row',
    marginBottom: 20,
  },
  rowTitle: {
    flexGrow: 1,
    fontWeight: 'bold',
  },
  button: {
    borderRadius: 5,
    flexGrow: 1,
    height: 44,
    alignSelf: 'stretch',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  buttonText: {
    fontSize: 18,
    margin: 5,
    textAlign: 'center',
  },
  modalButton: {
    marginTop: 10,
  },
  textArea: {
    height: 90,
    padding: 4,
    flexGrow: 4,
    fontSize: 20,
    borderWidth: 0,
    borderRadius: 8,
    color: '#49621c',
    textAlignVertical: 'top',
    backgroundColor: '#ffffff'
  },
  borderView: {
    marginRight: 5,
    marginLeft: 5,
    marginBottom: 10,
    padding: 5,
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 8,
  },
  okBtn: {
    width: 80,
    height: 36,
    backgroundColor: '#B2D234',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#B2D234',
    borderWidth: 1,
    borderRadius: 4,
  },
  inactiveOkBtn: {
    width: 80,
    height: 36,
    backgroundColor: '#90A3AE',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#90A3AE',
    borderWidth: 1,
    borderRadius: 4,
  },
  cancelBtn: {
    width: 80,
    height: 36,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#000000',
    borderWidth: 1,
    borderRadius: 4,
    marginRight: 10,
  },
});

export default CancelModel;
