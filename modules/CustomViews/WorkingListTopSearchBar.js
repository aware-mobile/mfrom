import React, {Component} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Image,
  TouchableHighlight,
  Dimensions,
  LayoutAnimation
} from 'react-native';

import Menu, {
  MenuContext,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from 'react-native-popup-menu';

import STRINGS from '../Util/strings'
import dismissKeyboard from 'react-native-dismiss-keyboard';
var Ionicons = require('react-native-vector-icons/Ionicons');
import AppText from '../Util/AppText'

var {height, width} = Dimensions.get('window');

class WorkingListTopSearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showSearchBar: false
    };
    this.keyboardText = ''
  }

  render() {

    let options = [];
    if (this.props.arrFilter) {
      for (let i = 0; i < this.props.arrFilter.length; i++) {
        options.push(<MenuOption
            style={this.props.arrFilter[i].active ? {backgroundColor: '#B2D234'} : {backgroundColor: 'white'}}
            key={'optionIndex' + i} value={this.props.arrFilter[i]}>
            <AppText
              style={this.props.arrFilter[i].active ? {color: 'white'} : {color: 'black'}}>
              {this.props.arrFilter[i].name}
            </AppText>
          </MenuOption>)
      }
    } else {
      options = null
    }

    let renderLastUpdate = [];
    if (this.props.pageName === "contact") {
      renderLastUpdate = null
    } else {
      renderLastUpdate =
        <View style={{flexDirection: 'row'}}>
          <AppText style={{fontSize: 12, color: '#EF775B', marginLeft: 8}}>{STRINGS.COMMON.LAST_UPDATED}</AppText>
          <AppText style={{fontSize: 12, color: 'black', marginLeft: 8}}>{this.props.lastUpdate}</AppText>
        </View>
    }

    let view = (this.state.showSearchBar) ?
      <View style={styles.search}>
        <View style={[styles.borderView, {flexGrow: 0.95}]}>
          <TextInput
            style={styles.input}
            returnKeyType={"search"}
            keyboardType={'default'}
            placeholder={STRINGS.COMMON.SEARCH}
            onChangeText={(text) => {
              this.keyboardText = text
            }}
            autoCorrect={false}
            multiline={false}
            onSubmitEditing={() => {
              this._search()
            }}
            underlineColorAndroid='rgba(0,0,0,0)'/>
        </View>

        <TouchableHighlight
          underlayColor='transparent'
          style={[styles.navBarBtn, {flexGrow: 0.1}]}
          onPress={() => this._hideSearchBar()}>

          <Ionicons name="md-close" size={25} color="#7F7F7F"
                    style={{justifyContent: 'center', alignItems: 'center'}}/>
        </TouchableHighlight>
        <TouchableHighlight
          underlayColor='transparent'
          style={[styles.navBarBtn, {flexGrow: 0.1}]}
          onPress={() => this._search()}>

          <Ionicons name="md-search" size={25} color="#7F7F7F"
                    style={{justifyContent: 'center', alignItems: 'center'}}/>
        </TouchableHighlight>
      </View>
      :
      <View style={styles.search}>
        <View style={styles.rowSearch}>
          {renderLastUpdate}
        </View>
        <View style={styles.rowContain}>
          <TouchableHighlight
            underlayColor='transparent'
            style={styles.navBarBtn}
            onPress={() => this._showSearchBar()}>

            <Image source={require('../img/iconSearch.png')} style={styles.navBtnImage}/>
          </TouchableHighlight>
          { (this.props.arrFilter) ?
            <View style={styles.navBarBtn}>
              <Menu onSelect={value => this.props.selectFilter(value)}>
                <MenuTrigger >
                  <Image source={require('../img/iconList.png')} style={styles.navBtnImage}/>
                </MenuTrigger>
                <MenuOptions style={{borderWidth: 1, borderColor: 'grey'}} optionsContainerStyle={{marginTop: 25}}>
                  {options}
                </MenuOptions>
              </Menu>
            </View> :
            <TouchableHighlight
              underlayColor='transparent'
              onPress={() => this.props.openFilter()}
              style={styles.navBarBtn}>
              <Image source={require('../img/iconList.png')} style={styles.navBtnImage}/>
            </TouchableHighlight>
          }
        </View>
      </View >;

    return (
      <View style={{flexDirection: 'row'}}>
        {view}
      </View>
    )

  }

  pressFilter(objFilter) {
    this.props.selectFilter(objFilter.value)
  }

  _search() {
    dismissKeyboard();
    this.props.searchTransactions(this.keyboardText);
  }

  _showSearchBar() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({showSearchBar: true})
  }

  _hideSearchBar() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({showSearchBar: false});
    this.keyboardText = '';
    this.props.searchTransactions('');
  }

}
var styles = StyleSheet.create({
  input: {
    // flexGrow: 1,
    // height: 30,
    // fontSize: 12,
    // paddingTop: 2,
    // paddingBottom: 2,
    // color: '#333435',
    height: 30,
    fontSize: 20,
    borderWidth: 0,
    color: '#333435',
    paddingTop: 2,
    paddingBottom: 2
  },
  borderView: {
    flexGrow: 1,
    marginRight: 2,
    marginLeft: 8,
    marginBottom: 4,
    marginTop: 10,
    padding: 1,
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 5,
  },
  search: {
    flexGrow: 1,
    height: 48,
    flexDirection: 'row',
    backgroundColor: '#F5F4F7',
    justifyContent: 'space-between',
  },
  rowSearch: {
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  rowContain: {
    flexDirection: 'row'
  },
  navBarBtn: {
    height: 35,
    marginTop: 10,
    marginRight: 15,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  navBtnImage: {
    width: 25,
    height: 25
  },
});

export default WorkingListTopSearchBar;
