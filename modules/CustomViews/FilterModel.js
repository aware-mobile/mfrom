import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Button,
  TouchableHighlight,
  Platform,
  Modal,
  Dimensions,
  ListView,
  Text
} from 'react-native';

let Ionicons = require('react-native-vector-icons/Ionicons');
let {height, width} = Dimensions.get('window');

import STRINGS from '../Util/strings';
import AppText from '../Util/AppText'

let dataSource = new ListView.DataSource({
  rowHasChanged: (r1, r2) => r1 !== r2,
  sectionHeaderHasChanged: (s1, s2) => s1 !== s2
});

class FilterModel extends Component {
  constructor(props) {
    super(props);
    //console.log(this.props.arrFilter);
    this.filter = JSON.parse(JSON.stringify(this.props.arrFilter));

    this.state = {
      dataSource: dataSource.cloneWithRowsAndSections(this.filter),
    };
  }

  render() {

    let headerTitle = STRINGS.COMMON.FILTER_TITLE;
    let iconBack = <View style={{paddingLeft: 13}}/>;
    let modal =
      <View style={styles.container}>
        <View style={styles.innerContainer}>
          <View style={styles.headerModal}>
            {iconBack}

            <AppText style={styles.textHeaderModal}>
              {headerTitle}
            </AppText>

            <TouchableHighlight style={{flexGrow: 1}}
                                underlayColor='transparent'
                                onPress={this.props.closeFilter.bind(this, this.filter)}>
              <Ionicons name="ios-close" size={30} color="black"
                        style={{justifyContent: 'center', alignItems: 'center'}}/>
            </TouchableHighlight>
          </View>

          <ListView
            style={{padding: 5}}
            dataSource={this.state.dataSource}
            renderRow={this.renderRow.bind(this)}
            renderSectionHeader={this.renderSectionHeader.bind(this)}
            enableEmptySections={true}/>
        </View>
      </View>;

    return (modal)
  }

  renderRow(rowData, sectionID, rowID) {
    let icon = (rowData.active) ?
      <View style={[styles.boxBorder]}>
        <Ionicons style={styles.icon} name="md-checkmark" size={15}/>
      </View>
      : <View style={[styles.boxBorder]}>
      </View>;
    return (
      <View style={styles.box}>
        <TouchableHighlight
          onPress={this.rowPressed.bind(this, rowData, sectionID, rowID)}
          underlayColor='#dddddd'
          style={[{backgroundColor: '#ffffff'}]}>

          <View style={styles.rowContainer}>
            {icon}
            <Text numberOfLines={1}
                  style={{
                    flexGrow: 0.5,
                    marginLeft: 5,
                    fontSize: 20,
                    color: '#B2D234',
                    fontFamily: 'DB Heavent',
                  }}>{this.getStatus(rowData.name)}</Text>
          </View>
        </TouchableHighlight>
      </View>
    );
  }

  renderSectionHeader(sectionData, category) {
    let headerText = this.getTranslatedHeader(category);
    return (
      <AppText style={{fontWeight: "700", padding: 4}}>{headerText}</AppText>
    )
  }

  getTranslatedHeader(txt) {
    if (txt === "All") {
      return STRINGS.COMMON.FILTER_ALL
    } else if (txt === "By Status") {
      return STRINGS.COMMON.FILTER_BY_STATUS
    } else if (txt === "By Category") {
      return STRINGS.COMMON.FILTER_BY_CATEGORY
    } else {
      return txt;
    }
  }

  rowPressed(formData, sectionID, rowID) {
    console.log(sectionID);
    if (sectionID !== 'All') {
      this.filter['All'][0].active = false;
      this.filter[sectionID][rowID].active = !this.filter[sectionID][rowID].active;
      this.props.saveFilter(this.filter[sectionID][rowID].name, this.filter[sectionID][rowID].active);

      this.filter['All'][0].active = this.findFilter(this.filter);

    } else {
      this.filter['All'][0].active = true;
      this.clearFilter(this.filter);
      this.props.saveFilter(this.filter[sectionID][rowID].name, null);
    }

    this.setState({
      dataSource: dataSource.cloneWithRowsAndSections(this.filter),
    });
  }

  findFilter(filter) {
    let list = filter['By Status'];
    if (list) {
      for (let i = 0; i < list.length; i++) {
        let item = list[i];
        if (item.active) {
          return false;
        }
      }
    }
    list = filter['By Category'];
    if (list) {
      for (let i = 0; i < list.length; i++) {
        let item = list[i];
        if (item.active) {
          return false;
        }
      }
    }
    return true;
  }

  clearFilter(filter) {
    let list = filter['By Status'];
    if (list) {
      for (let i = 0; i < list.length; i++) {
        list[i].active = false;
      }
    }
    list = filter['By Category'];
    if (list) {
      for (let i = 0; i < list.length; i++) {
        list[i].active = false;
      }
    }
  }

  getStatus(status) {
    if (status === "NEW") {
      return 'New';
    } else if (status === "REJECTED") {
      return 'Reject';
    } else if (status === "INPROGRESS") {
      return 'Inprogress';
    } else if (status === "EXPIRED") {
      return 'Expire';
    } else if (status === "COMPLETED") {
      return 'Complete';
    } else if (status === "CANCELED") {
      return 'Cancel';
    } else {
      return status;
    }
  }
}

var styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent: 'center',
    padding: 20,
    zIndex: 99
  },
  headerModal: {
    height: 60,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#efeff1'
  },
  textHeaderModal: {
    fontWeight: 'bold',
    fontSize: 18,
    flexGrow: 8
  },
  innerContainer: {
    height: height - 100,
    width: width - 40,
    // alignItems: 'center',
    backgroundColor: '#ffffff'
  },
  rowContainer: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center'
  },
  rowContain: {
    flexDirection: 'row'
  },
  row: {
    alignItems: 'center',
    flexGrow: 1,
    flexDirection: 'row',
    marginBottom: 20,
  },
  rowTitle: {
    flexGrow: 1,
    fontWeight: 'bold',
  },
  button: {
    borderRadius: 5,
    flexGrow: 1,
    height: 44,
    alignSelf: 'stretch',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  buttonText: {
    fontSize: 18,
    margin: 5,
    textAlign: 'center',
  },
  modalButton: {
    marginTop: 10,
  },
  box: {
    paddingBottom: 1,
  },
  boxBorder: {
    borderWidth: 1,
    width: 17,
    height: 17,
    alignSelf: 'center'
  },
  icon: {
    alignSelf: 'center',
    backgroundColor: 'transparent'
  },
  label: {
    marginTop: 5,
    marginLeft: 10,
    marginRight: 2,
    fontSize: 14
  }
});

export default FilterModel;
