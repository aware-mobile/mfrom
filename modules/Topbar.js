import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableHighlight,
  ActivityIndicator,
  AsyncStorage,
  Platform,
  NativeModules,
  PermissionsAndroid
} from 'react-native';

import DFDownloadPDF from './FormComponents/DFDownloadPDF'
import DFService from './Service/DFService';
import apiConfig from './Util/Config';
import AppText from './Util/AppText';

let Ionicons = require('react-native-vector-icons/Ionicons');
let Icon = require('react-native-vector-icons/FontAwesome');

class Topbar extends Component {
  constructor(props) {
    super(props);

    this.isClear = "onClear" in this.props;
    this.isGPS = "searchGPS" in this.props;
    this.isCustomerContact = this.props.isCustomerContact ? this.props.isCustomerContact : false;

    this.isGenPDF = false;
    if ("formPage" in this.props) {
      if ("formData" in this.props) {
        this.isGenPDF = false;
        if(this.props.formData.form_job_id.form_data_id !== undefined && this.props.formData.form_job_id.current_step !== 1){
          this.isGenPDF = true;
        }

        if(this.props.formData.status === 'COMPLETED'){
          this.isGenPDF = true;
        }

        if(this.props.formData.form_job_id.current_step === 1 && this.props.formData.status === 'REJECTED'){
          this.isGenPDF = true;
        }
      }
    }

    this.state = {
      isLoading: false,
      fileForGenerate: '',
      isCustomerContact: this.isCustomerContact
    };

  }

  changeStateCustomerContact() {
    this.setState({
      isCustomerContact: true
    })
  }

  changeStateUserContact() {
    this.setState({
      isCustomerContact: false
    })
  }

  render() {

    var clearNotiButton = this.isClear ?
      <TouchableHighlight
        underlayColor='transparent'
        style={styles.navBarBtn}
        onPress={ this.onClear.bind(this)}>
        <Ionicons name="md-trash" size={35} color="white"/>
      </TouchableHighlight> : null;

    var genPDFButton = this.isGenPDF ?
      <TouchableHighlight
        underlayColor='transparent'
        style={styles.navBarBtn}
        onPress={() => this.checkPermissionFile()}>
        <Ionicons name="md-download" size={35} color="white"/>

      </TouchableHighlight> : null;

    var searchGPSButton = this.isGPS ?
      <TouchableHighlight
        underlayColor='transparent'
        style={styles.navBarBtn}
        onPress={this.onGPSSearch.bind(this)}>
        <Ionicons name="ios-search" size={35} color="white"/>
      </TouchableHighlight> : null;

    var addCustomerContactButton = this.state.isCustomerContact ?
      <TouchableHighlight
        underlayColor='transparent'
        style={styles.navBarBtn}
        onPress={this.props.customerContact.bind(this)}>
        <Ionicons name="ios-person-add-outline" size={30} color="white"/>
      </TouchableHighlight> : null;

    var topBarButtons = (this.isClear || this.isGPS || this.isGenPDF || this.state.isCustomerContact) ?
      <View style={styles.rowContain}>
        {clearNotiButton}
        {searchGPSButton}
        {genPDFButton}
        {addCustomerContactButton}
      </View> : <View style={styles.navBarBtn}/>;

    return (
      <View style={this.isStyleIOS()}>
        <View style={styles.rowContain}>
          <TouchableHighlight
            underlayColor='transparent'
            style={styles.navBarBtn}
            onPress={this.onBack.bind(this)}>
            <Ionicons name="ios-arrow-back" size={30} color="white"
                      style={{justifyContent: 'center', alignItems: 'center'}}/>
          </TouchableHighlight>

        </View>
        <AppText numberOfLines={1} style={styles.title}>{this.props.title}</AppText>
        {topBarButtons}

      </View>)
  }

  onBack() {
    let popToTop = false;
    let customBackFunction = false;

    if ("refreshWorkingList" in this.props) {
      // console.log("getCurrentRoutes", this.props.navigator.getCurrentRoutes().length);
      // console.log("getCurrentRoutes 2 : ", this.props.navigator.getCurrentRoutes())

      if (this.props.navigator.getCurrentRoutes().length <= 2) {
        this.props.refreshWorkingList();
      }

      popToTop = true;
      customBackFunction = true
    }

    if ("refreshCustomer" in this.props) {
      // console.log("getCurrentRoutes", this.props.navigator.getCurrentRoutes().length);
      // console.log("getCurrentRoutes 2 : ", this.props.navigator.getCurrentRoutes())

      if (this.props.navigator.getCurrentRoutes().length <= 3) {
        this.props.refreshCustomer(0);
      }
    }

    if ("refreshUser" in this.props) {
      if (this.props.navigator.getCurrentRoutes().length <= 3) {
        this.props.refreshUser(0);
      }
    }

    if ("draft_form_id" in this.props) {
      this.props.refreshWorkingList();

      popToTop = true;
      customBackFunction = true
    }

    if ("refreshNoti" in this.props) {
      this.props.refreshNoti();
    }

    if ("clearAll" in this.props) {
      this.props.clearAll();
      customBackFunction = false
    }

    if ('fromNotification' in this.props) {
      customBackFunction = false
    }

    if ('fromLogin' in this.props) {
      customBackFunction = true
    }

    if ('fromProfile' in this.props) {
      this.props.navigator.refs['dash'].refreshList();
    }

    if ('fromChangePass' in this.props) {
      const routes = this.props.navigator.getCurrentRoutes();
      if (routes.length > 1) {
        let routeName = routes[routes.length - 2].name;
        this.props.navigator.refs[routeName]._addKeyBoardListeners();
      }
    }

    if (customBackFunction) {
      this.props.onBack(popToTop);
    } else {
      this.props.navigator.pop();
    }

  }

  onClear() {
    this.props.onClear();
  }

  checkPermissionFile(){
    let self = this;
    if (Platform.OS === 'ios') {
      self.onGenPDF()
    }else{
      PermissionsAndroid.checkPermission(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
        .then(function (granted) {
          if (granted) {
            self.onGenPDF()
          } else {
            PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
              .then(function (userGranted) {
                if (userGranted) {
                  self.onGenPDF()
                }
              });
          }
        });
    }
  }

  onGenPDF() {
    this.props.navigator.refs['submit_form'].generatePDF()
  }

  onGPSSearch() {
    this.props.searchGPS();
  }

  isStyleIOS() {
    if (Platform.OS === 'ios') {
      return {
        height: 60,
        backgroundColor: '#2d313c',
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 15
      }
    } else {
      return {
        height: 50,
        backgroundColor: '#2d313c',
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center'
      }
    }
  }

  showSpinner(show) {
    if (this.state.fileForGenerate !== '') {
      this.setState({
        isLoading: show,
        fileForGenerate: ''
      });
    } else {
      this.setState({
        isLoading: show,
      });
    }

  }
}

const styles = StyleSheet.create({
  navBtnImage: {
    width: 30,
    height: 30
  },
  nav: {
    height: 60,
    flexGrow: 1,
    backgroundColor: '#779e2f'
  },
  navBarBtn: {
    height: 50,
    width: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowContain: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 18,
    width: 200,
    color: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center'
  },
});

export default Topbar;
