import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Keyboard,
  Text,
  ScrollView,
  Image,
  Dimensions,
  TouchableHighlight
} from 'react-native';

import DFLabel from './FormComponents/DFLabel'
import DFErrorLabel from './FormComponents/DFErrorLabel'
import DFButton from './FormComponents/DFButton'
import DFAddress from './FormComponents/DFAddress'
import ImagePicker from 'react-native-image-picker';
import config from './Util/Config';
import DFService from './Service/DFService';
import ProgressSpinner from './Animation/ProgressSpinner'
import Util from './Util/Util';
import AppText from './Util/AppText';
import STRINGS from './Util/strings';

const Timer = require('react-native-timer');

let Ionicons = require('react-native-vector-icons/Ionicons');

export default class CreateCustomerContact extends Component {
  constructor(props) {
    super(props);
    var isEdit = 'isEdit' in props;
    console.log('props.data : ', this.props.data);
    this.state = {
      scrollViewAdjustment: 0,
      visibleHeight: Dimensions.get('window').height,
      txt_phone: '',
      phone: this.props.data && this.props.data.telephone ? this.props.data.telephone : [],
      imgPath: null,
      errorEmail: '',
      errorName: '',
      errorPhone: '',
      errorAdd: '',
      errorPostCode: '',
      isEdit: isEdit,
      nameValue: this.props.data && this.props.data.name ? this.props.data.name : '',
      emailValue: this.props.data && this.props.data.email ? this.props.data.email : '',
      postCodeValue: this.props.data && this.props.data.postal_code ? this.props.data.postal_code : '',
      addressValue: this.props.data && this.props.data.address ? this.props.data.address : '',
      propertiesAddress: {
        province: {
          name: STRINGS.USER_DATA.CITY,
          placeholder: STRINGS.COMMON.PLEASE_SELECT,
          hidden: false,
          value: this.props.data && this.props.data.province ? parseInt(this.props.data.province.province_code) : '',
        },
        district: {
          name: STRINGS.USER_DATA.DISTRICT,
          placeholder: STRINGS.COMMON.PLEASE_SELECT,
          hidden: false,
          value: this.props.data && this.props.data.district ? parseInt(this.props.data.district.amphur_code) : '',
        },
        subdistrict: {
          name: STRINGS.USER_DATA.SUB_DISTRICT,
          placeholder: STRINGS.COMMON.PLEASE_SELECT,
          hidden: false,
          value: this.props.data && this.props.data.sub_district ? parseInt(this.props.data.sub_district.district_code) : '',
        },
        address: {
          name: STRINGS.USER_DATA.ADDRESS,
          value: this.props.data && this.props.data.address ? this.props.data.address : "",
          placeholder: STRINGS.USER_DATA.ADDRESS
        }
      },
      doneSaving: false,
      saveSuccess: false,
      spinnerText: '',
      isLoading: false
    };
    let self = this;
    this.api_config = {}
    this.email = this.state.emailValue
    this.name = this.state.nameValue
    this.phone = []
    this.state.phone.forEach(function (current_value) {
      self.phone.push(current_value);
    });
    //this.phone.push(this.state.phone)
    this.postCode = this.state.postCodeValue
    this.address = this.state.addressValue
    this.province = this.state.propertiesAddress.province.value
    this.district = this.state.propertiesAddress.district.value
    this.sub_district = this.state.propertiesAddress.subdistrict.value
    console.log('this.props.navigator.getCurrentRoutes().length : ', this.props.navigator.getCurrentRoutes().length)
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  componentWillUnmount() {
    Timer.clearTimeout(this);
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = (e) => {
    let newSize = Dimensions.get('window').height - e.endCoordinates.height;
    let keyboardHeight = this.state.visibleHeight - newSize;
    this.setState({
      //visibleHeight: newSize,
      scrollViewAdjustment: keyboardHeight
    });
  };

  _keyboardDidHide = (e) => {
    this.setState({
      // visibleHeight: Dimensions.get('window').height,
      scrollViewAdjustment: 0
    });
  };

  render() {
    var mainSpinner = this.state.isLoading ?
      <ProgressSpinner
        setSuccess={this.state.doneSaving}
        isSuccessfull={this.state.saveSuccess}
        spinnerText={this.state.spinnerText}
      /> : <View/>;

    let phoneList = [];
    for (let i = 0; i < this.state.phone.length; i++) {
      phoneList.push(
        <View key={"phone_" + i} style={styles.dropdownBtn}>
          <View>
            <Text numberOfLines={1} style={styles.dropdownBtnTxt}>{this.state.phone[i]}</Text>
          </View>
          <View>
            <TouchableHighlight
              underlayColor='transparent'
              onPress={this.removeTel.bind(this, this.phone[i])}>
              <Ionicons name="md-close" size={20} color="black"
                        style={{justifyContent: 'center', alignItems: 'center'}}/>
            </TouchableHighlight>
          </View>
        </View>)
    }
    let formView =
      <View style={{marginBottom: 20}}>
        <DFLabel key={"name"} text={STRINGS.USER_DATA.NAME_CUSTOMER} color="black" isRequired={true}/>
        <View style={styles.borderView}>
          <TextInput
            style={styles.input}
            placeholder={STRINGS.USER_DATA.NAME_CUSTOMER}
            maxLength={100}
            editable={true}
            underlineColorAndroid='rgba(0,0,0,0)'
            value={this.state.nameValue}
            onChangeText={(text) => {
              this.name = text;
              this.setState({
                errorName: '',
                nameValue: text
              });
            }}
          />
        </View>
        <DFErrorLabel
          key={"name-required"}
          isShow={true}
          text={this.state.errorName}/>
        <DFLabel key={"email"} text={STRINGS.USER_DATA.EMAIL} color="black" isRequired={true}/>
        <View style={styles.borderView}>
          <TextInput
            keyboardType="email-address"
            style={styles.input}
            placeholder={STRINGS.USER_DATA.EMAIL}
            maxLength={100}
            editable={true}
            value={this.state.emailValue}
            underlineColorAndroid='rgba(0,0,0,0)'
            onChangeText={(text) => {
              this.email = text;
              this.setState({
                errorEmail: '',
                emailValue: text
              });
            }}
          />
        </View>
        <DFErrorLabel
          key={"email-required"}
          isShow={true}
          text={this.state.errorEmail}/>
        <DFLabel key={"phone"} text={STRINGS.USER_DATA.TELEPHONE} color="black" isRequired={false}/>
        <View style={{paddingLeft: 10, paddingBottom: 5}}>
          <View style={styles.listAssign}>
            {phoneList}
          </View>
        </View>
        <View style={{flexDirection: 'row'}}>
          <View style={[styles.borderViewTelPhone, {flexGrow: 1}]}>
            <TextInput
              key={"phoneInput"}
              style={styles.input}
              placeholder={STRINGS.USER_DATA.TELEPHONE}
              value={this.state.txt_phone}
              maxLength={50}
              editable={true}
              underlineColorAndroid='rgba(0,0,0,0)'
              onChangeText={(text) => {
                this.state.txt_phone = text;
                this.setState({
                  errorPhone: '',
                  errorAdd: ''
                });
              }}
              onSubmitEditing={this.addTel.bind(this)}
            />
          </View>
          <TouchableHighlight
            style={styles.button}
            underlayColor={'#B2D234'}
            onPress={this.addTel.bind(this)}>
            <Ionicons name="md-add" size={20} color="white"
                      style={{justifyContent: 'center', alignItems: 'center'}}/>
          </TouchableHighlight>
        </View>
        <DFErrorLabel
          key={"phone-required"}
          isShow={true}
          text={this.state.errorPhone}/>
        <DFErrorLabel
          key={"add-required"}
          isShow={true}
          text={this.state.errorAdd}/>
        <DFLabel key={"address"} text={STRINGS.USER_DATA.ADDRESS} color="black" isRequired={false}/>
        <DFAddress
          viewMode={false}
          properties={this.state.propertiesAddress}
          editable={true}
          maxLength={100}
          insertData={this.addAddressData.bind(this)}
        />

        <DFLabel key={"postCode"} text={STRINGS.USER_DATA.POSTAL_CODE} color="black" isRequired={false}/>
        <View style={styles.borderView}>
          <TextInput
            style={styles.input}
            placeholder={STRINGS.USER_DATA.POSTAL_CODE}
            maxLength={5}
            editable={true}
            keyboardType={'numeric'}
            value={this.state.postCodeValue}
            underlineColorAndroid='rgba(0,0,0,0)'
            onChangeText={(text) => {
              this.postCode = text;
              this.setState({
                errorPostCode: '',
                postCodeValue: text
              });
            }}
          />
        </View>
        <DFErrorLabel
          key={"postCode-error"}
          isShow={true}
          text={this.state.errorPostCode}/>

        {this.state.isEdit ?
          <View style={{
            flexDirection: 'row'
          }}>
            <DFButton
              text={STRINGS.POPUP_MSG.BTN_CANCEL}
              onPress={() => this.cancelEdit()}
              style={{backgroundColor: "#DC6565", borderColor: "#DC6565"}}/>
            <DFButton
              text={STRINGS.POPUP_MSG.BTN_SAVE}
              onPress={() => this.saveCustomer()}
              style={{backgroundColor: "#B2D234", borderColor: "#B2D234"}}
              underlayColor='#B2D234'/>
          </View> : <DFButton
            text={STRINGS.POPUP_MSG.BTN_CREATE}
            onPress={() => this.saveCustomer()}
            style={{backgroundColor: "#B2D234", borderColor: "#B2D234"}}
            underlayColor='#B2D234'/>
        }
      </View>;
    // var _scrollView: ScrollView;
    return (
      <View style={{flexGrow: 1}}>
        <ScrollView
          ref={(scrollView) => {
            _scrollView = scrollView;
          }}
          automaticallyAdjustContentInsets={false}
          keyboardShouldPersistTaps={true}
          onScroll={() => {
          }}
          scrollEventThrottle={200}>
          {formView}
          <View style={{height: this.state.scrollViewAdjustment}}>
          </View>
          <View style={{height: 40}}>
          </View>
        </ScrollView>
        {mainSpinner}
      </View>
    );
  }

  cancelEdit() {
    this.props.openMainConfirm(STRINGS.POPUP_MSG.EDIT_CUSTOMER_CANCEL, this.backToView)
  }

  backToView = () => {
    this.props.navigator.pop()
  };

  addAddressData(properties) {
    console.log('properties: ', properties);
    this.setState({
      showErr: true
    });

    this.province = properties.province.value;
    this.district = properties.district.value;
    this.sub_district = properties.subdistrict.value;
    this.address = properties.address.value
  }

  saveCustomer() {
    // console.log('this.province : ', this.province)
    // console.log('this.district : ', this.district)
    // console.log('this.sub_district : ', this.sub_district)
    // console.log('this.address : ', this.address)
    let self = this;
    if (this.validateInput()) {
      this.data = {
        name: this.name,
        email: this.email,
        postal_code: this.postCode,
        address: this.address,
        province: null,
        district: null,
        sub_district: null,
        codeProvince: parseInt(this.province),
        codeDistrict: parseInt(this.district),
        codeSubDistrict: parseInt(this.sub_district),
        telephone: this.phone
      };

      if (this.state.isEdit) {
        this.props.openMainConfirm(STRINGS.POPUP_MSG.EDIT_CUSTOMER, self.ws_sumbit)
      } else {
        this.props.openMainConfirm(STRINGS.POPUP_MSG.CREATE_CUSTOMER, self.ws_sumbit)
      }
      console.log('validate  true : ', this.data)
    } else {
      console.log('validate  false')
    }
  }

  ws_sumbit = () => {
    var api_config
    if (this.state.isEdit) {
      api_config = JSON.parse(JSON.stringify(config.editCustomer));
      api_config.url += this.props.data._id
    } else {
      api_config = JSON.parse(JSON.stringify(config.addCustomer));
    }
    var self = this
    this.setState({
      isLoading: true,
      doneSaving: false,
      spinnerText: STRINGS.COMMON.SAVING,
      saveSuccess: false,
    })
    DFService.callApiWithHeader(this.data, api_config, function (error, response) {
        console.log(response);
        if (response) {
          if (response.resCode === 'DF2000000') {
            Timer.setTimeout(self, 'success', () => {
              self.setState({
                doneSaving: true,
                saveSuccess: true,
                spinnerText: STRINGS.COMMON.SUCCESS,
              });
              self._goBack(1500)
            }, 2000);
          } else {
            // Failed Response
            self.setState({
              doneSaving: false,
              saveSuccess: false,
              spinnerText: STRINGS.COMMON.SAVING_FAILED,
              isLoading: false
            })
            self.props.openMainModal(response.resMessage)
          }
        } else {
          // Error from application calling service
          self.setState({
            doneSaving: false,
            saveSuccess: false,
            spinnerText: STRINGS.COMMON.SAVING_FAILED,
            isLoading: false
          })
          self.props.openMainModal(STRINGS.COMMON.CONNECTION_TO_SERVICE)
        }
      },
      (error) => {
        //   this.isLoading = false;
        self.props.openMainModal(error)
      })
  }

  _goBack(delay) {
    var self = this
    //noinspection JSAnnotator
    if (this.state.isEdit) {
      Timer.setTimeout(self, '_goBack', () => {
        // self.props.refreshCustomer();
        self.props.refreshCustomer(0);
        self.props.navigator.popN(2)
      }, delay)
    } else {
      Timer.setTimeout(self, '_goBack', () => {
        // self.props.refreshCustomer();
        self.props.refreshCustomer();
        self.props.navigator.pop()
      }, delay)
    }


  }

  isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  removeTel(number) {
    let foundIndex = -1;
    for (let i = 0; i < this.phone.length; i++) {
      if (this.phone[i] === number) {
        foundIndex = i;
        break
      }
    }
    if (foundIndex !== -1) {
      this.phone.splice(foundIndex, 1)
    }
    this.setState({phone: this.phone});
  }


  addTel() {
    if (this.state.txt_phone.length > 0) {
      if (this.validatePhone()) {
        if (this.phone.indexOf(this.state.txt_phone) !== -1) {
          this.setState({errorPhone: STRINGS.VALIDATION_MESSAGES.DUPLICATE_PHONE});
        } else {
          this.phone.push(this.state.txt_phone);
          this.setState({txt_phone: ''});
        }
      }
      this.setState({phone: this.phone});
    }
  }

  validatePhone() {
    //let phoneNo = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    let str = this.state.txt_phone;
    if (Util.isInternationalPhoneNoCorrect(str)) {
      this.setState({errorPhone: '', errorAdd: ''});
      return true;
    } else {
      this.setState({errorPhone: STRINGS.VALIDATION_MESSAGES.INVALID_PHONE});
      return false;
    }
  }

  validateInput() {
    if (this.email === '') {
      this.setState({errorEmail: STRINGS.VALIDATION_MESSAGES.REQUIRED});
    }
    if (this.name === '') {
      this.setState({errorName: STRINGS.VALIDATION_MESSAGES.REQUIRED});
    }

    //  if (this.postCode === '') {
    //     this.setState({ errorPostCode: "This field is number format." });
    // }

    // if (this.phone.length === 0) {
    //     this.setState({ errorPhone: "Please add at least one telephone number." });
    // }
    if (this.state.txt_phone.length !== 0) {
      this.setState({errorAdd: STRINGS.VALIDATION_MESSAGES.ADD_TELEPHONE});
    }
    if (this.surname === '') {
      this.setState({errorSurname: STRINGS.VALIDATION_MESSAGES.REQUIRED});
    }
    if (this.company === '') {
      this.setState({errorCompany: STRINGS.VALIDATION_MESSAGES.REQUIRED});
    }
    return !(this.email === '' || this.name === '' || !this.valideNumber(this.postCode) || !this.valideEmail(this.email) || this.state.txt_phone.length !== 0);
  }

  valideEmail(email) {
    if (email === '' || email === undefined) {
      return true;
    } else {
      let emailFilter = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
      if (!emailFilter) {
        this.setState({errorEmail: STRINGS.VALIDATION_MESSAGES.EMAIL_INCORRECT_FORMAT});
      }
      return emailFilter;
    }
  }

  valideNumber(postCode) {
    if (postCode === '' || postCode === undefined) {
      return true;
    } else {
      var number = postCode;
      var reg = /^[0-9]+$/;
      if (reg.test(number)) {
        return true
      } else {
        this.setState({errorPostCode: STRINGS.VALIDATION_MESSAGES.INT_FORMAT});
        return false
      }
    }
  }

  onBack() {
    this.props.userHasSignUp(false);
  }
}

const styles = StyleSheet.create({
  input: {
    height: 30,
    fontSize: 20,
    borderWidth: 0,
    color: '#333435',
    paddingTop: 2,
    paddingBottom: 2
  },
  inputPhone: {
    width: 150,
    height: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#333435',
    paddingTop: 2,
    paddingBottom: 2
  },
  borderView: {
    borderColor: '#95989A',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  borderViewTelPhone: {
    borderColor: '#95989A',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 7,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  borderViewDisable: {
    borderColor: '#c0c0c0',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  disableInput: {
    fontSize: 14,
    borderWidth: 0,
    color: '#cdcdcd',
    paddingTop: 5,
    paddingBottom: 5,
  },
  disableInputNoText: {
    height: 25,
    fontSize: 14,
    borderWidth: 0,
    color: '#cdcdcd',
    paddingTop: 5,
    paddingBottom: 5,
  },
  dropdownBtn: {
    height: 35,
    flexDirection: 'row',
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 2,
    padding: 5,
    borderRadius: 18,
  },
  dropdownBtnTxt: {
    fontFamily: 'DB Heavent',
    marginRight: 4,
    fontSize: 20,
    maxWidth: 140
  },
  viewMode: {
    minHeight: 25,
    fontSize: 14,
    borderWidth: 0,
    color: '#606060',
    textAlignVertical: 'center',
    paddingTop: 5,
    paddingBottom: 5,
  },
  circle: {
    width: 120,
    height: 120
  },
  listAssign: {
    flexGrow: 1,
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  button: {
    height: 30,
    width: 30,
    paddingTop: 2,
    paddingBottom: 2,
    marginRight: 15,
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    borderWidth: 1,
    borderRadius: 4,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonClose: {
    height: 30,
    width: 30,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center'
  },
});
