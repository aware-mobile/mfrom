'use strict';
import React, {Component} from 'react';
import {
  AsyncStorage,
  Platform
} from 'react-native';

import API_CONFIG from '../Util/Config';
import utils from '../Util/Util';
import STRINGS from '../Util/strings';

export default class DFService {

  static async callApiNoHeader(obj, apiConfig, callback) {
    try {
      let value = await AsyncStorage.getItem("Profile");
      let profile = await JSON.parse(value);
      let displayName = profile ? profile.user.displayName : '';

      let lang = STRINGS.getLanguage();
      lang = lang.toLowerCase();

      let response = null;
      if (apiConfig.method === "GET") {
        response = await fetch(apiConfig.url, {
          method: apiConfig.method,
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=UTF-8',
            'Accept-Language': 'th',
            'df-language': lang,
            'df-transaction-id-log': utils.transactionIdTokenGenerator(''),
            'df-action': apiConfig.action_name,
            'df-user-name-log': (apiConfig.action_name === 'Login' ? '' : encodeURIComponent(displayName)),
            'df-channel': Platform.OS === 'ios' ? 'MOBILE-IOS' : 'MOBILE-ANDROID'
          }
        });
      } else {
        response = await fetch(apiConfig.url, {
          method: apiConfig.method,
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=UTF-8',
            'Accept-Language': 'th',
            'df-language': lang,
            'df-transaction-id-log': utils.transactionIdTokenGenerator(''),
            'df-action': apiConfig.action_name,
            'df-user-name-log': (apiConfig.action_name === 'Login' ? '' : encodeURIComponent(displayName)),
            'df-channel': Platform.OS === 'ios' ? 'MOBILE-IOS' : 'MOBILE-ANDROID'
          },
          body: JSON.stringify(obj)
        });
      }
      let responseJson = await response.json();
      console.log('DFService callApiNoHeader Response: ', responseJson);
      callback(null, responseJson);
    } catch (error) {
      callback(error, null);
    }
  }

  static async callApiWithHeader(obj, apiConfig, callback) {
    try {

      let value = await AsyncStorage.getItem("Profile");
      let profile = await JSON.parse(value);

      let lang = STRINGS.getLanguage();
      lang = lang.toLowerCase();

      //console.log('user value ==> ', profile);
      if (value !== null) {
        //console.log("Profile Info = ", profile);
        let response = null;
        if (apiConfig.method === "GET") {
          response = await fetch(apiConfig.url, {
            method: apiConfig.method,
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json; charset=UTF-8',
              'Accept-Language': 'th',
              'df-language': lang,
              'df-access-token': profile ? profile.token : '',
              'df-user-id': profile.user.id,
              'df-company-id': profile.company.id,
              'df-transaction-id-log': utils.transactionIdTokenGenerator(profile.token),
              'df-action': apiConfig.action_name,
              'df-user-name-log': profile ? encodeURIComponent(profile.user.displayName) : '',
              'df-channel': Platform.OS === 'ios' ? 'MOBILE-IOS' : 'MOBILE-ANDROID'
            }
          });
        } else {
          try {
            response = await fetch(apiConfig.url, {
              method: apiConfig.method,
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept-Language': 'th',
                'df-language': lang,
                'df-access-token': profile.token,
                'df-user-id': profile.user.id,
                'df-company-id': profile.company.id,
                'df-transaction-id-log': utils.transactionIdTokenGenerator(profile.token),
                'df-action': apiConfig.action_name,
                'df-user-name-log': profile ? encodeURIComponent(profile.user.displayName) : '',
                'df-channel': Platform.OS === 'ios' ? 'MOBILE-IOS' : 'MOBILE-ANDROID'
              },
              body: JSON.stringify(obj)
            });
          } catch (error) {
            callback("JsonError", null);
          }
        }
        let responseJson = await response.json();
        console.log('DFService callApiWithHeader Response: ', responseJson);
        callback(null, responseJson);
      }
    } catch (error) {
      callback(error, null);
    }
  }

  static async callApiWithHeaderObj(obj, apiConfig, callback) {
    try {

      let value = await AsyncStorage.getItem("Profile");
      let profile = await JSON.parse(value);

      let lang = STRINGS.getLanguage();
      lang = lang.toLowerCase();

      console.log("df-access-token = " + profile.token);
      console.log("df-user-id = " + profile.user.id);
      console.log("df-company-id = " + profile.company.id);

      if (value !== null) {
        try {
          let response = null;
          if (apiConfig.method === "GET") {
            response = await fetch(apiConfig.url, {
              method: apiConfig.method,
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data; charset=UTF-8',
                'Accept-Language': 'th',
                'df-language': lang,
                'df-access-token': profile.token,
                'df-user-id': profile.user.id,
                'df-company-id': profile.company.id,
                'df-transaction-id-log': utils.transactionIdTokenGenerator(profile.token),
                'df-action': apiConfig.action_name,
                'df-user-name-log': profile ? encodeURIComponent(profile.user.displayName) : '',
                'df-channel': Platform.OS === 'ios' ? 'MOBILE-IOS' : 'MOBILE-ANDROID'
              }
            });
          } else {
            response = await fetch(apiConfig.url, {
              method: apiConfig.method,
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data; charset=UTF-8',
                'Accept-Language': 'th',
                'df-language': lang,
                'df-access-token': profile.token,
                'df-user-id': profile.user.id,
                'df-company-id': profile.company.id,
                'df-transaction-id-log': utils.transactionIdTokenGenerator(profile.token),
                'df-action': apiConfig.action_name,
                'df-user-name-log': profile ? encodeURIComponent(profile.user.displayName) : '',
                'df-channel': Platform.OS === 'ios' ? 'MOBILE-IOS' : 'MOBILE-ANDROID'
              },
              body: obj
            });
          }
          let responseJson = await response.json();
          console.log('DFService callApiWithHeaderObj Response: ', responseJson);
          callback(null, responseJson);
        } catch (error) {
          callback(error, null);
        }
      }
    } catch (error) {
      console.log(error)
    }
  }

  static async callApiWithoutHeaderObj(obj, apiConfig, callback) {
    try {

      let lang = STRINGS.getLanguage();
      lang = lang.toLowerCase();

      try {
        let response = null;
        response = await fetch(apiConfig.url, {
          method: apiConfig.method,
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'multipart/form-data; charset=UTF-8',
            'Accept-Language': 'th',
            'df-language': lang,
            'df-action': apiConfig.action_name,
            'df-channel': Platform.OS === 'ios' ? 'MOBILE-IOS' : 'MOBILE-ANDROID'
          },
          body: obj
        });
        let responseJson = await response.json();
        console.log('DFService callApiWithoutHeaderObj Response: ', responseJson);
        callback(null, responseJson);
      } catch (error) {
        callback(error, null);
      }
    } catch (error) {
      console.log(error)
    }
  }

  static async callApiWithProfileObj(profile, apiConfig, callback) {
    try {

      let response = null;

      let lang = STRINGS.getLanguage();
      lang = lang.toLowerCase();

      response = await fetch(apiConfig.url, {
        method: apiConfig.method,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json; charset=UTF-8',
          'df-language': lang,
          'df-access-token': profile.token,
          'df-user-id': profile.user.id,
          'df-company-id': profile.company.id,
          'df-transaction-id-log': utils.transactionIdTokenGenerator(profile.token),
          'df-action': apiConfig.action_name,
          'df-user-name-log': profile ? encodeURIComponent(profile.user.displayName) : '',
          'df-channel': Platform.OS === 'ios' ? 'MOBILE-IOS' : 'MOBILE-ANDROID'
        }
      });
      let responseJson = await response.json();
      console.log('DFService callApiWithProfileObj Response: ', responseJson);
      callback(null, responseJson);
    } catch (error) {
      callback(error, null);
    }
  }

  static async formListApi(profile, callback) {
    try {

      let lang = STRINGS.getLanguage();
      lang = lang.toLowerCase();

      let response = await fetch(url + '/dfapi/forms/', {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json; charset=UTF-8',
          'Accept-Language': 'th',
          'df-language': lang,
          'df-access-token': profile.accessToken,
          'df-user-id': profile.user._id,
          'df-company-id': profile.user.company_id,
          'df-role-id': profile.user.roles.join(','),
          'df-transaction-id-log': utils.transactionIdTokenGenerator(profile.accessToken),
          'df-action': 'Get form list',
          'df-user-name-log': profile ? encodeURIComponent(profile.user.displayName) : '',
          'df-channel': Platform.OS === 'ios' ? 'MOBILE-IOS' : 'MOBILE-ANDROID'
        }
      });
      let responseJson = await response.json();
      console.log('DFService formListApi Response: ', responseJson);
      callback(null, responseJson);
    } catch (error) {
      callback(error, null);
    }
  }

  static async formDetailApi(formId, callback) {
    try {

      let value = await AsyncStorage.getItem("Profile");
      let profile = await JSON.parse(value);

      let lang = STRINGS.getLanguage();
      lang = lang.toLowerCase();

      let response = await fetch(url + '/dfapi/forms/' + formId, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json; charset=UTF-8',
          'Accept-Language': 'th',
          'df-language': lang,
          'df-transaction-id-log': utils.transactionIdTokenGenerator(''),
          'df-action': 'Get form detail',
          'df-user-name-log': profile ? encodeURIComponent(profile.user.displayName) : '',
          'df-channel': Platform.OS === 'ios' ? 'MOBILE-IOS' : 'MOBILE-ANDROID'
        }
      });
      let responseJson = await response.json();
      console.log('DFService formDetailApi Response: ', responseJson);
      callback(null, responseJson);
    } catch (error) {
      callback(error, null);
    }
  }

  static async formSubmitApi(form, callback) {
    try {

      let value = await AsyncStorage.getItem("Profile");
      let profile = await JSON.parse(value);

      let lang = STRINGS.getLanguage();
      lang = lang.toLowerCase();

      let response = await fetch(url + '/dfapi/forms/' + form.template_id, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json; charset=UTF-8',
          'df-language': lang,
          'df-transaction-id-log': utils.transactionIdTokenGenerator(''),
          'df-action': 'Submit form',
          'df-user-name-log': profile ? encodeURIComponent(profile.user.displayName) : '',
          'df-channel': Platform.OS === 'ios' ? 'MOBILE-IOS' : 'MOBILE-ANDROID'
        },
        body: JSON.stringify(form)
      });
      let responseJson = await response.json();
      console.log('DFService formSubmitApi Response: ', responseJson);
      callback(null, responseJson);
    } catch (error) {
      callback(error, null);
    }
  }

  static async getNotificationCount(callback) {
    try {

      let value = await AsyncStorage.getItem("Profile");
      let profile = await JSON.parse(value);

      let lang = STRINGS.getLanguage();
      lang = lang.toLowerCase();

      if (value !== null) {
        //console.log("Profile Info = ", profile);
        try {
          let response = null;
          if (API_CONFIG.countNotifications.method === "GET") {
            response = await fetch(API_CONFIG.countNotifications.url, {
              method: API_CONFIG.countNotifications.method,
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept-Language': 'th',
                'df-language': lang,
                'df-access-token': profile.token,
                'df-user-id': profile.user.id,
                'df-company-id': profile.company.id,
                'df-transaction-id-log': utils.transactionIdTokenGenerator(profile.token),
                'df-action': API_CONFIG.countNotifications.action_name,
                'df-user-name-log': profile ? encodeURIComponent(profile.user.displayName) : '',
                'df-channel': Platform.OS === 'ios' ? 'MOBILE-IOS' : 'MOBILE-ANDROID'
              }
            });
          } else {
            response = await fetch(API_CONFIG.countNotifications.url, {
              method: API_CONFIG.countNotifications.method,
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept-Language': 'th',
                'df-language': lang,
                'df-access-token': profile.token,
                'df-user-id': profile.user.id,
                'df-company-id': profile.company.id,
                'df-transaction-id-log': utils.transactionIdTokenGenerator(profile.token),
                'df-action': API_CONFIG.countNotifications.action_name,
                'df-user-name-log': profile ? encodeURIComponent(profile.user.displayName) : '',
                'df-channel': Platform.OS === 'ios' ? 'MOBILE-IOS' : 'MOBILE-ANDROID'
              },
            });
          }
          let responseJson = await response.json();
          console.log('DFService getNotificationCount Response: ', responseJson);
          callback(null, responseJson);
        } catch (error) {
          callback(error, null);
        }
      }
    } catch (error) {
      console.log(error)
    }
  }

}
