import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  ScrollView,
  Keyboard,
  Image,
  Platform,
  Dimensions,
  TouchableHighlight,
  Linking
} from 'react-native';
const Timer = require('react-native-timer');
import DFTextBox from './FormComponents/DFTextBox'
import Topbar from './Topbar'
import DFLabel from './FormComponents/DFLabel'
import DFErrorLabel from './FormComponents/DFErrorLabel'
import DFButton from './FormComponents/DFButton'
import ImagePicker from 'react-native-image-picker';
import apiConfig from './Util/Config';
import DFService from './Service/DFService';
import SuccessModel from './CustomViews/SuccessModel';
import Util from './Util/Util';
import STRINGS from './Util/strings.js';
import AppText from './Util/AppText';

let Ionicons = require('react-native-vector-icons/Ionicons');

export default class SignUpPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollViewAdjustment: 0,
      visibleHeight: Dimensions.get('window').height,
      txt_phone: '',
      phone: [],
      company: '',
      email: '',
      name: '',
      surname: '',
      department: '',
      imgPath: null,
      errorEmail: '',
      errorName: '',
      errorSurname: '',
      errorPhone: '',
      errorAdd: '',
      errorCompany: '',
      showSuccess: false,
      successMsg: ''
    };

    this.email = "";
    this.name = "";
    this.surname = "";
    this.phone = [];
    this.company = "";
    this.department = "";
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow)
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide)
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = (e) => {
    let newSize = Dimensions.get('window').height - e.endCoordinates.height
    let keyboardHeight = this.state.visibleHeight - newSize
    this.setState({
      visibleHeight: newSize,
      scrollViewAdjustment: keyboardHeight
    })
  }

  _keyboardDidHide = (e) => {
    this.setState({
      visibleHeight: Dimensions.get('window').height,
      scrollViewAdjustment: 0
    })
  }

  render() {
    let phoneList = [];
    for (let i = 0; i < this.state.phone.length; i++) {
      phoneList.push(
        <View style={styles.dropdownBtn}>
          <View>
            <AppText numberOfLines={1} style={styles.dropdownBtnTxt}>{this.state.phone[i]}</AppText>
          </View>
          <View>
            <TouchableHighlight
              underlayColor='transparent'
              onPress={this.removeTel.bind(this, this.phone[i])}>
              <Ionicons name="md-close" size={20} color="black"
                        style={{justifyContent: 'center', alignItems: 'center'}}/>
            </TouchableHighlight>
          </View>
        </View>)
    }
    let imgPerson = (this.state.imgPath !== null) ?
      <Image resizeMode={Image.resizeMode.cover} source={this.state.imgPath} style={styles.circleImage}/> :
      <Image resizeMode={Image.resizeMode.cover} source={require('./img/img_person.png')} style={styles.circleImage}/>;
    let imgClose = (this.state.imgPath !== null) ?
      <TouchableHighlight
        onPress={this.removeImage.bind(this)}
        style={styles.buttonClose}>
        <Ionicons name="md-close" size={30} color="black"/>
      </TouchableHighlight> : null;
    let imgCamera = (this.state.imgPath === null) ?
      <Image resizeMode={Image.resizeMode.cover} source={require('./img/camera.png')} style={styles.camera}/> : null;
    let formView =
      <View>
        <View style={styles.image}>
          <TouchableHighlight
            style={{justifyContent: 'flex-end', alignItems: 'flex-end'}}
            underlayColor='transparent'
            onPress={this.pickImage.bind(this)}>
            <View style={styles.circle}>
              {imgPerson}
              {imgClose}
              {imgCamera}
            </View>
          </TouchableHighlight>
        </View>
        <DFLabel key={"email"} text={STRINGS.SIGNUP_PAGE.LBL_EMAIL} color="black" isRequired={true}/>
        <View style={styles.borderView}>
          <TextInput
            keyboardType="email-address"
            style={styles.input}
            placeholder=''
            value={this.state.email}
            maxLength={100}
            editable={true}
            underlineColorAndroid='rgba(0,0,0,0)'
            onChangeText={(text) => {
              this.email = text;
              this.state.email = text;
              this.setState({errorEmail: ''});
            }}
          />
        </View>
        <DFErrorLabel
          key={"email-required"}
          isShow={true}
          text={this.state.errorEmail}/>
        <DFLabel key={"name"} text={STRINGS.SIGNUP_PAGE.LBL_NAME} color="black" isRequired={true}/>
        <View style={styles.borderView}>
          <TextInput
            style={styles.input}
            placeholder=''
            value={this.state.name}
            maxLength={100}
            editable={true}
            underlineColorAndroid='rgba(0,0,0,0)'
            onChangeText={(text) => {
              this.name = text;
              this.state.name = text;
              this.setState({errorName: ''});
            }}
          />
        </View>
        <DFErrorLabel
          key={"name-required"}
          isShow={true}
          text={this.state.errorName}/>
        <DFLabel key={"surname"} text={STRINGS.SIGNUP_PAGE.LBL_SURNAME} color="black" isRequired={true}/>
        <View style={styles.borderView}>
          <TextInput
            style={styles.input}
            placeholder=''
            value={this.state.surname}
            maxLength={100}
            editable={true}
            underlineColorAndroid='rgba(0,0,0,0)'
            onChangeText={(text) => {
              this.surname = text;
              this.state.surname = text;
              this.setState({errorSurname: ''});
            }}
          />
        </View>
        <DFErrorLabel
          key={"surname-required"}
          isShow={true}
          text={this.state.errorSurname}/>
        <DFLabel key={"phone"} text={STRINGS.SIGNUP_PAGE.LBL_TELEPHONE} color="black" isRequired={false}/>
        <View style={{paddingLeft: 10, paddingBottom: 5}}>
          <View style={styles.listAssign}>
            {phoneList}
          </View>
        </View>
        <View style={{flexDirection: 'row'}}>
          <View style={[styles.borderView, {flexGrow: 1}]}>
            <TextInput
              // keyboardType="numeric"
              key={"phoneInput"}
              style={styles.input}
              placeholder=''
              value={this.state.txt_phone}
              maxLength={50}
              editable={true}
              underlineColorAndroid='rgba(0,0,0,0)'
              onChangeText={(text) => {
                this.state.txt_phone = text;
                this.setState({
                  errorPhone: '',
                  errorAdd: ''
                });
              }}
              onSubmitEditing={this.addTel.bind(this)}
            />
          </View>
          <TouchableHighlight
            style={styles.button}
            underlayColor={'#B2D234'}
            onPress={this.addTel.bind(this)}>
            <Ionicons name="md-add" size={20} color="white"
                      style={{justifyContent: 'center', alignItems: 'center'}}/>
          </TouchableHighlight>
        </View>
        <DFErrorLabel
          key={"phone-required"}
          isShow={true}
          text={this.state.errorPhone}/>
        <DFErrorLabel
          key={"add-required"}
          isShow={true}
          text={this.state.errorAdd}/>
        <DFLabel key={"company"} text={STRINGS.SIGNUP_PAGE.LBL_COMPANY_CODE} color="black" isRequired={true}/>
        <View style={styles.borderView}>
          <TextInput
            style={styles.input}
            placeholder=''
            value={this.state.company}
            maxLength={10}
            editable={true}
            underlineColorAndroid='rgba(0,0,0,0)'
            onChangeText={(text) => {
              this.company = text.trim().toUpperCase();
              this.setState({company: text.trim().toUpperCase()});
              this.setState({errorCompany: ''});
            }}/>
        </View>
        <DFErrorLabel
          key={"company-required"}
          isShow={true}
          text={this.state.errorCompany}/>
        <DFLabel key={"department"} text={STRINGS.SIGNUP_PAGE.LBL_DEPARTMENT} color="black" isRequired={false}/>
        <View style={styles.borderView}>
          <TextInput
            style={styles.input}
            placeholder=''
            maxLength={100}
            editable={true}
            underlineColorAndroid='rgba(0,0,0,0)'
            onChangeText={(text) => {
              this.department = text;
            }}/>
        </View>

        <DFButton
          text={STRINGS.SIGNUP_PAGE.BTN_SIGN_UP}
          onPress={this.signUp.bind(this)}
          style={{backgroundColor: "#B2D234", borderColor: "#B2D234"}}
          underlayColor='#B2D234'/>
      </View>;
    // var _scrollView: ScrollView;
    return (
      <View style={{flexGrow: 1}}>
        <Topbar fromLogin={true} onBack={this.onBack.bind(this)} title={STRINGS.SIGNUP_PAGE.HEADER}/>
        <ScrollView
          ref={(scrollView) => {
            _scrollView = scrollView;
          }}
          automaticallyAdjustContentInsets={false}
          contentInset={{bottom: this.state.scrollViewAdjustment}}
          onScroll={() => {
            console.log('onScroll!');
          }}
          scrollEventThrottle={200}>
          {formView}
        </ScrollView>
        { (this.state.showSuccess) ?
          <SuccessModel header={STRINGS.SUCCESS_MODAL.HEADER} msg={this.state.successMsg} btn={this.state.successBtn}
                        successAction={this.successAction.bind(this)}/> : null}
      </View>
    );
  }

  isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  removeTel(number) {
    let foundIndex = -1;
    for (let i = 0; i < this.phone.length; i++) {
      if (this.phone[i] === number) {
        foundIndex = i;
        break
      }
    }
    if (foundIndex !== -1) {
      this.phone.splice(foundIndex, 1)
    }
    this.setState({phone: this.phone});
  }

  removeImage() {
    this.setState({
      imgPath: null
    });
  }

  addTel() {
    if (this.state.txt_phone.length > 0) {
      if (this.validatePhone()) {
        if (this.phone.indexOf(this.state.txt_phone) !== -1) {
          this.setState({errorPhone: STRINGS.VALIDATION_MESSAGES.DUPLICATE_PHONE});
        } else {
          this.phone.push(this.state.txt_phone);
          this.setState({txt_phone: ''});
        }
      }
      this.setState({phone: this.phone});
    }
  }

  validatePhone() {
    //let phoneNo = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    let str = this.state.txt_phone;
    if (Util.isInternationalPhoneNoCorrect(str)) {
      this.setState({errorPhone: '', errorAdd: ''});
      return true;
    } else {
      this.setState({errorPhone: STRINGS.VALIDATION_MESSAGES.INVALID_PHONE});
      return false;
    }
  }

  checkBtn() {
    let email = this.email.trim();
    let yahoo = /@yahoo.com\s*$/.test(email);
    let hotmail = /@hotmail.com\s*$/.test(email);
    let gmail = /@gmail.com\s*$/.test(email);
    if (gmail) {
      this.state.successBtn = STRINGS.SIGNUP_PAGE.BTN_CHECK_EMAIL
    } else if (yahoo) {
      this.state.successBtn = STRINGS.SIGNUP_PAGE.BTN_CHECK_EMAIL
    } else if (hotmail) {
      this.state.successBtn = STRINGS.SIGNUP_PAGE.BTN_CHECK_EMAIL
    } else {
      this.state.successBtn = STRINGS.POPUP_MSG.BTN_OK
    }
  }

  checkEmail() {
    let email = this.email.trim();
    let yahoo = /@yahoo.com\s*$/.test(email);
    let hotmail = /@hotmail.com\s*$/.test(email);
    let gmail = /@gmail.com\s*$/.test(email);
    if (gmail) {
      Linking.openURL("https://mail.google.com/mail/");
    } else if (yahoo) {
      Linking.openURL("https://www.yahoo.com/");
    } else if (hotmail) {
      Linking.openURL("https://www.hotmail.com/");
    }
  }

  validateInput() {
    this.setState({
      email: this.email.trim(),
      name: this.name.trim(),
      surname: this.surname.trim(),
      department: this.department.trim()
    });

    if (this.email.trim() === '') {
      this.setState({errorEmail: STRINGS.VALIDATION_MESSAGES.REQUIRED});
    }
    if (!this.valideEmail(this.email.trim())) {
      this.setState({errorEmail: STRINGS.VALIDATION_MESSAGES.EMAIL_INCORRECT_FORMAT});
    }
    if (this.name.trim() === '') {
      this.setState({errorName: STRINGS.VALIDATION_MESSAGES.REQUIRED});
    }
    if (this.state.txt_phone.length !== 0) {
      this.setState({errorPhone: STRINGS.VALIDATION_MESSAGES.ADD_TELEPHONE});
    }
    if (this.surname.trim() === '') {
      this.setState({errorSurname: STRINGS.VALIDATION_MESSAGES.REQUIRED});
    }
    if (this.company.trim() === '') {
      this.setState({errorCompany: STRINGS.VALIDATION_MESSAGES.REQUIRED});
    }
    return !(this.email.trim() === '' || this.name.trim() === '' || this.surname.trim() === '' || this.company === '' || !this.valideEmail(this.email.trim()) || this.state.txt_phone.length !== 0);
  }

  valideEmail(email) {
    if (email === '' || email === undefined) {
      return true;
    } else {
      let emailFilter = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
      return emailFilter;
    }
  }

  signUp() {

    if (!this.validateInput()) {
      return;
    }

    this.checkBtn();

    this.props.toggleMainSpinner(true);

    let self = this;
    let data = {
      'email': this.email.trim(),
      'firstName': this.name.trim(),
      'lastName': this.surname.trim(),
      'company': this.company.trim(),
      'department': this.department.trim(),
      'phone': this.phone
    };

    if (this.state.imgPath !== null) {
      let formPhoto = new FormData();
      formPhoto.append('photo', this.state.imgPath, this.state.imgPath.name);
      DFService.callApiWithoutHeaderObj(formPhoto, apiConfig.uploadPhoto, function (error, response) {
        if (response) {
          if (response.resCode === 'DF2000000') {
            data.img_path = response.data;
            self.requestSignUp(data);
          } else {
            self.props.toggleMainSpinner(false);
            self.props.openMainModal(response.resMessage)
          }
        } else {
          self.props.toggleMainSpinner(false);
          self.props.openMainModal(STRINGS.COMMON.UNEXPECTED_ERROR);
        }
      });
    }else{
      self.requestSignUp(data);
    }
  }

  requestSignUp(data) {

    let self = this;
    let formData = new FormData();
    formData.append('data', JSON.stringify(data));

    DFService.callApiWithoutHeaderObj(formData, apiConfig.signUp, function (error, response) {
      if (response) {
        if (response.resCode === 'DF2000000') {
          self.props.toggleMainSpinner(false);
          self.setState({
            successMsg: STRINGS.formatString(STRINGS.SIGNUP_PAGE.SUCCESS, self.email),
            showSuccess: true,
          });
        } else {
          self.props.toggleMainSpinner(false);
          self.props.openMainModal(response.resMessage)
        }
      } else {
        self.props.toggleMainSpinner(false);
        self.props.openMainModal(STRINGS.COMMON.UNEXPECTED_ERROR);
      }
    });
  }

  successAction(number) {
    this.setState({
      showSuccess: false
    });
    if (number !== null) {
      this.checkEmail();
    }
    this.onBack();
  }

  onBack() {
    this.props.userHasSignUp(false);
  }

  pickImage() {
    let options = {
      title: STRINGS.SIGNUP_PAGE.TITLE_SELECT_PHOTO, // specify null or empty string to remove the title
      cancelButtonTitle: STRINGS.IMAGE_PICKER.CANCEL,
      takePhotoButtonTitle: STRINGS.IMAGE_PICKER.LBL_TAKE_PHOTO, // specify null or empty string to remove this button
      chooseFromLibraryButtonTitle: STRINGS.IMAGE_PICKER.LBL_PHOTO_LIBRARY, // specify null or empty string to remove this button
      cameraType: 'back', // 'front' or 'back'
      mediaType: 'photo', // 'photo' or 'video'
      maxWidth: 500, // photos only
      maxHeight: 500, // photos only
      aspectX: 1, // android only - aspectX:aspectY, the cropping image's ratio of width to height
      aspectY: 1, // android only - aspectX:aspectY, the cropping image's ratio of width to height
      quality: 0.2, // 0 to 1, photos only
      angle: 0, // android only, photos only
      allowsEditing: false, // Built in functionality to resize/reposition the image after selection
      noData: true, // photos only - disables the base64 `data` field from being generated (greatly improves performance on large photos)
      storageOptions: { // if this key is provided, the image will get saved in the documents directory on ios, and the pictures directory on android (rather than a temporary directory)
        skipBackup: true, // ios only - image will NOT be backed up to icloud
        path: 'images' // ios only - will save image at /Documents/images rather than the root
      }
    };

    ImagePicker.showImagePicker(options, (response) => {

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        // You can display the image using either data...
        // const source = {uri: 'data:image/jpeg;base64,' + response.data, isStatic: true};
        // or a reference to the platform specific asset location
        let source;
        console.log('PHOTO RESPONSE: ', response);
        if (Platform.OS === 'ios') {
          source = {uri: response.uri, name: 'img.jpg', type: 'image/jpg', uploadType: 'image'}
        } else {
          source = {
            uri: response.uri,
            isStatic: true,
            name: response.fileName,
            type: response.type,
            uploadType: 'image'
          };
        }

        this.setState({
          imgPath: source
        });
      }
    });
  }

}

const styles = StyleSheet.create({
  input: {
    height: 30,
    fontSize: 20,
    borderWidth: 0,
    color: '#333435',
    paddingTop: 2,
    paddingBottom: 2
  },
  inputPhone: {
    width: 150,
    height: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#333435',
    paddingTop: 2,
    paddingBottom: 2
  },
  borderView: {
    borderColor: '#95989A',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  borderViewDisable: {
    borderColor: '#c0c0c0',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  disableInput: {
    fontSize: 14,
    borderWidth: 0,
    color: '#cdcdcd',
    paddingTop: 5,
    paddingBottom: 5,
  },
  disableInputNoText: {
    height: 25,
    fontSize: 14,
    borderWidth: 0,
    color: '#cdcdcd',
    paddingTop: 5,
    paddingBottom: 5,
  },
  dropdownBtn: {
    height: 35,
    flexDirection: 'row',
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 2,
    marginLeft: 2,
    padding: 8,
    borderRadius: 18,
  },
  dropdownBtnTxt: {
    marginRight: 4,
    fontSize: 14,
    maxWidth: 140
  },
  viewMode: {
    minHeight: 25,
    fontSize: 14,
    borderWidth: 0,
    color: '#606060',
    textAlignVertical: 'center',
    paddingTop: 5,
    paddingBottom: 5,
  },
  circle: {
    width: 120,
    height: 120
  },
  circleImage: {
    margin: 10,
    width: 100,
    height: 100,
    borderRadius: 100 / 2,
    position: 'absolute',
  },
  image: {
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  listAssign: {
    flexGrow: 1,
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  button: {
    height: 30,
    width: 30,
    paddingTop: 2,
    paddingBottom: 2,
    marginRight: 15,
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    borderWidth: 1,
    borderRadius: 4,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonClose: {
    height: 30,
    width: 30,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonCamera: {
    height: 30,
    width: 30,
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    borderWidth: 1,
    borderRadius: 15,
    marginTop: 90,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center'
  },
  camera: {
    height: 35,
    width: 35,
    marginTop: 80,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center'
  },
});
