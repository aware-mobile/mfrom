import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableHighlight,
  ActivityIndicator,
  AsyncStorage,
  Platform,
  NativeModules
} from 'react-native';

import DFService from './Service/DFService';
import apiConfig from './Util/Config';
import STRINGS from './Util/strings';
import AppText from './Util/AppText';
import ProfilePage from './ProfilePage';
import SettingsPage from './SettingsPage';
import ChangePasswordPage from './ChangePasswordPage';
import Menu, {
  MenuContext,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from 'react-native-popup-menu';
const Timer = require('react-native-timer');
import Entypo from 'react-native-vector-icons/Entypo';
let Ionicons = require('react-native-vector-icons/Ionicons');

class HeaderMain extends Component {
  constructor(props) {
    super(props);
    this.state = {opened: false}
    //console.log("HeaderMain", props);
  }


  onTriggerPress() {
    console.log('onTriggerPress');
  }

  onBackdropPress() {
    this.setState({opened: false});
  }

  render() {
    let viewNoti = this.props.notiCount > 0 ?
      <View style={(this.props.notiCount >= 100) ? styles.circleWide : styles.circle}>
        <AppText style={{
          fontSize: 12, color: 'white', backgroundColor: 'transparent'
        }}>{this.props.notiCount < 100 ? this.props.notiCount : '99+'}</AppText>
      </View> : null;
    return (
      <View style={this.isStyleIOS()}>
        <Image source={require('./img/top_banner.png') } style={this.logoIOS()}/>
        <View style={styles.rowContain}>
          <TouchableHighlight
            underlayColor='transparent'
            style={this.btnIOS()}
            onPress={this.props.toMainContact.bind(this)}>
            <Image source={require('./img/tobbarUser.png') } style={styles.navBtnImage}/>
          </TouchableHighlight>
          <TouchableHighlight
            underlayColor='transparent'
            style={[this.btnIOS(), {marginLeft: 5}]}
            onPress={this.props.toNotiPage.bind(this) }>
            <View>
              <Image source={require('./img/tobbarNoti.png') } style={styles.navBtnImage}/>
              {viewNoti}
            </View>
          </TouchableHighlight>

          <Menu style={[this.btnIOS(), {marginLeft: 5}]} onOpen={this.props.showMenu.bind(this, true)}
                onClose={this.props.showMenu.bind(this, false)}>
            <MenuTrigger
              key={'MenuTriggerIndex' + 0}>
              <View style={{padding: 8}}>
                <Ionicons name="md-more" size={30} color="white"/>
              </View>
            </MenuTrigger>
            <MenuOptions optionsContainerStyle={styles.optionsContainerStyle}>
              <MenuOption onSelect={this.toProfile.bind(this)}
                          key={'optionIndex' + 0}
                          style={styles.optionMenuItemContainer}>
                <AppText style={styles.optionMenuItems}>{STRINGS.HEADER_MAIN.PROFILE}</AppText>
              </MenuOption>
              <MenuOption onSelect={this.toChangePassword.bind(this)}
                          key={'optionIndex' + 1}
                          style={styles.optionMenuItemContainer}>
                <AppText style={styles.optionMenuItems}>{STRINGS.HEADER_MAIN.CHANGE_PASSWORD}</AppText>
              </MenuOption>
              <MenuOption onSelect={this.toSetting.bind(this)}
                          key={'optionIndex' + 2}
                          style={styles.optionMenuItemContainer}>
                <AppText style={styles.optionMenuItems}>{STRINGS.HEADER_MAIN.SETTINGS}</AppText>
              </MenuOption>
              <MenuOption onSelect={this.logout.bind(this)}
                          key={'optionIndex' + 3}
                          style={styles.optionMenuItemContainer}>
                <AppText style={styles.optionMenuItems}>{STRINGS.HEADER_MAIN.LOG_OUT}</AppText>
              </MenuOption>
            </MenuOptions>
          </Menu>

        </View>
      </View>
    )
  }

  logout() {
    Timer.setTimeout(this, 'Log out', () => {
      this.props.logout();
    }, 500);
  }


  async toSetting() {
    this.props.navigator.push({
      name: 'setting',
      title: STRINGS.SETTINGS_PAGE.HEADER,
      component: SettingsPage,
      passProps: {
        navigator: this.props.navigator,
        toggleMainSpinner: this.props.toggleMainSpinner.bind(this),
        openMainModal: this.props.openMainModal.bind(this),
        openMainConfirm: this.props.openMainConfirm.bind(this)
      }
    });
  }

  async toProfile() {
    let self = this;
    self.props.navigator.push({
      name: 'profile',
      title: STRINGS.PROFILE_PAGE.HEADER,
      component: ProfilePage,
      passProps: {
        fromProfile: true,
        navigator: self.props.navigator,
        toggleMainSpinner: self.props.toggleMainSpinner.bind(self),
        openMainModal: self.props.openMainModal.bind(self),
        openMainConfirm: self.props.openMainConfirm.bind(self)
      }
    });
  }

  async toChangePassword() {

    this.props.navigator.refs['dash']._removeKeyboardListeners();

    let self = this;
    self.props.navigator.push({
      name: 'changepassword',
      title: STRINGS.CHANGE_PASSWORD_PAGE.HEADER,
      component: ChangePasswordPage,
      passProps: {
        fromChangePass: true,
        navigator: self.props.navigator,
        toggleMainSpinner: self.props.toggleMainSpinner.bind(self),
        openMainModal: self.props.openMainModal.bind(self),
        openMainConfirm: self.props.openMainConfirm.bind(self)
      }
    });
  }

  isStyleIOS() {
    if (Platform.OS === 'ios') {
      return {
        height: 80,
        backgroundColor: '#2d313c',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 10
      }
    } else {
      return {
        height: 55,
        backgroundColor: '#2d313c',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
      }
    }
  }

  logoIOS() {
    if (Platform.OS === 'ios') {
      return {
        marginTop: 5,
        marginLeft: 10,
        height: 35,
        width: 91,
        justifyContent: 'center',
        alignItems: 'flex-start'
      }
    } else {
      return {
        marginLeft: 10,
        height: 40,
        width: 103,
        justifyContent: 'center',
        alignItems: 'flex-start'
      }
    }
  }

  btnIOS() {
    if (Platform.OS === 'ios') {
      return {
        height: 40,
        marginTop: 10,
        marginRight: 15,
        justifyContent: 'center',
        alignItems: 'flex-end'
      }
    } else {
      return {
        height: 40,
        marginRight: 15,
        justifyContent: 'center',
        alignItems: 'flex-end'
      }
    }
  }
}

const styles = StyleSheet.create({
  navBtnImage: {
    width: 30,
    height: 30
  },
  rowContain: {
    flexDirection: 'row'
  },

  circle: {
    minWidth: 20,
    height: 20,
    borderRadius: 20 / 2,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: 0,
    top: 0,
  },
  circleWide: {
    minWidth: 26,
    height: 20,
    borderRadius: 26 / 2,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: 0,
    top: 0,
  },
  optionsContainerStyle: {
    width: 150,
    backgroundColor: "#ffffff"
  },
  optionMenuItemContainer: {
    margin: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  optionMenuItems: {
    fontSize: 16,
    color: 'black',
    // fontWeight: 'bold',
    flexGrow: 0.5
  }
});

export default HeaderMain;
