import React, {Component} from 'react';
import {AsyncStorage, Image, Linking, StyleSheet, Switch, TouchableHighlight, View} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import DeviceInfo from 'react-native-device-info';
import DFService from './Service/DFService';
import apiConfig from './Util/Config';
import FadeInView from './Animation/FadeInView'
import STRINGS from './Util/strings'
import AppText from './Util/AppText';

class SettingsPage extends Component {

  constructor(props) {
    super(props);
    this.devices = [];
    this.state = {
      trueSwitchIsOn: true,
      showSelectLanguage: false,
      currentLanguageCode: 'en',
      currentLanguage: 'English(EN)'
    };
    this.getProfile();

    AsyncStorage.getItem('LANGUAGE', (err, result) => {
      if (err) {
        console.log('Unable to get LANGUAGE');
      } else {
        let langCode = result == 'th' ? result : 'en';
        let lang = langCode == 'th' ? STRINGS.SETTINGS_PAGE.THAI_LANG : STRINGS.SETTINGS_PAGE.ENGLISH_LANG;
        this.setState({
          currentLanguageCode: langCode,
          currentLanguage: lang
        })
      }
    });
  }

  setUpdateSound(value) {
    let self = this;
    self.setState({trueSwitchIsOn: value});
    let bodyParam = {
      deviceId: DeviceInfo.getUniqueID(),
      sound: "on"
    };

    if (!value) {
      bodyParam.sound = "off";
    }

    DFService.callApiWithHeader(bodyParam, apiConfig.setNotification, function (error, response) {
      if (response) {
        console.log(response);
        if (response.resCode === 'DF2000000') {
          self.setState({trueSwitchIsOn: value})
        } else {
          self.setState({trueSwitchIsOn: !value})
        }
      } else {
        console.log(error);
        self.setState({trueSwitchIsOn: !value})
      }

      AsyncStorage.setItem('SoundNoti', self.state.trueSwitchIsOn ? "on" : "off", (err, result) => {
        if (err) {
          console.log('unable to save SoundNoti');
        }
      });
    });
  }

  async getProfile() {

    let self = this;
    self.props.toggleMainSpinner(true);
    let value = await AsyncStorage.getItem("Profile");
    let profile = await JSON.parse(value);
    let config = {
      method: apiConfig.getUserDetail.method,
      url: apiConfig.getUserDetail.url,
      action_name: apiConfig.getUserDetail.action_name
    };
    config.url += profile.user.id;

    DFService.callApiWithHeader(null, config, function (error, response) {
      self.props.toggleMainSpinner(false);
      if (response) {

        if (response.resCode === 'DF2000000') {
          console.log(response);
          // Successfull Response
          self.setState({trueSwitchIsOn: self.checkDevice(response.data.devices)})

        } else {
          // Failed Response
          self.props.openMainModal(response.resMessage)
        }
      } else {
        // Error from application calling service
        self.props.openMainModal('Unexpected error has occured in sevice');
      }

    });
  }

  checkDevice(devices) {
    if (!devices) {
      return true;
    }
    for (let i = 0; i < devices.length; i++) {
      if (devices[i].device_id === DeviceInfo.getUniqueID() && devices[i].notificationSound === "off") {
        console.log("notificationSound Off");
        return false;
      }
    }
    return true;
  }

  switchLanguage(languageCode) {
    if (languageCode != this.state.currentLanguageCode) {
      STRINGS.setLanguage(languageCode);
      let newLanguage = languageCode == 'en' ? STRINGS.SETTINGS_PAGE.ENGLISH_LANG : STRINGS.SETTINGS_PAGE.THAI_LANG;
      this.setState({
        currentLanguageCode: languageCode,
        currentLanguage: newLanguage,
        showSelectLanguage: false
      });

      AsyncStorage.setItem('LANGUAGE', languageCode, (err, result) => {
        if (err) {
          console.log('unable to save LANGUAGE');
        }
      });
    } else {
      // Hide the language view
      this.showLanguageOptions();
    }

  }

  showLanguageOptions() {
    this.setState({showSelectLanguage: !this.state.showSelectLanguage})
  }

  handleOpenUrl(url) {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log('Don\'t know how to open URI: ' + url);
      }
    });
  };


  render() {

    let enCheck = this.state.currentLanguageCode == 'en' ?
      <Ionicons name="md-checkmark" size={24} color="#797979"/> : null;
    let thCheck = this.state.currentLanguageCode == 'th' ?
      <Ionicons name="md-checkmark" size={24} color="#797979"/> : null;

    let selectLangView = null;
    if (this.state.showSelectLanguage) {
      selectLangView =
        <FadeInView>
          <View style={styles.containerSelectLang}>
            <TouchableHighlight
              underlayColor='transparent'
              onPress={() => this.switchLanguage("en")}>
              <View style={[styles.rowContainer, styles.languageRow]}>
                <AppText>{STRINGS.SETTINGS_PAGE.ENGLISH_LANG}</AppText>
                {enCheck}
              </View>
            </TouchableHighlight>
            <View style={styles.seperator}/>

            <TouchableHighlight
              underlayColor='transparent'
              onPress={() => this.switchLanguage("th")}>
              <View style={[styles.rowContainer, styles.languageRow]}>
                <AppText>{STRINGS.SETTINGS_PAGE.THAI_LANG}</AppText>
                {thCheck}
              </View>
            </TouchableHighlight>
          </View>
        </FadeInView>
    }

    return (
      <View style={styles.container}>
        <View style={styles.rowContainer}>
          <AppText style={styles.input}>{STRINGS.SETTINGS_PAGE.SOUND_NOTIFICATION}</AppText>
          <Switch
            onValueChange={(value) => this.setUpdateSound(value)}
            value={this.state.trueSwitchIsOn}/>
        </View>

        {/* PUT SELECT LANGUAGE VIEW HERE  */}

        <View style={styles.rowContainer}>
          <AppText style={styles.input}>{STRINGS.SETTINGS_PAGE.LBL_USER_MANUAL}</AppText>
          <TouchableHighlight underlayColor='transparent'
                              onPress={this.handleOpenUrl.bind(this, apiConfig.MOBILE_USER_MANUAL_URL)}>
            <View style={styles.userManualContainer}>
              <AppText>Download</AppText>
              <Image source={require('./img/user_manual.png')}
                     style={styles.userManualIcon}/>
            </View>
          </TouchableHighlight>
        </View>

        <View style={styles.rowContainer}>
          <AppText style={styles.input}>{STRINGS.SETTINGS_PAGE.LBL_VERSION}</AppText>
          <AppText>{DeviceInfo.getVersion()}</AppText>
        </View>

      </View>);
  }

}

/*
// SELECT LANGUAGE

<TouchableHighlight
  underlayColor='transparent'
  onPress={this.showLanguageOptions.bind(this)}>
  <View style={styles.rowContainer}>
    <AppText style={styles.input}>{STRINGS.SETTINGS_PAGE.LANGUAGE}</AppText>
    <AppText style={styles.languageLbl}>{this.state.currentLanguage}</AppText>
  </View>
</TouchableHighlight>

{selectLangView}

*/

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    flexGrow: 1,
  },
  containerSelectLang: {
    // borderColor: '#ababab',
    // borderWidth: 1,
    backgroundColor: "#ffffff",
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,
    elevation: 3,
    shadowColor: "#4d4d4d",
    shadowOpacity: 0.8,
    marginBottom: 50,
    shadowRadius: 2,
    shadowOffset: {
      height: 2,
      width: 0
    }
  },
  rowContainer: {
    flexDirection: 'row',
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  languageRow: {
    paddingBottom: 10
  },
  seperator: {
    height: 1,
    backgroundColor: '#ababab'
  },
  borderView: {
    borderColor: '#95989A',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  button: {
    height: 30,
    width: 30,
    paddingTop: 2,
    paddingBottom: 2,
    marginRight: 15,
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    borderWidth: 1,
    borderRadius: 4,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center'
  },
  input: {
    height: 30,
    fontSize: 16,
    borderWidth: 0,
    color: '#333435',
    paddingTop: 2,
    paddingBottom: 2
  },
  languageLbl: {
    height: 30,
    fontSize: 16,
    borderWidth: 0,
    color: '#737373',
    paddingTop: 2,
    paddingBottom: 2
  },
  userManualIcon: {
    width: 30,
    height: 30,
    marginLeft: 5
  },
  userManualContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default SettingsPage;
