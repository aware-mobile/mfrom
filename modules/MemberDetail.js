import React, { Component } from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  Image,
  TouchableHighlight,
  ListView,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  Platform
} from 'react-native';
import DFService from './Service/DFService';
// import DFService2 from './Service/DFService';
import apiConfig from './Util/Config';
import Util from './Util/Util';
import STRINGS from './Util/strings';

import FormPage from './FormPage'
import BlankModal from './Util/BlankModal'
import AppText from './Util/AppText';

var self;

var Ionicons = require('react-native-vector-icons/Ionicons');
var {height, width} = Dimensions.get('window');

var dataSource = new ListView.DataSource(
  {rowHasChanged: (r1, r2) => r1._id !== r2._id}
);

export class MemberDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: dataSource.cloneWithRows(this.props.users),
    };
    console.log('this.props.users: ', this.props.users);
    self = this
  }

  renderRow(rowData, sectionID, rowID) {
    var icon = null
    if (rowData.type == 'user') {
      icon = <Ionicons name="md-person" size={20} color="black"
        style={{ justifyContent: 'center', alignItems: 'center' }} />
    } else if (rowData.type == 'customer') {
      icon = <Ionicons name="md-bookmarks" size={20} color="black"
        style={{ justifyContent: 'center', alignItems: 'center' }} />;
    } else{
      icon = <Ionicons name="md-at" size={20} color="black"
        style={{ justifyContent: 'center', alignItems: 'center' }} />;
    }

    return (
      <View style={{flexDirection:'column'}}>
      <View style={styles.rowContainer}>
        <View style={styles.innerContainerList}>
          <View style={styles.circle}>
            {icon}
          </View>
          <AppText style={[styles.nameText, {width: width - 100}]}>
            {rowData.name}
          </AppText>

        </View>
      </View>
      </View>
    );
  }

  render() {
    return (
      <BlankModal>
        <View style={styles.container}>
          <View style={styles.innerContainer}>
            <View style={styles.headerModal}>

              <AppText style={styles.textHeaderModal}>
                {this.props.title}
              </AppText>

              <AppText style={{}}
                    onPress={this.props.closeModal}>
                <Ionicons name="ios-close" size={30} color="black"
                          style={{justifyContent: 'center', alignItems: 'center'}}/>
              </AppText>
            </View>
            <View>
              <AppText style={{padding: 10, fontWeight: 'bold'}}>
                {STRINGS.COMMON.MEMBERS_LBL}
              </AppText>
              <ListView
                style={{marginTop: 5, height:height-210}}
                dataSource={this.state.dataSource}
                renderRow={this.renderRow.bind(this) }/>
            </View>
          </View>
        </View>
      </BlankModal>
    )
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent: 'center',
    // paddingTop: 60,
    // paddingBottom: 60,
    paddingLeft: 20,
    paddingRight: 20
  },
  ListContainer: {},
  headerModal: {
    height: 60,
    paddingRight: 10,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#efeff1'
  },
  textHeaderModal: {
    marginLeft: 10,
    fontWeight: 'bold',
    fontSize: 18,
    flexGrow: 8
  },
  innerContainer: {
    height: height - 100,
    width: width - 40,
    // alignItems: 'center',
    backgroundColor: '#ffffff'
  },
  innerContainerList: {
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  rowContainer: {
    backgroundColor: '#ffffff',
    flexDirection: 'row',
  },
  nameText: {
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'left',
    alignSelf: 'center',
    paddingLeft: 10,
    paddingRight: 10
  },
  row: {
    alignItems: 'center',
    flexGrow: 1,
    flexDirection: 'row',
    marginBottom: 20,
    flexWrap: 'wrap'
  },
  rowTitle: {
    flexGrow: 1,
    fontWeight: 'bold',
  },
  button: {
    borderRadius: 5,
    flexGrow: 1,
    height: 44,
    alignSelf: 'stretch',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  buttonText: {
    fontSize: 18,
    margin: 5,
    textAlign: 'center',
  },
  modalButton: {
    marginTop: 10,
  },
  circle: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ADB0B1',
    marginLeft: 3,
    marginRight: 3
  },
});

export default MemberDetail;
