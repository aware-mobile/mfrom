import React, {Component} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Image,
  TouchableHighlight,
  ActivityIndicator,
  Platform,
  Dimensions
} from 'react-native';

let {height, width} = Dimensions.get('window');

import Camera from 'react-native-camera';

let self;

class QRPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      torchMode: Camera.constants.TorchMode.off,
      cameraType: Camera.constants.Type.back,
    };
    this.isFind = false;

    self = this;
  }

  barcodeReceived(e) {
    console.log('Barcode: ' + e.data);
    console.log('Type: ' + e.type);

    if (!self.isFind) {
      self.isFind = true;
      self.props.setBarcode(e.data);
      self.props.navigator.pop();
    }
  }

  render() {
    let scanArea = (
      <View style={styles.rectangleContainer}>
        <View style={styles.rectangle}/>
      </View>);
    return (
      <Camera
        onBarCodeRead={this.barcodeReceived}
        torchMode={this.state.torchMode}
        type={this.state.cameraType}
        style={styles.camera}>
        {scanArea}
      </Camera>);
  }
}

const styles = StyleSheet.create({
  camera: {
    flexGrow: 1
  },
  rectangleContainer: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  rectangle: {
    height: 250,
    width: 250,
    borderWidth: 2,
    borderColor: '#00FF00',
    backgroundColor: 'transparent'
  },
  redLine: {
    height: 1,
    width: width,
    backgroundColor: '#FF0000',
  }
});
export default QRPage;
