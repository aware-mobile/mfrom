import React, {Component} from 'react';
import {
  AsyncStorage,
  Dimensions,
  Image,
  Keyboard,
  LayoutAnimation,
  Linking,
  Platform,
  StyleSheet,
  TextInput,
  TouchableHighlight,
  TouchableWithoutFeedback,
  UIManager,
  View
} from 'react-native';

import AppText from './Util/AppText';

import FCM from 'react-native-fcm';

import dismissKeyboard from 'react-native-dismiss-keyboard';
import DFService from './Service/DFService';
import md5 from 'md5';
import apiConfig from './Util/Config';
import DeviceInfo from 'react-native-device-info';

import ForgotPasswordModel from './CustomViews/ForgotPasswordModel';
import SuccessModel from './CustomViews/SuccessModel';

import STRINGS from './Util/strings.js';

let self;

let {height, width} = Dimensions.get('window');

let Ionicons = require('react-native-vector-icons/Ionicons');
//var {Icon,} = require('react-native-icons');

let TextLines = React.createClass({
  render: function () {
    let lines = this.props.lines;

    let br = lines.map(function (line) {
      return (<span>{line}<br/></span>);
    });
    return (<div>{br}</div>);
  }
});

class LoginPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      usernameInput: '',
      username: '',
      passwordInput: '',
      message: '',
      usernameMsg: '',
      passwordMsg: '',
      errorMessage: this.props.errorMessage,
      deviceToken: this.props.deviceToken,
      keyboardHeight: 0,
      logo: {},
      containerFooter: {},
      showForgot: false,
      showSuccess: false,
      successMsg: '',
      successBtn: 'CHECK EMAIL'
    };
    this.email = '';
    self = this;

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental &&
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }

    this.getUser()
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow)
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide)
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = (e) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    self.setState({
      keyboardHeight: 100,
      containerFooter: {paddingTop: 5},
      // logo: {width: 80, height: 94}
    })
  };

  _keyboardDidHide = (e) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    self.setState({
      keyboardHeight: 0,
      containerFooter: {},
      logo: {}
    })
  };

  successAction(number) {
    this.setState({
      showSuccess: false
    });
    if (number !== null) {
      this.checkEmail();
    }
  }

  checkBtn() {
    let email = this.email;
    let yahoo = /@yahoo.com\s*$/.test(email);
    let hotmail = /@hotmail.com\s*$/.test(email);
    let gmail = /@gmail.com\s*$/.test(email);
    if (gmail) {
      this.state.successBtn = "CHECK EMAIL"
    } else if (yahoo) {
      this.state.successBtn = "CHECK EMAIL"
    } else if (hotmail) {
      this.state.successBtn = "CHECK EMAIL"
    } else {
      this.state.successBtn = "OK"
    }
  }

  checkEmail() {
    var email = this.email;
    var yahoo = /@yahoo.com\s*$/.test(email);
    var hotmail = /@hotmail.com\s*$/.test(email);
    var gmail = /@gmail.com\s*$/.test(email);
    if (gmail) {
      Linking.openURL("https://mail.google.com/mail/");
    } else if (yahoo) {
      Linking.openURL("https://www.yahoo.com/");
    } else if (hotmail) {
      Linking.openURL("https://www.hotmail.com/");
    }
  }

  closeAction(email) {
    this.setState({
      showForgot: false
    });
    this.props.userHasShowForgot(false);

    if (email !== null) {
      this.email = email;
      this.checkBtn();
      self.props.toggleMainSpinner(true);
      let data = {
        email
      };

      DFService.callApiNoHeader(data, apiConfig.forgotPassword, function (error, response) {
        if (response) {
          if (response.resCode === 'DF2000000') {
            // Successfull Response
            self.props.toggleMainSpinner(false);
            self.setState({
              successMsg: STRINGS.POPUP_MSG.INSTRUCTIONS_SENT_MESSAGE,
              showSuccess: true,
            });
          } else {
            // Failed Response
            console.log(response);
            self.props.toggleMainSpinner(false);
            self.props.openMainModal(response.resMessage)
          }
        } else {
          // Error from application calling service
          self.props.toggleMainSpinner(false);
          console.log("error = ", error);
        }
      });
    }
  }

  render() {
    let forgotView = null;
    if (this.state.showForgot) {
      forgotView = <ForgotPasswordModel
        closeAction={this.closeAction.bind(this)}/>
    }
    return (
      <View style={{flexGrow: 1}}>
        <TouchableWithoutFeedback onPress={() => {
          dismissKeyboard();
        }}>

          <Image source={require('./img/backgroup.png')} style={styles.containerBG}>
            <View style={styles.container}>

              <View style={{paddingTop: 80}}>
                <Image source={require('./img/loginLogo.png')} style={[styles.logo, this.state.logo]}/>
              </View>

              <View style={[styles.containerFooter, this.state.containerFooter]}>

                <View style={styles.textLoginContainer}>

                  <Image source={require('./img/login_user.png')} style={styles.textLoginIcon}/>

                  <TextInput
                    style={styles.input}
                    value={this.state.username}
                    onChangeText={(text) => this.setState({usernameInput: text, username: text, message: ''})}
                    placeholder={STRINGS.LOGIN_LABEL.EMAIL}
                    placeholderTextColor='gray'
                    underlineColorAndroid='rgba(0,0,0,0)'
                    autoCapitalize="none"
                    autoCorrect={false}/>
                </View>

                <View style={styles.textInputUnderline}/>

                <AppText style={styles.messageErr}>
                  {this.state.usernameMsg}
                </AppText>

                <View style={styles.textLoginContainer}>
                  <Image source={require('./img/login_lock.png')} style={styles.textLoginIcon}/>
                  <TextInput
                    style={styles.input}
                    placeholder={STRINGS.LOGIN_LABEL.PASSWORD}
                    placeholderTextColor='gray'
                    value={this.state.searchString}
                    onChangeText={(text) => this.setState({passwordInput: text, message: ''})}
                    secureTextEntry={true}
                    underlineColorAndroid='rgba(0,0,0,0)'
                  />
                </View>

                <View style={styles.textInputUnderline}/>

                <AppText style={styles.messageErr}>
                  {this.state.passwordMsg}
                </AppText>

                <TouchableHighlight
                  underlayColor='transparent'
                  onPress={this.onForgotPressed.bind(this)}>
                  <View>
                    <AppText style={styles.messageText}>
                      {STRINGS.LOGIN_LABEL.FORGOT}
                    </AppText>
                  </View>
                </TouchableHighlight>

                <TouchableHighlight
                  style={styles.button}
                  underlayColor='#B2D234'
                  onPress={this.onLoginPressed.bind(this)}>
                  <View>
                    <AppText style={styles.buttonText}>{STRINGS.LOGIN_LABEL.BTN_LOGIN}</AppText>
                  </View>
                </TouchableHighlight>

                <AppText style={styles.messageErr}>
                  {this.state.message}
                </AppText>
              </View>

              <View style={{height: this.state.keyboardHeight}}/>
            </View>
            <View style={{height: this.state.keyboardHeight}}/>
            <View>
              <AppText style={styles.messageVersion}>
                V. {DeviceInfo.getVersion()}
              </AppText>
            </View>
          </Image>
        </TouchableWithoutFeedback>
        {forgotView}
        {(this.state.showSuccess) ?
          <SuccessModel header={STRINGS.LOGIN_LABEL.INSTRUCTIONS_SENT} msg={this.state.successMsg}
                        btn={this.state.successBtn}
                        successAction={this.successAction.bind(this)}/> : null}
      </View>)
  }

  onForgotPressed() {

    this.setState({showForgot: true});
    this.props.userHasShowForgot(true);
  }

  onLoginPressed() {

    this.setState({errorMessage: ''});

    dismissKeyboard();

    console.log('Login Pressed');
    console.log('Username: ' + this.state.usernameInput);
    console.log('Password: ' + this.state.passwordInput);

    let device_token = this.state.deviceToken;
    let device_type = this.props.deviceType;

    let CONTINUE = true;

    if (device_token === undefined || device_token === null || device_token === '') {
      CONTINUE = false
    }

    if (CONTINUE) {
      self.sendLoginRequest(device_token, device_type)
    } else {
      FCM.getFCMToken().then(token => {
        let theToken = token;
        if (token === undefined || token === null) {
          theToken = '';
        }
        self.setState({deviceToken: theToken});
        self.sendLoginRequest(theToken, device_type)
      });
    }

  }

  toSignUp() {
    this.props.userHasSignUp(true);
  }

  sendLoginRequest(device_token, device_type) {
    console.log('Sending Device Token: ', device_token);

    this.saveUser(this.state.usernameInput);

    let apiData = {
      username: this.state.usernameInput,
      pwd: md5(this.state.passwordInput),
      channel: "app",
      device_token,
      device_type,
      device_id: DeviceInfo.getUniqueID()
    };

    if (this.state.usernameInput === '' || this.state.passwordInput === '') {
      if (this.state.usernameInput === '') {
        this.setState({usernameMsg: STRINGS.INPUT_MSG.USERNAME_REQUIRE});
      } else {
        this.setState({usernameMsg: ""});
      }
      if (this.state.passwordInput === '') {
        this.setState({passwordMsg: STRINGS.INPUT_MSG.PASSWORD_REQUIRE});
      } else {
        this.setState({passwordMsg: ""});
      }

    } else {
      this.setState({usernameMsg: ""});
      this.setState({passwordMsg: ""});

      self.props.toggleMainSpinner(true);

      this._checkLogoutQueue(apiData)
    }
  }

  _checkLogoutQueue(apiData) {
    AsyncStorage.getItem('LOGOUT_QUEUE', (err, result) => {
      if (err) {
        console.log('LogoutQueue: error in getting logout queue');
        self._callLoginApi(apiData)
      } else {
        console.log('LogoutQueue: found Logout Queue Obj - ', result);
        if (result !== null) {
          let profile = JSON.parse(result);
          DFService.callApiWithProfileObj(profile, apiConfig.logout, function (error, response) {
            if (response) {
              AsyncStorage.removeItem('Profile', (err) => {
              });
            } else {
              console.log(error);
            }

            AsyncStorage.removeItem('LOGOUT_QUEUE', (err) => {
              self._callLoginApi(apiData)
            });

          });
        } else {
          console.log('LogoutQueue: value is null!!');
          self._callLoginApi(apiData)
        }
      }
    });
  }

  _callLoginApi(apiData) {
    console.log('_callLoginApi');
    DFService.callApiNoHeader(apiData, apiConfig.login, function (error, response) {
      if (error) {
        console.log('error: ' + error.message);
      }
      if (response) {
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          self.props.toggleMainSpinner(false);
          self.success(response);
        } else {
          // Failed Response
          self.props.toggleMainSpinner(false);
          self.props.openMainModal(response.resMessage)
        }
      } else {
        // Error from application calling service
        self.props.toggleMainSpinner(false);
        self.props.openMainModal(error.message);
      }
    });
  }

  onClearMsg() {
    this.setState({message: ''})
  }

  async onResponseSuccess(response) {
    let capObj = [];
    let userCapabilities = response.data.user.capabilities;
    for (let i = 0; i < userCapabilities.length; i++) {
      capObj.push(userCapabilities[i].name);
    }
    let profile = {
      token: response.data.accessToken,
      user: {
        id: response.data.user._id,
        displayName: response.data.user.displayName,
        username: response.data.user.username,
        capabilities: capObj,
        devices: response.data.user.devices
      },
      company: {
        id: response.data.user.company_id._id,
        name: response.data.user.company_id.company_name,
      }
    };
    try {
      await AsyncStorage.setItem('Profile', JSON.stringify(profile));
    } catch (error) {
      this._appendMessage('AsyncStorage error: ' + error.message);
    }
  }

  async saveUser(username) {
    console.log('saveUser: ' + username);
    try {
      await AsyncStorage.setItem('Username', username);
    } catch (error) {
      console.log(error)
    }
  }

  async getUser() {
    console.log('getUser');
    try {
      let value = await AsyncStorage.getItem('Username');
      console.log('getUser: ' + value);
      this.setState({
        username: value ? value : '',
        usernameInput: value ? value : ''
      })
    } catch (error) {
      console.log(error)
    }
  }

  success(response) {
    if (response.resCode === 'DF2000000') {
      this.onResponseSuccess(response);
      this.props.userHasAuthenticated()
    } else {
      this.setState({message: response.resMessage});
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1
  },
  containerFooter: {
    flexGrow: 1,
    //alignItems: 'center',
    paddingTop: 30,
    marginTop: 30
  },
  containerBG: {
    flexGrow: 1,
    width: undefined,
    height: undefined,
    backgroundColor: 'transparent',
    //justifyContent: 'center',
    //alignItems: 'center'
  },
  description: {
    marginBottom: 14,
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#8cbb39'
  },

  input: {
    height: 36,
    padding: 4,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    paddingLeft: 10,
    fontSize: 21,
    // fontFamily: 'DB Heavent',
    borderWidth: 0,
    borderColor: '#B2D234',
    borderRadius: 8,
    color: '#49621c',
    flexGrow: 3
  },

  input2: {
    height: 36,
    padding: 4,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    paddingLeft: 10,
    fontSize: 15,
    // fontFamily: 'DB Heavent',
    borderWidth: 0,
    borderColor: '#B2D234',
    borderRadius: 8,
    color: '#49621c',
    flexGrow: 3
  },
  button: {
    width: width - 60,
    height: 36,
    flexDirection: 'row',
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    borderWidth: 1,
    borderRadius: 2,
    marginTop: 10,
    marginRight: 30,
    marginLeft: 30,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  messageText: {
    // textDecorationLine: 'underline',
    textDecorationStyle: 'solid',
    marginTop: 5,
    fontSize: 14,
    textAlign: 'center',
    color: '#6A6B6B'
  },
  messageVersion: {
    textDecorationStyle: 'solid',
    marginTop: 5,
    fontSize: 14,
    textAlign: 'right',
    color: '#6A6B6B'
  },

  messageErr: {
    marginTop: 10,
    fontSize: 14,
    textAlign: 'center',
    color: 'red'
  },
  bottomImage: {
    bottom: 0,
    position: 'absolute',
    width: 350,
    height: 80
  },
  topImage: {
    top: 0,
    left: 0,
    position: 'absolute',
    width: 160,
    height: 95
  },
  logo: {
    top: 3,
    bottom: 10,
    //right: 5,
    //position: 'absolute',
    // width: 150,
    // height: 71,
    alignSelf: 'center'
  },

  textInputUnderline: {
    width: width - 60,
    height: 1.5,
    backgroundColor: '#B2D234',
    marginLeft: 30,
    marginRight: 30
  },

  textLoginContainer: {
    flexDirection: 'row',
    justifyContent: "space-between",
    alignItems: 'center'
  },
  textLoginIcon: {
    //flexGrow: 1
    width: 16,
    height: 20,
    marginTop: 8,
    marginLeft: 35
  }

});

export default LoginPage;
