import React, {Component} from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  Image,
  TouchableHighlight,
  ListView,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  Platform
} from 'react-native';
import DFService from './Service/DFService';
import apiConfig from './Util/Config';
import Util from './Util/Util';
import STRINGS from './Util/strings';
import FormPage from './FormPage';
import ReasonRejected from './ReasonRejected';
import MemberDetail from './MemberDetail';
import AppText from './Util/AppText';

let self;

let Ionicons = require('react-native-vector-icons/Ionicons');
let {height, width} = Dimensions.get('window');

class WorkingDetail extends Component {
  constructor(props) {
    super(props);

    self = this;
    this.isShowAction = true;
    this.isFormWorking = true;

    this.state = {
      isLoading: true,
      formDetail: null,
      formSelect: null,
      currentStep: this.props.formData.form_job_id.current_step,
      indexStep: this.props.formData.form_job_id.current_step - 1,
      showModal: false,
      showModalMember: false,
      alertMsg: '',
      showBtnMember: false
    };

    if ('fromNotification' in this.props) {
      this.isFormWorking = false
    }

    if (this.props.formData.draft_form_id !== undefined && this.props.formData.draft_form_id !== null && this.isFormWorking && this.props.formData.status !== 'INPROGRESS') {
      console.log('SKIPPING TO SUBMIT FORM');
      console.log('this.props.formData: ', this.props.formData);
      console.log('this.isFormWorking: ', this.isFormWorking);
      this.props.navigator.push({
        name: 'submit_form',
        title: this.props.formData.template_name,
        component: FormPage,
        passProps: {
          formPage: true,
          onBack: this._onBack.bind(this),
          formData: this.props.formData,
          refreshWorkingList: this.props.refreshWorkingList.bind(this),
          logout: this.props.logout.bind(this),
          openMainModal: this.props.openMainModal.bind(this)
        }
      });
    } else {
      this._getWorkingDetail();
      if (this.props.status !== this.props.formData.status || this.state.currentStep !== this.props.step) {
        this.isShowAction = false;
      }
      if (this.props.status !== this.props.formData.status || this.state.currentStep !== this.props.step) {
        this.isShowAction = false;
      }
      if (this.props.status === "CANCELED" || this.props.status === "EXPIRED") {
        this.isShowAction = true
      }
    }

    this.isNotDrag = false;
  }

  _closeModalFail = () => {
    console.log('Closing Working Detail Page. Error Occurred');
    self.props.refreshWorkingList();
    self.props.navigator.popToTop(0);
  };

  componentDidMount() {
    self = this;
    clearTimeout(this.timer);
  }

  _onBack = (popToTop = false) => {

    let routes = this.props.navigator.getCurrentRoutes();
    let currentRouteName = routes[routes.length - 1].name;

    try {
      this.props.navigator.refs[currentRouteName].onBackPressed(popToTop)
    } catch (error) {
      if (popToTop) {
        this.props.navigator.popToTop(0)
      } else {
        this.props.navigator.pop()
      }
    }

  };

  async _getWorkingDetail() {
    try {
      self = this;
      let apiData = {};
      let config = {
        method: apiConfig.getWorkinglistDetail.method,
        url: apiConfig.getWorkinglistDetail.url,
        action_name: apiConfig.getWorkinglistDetail.action_name
      };
      config.url += this.props.formData.form_job_id._id;

      DFService.callApiWithHeader(apiData, config, function (error, response) {
        if (response) {
          if (response.resCode === 'DF2000000') {
            // Successfull Response
            console.log("response WorkingDetail = ", response.data);
            self._successGetWorkingDetail(response.data)
          } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
            self.setState({
              isLoading: false
            });
            let callback = self._closeModalFail;
            self.props.openMainModal(response.resMessage, callback)
          } else {
            // Failed Response
            self.setState({isLoading: false});
            self.props.openMainModal(response.resMessage)
          }
        } else {
          // Error from application calling service
          self.setState({isLoading: false});
          console.log("error = ", error);
        }
      });

    } catch (error) {
      self.setState({isLoading: false});
      console.log("error = ", error);
    }

  }

  _successGetWorkingDetail(data) {

    this.setState({
      isLoading: false,
      formSelect: data[this.props.formData.form_job_id.current_step - 1],
      formDetail: data
    });
    console.log('formSelect : ', this.state.formSelect.customer_tokens);
  }

  _onPressAccept() {

    self = this;

    if (this.props.formData.status === 'NEW' || this.props.formData.status === 'REJECTED') {
      console.log('this.state.formSelect : ' + JSON.stringify(this.state.formSelect))
      let apiData = {
        "form_transaction_id": this.state.formSelect._id
      };

      if (!this.isNotDrag) {
        this.isNotDrag = true;

        DFService.callApiWithHeader(apiData, apiConfig.createFormDraft, function (error, response) {
          console.log("createFormDraft response = ", response);
          if (response) {
            if (response.resCode === 'DF2000000') {
              self.isNotDrag = false;
              console.log("draft_form_id = ", response.data.draft_form_id);
              self.props.formData.draft_form_id = response.data.draft_form_id;
              self.props.navigator.push({
                name: 'submit_form',
                title: self.props.formData.template_name,
                component: FormPage,
                passProps: {
                  formPage: true,
                  onBack: self._onBack.bind(self),
                  formData: self.props.formData,
                  refreshWorkingList: self.props.refreshWorkingList.bind(self),
                  draft_form_id: response.data.draft_form_id,
                  logout: self.props.logout.bind(self),
                  openMainModal: self.props.openMainModal.bind(self)
                }
              });
            } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
              self.setState({
                isLoading: false
              });
              let callback = self._closeModalFail;
              self.props.openMainModal(response.resMessage, callback)
            } else {
              self.isNotDrag = false;

              let callback = self._closeModalFail;
              self.props.openMainModal(response.resMessage, callback)
            }

          } else {
            console.log("createFormDraft error = ", error);
          }
        });
      }
    } else {
      self.props.navigator.push({
        name: 'submit_form',
        title: self.props.formData.template_name,
        component: FormPage,
        passProps: {
          formPage: true,
          onBack: self._onBack.bind(self),
          formData: self.props.formData,
          refreshWorkingList: self.props.refreshWorkingList.bind(self),
          logout: self.props.logout.bind(self),
          openMainModal: self.props.openMainModal.bind(self)
        }
      });
    }
  }

  dateDeadline(date, status) {
    if (date !== null && date !== undefined && date !== '') {
      let d = new Date(date);
      return Util.format(d)
    } else {
      return "None"
    }
  }

  validateBtnName(status) {
    console.log('statusstatus', status);
    if (status === 'NEW' || status === 'REJECTED') {
      return STRINGS.POPUP_MSG.BTN_ACCEPT
    } else {
      return STRINGS.POPUP_MSG.BTN_VIEW
    }
  }

  updateSelectWork(index) {
    if (this.state.indexStep === index) {

    } else {
      this.setState({
        isLoading: false,
        formSelect: this.state.formDetail[index],
        indexStep: index
      });
    }
  }

  _openReason() {
    this.setState({showModal: true})
  }

  _closeReason() {
    this.setState({showModal: false})
  }

  _openMember() {
    this.setState({showModalMember: true})
  }

  _closeMember() {
    this.setState({showModalMember: false})
  }

  _getNames() {
    let nameList = [];
    if (this.state.formSelect.user_transaction) {
      for (let i = 0; i < this.state.formSelect.user_transaction.length; i++) {
        if (this.state.formSelect.user_transaction[i].status === 'ACTIVE') {
          nameList.push(this.state.formSelect.user_transaction[i].firstName)
        }
      }
    }

    if (this.state.formSelect.customer_contacts) {
      for (let i = 0; i < this.state.formSelect.customer_contacts.length; i++) {
        if (this.state.formSelect.customer_contacts[i].name) {
          nameList.push(this.state.formSelect.customer_contacts[i].name)
        }
      }
    }

    if (this.state.formSelect.emails) {
      for (let i = 0; i < this.state.formSelect.emails.length; i++) {
        if (this.state.formSelect.emails[i]) {
          nameList.push(this.state.formSelect.emails[i])
        }
      }
    }

    return nameList.join(', ');
  }

  _countUsersActive() {
    let nameList = [];
    for (let i = 0; i < this.state.formSelect.users.length; i++) {
      if (this.state.formSelect.users[i].status === 'ACTIVE') {
        nameList.push(this.state.formSelect.users[i].displayName);
      }
    }

    for (let i = 0; i < this.state.formSelect.customer_contacts.length; i++) {
      nameList.push(this.state.formSelect.customer_contacts[i].name);
    }

    for (let i = 0; i < this.state.formSelect.emails.length; i++) {
      nameList.push(this.state.formSelect.emails[i]);
    }

    return nameList;
  }

  _getUsersList() {
    let objList = [];

    for (let i = 0; i < this.state.formSelect.users.length; i++) {
      if (this.state.formSelect.users[i].status === 'ACTIVE') {
        let obj = {
          type: 'user',
          name: this.state.formSelect.users[i].displayName,
        };
        objList.push(obj);
      }
    }

    for (let i = 0; i < this.state.formSelect.emails.length; i++) {
      let obj = {
        type: 'email',
        name: this.state.formSelect.emails[i],
      };
      objList.push(obj);
    }

    for (let i = 0; i < this.state.formSelect.customer_contacts.length; i++) {
      let obj = {
        type: 'customer',
        name: this.state.formSelect.customer_contacts[i].name,
      };
      objList.push(obj);
    }

    return objList;
  }

  render() {

    if (this.state.isLoading === false) {
      let btnName = this.validateBtnName(this.props.formData.status);
      let last = Util.formatDate(this.props.formData.form_job_id.createdDate);
      let submitted_date = 'None';
      let submitted_by = '-';
      let accepted_by = '-';
      if (this.state.formSelect.submitted_date) {
        submitted_date = Util.formatDate(this.state.formSelect.submitted_date);
      }
      if ('submitted_by' in this.state.formSelect && this.state.formSelect.submitted_by !== null) {
        submitted_by = this.state.formSelect.submitted_by.firstName
      } else if ('submitted_by_customer_token' in this.state.formSelect && this.state.formSelect.submitted_by_customer_token !== null) {
        if ('customer_email' in this.state.formSelect.submitted_by_customer_token) {
          submitted_by = this.state.formSelect.submitted_by_customer_token.customer_email;
        } else {
          submitted_by = this.state.formSelect.submitted_by_customer_token.customer_id.name;
        }
      } else if('acceptedBy' in this.state.formSelect && this.state.formSelect.acceptedBy !== null){
        console.log("ACCEPTED_BY: ", this.state.formSelect.acceptedBy.displayName)
        accepted_by = this.state.formSelect.acceptedBy.displayName;
      }

      var viewWorkingDetail;
      let deadLine = this.dateDeadline(this.state.formSelect.deadline, this.state.formSelect.status);
      let viewStep = [];

      for (let i = 0; i < this.state.formDetail.length; i++) {
        var viewBtn =
          <TouchableHighlight
            style={styles.button}
            underlayColor='#B2D234'
            onPress={() => this._onPressAccept()}>
            <View>
              <AppText style={styles.buttonText}>{btnName}</AppText>
            </View>
          </TouchableHighlight>;
        if (i + 1 <= this.state.formSelect.form_job_id.current_step) {
          if (i === this.state.indexStep) {
            viewStep.push(
              <TouchableHighlight
                key={'btn' + i}
                style={styles.circleCurrentStep}
                underlayColor='#B2D234'
                onPress={() => this.updateSelectWork(i)}>
                <View>
                  <AppText style={{fontSize: 12, color: 'white'}}>
                    {i + 1}
                  </AppText>
                </View>
              </TouchableHighlight>
            )
          } else {
            viewStep.push(
              <TouchableHighlight
                key={'btn' + i}
                style={styles.circleNotCurrentStep}
                underlayColor='#B2D234'
                onPress={() => this.updateSelectWork(i)}>
                <View style={{backgroundColor: 'transparent'}}>
                  <AppText style={{fontSize: 12, color: 'white'}}>
                    {i + 1}
                  </AppText>
                </View>
              </TouchableHighlight>)
          }

        } else {
          if (i === this.state.indexStep) {
            viewStep.push(
              <TouchableHighlight
                key={'btn' + i}
                style={styles.circleCurrentStep}
                underlayColor='#B2D234'
                onPress={() => this.updateSelectWork(i)}>
                <View style={{backgroundColor: 'transparent'}}>
                  <AppText style={{fontSize: 12, color: 'white'}}>
                    {i + 1}
                  </AppText>
                </View>
              </TouchableHighlight>
            )
          } else {
            viewStep.push(
              <TouchableHighlight
                key={'btn' + i}
                style={styles.circleBehindCurrentStep}
                underlayColor='#B2D234'
                onPress={() => this.updateSelectWork(i)}>
                <View style={{backgroundColor: 'transparent'}}>
                  <AppText style={{fontSize: 12, color: '#EF775B'}}>
                    {i + 1}
                  </AppText>
                </View>
              </TouchableHighlight>
            )
          }

        }

        if ((i + 1) < this.state.formDetail.length) {
          viewStep.push(
            <View key={'line' + i} style={styles.lineStep}>
            </View>
          )
        }

      }
      var iconDetail;
      if (this.state.formSelect.status === 'REJECTED' || this.state.formSelect.status === 'CANCELED') {
        iconDetail = <TouchableHighlight onPress={() => this._openReason()}>
          <Ionicons name="ios-information-circle-outline" size={25} color="#EF775B" style={{}}/>
        </TouchableHighlight>
      } else {
        iconDetail = null
      }

      let innerContainerWidth = width - 180;

      viewWorkingDetail =
        <ScrollView
          automaticallyAdjustContentInsets={false}
          horizontal={false}>
          <View style={styles.container}>

            <View style={styles.containerDetail}>
              <View style={{alignItems: 'center', justifyContent: 'center'}}>
                <View style={[styles.circle, {backgroundColor: this.props.formData.user_form_id.category_id.color}]}>
                  <Image source={require('./img/wl_transaction_icon.png')} style={styles.listImage}/>
                </View>
              </View>
              <View
                style={{flexDirection: 'column', paddingLeft: 10, width: width - 90, justifyContent: 'space-between'}}>
                <View style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                  <AppText style={{
                    fontSize: 13,
                    fontWeight: 'bold',
                    marginRight: iconDetail === null ? 0 : 35,
                    color: '#1C1D21'
                  }}>{this.props.formData.template_name}</AppText>

                  <View style={{position: 'absolute', top: 0, right: 10, alignSelf: "flex-end", flexGrow: undefined}}>
                    {iconDetail}
                  </View>
                </View>

                <AppText numberOfLines={2}
                      style={{fontSize: 11, color: '#666666'}}>{this.state.formSelect.user_form_id.description}</AppText>
                <View>
                  <AppText numberOfLines={1} style={{fontSize: 11, color: '#B2D234'}}>
                    {this.props.formData.form_job_id.createdBy.displayName}
                  </AppText>
                  <AppText numberOfLines={1} style={{fontSize: 11, color: '#666666'}}>
                    {last}
                  </AppText>
                </View>
              </View>
            </View>

            <View style={[styles.stepDesciption, {flexWrap: 'wrap'}]}>

              <View style={{paddingTop: 20, paddingLeft: 10, paddingRight: 10, flexDirection: 'column'}}>
                <View style={{flexDirection: 'column'}}>
                  <View style={{
                    paddingTop: 5,
                    paddingBottom: 5,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between'
                  }}>
                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                      <Image source={require('./img/workID.png')} style={{
                        width: 25,
                        height: 25,
                      }}/>
                      <AppText style={{fontSize: 11, paddingLeft: 5, textAlign: 'left'}} numberOfLines={1}>
                        {STRINGS.CUSTOM_MODALS.STEP_ID}
                      </AppText>
                    </View>

                    <AppText
                      numberOfLines={2}
                      style={{
                        paddingLeft: 10,
                        flex: 0.7,
                        fontSize: 11,
                        fontWeight: 'bold',
                        textAlign: 'right',
                        flexWrap: 'wrap'
                      }}>
                      {this.state.formSelect._id}
                    </AppText>
                  </View>
                  <View style={{
                    paddingTop: 5, paddingBottom: 5, flexDirection: 'row', alignItems: 'center',
                    justifyContent: 'space-between'
                  }}>
                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                      <Image source={require('./img/login_user.png')} style={{
                        width: 16,
                        height: 20,
                        marginLeft: 5
                      }}/>
                      <AppText style={{fontSize: 11, paddingLeft: 5, marginLeft: 4}} numberOfLines={1}>
                        {submitted_by != '-' ? STRINGS.CUSTOM_MODALS.SUBMITTED_BY: STRINGS.CUSTOM_MODALS.ACCEPTED_BY }
                      </AppText>
                    </View>

                    <AppText
                      numberOfLines={2}
                      style={{
                        flex: 0.9,
                        paddingLeft: 10,
                        fontSize: 11,
                        fontWeight: 'bold',
                        textAlign: 'right'
                      }}>
                      {submitted_by != '-' ? submitted_by: accepted_by }

                    </AppText>
                  </View>
                  <View style={{
                    paddingTop: 5, paddingBottom: 5, flexDirection: 'row', alignItems: 'center',
                    justifyContent: 'space-between'
                  }}>
                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                      <Image source={require('./img/workDead.png')} style={{
                        width: 25,
                        height: 25
                      }}/>
                      <AppText style={{fontSize: 11, paddingLeft: 5}} numberOfLines={1}>
                        {STRINGS.CUSTOM_MODALS.SUBMITTED_DATE}
                      </AppText>
                    </View>

                    <AppText
                      numberOfLines={2}
                      style={{
                        flex: 0.9,
                        paddingLeft: 10,
                        fontSize: 11,
                        fontWeight: 'bold',
                        color: '#EF775B',
                        textAlign: 'right'
                      }}>
                      {submitted_date}
                    </AppText>
                  </View>
                  <View style={{
                    paddingTop: 5, paddingBottom: 5, flexDirection: 'row', alignItems: 'center',
                    justifyContent: 'space-between'
                  }}>
                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                      <Image source={require('./img/workDead.png')} style={{
                        width: 25,
                        height: 25
                      }}/>
                      <AppText style={{fontSize: 11, paddingLeft: 5}} numberOfLines={1}>
                        {STRINGS.CUSTOM_MODALS.DEADLINE}
                      </AppText>
                    </View>

                    <AppText
                      numberOfLines={2}
                      style={{
                        flex: 0.9,
                        paddingLeft: 10,
                        fontSize: 11,
                        fontWeight: 'bold',
                        color: '#EF775B',
                        textAlign: 'right'
                      }}>
                      {deadLine}
                    </AppText>
                  </View>
                </View>
                <ScrollView
                  style={{flexGrow: 1, height: 40, marginTop: 10}}
                  horizontal={true}>
                  <View style={{flexGrow: 1, flexDirection: 'row', alignItems: 'center'}}>
                    {viewStep}
                  </View>

                </ScrollView>

              </View>
              <View style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingLeft: 20,
                paddingBottom: 20,
              }}>
                <AppText style={{fontSize: 11}}>
                  {STRINGS.CUSTOM_MODALS.MEMBER}
                </AppText>
                <View style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  flexDirection: 'row',
                  paddingLeft: 10,
                  paddingRight: 10,
                  flex: 0.8
                }}>
                  <AppText numberOfLines={2}
                        style={{
                          fontSize: 11,
                          fontWeight: 'bold',
                          alignItems: 'center',
                          alignSelf: 'center',
                          textAlign: 'right',
                          width: width - 170
                        }}>
                    {this._getNames()}
                  </AppText>
                  <TouchableHighlight underlayColor='transparent' style={{paddingLeft: 10}}
                                      onPress={() => this._openMember()}>
                    <Ionicons name="ios-more" size={30} color="gray" style={{}}/>
                  </TouchableHighlight>
                </View>
              </View>
            </View>
            {(this.state.currentStep === this.state.formSelect.step) && this.isShowAction ? viewBtn : <View/>}
          </View>
        </ScrollView>
    }

    return (
      <View style={{flexGrow: 1}}>
        {viewWorkingDetail}
        {this.state.showModal ?
          <ReasonRejected
            title={this.state.formSelect.status === 'REJECTED' ? 'Reason Of Reject' : 'Reason Of Cancel'}
            closeModal={this._closeReason.bind(this)}
            message={this.state.formSelect.message}/> : null}
        {this.state.showModalMember ?
          <MemberDetail
            title={'Step ' + (this.state.indexStep + 1)}
            closeModal={this._closeMember.bind(this)}
            users={this._getUsersList()}
          /> : null}
      </View>
    )
  }

}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    paddingLeft: 17,
    paddingRight: 17,
    paddingTop: 20
  },
  containerDetail: {
    flexDirection: 'row',

  },
  circle: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center'
  },
  listImage: {
    width: 16,
    height: 20
  },
  stepDesciption: {
    marginTop: 30,
    //   backgroundColor: '#12ce89',
    borderColor: 'grey',
    borderWidth: 1,
    flex: 1,
  },
  circleNotCurrentStep: {
    width: 25,
    height: 25,
    borderRadius: 25 / 2,
    backgroundColor: '#B2D234',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 3,
    borderColor: '#34415A'
  },
  circleCurrentStep: {
    width: 32,
    height: 32,
    borderRadius: 32 / 2,
    backgroundColor: '#B2D234',
    alignItems: 'center',
    justifyContent: 'center',
  },
  circleBehindCurrentStep: {
    width: 25,
    height: 25,
    borderRadius: 25 / 2,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 3,
    borderColor: '#34415A'
  },
  lineStep: {
    width: 15,
    height: 3,
    backgroundColor: '#34415A',
  },
  button: {
    width: width - 34,
    height: 36,
    flexDirection: 'row',
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    borderWidth: 1,
    borderRadius: 2,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },


})

export default WorkingDetail;
