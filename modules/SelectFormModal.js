import React, {Component} from 'react';
import {Dimensions, Image, StyleSheet, TouchableHighlight, View} from 'react-native';
import STRINGS from './Util/strings'
import AppText from './Util/AppText';

var Ionicons = require('react-native-vector-icons/Ionicons');
var {height, width} = Dimensions.get('window');

class SelectFormModal extends Component {
  constructor(props) {
    super(props);
    console.log('PropsModal: ', props)
    this.state = {
      headerTitle: STRINGS.CUSTOM_MODALS.CREATE_WORK_HEADER,
    };
  }

  nextstep(num) {
    this.props.nextPage(num)
  }

  render() {
    return (<View style={styles.container}>
      <View style={{flexGrow: 1}}>
        <AppText style={styles.headerText}>
          {STRINGS.SUBMIT_ASSIGN.SUB_HEADING}
        </AppText>
        <AppText style={styles.bodyText}>
          {STRINGS.SUBMIT_ASSIGN.SUB_HEADING_2}
        </AppText>
        <View style={styles.containerChoice}>
          <View style={{flexDirection: 'row', flexGrow: 5}}>
            <View style={{justifyContent: 'center', flexGrow: 9}}>
              <TouchableHighlight
                underlayColor='transparent'
                onPress={() => this.nextstep(2)}>
                <View style={styles.circleLeft}>
                  <Image source={require('./img/create_work_icon.png')}
                         style={{alignItems: 'center', justifyContent: 'center', height: 50, width: 40}}/>
                </View>
              </TouchableHighlight>
            </View>
            <View style={{flexGrow: 0.2, backgroundColor: 'grey'}}>
            </View>
            <View style={{justifyContent: 'center', flexGrow: 9}}>
              <TouchableHighlight
                underlayColor='transparent'
                onPress={() => this.nextstep(4)}>
                <View style={styles.circleRight}>
                  <Image source={require('./img/assign_icon.png')}
                         style={{alignItems: 'center', justifyContent: 'center', height: 50, width: 40}}/>
                </View>
              </TouchableHighlight>
            </View>
          </View>
          <View style={{flexDirection: 'row', flexGrow: 2, marginTop: 10}}>
            <View style={{flexGrow: 1}}>
              <AppText style={{fontWeight: 'bold', justifyContent: 'center', alignSelf: 'center', marginRight: 30}}>
                {STRINGS.CUSTOM_MODALS.CREATE_WORK_LBL}
              </AppText>
            </View>
            <View style={{flexGrow: 1}}>
              <AppText style={{fontWeight: 'bold', justifyContent: 'center', alignSelf: 'center', marginLeft: 30}}>
                {STRINGS.CUSTOM_MODALS.ASSIGN_LBL}
              </AppText>
            </View>
          </View>
        </View>
      </View>

      <View style={styles.bottomContainer}>
        <AppText style={styles.stepCounter}>
          {STRINGS.formatString(STRINGS.COMMON.STEP, 1)}
        </AppText>
      </View>

    </View>)
  }
}

var styles = StyleSheet.create({
  container: {
    height: height - 180,
    flexGrow: 1,
    paddingLeft: 13,
    paddingRight: 13,
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  headerText: {
    marginTop: 18,
    fontWeight: 'bold',
    fontSize: 15,
  },
  bodyText: {
    marginTop: 9,
    fontSize: 14,
    color: 'grey'
  },
  containerChoice: {
    marginTop: 80,
    height: 130,
    flexDirection: 'column'
  },
  circleLeft: {
    width: 85,
    height: 85,
    borderRadius: 85 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ee7155',
    alignSelf: 'center',
    marginRight: 30
  },
  circleRight: {
    width: 85,
    height: 85,
    borderRadius: 85 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000000',
    alignSelf: 'center',
    marginLeft: 30
  },
  footer: {
    backgroundColor: '#FFFFFF',
    height: 30,
    position: 'absolute',
    flexDirection: 'row',
    bottom: 0,
  },
  bottomContainer: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
  stepCounter: {
    justifyContent: 'center',
  },
});

export default SelectFormModal;
