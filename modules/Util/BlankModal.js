import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    Button,
    TouchableHighlight,
    Platform,
    Modal,
    Dimensions
} from 'react-native';

import DFButton from '../FormComponents/DFButton'

var Ionicons = require('react-native-vector-icons/Ionicons');
var {height, width} = Dimensions.get('window');

class BlankModal extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        var modal =
            <Modal
                animationType='fade'
                transparent={true}
                visible={true}
                onRequestClose={ () => console.log('Modal Has Been Closed') }>
                {this.props.children}
            </Modal>

        return (
            modal
        )
    }
}


export default BlankModal;
