import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Button,
  TouchableHighlight,
  Platform,
  Modal,
  Dimensions
} from 'react-native';

import SelectForm from '../SelectFormModal'
import SelectFormList from '../SelectFormListModal'
import StartFormSelected from '../StartFormSelected'
import AssignUserList from '../AssignUserList'
import SubmitAssignUser from '../SubmitAssignUser'
import STRINGS from './strings';
import AppText from './AppText'

var Ionicons = require('react-native-vector-icons/Ionicons');
var {height, width} = Dimensions.get('window');

var self;

class CustomModalContent extends Component {
  constructor(props) {
    super(props);
    self = this
    this.state = {
      // headerTitle: this.props.headerTitle,
      stepWorking: this.props.stepPage,
      dataFormCreate: {},
      userList: {}
      // createWorkData : {}
    }
  }

  nextPageWorking(num) {
    this.setState({stepWorking: num})
  }

  formCreate(form, num) {
    this.setState
    ({
      dataFormCreate: form,
      stepWorking: num
    })
  }

  backPress(stepPage, dataForm) {
    if (stepPage == 5) {
      this.setState
      ({
        stepWorking: stepPage,
      })
    } else {
      this.setState
      ({
        stepWorking: stepPage,
        dataFormCreate: dataForm
      })
    }
  }

  assignForm(userList, dataForm) {
    this.setState
    ({
      userList: userList,
      dataFormCreate: dataForm
    })
  }

  render() {
    var page;
    var headerTitle;
    var iconBack;

    if (this.state.stepWorking == 1) {
      page = <SelectForm nextPage={this.nextPageWorking.bind(this)}/>
      headerTitle = STRINGS.CUSTOM_MODALS.CREATE_WORK_HEADER
      iconBack = <View style={{marginLeft: 13}}/>
    } else if (this.state.stepWorking == 2) {
      page = <SelectFormList createWorkModal={this.props.createWork.bind(this)} closeModal={this.props.closeModal}
                             nextToStep={3} dataFormCreate={this.state.dataFormCreate}
                             nextPage={this.formCreate.bind(this)}
                             openMainModal={(message) => self.props.openMainModal(message)}
                             toggleMainSpinner={this.props.toggleMainSpinner.bind(this)}/>
      headerTitle = STRINGS.CUSTOM_MODALS.SELECT_FORM_HEADER
      iconBack = <TouchableHighlight underlayColor='transparent' onPress={() => this.backPress(1, {})}
                                     style={{flexGrow: 1, marginLeft: 13}}>
        <Ionicons name="ios-arrow-back" size={25} color="black"
                  style={{justifyContent: 'center', alignItems: 'center'}}/>
      </TouchableHighlight>
    } else if (this.state.stepWorking == 3) { // Not use in this version
      page = <StartFormSelected createWorkModal={this.props.createWork.bind(this)} closeModal={this.props.closeModal}
                                getForm={this.state.dataFormCreate}
                                openMainModal={(message) => self.props.openMainModal(message)}/>
      headerTitle = STRINGS.CUSTOM_MODALS.COMPLETE_HEADER
      iconBack = <TouchableHighlight underlayColor='transparent' onPress={() => this.backPress(2, {})}
                                     style={{flexGrow: 1, marginLeft: 13}}>
        <Ionicons name="ios-arrow-back" size={25} color="black"
                  style={{justifyContent: 'center', alignItems: 'center'}}/>
      </TouchableHighlight>
    } else if (this.state.stepWorking == 4) { //Step 2 for assign
      page = <SelectFormList nextToStep={5} dataFormCreate={this.state.dataFormCreate}
                             nextPage={this.formCreate.bind(this)}
                             openMainModal={(message) => self.props.openMainModal(message)}
                             toggleMainSpinner={this.props.toggleMainSpinner.bind(this)}/>
      headerTitle = STRINGS.CUSTOM_MODALS.SELECT_FORM_HEADER
      iconBack = <TouchableHighlight underlayColor='transparent' onPress={() => this.backPress(1, {})}
                                     style={{flexGrow: 1, marginLeft: 13}}>
        <Ionicons name="ios-arrow-back" size={25} color="black"
                  style={{justifyContent: 'center', alignItems: 'center'}}/>
      </TouchableHighlight>
    } else if (this.state.stepWorking == 5) { //Step 3 for assign - Last Step assign
      page = <AssignUserList createWorkModal={this.props.createWork.bind(this)} closeModal={this.props.closeModal}
                             getForm={this.state.dataFormCreate} assignForm={this.assignForm.bind(this)}
                             nextPage={this.nextPageWorking.bind(this)}
                             openMainModal={(message) => self.props.openMainModal(message)}/>
      headerTitle = STRINGS.CUSTOM_MODALS.ASSIGN_HEADER
      iconBack = <TouchableHighlight underlayColor='transparent' onPress={() => this.backPress(4, {})}
                                     style={{flexGrow: 1, marginLeft: 13}}>
        <Ionicons name="ios-arrow-back" size={25} color="black"
                  style={{justifyContent: 'center', alignItems: 'center'}}/>
      </TouchableHighlight>
    } else if (this.state.stepWorking == 6) { //Step done for assign
      page = <SubmitAssignUser createWorkModal={this.props.createWork.bind(this)} assignUser={this.state.userList}
                               closeModal={this.props.closeModal} getForm={this.state.dataFormCreate}
                               openMainModal={(message) => self.props.openMainModal(message)}/>
      headerTitle = STRINGS.CUSTOM_MODALS.COMPLETE_HEADER
      iconBack = <TouchableHighlight underlayColor='transparent' onPress={() => this.backPress(5, {})}
                                     style={{flexGrow: 1, marginLeft: 13}}>
        <Ionicons name="ios-arrow-back" size={25} color="black"
                  style={{justifyContent: 'center', alignItems: 'center'}}/>
      </TouchableHighlight>
    }

    var modal =
      <View style={styles.container}>
        <View style={styles.innerContainer}>
          <View style={styles.headerModal}>
            {iconBack}
            <AppText style={styles.textHeaderModal}>
              {headerTitle}
            </AppText>
            <TouchableHighlight style={{marginRight: 13}}
                                underlayColor='transparent'
                                onPress={this.props.closeModal}>
              <Ionicons name="md-close" size={25} color="black"
                        style={{justifyContent: 'center', alignItems: 'center'}}/>
            </TouchableHighlight>
          </View>
          <View style={{flexGrow: 9}}>
            {page}
          </View>
        </View>
      </View>

    return (modal)
  }
}

var styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent: 'center',
    padding: 20,
    zIndex: 99
  },
  headerModal: {
    paddingTop:10,
    paddingBottom:10,
    flexGrow: 1,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#efeff1'
  },
  textHeaderModal: {
    fontWeight: 'bold',
    fontSize: 18,
    flexGrow: 8
  },
  innerContainer: {
    height: height - 100,
    width: width - 40,
    // alignItems: 'center',
    backgroundColor: '#ffffff'
  },
  row: {
    alignItems: 'center',
    flexGrow: 1,
    flexDirection: 'row',
    marginBottom: 20,
  },
  rowTitle: {
    flexGrow: 1,
    fontWeight: 'bold',
  },
  button: {
    borderRadius: 5,
    flexGrow: 1,
    height: 44,
    alignSelf: 'stretch',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  buttonText: {
    fontSize: 18,
    margin: 5,
    textAlign: 'center',
  },
  modalButton: {
    marginTop: 10,
  },
});

export default CustomModalContent;
