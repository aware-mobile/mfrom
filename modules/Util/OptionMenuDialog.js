import React, { Component } from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Image,
  Button,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Platform,
  Modal,
  Dimensions,
  AsyncStorage
} from 'react-native';

import DFButton from '../FormComponents/DFButton'
import ProgressSpinner from '../Animation/ProgressSpinner'
import DFService from '../Service/DFService';
import AppText from './AppText'

const Timer = require('react-native-timer');

import apiConfig from './Config';
import dismissKeyboard from 'react-native-dismiss-keyboard';

var Ionicons = require('react-native-vector-icons/Ionicons');
var {height, width} = Dimensions.get('window');

class OptionMenuDialog extends Component {
  constructor(props) {
    super(props);

    let form_job_id = this.props.optionMenuFormJobId ? this.props.optionMenuFormJobId : null;
    let transaction_id = this.props.optionMenuTransId ? this.props.optionMenuTransId : null;
    let flow_id = this.props.optionMenuFlowId ? this.props.optionMenuFlowId : null;
    let current_step = this.props.optionMenuCurrentStep ? this.props.optionMenuCurrentStep : null;

    let viewType = this.props.viewType ? this.props.viewType : 'workingList';

    if (current_step !== null) {
      current_step = parseInt(current_step)
    }

    console.log('CURRENT STEP: ', current_step)

    this.state = {
      isLoading: false,
      showRejectMessage: false,
      viewType,
      form_job_id,
      transaction_id,
      flow_id,
      current_step,
      rejectText: '',
      showConfirmDialog: false,
      confirmMessage: STRINGS.POPUP_MSG.CONFIRM_REQUEST,
      showErr: false,
      doneSaving: false,
      saveSuccess: false,
      spinnerText: STRINGS.COMMON.PROCESSING,
      confirmType: null,
      typeReason: ''
    }
  }

  componentWillUnmount() {
    Timer.clearTimeout(this);
  }

  render() {

    let rejectBtn = <TouchableHighlight
      underlayColor='#4E949C'
      style={[styles.actionButton, { backgroundColor: '#5AAAB3' }]}
      onPress={() => this.showRejectMessage('REJECT')}>
      <View style={{ alignItems: 'center', flexDirection: 'column' }}>
        <Image source={require('../img/reject_icon.png')} style={styles.actionIcon} />
        <AppText style={styles.text}>
          {STRINGS.POPUP_MSG.BTN_REJECT}
        </AppText>
      </View>
    </TouchableHighlight>

    let deleteBtn = <TouchableHighlight
      underlayColor='#B04146'
      style={[styles.actionButton, { backgroundColor: '#D14E53' }]}
      onPress={() => this.showRejectMessage('DELETE')}>
      <View style={{ alignItems: 'center', flexDirection: 'column' }}>
        <Image source={require('../img/delete_transaction_icon.png')} style={styles.actionIcon} />
        <AppText style={styles.text}>
          {STRINGS.POPUP_MSG.BTN_DELETE}
        </AppText>
      </View>
    </TouchableHighlight>


    var draftList_view =
      <View style={styles.innerContainer}>
        <View>
          <View style={{ flexGrow: 1, flexDirection: 'row', justifyContent: 'center' }}>
            {(this.state.current_step > 1) ? rejectBtn : null}
            <TouchableHighlight
              underlayColor='#3F7F89'
              style={[styles.actionButton, { backgroundColor: '#4D99A6' }]}
              onPress={() => this.confirmDialog('CANCEL')}>
              <View style={{ alignItems: 'center', flexDirection: 'column' }}>
                <Image source={require('../img/cancel_draft_icon.png')} style={styles.cancel_draft_icon} />
                <AppText style={styles.text}>
                  {STRINGS.POPUP_MSG.BTN_CANCEL}
                </AppText>
              </View>
            </TouchableHighlight>
            {deleteBtn}
          </View>
        </View>
      </View>

    var workingList_view =
      <View style={styles.innerContainer}>
        <View>
          <View style={{ flexGrow: 1, flexDirection: 'row', justifyContent: 'center' }}>
            {(this.state.current_step > 1) ? rejectBtn : null}
            {deleteBtn}
          </View>
        </View>
      </View>

    let actionViews = (this.state.viewType == 'workingList') ? workingList_view : draftList_view

    let rejectMessage = <View style={styles.innerContainerBackground}>
      <View style={{ padding: 15 }}>
        <AppText style={{ fontSize: 16, paddingLeft: 10, marginBottom: 10, marginTop: 5 }}>{this.state.typeReason == 'DELETE' ? STRINGS.POPUP_MSG.REASON_OF_DELETE : STRINGS.POPUP_MSG.REASON_OF_REJECT} </AppText>
        <View style={styles.borderView}>
          <TextInput
            multiline={true}
            style={styles.textArea}
            maxLength={100}
            onChangeText={(text) => this.setState({ rejectText: text })}
            value={this.state.rejectText}
            />
        </View>
        <View style={{ flexGrow: 1, flexDirection: 'row', alignSelf: "flex-end", marginTop: 10, marginBottom: 10, paddingRight: 10 }}>
          <TouchableHighlight
            onPress={() => this.cancel()}
            underlayColor='#dddddd'
            style={styles.cancelBtn}>
            <View>
              <AppText style={{ color: '#000000' }}>
                {STRINGS.POPUP_MSG.BTN_CANCEL}
              </AppText>
            </View>
          </TouchableHighlight>

          <TouchableHighlight
            onPress={() => this.confirmDialog(this.state.typeReason)}
            underlayColor='#dddddd'
            style={(this.state.rejectText != '') ? styles.okBtn : styles.inactiveOkBtn}>
            <View>
              <AppText style={{ color: '#ffffff' }}>
                {STRINGS.POPUP_MSG.BTN_OK}
              </AppText>
            </View>
          </TouchableHighlight>

        </View>
      </View>
    </View>

    let confirmDialog = <View style={styles.innerContainerBackground}>
      <AppText style={{ padding: 10, marginBottom: 5 }}>{this.state.confirmMessage}</AppText>
      <View style={{ flexGrow: 1, flexDirection: 'row', alignSelf: "flex-end", margin: 20 }}>
        <TouchableHighlight
          onPress={() => this.no()}
          underlayColor='#dddddd'
          style={styles.cancelBtn}>
          <View>
            <AppText style={{ color: '#000000' }}>
              {STRINGS.POPUP_MSG.BTN_CANCEL}
            </AppText>
          </View>
        </TouchableHighlight>

        <TouchableHighlight
          onPress={() => this.yes()}
          underlayColor='#dddddd'
          style={styles.okBtn}>
          <View>
            <AppText style={{ color: '#ffffff' }}>
              {STRINGS.POPUP_MSG.BTN_OK}
            </AppText>
          </View>
        </TouchableHighlight>

      </View>
    </View>

    let spinner =
      <View style={styles.container}>
        <ProgressSpinner
          setSuccess={this.state.doneSaving}
          isSuccessfull={this.state.saveSuccess}
          spinnerText={this.state.spinnerText}
          />
      </View>;

    let returnView = actionViews;
    returnView = (this.state.showRejectMessage) ? rejectMessage : returnView
    returnView = (this.state.showConfirmDialog) ? confirmDialog : returnView
    returnView = (this.state.isLoading) ? spinner : returnView

    returnView = <View>{returnView}</View>

    console.log('showMessage : ', this.state.showRejectMessage)

    let mainDialogView = (this.state.showRejectMessage || this.state.showConfirmDialog) ?
      <TouchableHighlight
        underlayColor='rgba(0, 0, 0, 0.8)'
        onPress={() => dismissKeyboard()}
        style={styles.container}>

        {returnView}

      </TouchableHighlight> : <TouchableHighlight
        underlayColor='rgba(0, 0, 0, 0.8)'
        onPress={() => this.closeModal()} style={styles.container}>

        {returnView}

      </TouchableHighlight>




    var modal =
      <Modal
        animationType='fade'
        transparent={true}
        visible={true}
        style={{ backgroundColor: "transparent" }}
        onRequestClose={() => console.log('Modal Has Been Closed')}>

        {(this.state.isLoading) ? spinner : mainDialogView}

      </Modal>

    return (modal)
  }

  closeModal() {
    if (!this.state.isLoading) {
      this.props.closeModal()
    }
  }

  showRejectMessage(type) {
    this.setState({ showRejectMessage: true, typeReason: type })
  }

  cancel() {
    this.setState({
      showRejectMessage: false,
      rejectText: ''
    })
  }

  confirmDialog(type) {
    let msg = STRINGS.POPUP_MSG.ARE_YOU_SURE
    // if(type == 'DELETE')msg = 'Are you sure you want to delete this transaction?'
    if (type == 'CANCEL') {
      msg = STRINGS.POPUP_MSG.CANCEL_DRAFT
      this.setState({
        confirmMessage: msg,
        confirmType: type,
        showConfirmDialog: true
      })
    }

    if (type == 'DELETE') {
      msg = STRINGS.WORKING_LIST_PAGE.DELETE_TRANS
      if (this.state.rejectText == '') {
        return
      } else {
        this.setState({
          confirmMessage: msg,
          confirmType: type,
          showConfirmDialog: false,
          isLoading: true
        })
        this.deleteTransaction()
      }
    }

    if (type == 'REJECT') {
      msg = STRINGS.WORKING_LIST_PAGE.REJECT_FORM
      if (this.state.rejectText == '') {
        return
      } else {
        this.setState({
          confirmMessage: msg,
          confirmType: type,
          showConfirmDialog: false,
          isLoading: true
        })
        this.rejectForm()
      }
    }
  }

  // User has confirmed their request
  yes() {
    this.setState({ isLoading: true })
    // if (this.state.confirmType == 'REJECT') {
    //   this.rejectForm()
    // }
    // if (this.state.confirmType == 'DELETE') {
    //   this.deleteTransaction()
    // }
    if (this.state.confirmType == 'CANCEL') {
      this.cancelDraft()
    }
  }

  no() {
    this.setState({ showConfirmDialog: false })
  }

  confirm() {
    this.setState({ isLoading: true })
  }

  rejectForm() {
    let self = this
    var rejectBody = {
      "form_job_id": this.state.form_job_id,
      "reject_msg": this.state.rejectText,
      "current_step": this.state.current_step
    };
    DFService.callApiWithHeader(rejectBody, apiConfig.rejectForm, function (error, response) {

      if (response) {
        if (response.resCode == 'DF2000000') {
          // Successfull Response
          if (self.state.viewType != 'workingList') {
            self.cancelDraft()
          } else {
            self.setState({
              saveSuccess: true,
              doneSaving: true,
              spinnerText: 'Success!'
            }, () => Timer.setTimeout(
              self, 'closeModal', () => self.props.closeModal(true), 2000
            ));
          }
        } else {
          // Failed Response
          self.setState({
            saveSuccess: false,
            doneSaving: true,
            spinnerText: "Failed"
          }, () => Timer.setTimeout(
            self, 'openMainModal', () => self.props.openMainModal(response.resMessage), 1000
          ));


        }
      } else {
        // Error from application calling service
        self.setState({
          saveSuccess: false,
          doneSaving: true,
          spinnerText: "Failed"
        });
        console.log("error = ", error);
      }

    });
  }

  deleteTransaction() {
    let self = this
    var config = JSON.parse(JSON.stringify(apiConfig.cancelForm));
    var cancelBody = {
      "form_job_id": this.state.form_job_id,
      "cancel_msg": this.state.rejectText,
      "current_step": this.state.current_step
    };
    DFService.callApiWithHeader(cancelBody, config, function (error, response) {

      if (response) {
        if (response.resCode == 'DF2000000') {
          // Successfull Response
          if (self.state.viewType != 'workingList') {
            self.cancelDraft()
          } else {
            self.setState({
              saveSuccess: true,
              doneSaving: true,
              spinnerText: 'Success!'
            }, () => Timer.setTimeout(
              self, 'closeModal', () => self.props.closeModal(true), 2000
            ));
          }
        } else {
          // Failed Response
          self.setState({
            saveSuccess: false,
            doneSaving: true,
            spinnerText: "Failed"
          }, () => Timer.setTimeout(
            self, 'openMainModal', () => self.props.openMainModal(response.resMessage), 1000
          ));
        }
      } else {
        // Error from application calling service
        self.setState({
          saveSuccess: false,
          doneSaving: true,
          spinnerText: "Failed"
        });
        console.log("error = ", error);
      }

    });
  }

  cancelDraft() {
    let self = this
    var config = JSON.parse(JSON.stringify(apiConfig.cancelDraftForm));
    var apiData = {};
    config.url += this.state.transaction_id;
    DFService.callApiWithHeader(apiData, config, function (error, response) {

      if (response) {
        if (response.resCode == 'DF2000000') {
          // Successfull Response
          self.setState({
            saveSuccess: true,
            doneSaving: true,
            spinnerText: 'Success!'
          }, () => Timer.setTimeout(
            self, 'closeModal', () => self.props.closeModal(true), 2000
          ));
        } else {
          // Failed Response
          self.setState({
            saveSuccess: false,
            doneSaving: true,
            spinnerText: "Failed"
          }, () => Timer.setTimeout(
            self, 'openMainModal', () => self.props.openMainModal(response.resMessage), 1000
          ));
        }
      } else {
        // Error from application calling service
        self.setState({
          saveSuccess: false,
          doneSaving: true,
          spinnerText: "Failed"
        });
        console.log("error = ", error);
      }

    });
  }

}

var styles = StyleSheet.create({
  text: {
    color: '#FFFFFF'
  },
  container: {
    flexGrow: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    justifyContent: 'center',
    padding: 30
  },
  headerModal: {
    height: 60,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#efeff1'
  },
  textHeaderModal: {
    marginLeft: 20,
    fontWeight: 'bold',
    fontSize: 18,
  },
  // innerContainer: {
  //   backgroundColor: '#ffffff'
  // },
  rowContainer: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center'
  },
  row: {
    alignItems: 'center',
    flexGrow: 1,
    flexDirection: 'row',
    marginBottom: 20,
  },
  innerContainer: {
    width: width - 60,
    backgroundColor: 'rgba(0, 0, 0, 0)',
    paddingTop: 10,
    paddingBottom: 10,
    borderRadius: 4
  },
  innerContainerBackground: {
    width: width - 60,
    backgroundColor: '#FFFFFF',
    paddingTop: 10,
    paddingBottom: 2,
    borderRadius: 8
  },
  actionButton: {
    width: 80,
    height: 80,
    justifyContent: 'center'
  },
  actionIcon: {
    width: 30,
    height: 30,
    marginBottom: 8
  },
  cancel_draft_icon: {
    width: 30,
    height: 31,
    marginBottom: 8
  },
  textArea: {
    height: 90,
    paddingTop: 4,
    flexGrow: 4,
    fontSize: 20,
    color: '#49621c',
    textAlignVertical: 'top',
    backgroundColor: '#ffffff'
  },
  borderView: {
    marginRight: 5,
    marginLeft: 5,
    marginBottom: 10,
    padding: 5,
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 8,
  },
  img: {
    width: 30,
    height: 30
  },
  okBtn: {
    width: 80,
    height: 36,
    backgroundColor: '#B2D234',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#B2D234',
    borderWidth: 1,
    borderRadius: 4,
  },
  inactiveOkBtn: {
    width: 80,
    height: 36,
    backgroundColor: '#90A3AE',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#90A3AE',
    borderWidth: 1,
    borderRadius: 4,
  },
  cancelBtn: {
    width: 80,
    height: 36,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#000000',
    borderWidth: 1,
    borderRadius: 4,
    marginRight: 10,
  },
});

export default OptionMenuDialog;
