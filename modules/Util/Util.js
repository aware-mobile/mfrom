'use strict';

let {NativeModules, Dimensions, Platform} = require('react-native');
let DeviceUtil = NativeModules.DeviceUtil;

let isPhone = false;
export default class Util {

  static isPhone() {
    // console.log('isPhone method');
    // console.log('DeviceUtil.model: ', DeviceUtil.model)
    // console.log('DeviceUtil.model.indexOf(iPhone)', DeviceUtil.model.indexOf('iPhone'))
    if (Platform.OS === 'ios') {
      return DeviceUtil.model.indexOf('iPhone') > -1 || DeviceUtil.model.indexOf('iPod') > -1;
    }
    return isPhone;
  }

  static setPhone(phone) {
    isPhone = phone;
  }

  static formatDate(date) {
    let createdDate = new Date(date);

    return this.pad(createdDate.getDate()) + '-'
      + this.pad((createdDate.getMonth() + 1)) + '-'
      + createdDate.getFullYear() + ', ' + this.pad(createdDate.getHours()) + ':' + this.pad(createdDate.getMinutes()) + ':' + this.pad(createdDate.getSeconds());
  }

  static formatTime(date) {
    let createdDate = new Date(date);

    return this.pad(createdDate.getHours()) + ':' + this.pad(createdDate.getMinutes()) + ':' + this.pad(createdDate.getSeconds());
  }

  static fnFormateTime(time, formatTime, date = null) {

    let hour = '';
    let minute = '';

    if (time === '') {
      if(date === null){
        date = new Date();
      }
      hour = date.getHours();
      minute = date.getMinutes();
    }

    let timeString = "";

    if (formatTime === 'AM/PM') {
      let format = (hour < 12) ? 'AM' : 'PM';
      if (hour > 12) {
        hour = hour - 12;
      }
      if (hour === 0) {
        hour = 12;
      }
      timeString = this.pad(hour) + ':' + this.pad(minute) + ' ' + format;
    }
    if (formatTime === 'HH:MM') {
      timeString = this.pad(hour) + ':' + this.pad(minute)
    }
    return timeString;
  }

  static formateTimeValue(time, formatTime, date = null) {

    let hour = '';
    let minute = '';

    if (time === '') {
      if(date === null){
        date = new Date();
      }
      hour = date.getHours();
      minute = date.getMinutes();
    }

    time = this.pad(hour) + ':' + this.pad(minute);

    return time;
  }

  static fnFormateDate(date, formatDate) {

    const day = date.getDate();
    const monthIndex = date.getMonth() + 1;
    const year = date.getFullYear();
    let dateString = "";

    if (formatDate === 'DD/MM/YYYY') {
      dateString = this.pad(day) + '/' + this.pad(monthIndex) + '/' + year;
    }
    if (formatDate === 'MM/DD/YYYY') {
      dateString = this.pad(monthIndex) + '/' + this.pad(day) + '/' + year;
    }
    if (formatDate === 'YYYY/MM/DD') {
      dateString = year + '/' + this.pad(monthIndex) + '/' + this.pad(day);
    }

    return dateString;
  }

  static format(date) {
    return this.pad(date.getDate()) + '-'
      + this.pad((date.getMonth() + 1)) + '-'
      + date.getFullYear() + ', ' + this.pad(date.getHours()) + ':' + this.pad(date.getMinutes()) + ':' + this.pad(date.getSeconds());
  }

  static pad(num) {
    if (num < 10) {
      return "0" + num;
    } else {
      return "" + num;
    }
  }

  static validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  static transactionIdTokenGenerator(access_token) {

    String.prototype.shuffle = function () {
      var a = this.split(""),
        n = a.length;

      for (var i = n - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
      }
      return a.join("");
    }

    var date = new Date();
    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;
    var min = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;
    var sec = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;
    var minSec = date.getMilliseconds();
    if (minSec < 10) {
      "00" + minSec;
    }
    if (minSec < 100) {
      "0" + minSec;
    }
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;
    var day = date.getDate();
    day = (day < 10 ? "0" : "") + day;
    var datetime = year + month + day + hour + min + sec + minSec


    return (access_token + '' + datetime).shuffle();
  }

  static isInternationalPhoneNoCorrect(phoneNo) {
    //let regex = /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|x)[\-\.\ \\\/]?(\d+))?$/i;
    let regex = /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){1,})(?:[\-\.\ \\\/]?(?:#|ext\.?|x)[\.\ \\\/]?(\d+))?$/i;
    let match = phoneNo.match(regex);
    if (match) {
      return true;
    } else {
      return false;
    }
  }

  static checkThaiCitizenIdCard(citizenId) {

    if (citizenId && citizenId.length === 13) {
      let sum = 0;
      for (let i = 0; i < 12; i++) {
        sum += parseInt(citizenId.charAt(i)) * (13 - i)
      }

      console.log('sum :' + sum);
      sum = sum % 11;
      console.log('sum%11 :' + sum);

      sum = 11 - sum;
      console.log('11-sum :' + sum);
      //console.log('sum :'+sum)
      return parseInt(citizenId.charAt(12)) === (sum % 10);
    } else {
      return false
    }
  }

  static validatePassword(password) {
    let re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*_-])[a-zA-Z0-9!@#$%^&*_-]{8,20}$/;
    return re.test(password);
  }
}
