import LocalizedStrings from 'react-native-localization';

import locale_en from '../../languages/locale_en.json';
import locale_th from '../../languages/locale_th.json';

let Strings = new LocalizedStrings({
  en:locale_en,
  th:locale_th
});


module.exports = Strings;
