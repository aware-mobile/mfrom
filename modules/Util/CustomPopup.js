import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    Button,
    Platform,
    Modal,
    Dimensions
} from 'react-native';
import AppText from './AppText'
var Ionicons = require('react-native-vector-icons/Ionicons');
var {height, width} = Dimensions.get('window');

class CustomPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            headerTitle: this.props.headerTitle,
        }
    }

    render() {
        var modal
            modal =
                <Modal
                    animationType='fade'
                    transparent={true}
                    visible={true}
                    onRequestClose={ () => console.log('Modal Has Been Closed') }>
                    <View style={styles.container}>
                        <View style={styles.innerContainer}>
                            <View style={styles.headerModal}>

                                <AppText style={styles.textHeaderModal}>
                                    {this.state.headerTitle}
                                </AppText>

                                <AppText style={{ flexGrow: 1 }}
                                onPress={this.props.closeModal}>
                                    <Ionicons name="ios-close" size={30} color="black" style={{ justifyContent: 'center', alignItems: 'center'}}/>
                                </AppText>
                            </View>
                            <View style={{ flexGrow: 1 }}>
                                {this.props.children}
                            </View>
                        </View>
                    </View>

                </Modal>
        // } else {
        //     modal = <Modal
        //             animationType='fade'
        //             transparent={false}
        //             visible={false}
        //             // onRequestClose={() => { this._setModalVisible(false) } }
        //             >
        //             </Modal>
        // }

        return (
            modal
        )
    }
}

var styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        justifyContent: 'center',
        padding: 20
    },
    headerModal: {
        height: 60,
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#efeff1'
    },
    textHeaderModal: {
        marginLeft:15,
        fontWeight: 'bold',
        fontSize: 18,
        flexGrow: 8
    },
    innerContainer: {
        height: height - 100,
        width: width - 40,
        // alignItems: 'center',
        backgroundColor: '#ffffff'
    },
    row: {
        alignItems: 'center',
        flexGrow: 1,
        flexDirection: 'row',
        marginBottom: 20,
    },
    rowTitle: {
        flexGrow: 1,
        fontWeight: 'bold',
    },
    button: {
        borderRadius: 5,
        flexGrow: 1,
        height: 44,
        alignSelf: 'stretch',
        justifyContent: 'center',
        overflow: 'hidden',
    },
    buttonText: {
        fontSize: 18,
        margin: 5,
        textAlign: 'center',
    },
    modalButton: {
        marginTop: 10,
    },
});

export default CustomPopup;
