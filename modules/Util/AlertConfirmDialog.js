import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Button,
  TouchableHighlight,
  Platform,
  Modal,
  Dimensions,
  AsyncStorage
} from 'react-native';

import DFButton from '../FormComponents/DFButton'
import STRINGS from './strings'
import AppText from './AppText'

var Ionicons = require('react-native-vector-icons/Ionicons');
var {height, width} = Dimensions.get('window');

class AlertConfirmDialog extends Component {
  constructor(props) {
    super(props);

    console.log('Alert Confirm Dialog Loaded!')

    let confirm_msg = this.props.confrimMessage ? this.props.confrimMessage : null;
    let alert_msg = this.props.alertMessage ? this.props.alertMessage : null;
    let ok_txt = this.props.okText ? this.props.okText : STRINGS.POPUP_MSG.BTN_OK;
    let cancel_txt = this.props.cancelText ? this.props.cancelText : STRINGS.POPUP_MSG.BTN_CANCEL;

    this.state = {
      confirm_msg,
      alert_msg,
      ok_txt,
      cancel_txt
    }
  }

  render() {

    var view = (this.state.alert_msg !== null) ?
      <View>
        <AppText style={[styles.textContainer,{textAlign: 'center',}]}>
          {this.state.alert_msg}
        </AppText>
        <View style={{
          flexDirection: 'row', justifyContent: 'center',
          alignItems: 'center', paddingBottom: 10
        }}>
          <TouchableHighlight
            onPress={this.props.closeModalFail}
            underlayColor='#dddddd'
            style={styles.okBtn}>
            <View>
              <AppText style={{color: '#ffffff'}}>
                {this.state.ok_txt}
              </AppText>
            </View>
          </TouchableHighlight>
        </View>
      </View> :
      <View>
        <AppText style={styles.textContainer}>
          {this.state.confirm_msg}
        </AppText>
        <View style={{flexDirection: 'row', alignSelf: "flex-end", margin: 20}}>
          <TouchableHighlight
            onPress={this.props.closeModalFail}
            underlayColor='#dddddd'
            style={styles.cancelBtn}>
            <View>
              <AppText style={{color: '#000000'}}>
                {this.state.cancel_txt}
              </AppText>
            </View>
          </TouchableHighlight>

          <TouchableHighlight
            onPress={this.props.closeModalSuccess}
            underlayColor='#dddddd'
            style={styles.okBtn}>
            <View>
              <AppText style={{color: '#ffffff'}}>
                {this.state.ok_txt}
              </AppText>
            </View>
          </TouchableHighlight>
        </View>

      </View>

    var modal =

    <View style={styles.container}>
      <View style={styles.innerContainer}>
        {view}
      </View>
    </View>

    return (modal)
  }

}

var styles = StyleSheet.create({
  textContainer: {
    marginTop: 20,
    marginBottom: 20,
    paddingLeft: 10,
    paddingRight: 10,
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    zIndex: 9999,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerContainer: {
    width: width - 60,
    backgroundColor: '#ffffff',
    borderRadius: 4,
  },
  okBtn: {
    width: 80,
    height: 36,
    backgroundColor: '#B2D234',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#B2D234',
    borderWidth: 1,
    borderRadius: 4,
  },
  cancelBtn: {
    width: 80,
    height: 36,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#000000',
    borderWidth: 1,
    borderRadius: 4,
    marginRight: 10,
  },
  bottomContainer: {
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'transparent'
  },

});

export default AlertConfirmDialog;
