'use strict';

export default class Color {
  static ribbon_new = '#F2AB4F';
  static ribbon_inprogress = '#7FBBD3';
  static ribbon_reject = '#F87F63';
  static ribbon_expire = '#6C6F71';
  static ribbon_cancel = '#CC4D4D';
  static ribbon_complete = '#438987';
}