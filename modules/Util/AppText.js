import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet
} from 'react-native';


// **** INCREASE THE CURRENT APP FONT SIZE BY 6
const INCREASE_FONT_SIZE_BY = 6
const DEFAULT_FONT_SIZE = 20

export default class AppText extends Component {

  constructor(props){
    super(props)

    var setStyle = {}

    // console.log("PROPS: ", props)
    if(props.hasOwnProperty("test")){


      var newStyle = StyleSheet.flatten(props.style)
      console.log("newStyle: ", newStyle)
    }

    if(props.hasOwnProperty("style")){
      var newStyle = StyleSheet.flatten(props.style)
      // console.log('OLD STYLE: ',newStyle)
      if(newStyle.hasOwnProperty("fontSize")){
        // console.log('Old font size --> ', newStyle.fontSize)
        var newFontSize = newStyle.fontSize + INCREASE_FONT_SIZE_BY
        // console.log('New font size --> ', newFontSize)
        setStyle = {
          ...newStyle,
          fontSize: newFontSize
        }
      }else {
        // NO FONT SIZE PRESENT SO SET TO DEFAULT SIZE
        setStyle = {
          ...newStyle,
          fontSize: 20
        }
      }

      // Remove the props style
      delete props.style

      setStyle = {
        fontFamily: 'DB Heavent',
        ...setStyle
      }
    }else{
      // NO STYLE PROPERTY WAS PRESENT
      setStyle = {
        fontFamily: 'DB Heavent',
        fontSize: 20
      }
    }



    // console.log('NEW STYLE: ',setStyle)
    this.props = props
    this.state = {
      style: setStyle
    }
  }

  render() {
    return(
      <Text
        {...this.props}
        style={this.state.style}> {this.props.children} </Text>
    );
  }
}
