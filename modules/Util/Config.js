import Config from 'react-native-config'

// LOCAL DEVELOPMENT
// let api = "http://172.16.53.44:8085";
//s3bucket_url = "http://172.16.53.32:4567";
//let api = "http://172.16.53.32:8085";

let api = Config.API_URL;
let s3bucket_url = "https://s3-ap-southeast-1.amazonaws.com";
let api_endpoint = api + "/dfapi";
let upload_url = s3bucket_url;
let download_url = api + "/uploads";
let MOBILE_USER_MANUAL_URL = "http://box.aismform.com/uploads/manual/User_Manual_Mobile.pdf"
module.exports = {
  MOBILE_USER_MANUAL_URL,
  download_url,
  upload_url,
  login: {
    url: api_endpoint + '/authen',
    method: 'POST',
    action_name: 'Login'
  },
  logout: {
    url: api_endpoint + '/authen/logout/',
    method: 'GET',
    action_name: 'Logout'
  },
  getWorkinglist: {
    url: api_endpoint + '/workinglist/',
    method: 'GET',
    action_name: 'Get working list'
  },
  getWorkinglistDetail: {
    url: api_endpoint + '/workinglist/detail/',
    method: 'GET',
    action_name: 'Get working list detail'
  },
  getHistorylist: {
    url: api_endpoint + '/workinglist/history',
    method: 'GET',
    action_name: 'Get history list'
  },
  getFormComponents: {
    url: api_endpoint + '/formFields',
    method: 'GET',
    action_name: 'Get form component'
  },
  saveFormData: {
    url: api_endpoint + '/forms/',
    method: 'POST',
    content_type: 'multipart/form-data',
    action_name: 'Save form data'
  },
  getFormList: {
    url: api_endpoint + '/forms/',
    method: 'GET',
    action_name: 'Get form list'
  },
  getFormData: {
    url: api_endpoint + '/forms/data/',
    method: 'GET',
    action_name: 'Get form data'
  },
  rejectForm: {
    url: api_endpoint + '/forms/rejectform',
    method: 'PUT',
    action_name: 'Reject form'
  },
  cancelForm: {
    url: api_endpoint + '/forms/cancelform',
    method: 'PUT',
    action_name: 'Cancel form'
  },
  getWorkinglistPublished: {
    url: api_endpoint + '/workinglist/forms/published',
    method: 'GET',
    action_name: 'Get working list (published)'
  },
  getUsersPermissions: {
    url: api_endpoint + '/forms/userpermission/',
    method: 'GET',
    action_name: 'Get user permission'
  },
  getNextUsers: {
    url: api_endpoint + '/forms/nextusers/',
    method: 'GET',
    action_name: 'Get next user (form transaction)'
  },
  createWork: {
    url: api_endpoint + '/workinglist/creatework',
    method: 'POST',
    action_name: 'Create work'
  },
  getUserFirstStep: {
    url: api_endpoint + '/forms/userfirststep/',
    method: 'GET',
    action_name: 'Get user first step'
  },
  assignForm: {
    url: api_endpoint + '/workinglist/assignform',
    method: 'POST',
    action_name: 'Assign work'
  },
  getProvince: {
    url: api_endpoint + '/address',
    method: 'GET',
    action_name: 'Get province'
  },
  getDistrict: {
    url: api_endpoint + '/address/district/',
    method: 'GET',
    action_name: 'Get district'
  },
  getUserfirststepNoFlow: {
    url: api_endpoint + '/forms/userfirststepNoFlow/',
    method: 'GET',
    action_name: 'Get user first step (no-flow)'
  },
  submitEndForm: {
    url: api_endpoint + '/forms/endform/',
    method: 'POST',
    content_type: 'multipart/form-data',
    action_name: 'Submit form (end form no-flow)'
  },
  uploadFile: {
    url: api_endpoint + '/forms/upload',
    method: 'POST',
    content_type: 'multipart/form-data',
    action_name: 'Upload file'
  },
  uploadFileImage: {
    url: api_endpoint + '/forms/uploadImage/',
    method: 'POST',
    content_type: 'multipart/form-data',
    action_name: 'Upload file'
  },
  uploadFileFile: {
    url: api_endpoint + '/forms/uploadFile/',
    method: 'POST',
    content_type: 'multipart/form-data',
    action_name: 'Upload file'
  },
  //========== Draft ======================================
  createFormDraft: {
    url: api_endpoint + '/forms/draft',
    method: 'POST',
    action_name: 'Create draft form data'
  },
  saveFormDraft: {
    url: api_endpoint + '/forms/draft/',
    method: 'PUT',
    action_name: 'Save draft form data'
  },
  listFormDraft: {
    url: api_endpoint + '/forms/draft/',
    method: 'GET',
    action_name: 'Get list draft form data'
  },
  cancelDraftForm: {
    url: api_endpoint + '/forms/draft/',
    method: 'delete',
    action_name: 'Cancel draft form data'
  },
  cancelDraftFormByUser: {
    url: api_endpoint + '/forms/canceldraft/',
    method: 'delete',
    action_name: 'Cancel draft form data by user'
  },
  getFormDraftDetail: {
    url: api_endpoint + '/forms/draft/',
    method: 'GET',
    action_name: 'Get draft form data detail'
  },
  getNotiForm: {
    url: api_endpoint + '/notifications/',
    method: 'GET',
    action_name: 'Get notifications'
  },
  countNotifications: {
    url: api_endpoint + '/notifications/count',
    method: 'GET',
    action_name: 'Count notifications'
  },
  clearNotiForm: {
    url: api_endpoint + '/notifications/',
    method: 'DELETE',
    action_name: 'Clear notifications'
  },
  deleteNoti: {
    url: api_endpoint + '/notifications/',
    method: 'DELETE',
    action_name: 'Delete notifications'
  },
  genPdfPath: {
    url: api_endpoint + '/forms/getPdfPath/',
    method: 'GET',
    action_name: 'Get pdf path'
  },
  getAppVersion: {
    url: api_endpoint + '/appversion/',
    method: 'GET',
    action_name: 'Get app version',
  },
  getCustomerContact: {
    url: api_endpoint + '/customers',
    method: 'GET',
    action_name: 'Get customer contact'
  },
  getUserContact: {
    url: api_endpoint + '/users/userContact',
    method: 'GET',
    action_name: 'Get user contact'
  },
  signUp: {
    url: api_endpoint + '/signup',
    method: 'POST',
    content_type: 'multipart/form-data',
    action_name: 'Sign-up'
  },
  addCustomer: {
    url: api_endpoint + '/customers',
    method: 'POST',
    action_name: 'Add customer'
  },
  editCustomer: {
    url: api_endpoint + '/customers/',
    method: 'PUT',
    action_name: 'Edit customer'
  },
  deleteCustomerContact: {
    url: api_endpoint + '/customers/deletecontact/delete',
    method: 'PUT',
    action_name: 'Delete customer contact'
  },
  forgotPassword: {
    url: api_endpoint + '/users/forgotpassword',
    method: 'POST',
    action_name: 'Forget password'
  },
  getAutoCompleteList: {
    url: api_endpoint + '/forms/getAutocompleteList/',
    method: 'POST',
    action_name: 'Get autocomplete list'
  },
  getUserDetail: {
    url: api_endpoint + '/users/',
    method: 'GET',
    action_name: 'Get user detail'
  },
  editUser: {
    url: api_endpoint + '/users/updateUser/',
    method: 'PUT',
    content_type: 'multipart/form-data',
    action_name: 'Edit user'
  },
  setNotification: {
    url: api_endpoint + '/notifications/sound',
    method: 'POST',
    action_name: 'Enable/Disable notification'
  },
  changePasswordMyself: {
    url: api_endpoint + '/users/changepassword-myself/',
    method: 'PUT',
    action_name: 'Change password myself'
  },
  uploadPhoto: {
    url: api_endpoint + '/upload',
    method: 'POST',
    content_type: 'multipart/form-data',
    action_name: 'Upload Photo'
  },
};
