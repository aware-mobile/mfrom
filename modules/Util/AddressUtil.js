'use strict';

const PROVINCES = require('./data/PROVINCES.json');
const DISTRICTS = require('./data/DISTRICTS.json');
const SUB_DISTRICTS = require('./data/SUB_DISTRICTS.json');

import _ from 'lodash'

export default class AddressUtil {

  static getProvinces() {
    return PROVINCES
  }

  static getDistricts(code) {
    return _.filter(DISTRICTS, (v) => {
      return (v.province_code.toString() == code.toString() )
    });
  }

  static getSubDistricts(code) {
    return _.filter(SUB_DISTRICTS, (v) => {
      return (v.amphur_code.toString()  == code.toString() )
    });
  }

  static findProvince(code) {
    return _.find(PROVINCES, (v) => {
      return (v.province_code.toString()  == code.toString() )
    });
  }

  static findDistrict(code) {
    return _.find(DISTRICTS, (v) => {
      return (v.amphur_code.toString()  == code.toString() )
    });
  }

  static findSubDistrict(code) {
    return _.find(SUB_DISTRICTS, (v) => {
      return (v.district_code.toString()  == code.toString() )
    });
  }
  
}