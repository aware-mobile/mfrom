import React, {Component} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  ListView,
  TouchableHighlight,
  Platform,
  TextInput,
  Keyboard
} from 'react-native';

let Ionicons = require('react-native-vector-icons/Ionicons');
import DFService from './Service/DFService';
import apiConfig from './Util/Config';
import AppText from './Util/AppText';
import _ from 'lodash'

let dataSource = new ListView.DataSource(
  {rowHasChanged: (r1, r2) => r1._id !== r2._id});

class AutoCompletePage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dataSource: dataSource.cloneWithRows([]),
      keyboardHeight: 0,
      text: this.props.text,
      dataRender: [],
    };
    this.autoDataBoth = [];
    this.autoCompleteBothData = [];
    //console.log(this.props.properties);
    this.type = this.getAutoCompleteProperty(this.props.properties);
    this.getAutoComplete('User Contacts & Customer Contacts');
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = (e) => {
    this.setState({
      keyboardHeight: e.endCoordinates.height,
    });
    this.searchTransactions(this.state.text);
  };

  _keyboardDidHide = (e) => {
    this.setState({
      keyboardHeight: 0,
    });
    this.searchTransactions(this.state.text);
  };

  getAutoCompleteProperty(properties) {
    if (properties === undefined) {
      return 'User Contacts & Customer Contacts'
    } else {
      for (let i = 0; i < properties.length; i++) {
        if (properties[i].checked) {
          return properties[i].name
        }
      }
    }
  }

  getAutoComplete(type) {

    let self = this;
    let bodyParam = {"option": type};

    DFService.callApiWithHeader(bodyParam, apiConfig.getAutoCompleteList, function (error, response) {
      if (response) {
        if (response.resCode === 'DF2000000') {
          self.autoCompleteBothData = response.data;
          self.getAutoCompleteDataByType();
        } else {

        }
      } else {

      }
    });
  }

  getAutoCompleteDataByType() {
    let autoData_customer = [];
    let autoData_user = [];

    for (let i = 0; i < this.autoCompleteBothData.user_contacts.length; i++) {
      let tmp = {
        "_id": this.autoCompleteBothData.user_contacts[i]._id,
        "name": this.autoCompleteBothData.user_contacts[i].displayName,
        "value": this.autoCompleteBothData.user_contacts[i].displayName.toLowerCase(),
        "email": this.autoCompleteBothData.user_contacts[i].email,
        "type": "user"
      };
      autoData_user.push(tmp)
    }
    for (let i = 0; i < this.autoCompleteBothData.customer_contacts.length; i++) {
      let tmp = {
        "_id": this.autoCompleteBothData.customer_contacts[i]._id,
        "name": this.autoCompleteBothData.customer_contacts[i].name,
        "value": this.autoCompleteBothData.customer_contacts[i].name.toLowerCase(),
        "email": this.autoCompleteBothData.customer_contacts[i].email,
        "type": "customer"
      };
      autoData_customer.push(tmp)
    }

    this.autoDataCustomer = autoData_customer;
    this.autoDataUser = autoData_user;
    if (this.type === 'User Contacts & Customer Contacts') {
      this.autoDataBoth = autoData_user.concat(autoData_customer);
    } else if (this.type === 'User Contacts') {
      this.autoDataBoth = autoData_user;
    } else if (this.type === 'Customer Contacts') {
      this.autoDataBoth = autoData_customer;
    } else {
      this.autoDataBoth = autoData_user.concat(autoData_customer);
    }

    this.setState({
      dataSource: dataSource.cloneWithRows(this.autoDataBoth),
      dataRender: this.autoDataBoth
    });

    if (this.state.text !== undefined && this.state.text !== null && this.state.text !== '') {
      this.searchTransactions(this.state.text);
    }
  }

  searchTransactions(query) {
    this.setState({
      dataSource: dataSource.cloneWithRows([])
    });
    let activeList = this.state.dataRender.slice();
    if (query !== undefined && query !== null && query !== '') {
      query = query.toLowerCase();
      if (activeList !== null && activeList.length !== 0) {
        let tmp_activeList = _.filter(activeList, (v) => {
          if ('name' in v) {
            let formName = v.name;
            return (formName.toLowerCase().indexOf(query) !== -1)
          } else {
            return true
          }
        });

        if (this.state.keyboardHeight > 0) {
          tmp_activeList.push({keyboardRow: true});
        }

        this.setState({
          dataSource: dataSource.cloneWithRows(tmp_activeList)
        });
      }
    } else {
      if (this.state.keyboardHeight > 0) {
        activeList.push({keyboardRow: true});
      }
      this.setState({
        dataSource: dataSource.cloneWithRows(activeList)
      })
    }
  }

  render() {
    let removeClippedSubviews = Platform.OS !== 'android';

    return (
      <View style={styles.container}>
        <View style={{flexDirection: 'row', marginTop: 10}}>
          <View style={[styles.borderView, {flexGrow: 1}]}>
            <TextInput
              key={"phoneInput"}
              style={styles.input}
              placeholder=''
              autoFocus={true}
              value={this.state.text}
              maxLength={200}
              editable={true}
              underlineColorAndroid='rgba(0,0,0,0)'
              onChangeText={(text) => {
                this.searchTransactions(text);
                this.setState({text});
              }}
              onSubmitEditing={this.setValue.bind(this, this.state.text, '', '')}
            />
          </View>
          <TouchableHighlight
            style={styles.button}
            onPress={this.setValue.bind(this, this.state.text, '', '')}
            underlayColor={'#B2D234'}>
            <Ionicons name="md-checkmark" size={20} color="white"
                      style={{justifyContent: 'center', alignItems: 'center'}}/>
          </TouchableHighlight>
        </View>
        <View style={{height: 1, backgroundColor: '#cdcdcd'}}>
        </View>
        <ListView
          keyboardShouldPersistTaps={true}
          dataSource={this.state.dataSource}
          renderRow={this.renderRow.bind(this)}
          //renderSeparator={this._renderSeparator.bind(this)}
          enableEmptySections={true}
          removeClippedSubviews={removeClippedSubviews}
        />
      </View>
    );
  }

  renderRow(rowData, sectionID, rowID) {
    let keyboardRow = ('keyboardRow' in rowData);
    if (keyboardRow) {
      return (<View style={{height: this.state.keyboardHeight}}/>)
    }
    let icon = null;
    if (rowData.type === 'group') {
      icon = <Ionicons name="md-people" size={20} color="black"
                       style={{justifyContent: 'center', alignItems: 'center'}}/>
    } else if (rowData.type === 'emails') {
      icon = <Ionicons name="md-at" size={20} color="black"
                       style={{justifyContent: 'center', alignItems: 'center'}}/>;
    } else if (rowData.type === 'customer') {
      icon = <Ionicons name="md-bookmarks" size={20} color="black"
                       style={{justifyContent: 'center', alignItems: 'center'}}/>;
    } else {
      icon = <Ionicons name="md-person" size={20} color="black"
                       style={{justifyContent: 'center', alignItems: 'center'}}/>;
    }
    return (
      <View>
        <TouchableHighlight
          onPress={() => this.rowPressed(rowData, sectionID, rowID)}
          underlayColor='#dddddd'
          style={[{backgroundColor: '#ffffff', zIndex: -1}]}>
          <View style={styles.rowContainer}>
            <View style={{flexDirection: 'row', alignItems: 'center', paddingLeft: 5, flexGrow: 1}}>
              {icon}
              <AppText style={{
                marginLeft: 5,
                fontSize: 13,
                color: '#1C1D21',
                fontWeight: 'bold'
              }} numberOfLines={1}>{rowData.name}</AppText>
            </View>
          </View>
        </TouchableHighlight>
        <View style={{height: 1, backgroundColor: '#cdcdcd'}}>
        </View>
      </View>
    );
  }

  rowPressed(rowData, sectionID, rowID) {
    if (rowData.type === 'user') {
      this.setValue(rowData.name, rowData._id, '');
    } else {
      this.setValue(rowData.name, '', rowData._id);
    }
  }

  setValue(text, userId, customerId) {
    this.props.setValue(text.trim(), userId, customerId);
    this.props.navigator.pop();

  }

  _renderSeparator(sectionID, rowID, adjacentRowHighlighted) {
    return (
      <View
        key={`${sectionID}-${rowID}`}
        style={{
          height: adjacentRowHighlighted ? 4 : 1,
          backgroundColor: adjacentRowHighlighted ? '#3B5998' : '#CCCCCC',
        }}
      />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    flexGrow: 1,
  },
  rowContainer: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center'
  },
  borderView: {
    borderColor: '#95989A',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  button: {
    height: 30,
    width: 30,
    paddingTop: 2,
    paddingBottom: 2,
    marginRight: 15,
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    borderWidth: 1,
    borderRadius: 4,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center'
  },
  input: {
    height: 30,
    fontSize: 20,
    borderWidth: 0,
    color: '#333435',
    paddingTop: 2,
    paddingBottom: 2
  },
});

export default AutoCompletePage;
