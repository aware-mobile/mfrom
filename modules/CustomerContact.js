import React, {Component} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Image,
  TouchableHighlight,
  ListView,
  ActivityIndicator,
  AsyncStorage,
  Platform,
  NativeModules,
  Alert,
  AppState,
  NetInfo,
  Keyboard,
  LayoutAnimation,
  UIManager,
  TouchableOpacity,
  Dimensions,
  Text
} from 'react-native';

let {height, width} = Dimensions.get('window');
// import { MenuContext } from 'react-native-popup-menu';
const Timer = require('react-native-timer');
import apiConfig from './Util/Config';
import AppText from './Util/AppText';
import STRINGS from './Util/strings';
import CustomerDetailPage from './CustomerDetailPage';
import DFService from './Service/DFService';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import WorkingListTopSearchBar from './CustomViews/WorkingListTopSearchBar';
import _ from 'lodash'
import Communications from 'react-native-communications';
import Menu, {
  MenuContext,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from 'react-native-popup-menu';

let dataSource = new ListView.DataSource(
  {rowHasChanged: (r1, r2) => r1._id !== r2._id}
);
let resData;
export default class CustomerContact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      message: '',
      dataSource: dataSource.cloneWithRows([{}]),
      isConnected: true,
      keyboardHeight: 0,
      username: '',
      dataLocalStorage: '',
      tempData: [],
      dataFilter: [],
      dataSearch: [],
      isSearching: false,
      dataFav: [],
      arrFilter: [{
        name: STRINGS.COMMON.FILTER_ALL,
        value: 0,
        active: true
      }, {
        name: STRINGS.COMMON.FILTER_FAV,
        value: 1,
        active: false
      }],
      isFavoriteSelected: 0,
      dataRender: [],
    };
    this.selectTel = '';
    this.selectEmail = '';
    this.selectName = '';
    //this.getCustomerFilter();
    this.initCustomerContact(0, false);
  }

  async initCustomerContact(type, check = true) {
    let newFilter = [];
    try {
      let value = await AsyncStorage.getItem('customerFilter');
      console.log('getCustomerFilter: ' + value);
      if (value) {
        this.state.arrFilter = JSON.parse(value);
      }
    } catch (error) {
      console.log(error)
    }

    if (check) {
      for (let i = 0; i < this.state.arrFilter.length; i++) {
        let newObjFilter = this.state.arrFilter[i];
        if (newObjFilter.value === type) {
          newObjFilter.active = true;
          newFilter.push(newObjFilter)
        } else {
          newObjFilter.active = false;
          newFilter.push(newObjFilter)
        }
      }
      this.saveCustomerFilter(JSON.stringify(newFilter));
    } else {
      for (let i = 0; i < this.state.arrFilter.length; i++) {
        let newObjFilter = this.state.arrFilter[i];
        if (newObjFilter.active) {
          console.log(newObjFilter);
          type = newObjFilter.value;
        }
      }
      newFilter = this.state.arrFilter;
    }

    this.getFavCustomer();
    let self = this;
    let apiData = {};
    this.setState({
      isLoading: true,
      arrFilter: newFilter
    });
    DFService.callApiWithHeader(apiData, apiConfig.getCustomerContact, function (error, response) {
      if (response) {
        if (response.resCode === 'DF2000000') {
          resData = [];
          for (let i = 0; i < response.data.length; i++) {
            let obj = response.data[i];
            obj.isFav = false;
            resData.push(obj)
          }
          console.log('response initCustomerContact : ', resData);
          self.initFavData(resData, type)
        } else {
          // Failed Response
          self.setState({
            isLoading: false,
            dataSource: dataSource.cloneWithRows([]),
          });
          self.props.openMainModal(response.resMessage)
        }
      } else {
        // Error from application calling service
        self.setState({
          isLoading: false,
          dataSource: dataSource.cloneWithRows([]),
        });
        self.props.openMainModal(error);
        console.log("initCustomerContact error = ", error);
      }
    });
  }

  initFavData(data, type) {
    let favLocalStore = this.state.dataLocalStorage === null ? [] : this.state.dataLocalStorage.split(',');
    let dataFav = [];
    Loop1:
      for (let i = 0; i < data.length; i++) {
        Loop2:
          for (let x = 0; x < favLocalStore.length; x++) {
            if (data[i]._id === favLocalStore[x]) {
              data[i].isFav = true;
              dataFav.push(data[i]);
              continue Loop1
            }
          }
      }
    console.log('dataFav : ', dataFav);
    this.setState({
      isLoading: false,
      message: '',
      isFavoriteSelected: type, // 0 == Not Selected, 1 == selected
      dataSource: type === 0 ? dataSource.cloneWithRows(data) : dataSource.cloneWithRows(dataFav),
      dataRender: type === 0 ? data : dataFav,
      dataSearch: [],
      isSearching: false,
      dataFav: dataFav,
      tempData: data,
      dataFilter: type === 0 ? data : dataFav
    });
  }

  render() {
    let removeClippedSubviews = Platform.OS !== 'android';
    let ListViewData = null;
    let emptyListText;
    if (this.state.tempData.length === 0) {
      emptyListText = "No data"
    } else {
      emptyListText = STRINGS.COMMON.NO_MATCH
    }

    let spinner =
      <View style={styles.spinner}>
        <ActivityIndicator
          hidden='true'
          size='large'/>
        <AppText style={{fontSize: 14}}>{STRINGS.COMMON.LOADING} </AppText>
      </View>

    if (this.state.dataSource.getRowCount() > 0) {
      ListViewData = <ListView
        key={'listView'}
        keyboardShouldPersistTaps={true}
        dataSource={this.state.dataSource}
        renderRow={this.renderRow.bind(this)}
        enableEmptySections={true}
        removeClippedSubviews={removeClippedSubviews}
        contentContainerStyle={{paddingBottom: 150}}
      />
    } else {
      ListViewData = <View style={{flexGrow: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Image source={require('./img/empty_list_logo.png')} style={{width: 160, resizeMode: 'contain'}}/>
        <Text style={{
          fontSize: 28,
          marginTop: 5,
          fontFamily: 'DB Heavent',
          textAlign: 'center',
          paddingLeft: 10,
          paddingRight: 10
        }}>{emptyListText}</Text>
      </View>;
    }

    return (
      <View style={{flexGrow: 1, backgroundColor: '#F7F6F9'}}>
        <WorkingListTopSearchBar pageName="contact" arrFilter={this.state.arrFilter}
                                 selectFilter={this.selectFilter.bind(this)}
                                 searchTransactions={this.searchTransactions.bind(this)}/>
        {this.state.isLoading ? spinner : ListViewData}
      </View>
    )
  }

  renderRow(rowData, sectionID, rowID) {
    var options_menu_ic = <TouchableHighlight
      onPress={() => this.pressFav(rowData, sectionID, rowID, true)}
      underlayColor='transparent'
      style={styles.option_menu_container}>
      <Ionicons name="md-star-outline" size={27} color="#ED9D34"/>
    </TouchableHighlight>
    var options_menu_ic_fav = <TouchableHighlight
      onPress={() => this.pressFav(rowData, sectionID, rowID, false)}
      underlayColor='transparent'
      style={styles.option_menu_container}>
      <Ionicons name="md-star" size={27} color="#ED9D34"/>
    </TouchableHighlight>
    var options = []
    // if (rowData.telephone.length > 1) {
    for (let i = 0; i < rowData.telephone.length; i++) {
      options.push(<MenuOption value={rowData.telephone[i]}
                               key={rowID + 'optionIndex' + i}
                               style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
        <Text style={{
          fontSize: 19,
          color: 'grey',
          fontWeight: 'bold',
          fontFamily: 'DB Heavent',
          flex: 0.4
        }}>{rowData.telephone[i]}</Text>
        <Entypo name="old-phone" size={23} color="#7F7F7F" style={styles.img}/>
      </MenuOption>)
    }

    var strAddress = rowData.address ? rowData.address : ''
    if (rowData.sub_district) {
      strAddress += rowData.sub_district.district ? ' ' + rowData.sub_district.district : ''
    }
    if (rowData.district) {
      strAddress += rowData.district.amphur ? ' ' + rowData.district.amphur : ''
    }
    if (rowData.province) {
      strAddress += rowData.province.province ? ' ' + rowData.province.province : ''
    }
    if (rowData.postal_code) {
      strAddress += rowData.postal_code ? ' ' + rowData.postal_code : ''
    }
    let txtphone = (rowData.telephone.length > 0) ? rowData.telephone[0] : '-';
    var bottomRowContent =
      <View style={[styles.rowContain, {borderWidth: 1, borderColor: '#dddddd'}]}>
        <Menu onSelect={value => this.callTelephone(value, rowData.name)}
              style={{flexDirection: 'row', flexGrow: 0.4, padding: 5, backgroundColor: '#EEEEF0'}}>
          <MenuTrigger
            key={'MenuTriggerIndex' + rowID}>
            <View style={{flexDirection: 'row', flexGrow: 0.4, padding: 5, backgroundColor: '#EEEEF0'}}>
              <Image source={require('./img/mobile-phone.png')} style={styles.img}/>
              <View style={{flexDirection: 'column', width: width / 2 - 50}}>
                <AppText style={{fontSize: 10, color: 'grey'}}>{STRINGS.USER_DATA.TELEPHONE_FORMAT_2}</AppText>
                <Text numberOfLines={1}
                      style={{
                        width: 100,
                        fontSize: 16,
                        color: 'grey',
                        fontWeight: 'bold',
                        fontFamily: 'DB Heavent'
                      }}>{txtphone}</Text>
              </View>
            </View>
          </MenuTrigger>
          <MenuOptions optionsContainerStyle={{}}>
            {options}
          </MenuOptions>
        </Menu>
        <View style={styles.pipe}/>
        <TouchableOpacity
          key={'pressSendMailIndex' + rowID}
          style={{flexDirection: 'row', flexGrow: 0.5, padding: 5, backgroundColor: '#EEEEF0'}}
          onPress={() => this.pressSendMail(rowData.email)}>
          <View style={{flexDirection: 'row', padding: 5, backgroundColor: '#EEEEF0'}}>
            <Image source={require('./img/email-icon.png')} style={styles.img}/>
            <View style={{flexDirection: 'column', width: width / 2 - 65}}>
              <AppText style={{fontSize: 10, color: 'grey'}}>{STRINGS.USER_DATA.EMAIL_FORMAT_2}</AppText>
              <Text numberOfLines={1} style={{
                fontSize: 16,
                color: 'grey',
                fontWeight: 'bold',
                fontFamily: 'DB Heavent'
              }}>{rowData.email}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    return (
      <View style={styles.box}>
        <TouchableHighlight
          onPress={() => this.rowPressed(rowData, sectionID, rowID)}
          underlayColor='#dddddd'
          style={[{backgroundColor: '#ffffff', zIndex: -1}]}>
          <View style={styles.rowContainer}>
            <View style={{flexDirection: 'column', paddingLeft: 10, width: width - 70}}>
              <Text style={{
                fontSize: 19,
                color: '#1C1D21',
                fontWeight: 'bold',
                fontFamily: 'DB Heavent'
              }}
                    numberOfLines={1}>{rowData.name}</Text>
              <Text numberOfLines={2}
                    style={{fontSize: 16, color: '#4E9AA9', fontFamily: 'DB Heavent'}}>{strAddress}</Text>
            </View>
          </View>
        </TouchableHighlight>
        {rowData.isFav ? options_menu_ic_fav : options_menu_ic}
        {bottomRowContent}
      </View>
    );
  }

  rowPressed(rowData, sectionID, rowID) {
    console.log('press row = ', rowData)
    this.props.navigator.push({
      name: 'customerDetail',
      title: STRINGS.CUSTOM_MODALS.CUSTOMER_DETAIL_HEADER,
      component: CustomerDetailPage,
      passProps: {
        data: rowData,
        refreshCustomer: this.initCustomerContact.bind(this),
        reloadCustomer: this.initCustomerContact.bind(this),
        openMainModal: this.props.openMainModal.bind(this),
        openMainConfirm: this.props.openMainConfirm.bind(this)
      }
    });
  }

  pressFav(data, sectionID, rowID, statusFav) {
    let tmpData = this.state.dataRender;
    if (this.state.isSearching) {
      tmpData = this.state.dataSearch;
    }

    tmpData[rowID].isFav = statusFav;

    this.setState({
      dataSource: dataSource.cloneWithRows(tmpData),
      // dataRender: tmpData
    });
    let dataSave = [];
    for (let i = 0; i < resData.length; i++) {
      if (resData[i].isFav) {
        dataSave.push(resData[i]._id)
      }
    }
    this.saveFavCustomer(dataSave.join())
  }

  selectFilter(obj) {
    this.initCustomerContact(obj.value)
  }

  callTelephone(telNo, name) {
    this.selectTel = telNo;
    Timer.setTimeout(this, 'showDailog', () => {
      console.log('callTelephone');
      this.props.openMainConfirm(STRINGS.formatString(STRINGS.COMMON.CALL_NUMBER, name), this.confirmCallPhone)
    }, 500);
  }

  confirmCallPhone = () => {
    Communications.phonecall(this.selectTel, false)
  };

  pressSendMail(email) {
    this.selectEmail = email;
    this.props.openMainConfirm(STRINGS.formatString(STRINGS.COMMON.EMAIL_USER, email), this.confirmPressSendMail)
  }

  confirmPressSendMail = () => {
    Communications.email([this.selectEmail], null, null, null, null)
  };

  searchTransactions(query) {

    this.setState({
      isLoading: true,
      dataSource: dataSource.cloneWithRows([]),
    });

    let activeList = [];
    activeList = this.state.dataRender;


    if (query !== undefined && query !== null && query !== '') {
      query = query.toLowerCase();
      if (activeList !== null && activeList.length !== 0) {
        let tmp_activeList = _.filter(activeList, (v) => {
          if ('name' in v || 'email' in v) {
            let formName = v.name.toString();
            let description = (v.email !== undefined) ? v.email.toString() : '';
            return (formName.toLowerCase().indexOf(query) !== -1 || description.toLowerCase().indexOf(query) !== -1)
          } else {
            return true
          }
        });

        Timer.setTimeout(this, 'Load Data', () => {
          this.setState({
            isLoading: false,
            dataSource: dataSource.cloneWithRows(tmp_activeList),
            dataSearch: tmp_activeList,
            isSearching: true
          });
        }, 500);

      } else {

        // NO DATA TO SEARCH
        Timer.setTimeout(this, 'Load Data', () => {
          this.setState({
            isLoading: false,
            dataSource: dataSource.cloneWithRows([]),
            dataSearch: [],
            isSearching: true
          });
        }, 500);

      }
    } else {
      this.setState({
        isLoading: false,
        dataSource: dataSource.cloneWithRows(activeList),
        dataSearch: [],
        isSearching: false
      })
    }

  }

  async saveFavCustomer(data) {
    console.log('saveFavCustomer: ', data);
    try {
      let value = await AsyncStorage.getItem('Username');
      await AsyncStorage.setItem('CustomerContact_fav_' + value, data);
    } catch (error) {
      console.log(error)
    }
  }

  async getFavCustomer() {
    console.log('getFavCustomer');
    try {
      let username = await AsyncStorage.getItem('Username');
      let value = await AsyncStorage.getItem('CustomerContact_fav_' + username);
      console.log('getFavCustomer value', value);
      this.setState({
        dataLocalStorage: value
      })
    } catch (error) {
      console.log(error)
    }
  }

  async saveCustomerFilter(data) {
    console.log('saveCustomerFilter: ' + data);
    try {
      await AsyncStorage.setItem('customerFilter', data);
    } catch (error) {
      console.log(error)
    }
  }

  async getCustomerFilter() {
    try {
      let value = await AsyncStorage.getItem('customerFilter');
      console.log('getCustomerFilter: ' + value);
      if (value) {
        this.state.arrFilter = JSON.parse(value);
      }
    } catch (error) {
      console.log(error)
    }
  }
}

const styles = StyleSheet.create({
  spinner: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  tabbar: {
    height: 45,
    flexDirection: 'row',
    backgroundColor: '#B2D234'
  },
  tabView: {
    flexDirection: 'row',
    flexGrow: 0.3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    flexGrow: 1,
    marginTop: 70,
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowContainer: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center'
  },
  rowContain: {
    flexDirection: 'row'
  },
  colContainer: {
    flexDirection: 'column',
    padding: 10
  },
  separator: {
    height: 1,
    backgroundColor: '#dddddd'
  },
  indicator: {
    height: 2,
    backgroundColor: 'black',
    flexDirection: 'row',
    flexGrow: 0.3
  },
  pipe: {
    width: 1,
    backgroundColor: '#dddddd'
  },
  listView: {
    marginTop: 65
  },
  listImage: {
    width: 16,
    height: 20
  },
  status: {
    alignItems: 'flex-end',
    width: 60,
    height: 16
  },
  backdropView: {
    height: 16,
    width: 60,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0)',
  },
  headline: {
    fontSize: 8,
    textAlign: 'center',
    backgroundColor: 'rgba(0,0,0,0)',
    color: 'white'
  },
  img: {
    width: 25,
    height: 25,
    marginRight: 5
  },
  box: {
    padding: 8,
    // shadowColor: "#000000",
    // shadowOpacity: 0.8,
    // shadowRadius: 2,
    // shadowOffset: {
    //   height: 1,
    //   width: 0
    // }
  },
  circle: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center'
  },
  search: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  navBtnImage: {
    width: 25,
    height: 25
  },
  navBarBtn: {
    height: 35,
    marginTop: 10,
    marginRight: 15,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  rowSearch: {
    marginTop: 5,
    flexDirection: 'row',
    alignItems: 'center'
  },
  containerError: {
    flexGrow: 1,
    marginTop: 70,
    justifyContent: 'center',
    alignItems: 'center'
  },
  option_menu_icon: {
    width: 3,
    height: 13
  },
  option_menu_container: {
    // backgroundColor: '#43D5FE',
    alignItems: 'center',
    justifyContent: 'center',
    width: 45,
    height: 45,
    right: 10,
    // paddingTop: 7,
    // paddingRight: 20,
    top: 10,
    zIndex: 99,
    position: 'absolute',
  },
  actionButton: {
    flexGrow: 1,
    height: 50,
    justifyContent: 'center'
  },
  actionIcon: {
    width: 15,
    height: 15,
    marginBottom: 2
  },
  text: {
    color: '#FFFFFF',
    fontSize: 10
  },
  cancel_draft_icon: {
    width: 15,
    height: 16,
    marginBottom: 2
  },
  textArea: {
    height: 30,
    fontSize: 12,
    paddingTop: 2,
    paddingBottom: 2,
    color: '#000000',
    backgroundColor: '#ffffff'
  },
  borderView: {
    flexGrow: 1,
    marginRight: 2,
    marginLeft: 2,
    marginBottom: 4,
    marginTop: 4,
    padding: 1,
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 5,
  },
  okBtn: {
    width: 60,
    height: 30,
    backgroundColor: '#B2D234',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#B2D234',
    borderWidth: 1,
    borderRadius: 4,
    marginRight: 5
  },
  inactiveOkBtn: {
    width: 60,
    height: 30,
    backgroundColor: '#90A3AE',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#90A3AE',
    borderWidth: 1,
    borderRadius: 4,
    marginRight: 5
  },
  noInternetText: {
    marginTop: 20,
    fontSize: 22,
    textAlign: 'center',
    color: '#047e94',
    fontWeight: 'bold'
  }
});
