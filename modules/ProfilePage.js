import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Text,
  ScrollView,
  Image,
  TouchableHighlight,
  AsyncStorage
} from 'react-native';

import DFLabel from './FormComponents/DFLabel'
import apiConfig from './Util/Config';
import DFService from './Service/DFService';
import EditProfilePage from './EditProfilePage';
import STRINGS from './Util/strings'
import AppText from './Util/AppText';

let Ionicons = require('react-native-vector-icons/Ionicons');
let Icon = require('react-native-vector-icons/FontAwesome');
export default class ProfilePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null
    };
    this.getProfile();
  }

  async getProfile() {
    let self = this;
    self.props.toggleMainSpinner(true);
    let value = await AsyncStorage.getItem("Profile");
    let profile = await JSON.parse(value);
    let config = {
      method: apiConfig.getUserDetail.method,
      url: apiConfig.getUserDetail.url,
      action_name: apiConfig.getUserDetail.action_name
    };
    config.url += profile.user.id;

    DFService.callApiWithHeader(null, config, function (error, response) {
      self.props.toggleMainSpinner(false);
      if (response) {
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          self.setState({
            data: response.data
          });
          profile.user.displayName = response.data.displayName;
          self.saveUser(profile);
        } else {
          // Failed Response
          self.props.openMainModal(response.resMessage)
        }
      } else {
        // Error from application calling service
        self.props.openMainModal('Unexpected error has occured in sevice');
      }

    });
  }

  async saveUser(profile) {
    try {
      await AsyncStorage.setItem('Profile', JSON.stringify(profile));
    } catch (error) {
      this._appendMessage('AsyncStorage error: ' + error.message);
    }
  }

  render() {
    let formView = null;
    if (this.state.data) {
      let editView =
        <View style={{
          flexDirection: 'row', justifyContent: 'flex-end',
          alignItems: 'flex-end', marginTop: 5, marginRight: 5
        }}>
          <TouchableHighlight
            style={styles.button}
            onPress={() => this.editProfile(this.state.data)}
            underlayColor={'#B2D234'}>
            <Icon name="edit" size={20} color="white"
                  style={{justifyContent: 'center', alignItems: 'center'}}/>
          </TouchableHighlight>
        </View>;

      let phoneList = [];
      let phones = this.state.data.phone.join(", ");
      if (this.state.data.phone.length > 0) {
        phoneList.push(<AppText key={'key_phone'} style={styles.dropdownBtnTxt}>{phones}</AppText>);
      } else {
        phoneList.push(<AppText key={'key_phone_empty'} style={styles.dropdownBtnTxt}>-</AppText>);
      }
      let roleList = [];
      for (let i = 0; i < this.state.data.roles.length; i++) {
        roleList.push(
          <View key={'key_role_' + i} style={styles.dropdownBtn}>
            <View>
              <AppText numberOfLines={1} style={styles.dropdownBtnTxt}>{this.state.data.roles[i].role_name}</AppText>
            </View>
          </View>)
      }
      let imgPath = null;
      if (this.state.data.img_path !== '' && this.state.data.img_path !== undefined) {
        imgPath = {uri: apiConfig.upload_url + this.state.data.img_path}
      }
      let imgPerson = (imgPath !== null) ?
        <Image resizeMode={Image.resizeMode.cover} source={imgPath} style={styles.circleImage}/> :
        <Image resizeMode={Image.resizeMode.cover} source={require('./img/img_person.png')}
               style={styles.circleImage}/>;

      formView =
        <View>
          {editView}
          <View style={styles.image}>
            <TouchableHighlight
              style={{justifyContent: 'flex-end', alignItems: 'flex-end'}}
              underlayColor='transparent'>
              <View style={styles.circle}>
                {imgPerson}
              </View>
            </TouchableHighlight>
          </View>
          <DFLabel fontWeight={'bold'} key={"email"} text={STRINGS.USER_DATA.EMAIL} color="black" isRequired={false}/>
          <View style={styles.borderView }>
            <Text style={styles.input}>
              {(this.state.data.email) ? this.state.data.email : '-'}
            </Text>
          </View>
          <DFLabel fontWeight={'bold'} key={"name"} text={STRINGS.USER_DATA.NAME} color="black" isRequired={false}/>
          <View style={styles.borderView }>
            <Text style={styles.input}>
              {(this.state.data.firstName) ? this.state.data.firstName : '-'}
            </Text>
          </View>

          <DFLabel fontWeight={'bold'} key={"surname"} text={STRINGS.USER_DATA.SURNAME} color="black"
                   isRequired={false}/>
          <View style={styles.borderView }>
            <Text style={styles.input}>
              {(this.state.data.lastName) ? this.state.data.lastName : '-'}
            </Text>
          </View>
          <DFLabel fontWeight={'bold'} key={"phone"} text={STRINGS.USER_DATA.TELEPHONE} color="black"
                   isRequired={false}/>
          <View style={{paddingLeft: 20, paddingBottom: 5}}>
            {phoneList}
          </View>

          <DFLabel fontWeight={'bold'} key={"role"} text={STRINGS.USER_DATA.ROLE} color="black" isRequired={false}/>
          <View style={{paddingLeft: 10, paddingBottom: 5}}>
            <View style={styles.listAssign}>
              {roleList}
            </View>
          </View>

          <DFLabel fontWeight={'bold'} key={"department"} text={STRINGS.USER_DATA.DEPARTMENT} color="black"
                   isRequired={false}/>
          <View style={styles.borderView }>
            <Text style={styles.input}>
              {(this.state.data.department) ? this.state.data.department : '-'}
            </Text>
          </View>
        </View>;
    }
    return (
      <ScrollView
        ref={(scrollView) => {
          _scrollView = scrollView;
        }}
        automaticallyAdjustContentInsets={false}
        onScroll={() => {
          console.log('onScroll!');
        }}
        scrollEventThrottle={200}>
        {formView}
        <View style={{height: 40}}>
        </View>
      </ScrollView>
    );
  }

  editProfile(data) {
    this.props.navigator.push({
      name: "edit_profile",
      title: STRINGS.PROFILE_PAGE.EDIT_HEADER,
      component: EditProfilePage,
      passProps: {
        navigator: this.props.navigator,
        data: data,
        getProfile: this.getProfile.bind(this),
        toggleMainSpinner: this.props.toggleMainSpinner.bind(this),
        openMainModal: this.props.openMainModal.bind(this),
        openMainConfirm: this.props.openMainConfirm.bind(this)
      }
    });
  }
}

const styles = StyleSheet.create({
  input: {
    fontSize: 20,
    borderWidth: 0,
    color: '#333435',
    paddingTop: 2,
    paddingBottom: 2,
    fontFamily: 'DB Heavent'
  },
  inputPhone: {
    width: 150,
    height: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#333435',
    paddingTop: 2,
    paddingBottom: 2
  },
  borderView: {
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  borderViewDisable: {
    borderColor: '#c0c0c0',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  disableInput: {
    fontSize: 14,
    borderWidth: 0,
    color: '#cdcdcd',
    paddingTop: 5,
    paddingBottom: 5,
  },
  disableInputNoText: {
    height: 25,
    fontSize: 14,
    borderWidth: 0,
    color: '#cdcdcd',
    paddingTop: 5,
    paddingBottom: 5,
  },
  dropdownBtn: {
    height: 30,
    flexDirection: 'row',
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 2,
    marginLeft: 2,
    paddingLeft: 8,
    paddingRight: 8,
    borderRadius: 18,
  },
  dropdownBtnTxt: {
    color: '#333435',
    marginRight: 4,
    fontSize: 14
  },
  viewMode: {
    minHeight: 25,
    fontSize: 14,
    borderWidth: 0,
    color: '#606060',
    textAlignVertical: 'center',
    paddingTop: 5,
    paddingBottom: 5,
  },
  circle: {
    width: 120,
    height: 120
  },
  circleImage: {
    margin: 10,
    width: 100,
    height: 100,
    borderRadius: 100 / 2,
    position: 'absolute',
  },
  image: {
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  listAssign: {
    paddingLeft: 8,
    paddingRight: 8,
    flexGrow: 1,
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  button: {
    height: 30,
    width: 30,
    paddingTop: 2,
    paddingBottom: 2,
    marginRight: 15,
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    borderWidth: 1,
    borderRadius: 4,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonClose: {
    height: 30,
    width: 30,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center'
  },
});
