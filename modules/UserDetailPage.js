import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Text,
  ScrollView,
  Image,
  TouchableHighlight
} from 'react-native';

import DFTextBox from './FormComponents/DFTextBox'
import Topbar from './Topbar'
import DFLabel from './FormComponents/DFLabel'
import DFErrorLabel from './FormComponents/DFErrorLabel'
import DFButton from './FormComponents/DFButton'
import ImagePicker from 'react-native-image-picker';
import apiConfig from './Util/Config';
import DFService from './Service/DFService';
import STRINGS from './Util/strings';
import AppText from './Util/AppText';

let Ionicons = require('react-native-vector-icons/Ionicons');

export default class UserDetailPage extends Component {
  constructor(props) {
    super(props);
    console.log(this.props.data);
  }

  render() {
    let phoneList = [];
    let phones = this.props.data.phone.join(", ");

    if (this.props.data.phone.length > 0) {
      phoneList.push(<AppText style={styles.dropdownBtnTxt}>{phones}</AppText>);
    } else {
      phoneList.push(<AppText style={styles.dropdownBtnTxt}>-</AppText>);
    }

    let roleList = [];
    for (let i = 0; i < this.props.data.roles.length; i++) {
      roleList.push(
        <View style={styles.dropdownBtn}>
          <View>
            <AppText numberOfLines={1} style={styles.dropdownBtnTxt}>{this.props.data.roles[i].role_name}</AppText>
          </View>
        </View>)
    }
    let imgPath = null;
    if(this.props.data.img_path !== ''&&this.props.data.img_path !== undefined){
      imgPath = {uri: apiConfig.upload_url + this.props.data.img_path}
    }
    let imgPerson = (imgPath !== null) ?
      <Image resizeMode={Image.resizeMode.cover} source={imgPath} style={styles.circleImage}/> :
      <Image resizeMode={Image.resizeMode.cover} source={require('./img/img_person.png')} style={styles.circleImage}/>;

    let formView =
      <View>
        <View style={styles.image}>
          <TouchableHighlight
            style={{justifyContent: 'flex-end', alignItems: 'flex-end'}}
            underlayColor='transparent'>
            <View style={styles.circle}>
              {imgPerson}
            </View>
          </TouchableHighlight>
        </View>
        <DFLabel fontWeight={'bold'} key={"email"} text={STRINGS.USER_DATA.EMAIL} color="black" isRequired={false}/>
        <View style={styles.borderView }>
          <AppText style={styles.input}>
            {(this.props.data.email) ? this.props.data.email : '-'}
          </AppText>
        </View>
        <DFLabel fontWeight={'bold'} key={"name"} text={STRINGS.USER_DATA.NAME} color="black" isRequired={false}/>
        <View style={styles.borderView }>
          <AppText style={styles.input}>
            {(this.props.data.firstName) ? this.props.data.firstName : '-'}
          </AppText>
        </View>

        <DFLabel fontWeight={'bold'} key={"surname"} text={STRINGS.USER_DATA.SURNAME} color="black" isRequired={false}/>
        <View style={styles.borderView }>
          <AppText style={styles.input}>
            {(this.props.data.lastName) ? this.props.data.lastName : '-'}
          </AppText>
        </View>
        <DFLabel fontWeight={'bold'} key={"phone"} text={STRINGS.USER_DATA.TELEPHONE} color="black" isRequired={false}/>
        <View style={{paddingLeft: 20, paddingBottom: 5}}>
            {phoneList}
        </View>

        <DFLabel fontWeight={'bold'} key={"role"} text={STRINGS.USER_DATA.ROLE} color="black" isRequired={false}/>
        <View style={{paddingLeft: 10, paddingBottom: 5}}>
          <View style={styles.listAssign}>
            {roleList}
          </View>
        </View>

        <DFLabel fontWeight={'bold'} key={"department"} text={STRINGS.USER_DATA.DEPARTMENT} color="black" isRequired={false}/>
        <View style={styles.borderView }>
          <AppText style={styles.input}>
            {(this.props.data.department) ? this.props.data.department : '-'}
          </AppText>
        </View>

      </View>;

    return (
      <ScrollView
        ref={(scrollView) => {
          _scrollView = scrollView;
        }}
        automaticallyAdjustContentInsets={false}
        onScroll={() => {
          console.log('onScroll!');
        }}
        scrollEventThrottle={200}>
        {formView}

        <View style={{height: 60}}>
        </View>
      </ScrollView>
    );
  }

}

const styles = StyleSheet.create({
  // input: {
  //   height: 30,
  //   fontSize: 14,
  //   borderWidth: 0,
  //   color: '#333435',
  //   paddingTop: 2,
  //   paddingBottom: 2
  // },
  input: {
    fontSize: 14,
    borderWidth: 0,
    color: '#333435',
    paddingTop: 2,
    paddingBottom: 2,
  },
  inputPhone: {
    width: 150,
    height: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#333435',
    paddingTop: 2,
    paddingBottom: 2
  },
  borderView: {
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  borderViewDisable: {
    borderColor: '#c0c0c0',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  disableInput: {
    fontSize: 14,
    borderWidth: 0,
    color: '#cdcdcd',
    paddingTop: 5,
    paddingBottom: 5,
  },
  disableInputNoText: {
    height: 25,
    fontSize: 14,
    borderWidth: 0,
    color: '#cdcdcd',
    paddingTop: 5,
    paddingBottom: 5,
  },
  dropdownBtn: {
    height: 30,
    flexDirection: 'row',
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 2,
    marginLeft: 2,
    paddingLeft: 8,
    paddingRight: 8,
    borderRadius: 18,
  },
  dropdownBtnTxt: {
    color: '#333435',
    marginRight: 4,
    fontSize: 14
  },
  viewMode: {
    minHeight: 25,
    fontSize: 14,
    borderWidth: 0,
    color: '#606060',
    textAlignVertical: 'center',
    paddingTop: 5,
    paddingBottom: 5,
  },
  circle: {
    width: 120,
    height: 120
  },
  circleImage: {
    margin: 10,
    width: 100,
    height: 100,
    borderRadius: 100 / 2,
    position: 'absolute',
  },
  image: {
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  listAssign: {
    paddingLeft: 8,
    paddingRight: 8,
    flexGrow: 1,
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  button: {
    height: 30,
    width: 30,
    paddingTop: 2,
    paddingBottom: 2,
    marginRight: 15,
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    borderWidth: 1,
    borderRadius: 4,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonClose: {
    height: 30,
    width: 30,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center'
  },
});
