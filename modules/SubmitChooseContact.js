import React, {Component} from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Image,
  ListView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  View
} from 'react-native';

import DFButton from './FormComponents/DFButton';
import DFService from './Service/DFService';
import apiConfig from './Util/Config';
import Util from './Util/Util';
import STRINGS from './Util/strings';
import AppText from './Util/AppText';

let {height, width} = Dimensions.get('window');

let Ionicons = require('react-native-vector-icons/Ionicons');

let mainUserGroupList = [];
let self;
let dataSource = new ListView.DataSource(
  {rowHasChanged: (r1, r2) => r1._id !== r2._id}
);

export default class SubmitChooseContact extends Component {
  constructor(props) {
    super(props);
    //console.log(props);
    this.state = {
      isLoading: true,
      headerTitle: STRINGS.CUSTOM_MODALS.CHOOSE_CONTACT_HEADER,
      dataSource: dataSource.cloneWithRows([{}]),
      isUserSelect: true,
      canAddEmail: false,
      isAddMore: false
    };
    self = this;

    this.usersPermissionList = this.props.usersPermissionList;
    this.nextUsersList = this.props.nextUsersList;

    //console.log('usersPermissionList', this.usersPermissionList);
    //console.log('nextUsersList: ', this.nextUsersList);

    mainUserGroupList = [];
    //this._getUsersPermissions();
    //this._setUsersGroupsList();
    this.searchText = ''
  }

  componentDidMount() {
    this._setUsersGroupsList();
  }

  rowPressed(rowData, sectionID, rowID) {
    console.log("rowPressed");
    for (let i = 0; i < mainUserGroupList.length; i++) {
      if (rowData._id === mainUserGroupList[i]._id) {
        mainUserGroupList[i].selected = !mainUserGroupList[i].selected;
        this.filterAssign(this.searchText);
        break;
      }
    }
  }

  _styleBtn() {
    let isSelected;
    for (let i = 0; i < mainUserGroupList.length; i++) {
      if (mainUserGroupList[i].selected) {
        isSelected = true;
        break;
      } else {
        isSelected = false
      }
    }

    if (isSelected) {
      console.log('Style : active');
      return {
        width: 80,
        height: 36,
        backgroundColor: '#B2D234',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#B2D234',
        borderWidth: 1,
        borderRadius: 4,
      }
    } else {
      console.log('Style : inactive');
      return {
        width: 80,
        height: 36,
        backgroundColor: '#90A3AE',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#90A3AE',
        borderWidth: 1,
        borderRadius: 4,
      }
    }
  }

  removeAssign(id) {
    for (let i = 0; i < mainUserGroupList.length; i++) {
      if (id === mainUserGroupList[i]._id) {
        mainUserGroupList[i].selected = false;
        this.filterAssign(this.searchText);
        break;
      }
    }
    if(this.state.isAddMore) {
      this.refs['assignInput'].blur();
    }
  }

  addEmailIcon() {
    if (this.usersPermissionList.any_email_flag === 'Y') {
      if (Util.validateEmail(this.searchText)) {
        let obj = {
          _id: this.searchText,
          type: 'emails',
          name: this.searchText,
          selected: true
        };
        let isAdd = true;
        for (let i = 0; i < mainUserGroupList.length; i++) {
          if (mainUserGroupList[i].name === this.searchText) {
            mainUserGroupList[i].selected = true;
            isAdd = false;
          }
        }
        if (isAdd) {
          mainUserGroupList.push(obj);
        }
        this.setState({
          dataSource: dataSource.cloneWithRows(mainUserGroupList)
        });
        if(this.state.isAddMore){
          this.refs['assignInput'].blur();
          this.refs['assignInput'].clear();
        }
        this.searchText = '';
        this.setState({
          canAddEmail: false
        });
      }
    }
  }

  styleValidateAddEmail(email) {
    if (this.usersPermissionList.any_email_flag === 'Y') {
      email = email.trim();
      if (Util.validateEmail(email)) {
        this.searchText = email;
        this.setState({
          canAddEmail: true
        });
      } else {
        this.setState({
          canAddEmail: false
        });
      }
    }
  }

  addEmail(email) {
    if (this.usersPermissionList.any_email_flag === 'Y') {
      email = email.trim();
      if (Util.validateEmail(email)) {
        let obj = {
          _id: email,
          type: 'emails',
          name: email,
          selected: true
        };
        let isAdd = true;
        for (let i = 0; i < mainUserGroupList.length; i++) {
          if (mainUserGroupList[i].name === email) {
            mainUserGroupList[i].selected = true;
            isAdd = false;
          }
        }
        if (isAdd) {
          mainUserGroupList.push(obj);
        }
        this.setState({
          dataSource: dataSource.cloneWithRows(mainUserGroupList)
        });
        if(this.state.isAddMore) {
          this.refs['assignInput'].blur();
          this.refs['assignInput'].clear();
        }
        this.searchText = '';
        this.setState({
          canAddEmail: false
        });
      }
    }
  }

  filterAssign(searchText) {
    //this.listView.scrollTo({y: 0})
    this.searchText = searchText.toLowerCase();
    let rows = [];

    for (let i = 0; i < mainUserGroupList.length; i++) {
      let stateName = mainUserGroupList[i].name.toLowerCase();
      if (stateName.search(this.searchText) !== -1) {
        rows.push(mainUserGroupList[i]);
      }
    }

    this.setState({
      dataSource: dataSource.cloneWithRows(rows),
    });
  }

  inputChange(searchText) {
    this.listView.scrollTo({y: 0});
    this.filterAssign(searchText);
    this.styleValidateAddEmail(searchText)
  }

  renderRow(rowData, sectionID, rowID) {

    let checkImg = rowData.selected ? require('./img/submit/choose_checked.png') : require('./img/submit/choose_unchecked.png');
    let bgColor = rowData.selected ? '#E3E9EC' : '#ffffff';
    let icon = null;
    if (rowData.type === 'group') {
      icon = <Ionicons name="md-people" size={20} color="black"
                       style={{justifyContent: 'center', alignItems: 'center'}}/>
    } else if (rowData.type === 'emails') {
      icon = <Ionicons name="md-at" size={20} color="black"
                       style={{justifyContent: 'center', alignItems: 'center'}}/>;
    } else if (rowData.type === 'customers') {
      icon = <Ionicons name="md-bookmarks" size={20} color="black"
                       style={{justifyContent: 'center', alignItems: 'center'}}/>;
    } else {
      icon = <Ionicons name="md-person" size={20} color="black"
                       style={{justifyContent: 'center', alignItems: 'center'}}/>;
    }

    return (
      <View style={styles.rowContainer}>
        <TouchableHighlight
          onPress={() => this.rowPressed(rowData, sectionID, rowID)}
          underlayColor='#dddddd'
          style={{backgroundColor: bgColor}}>
          <View style={styles.innerContainer}>
            <View style={styles.circle}>
              {icon}
            </View>
            <Text style={styles.nameText}>
              {rowData.name}
            </Text>
            <Image source={checkImg} style={styles.checkbox}/>
          </View>
        </TouchableHighlight>
      </View>
    );
  }

  render() {

    let loadView;
    if (this.state.isLoading) {
      loadView =
        <View style={styles.spinner}>
          <ActivityIndicator
            hidden='true'
            size='large'/>
          <AppText>{STRINGS.COMMON.LOADING}</AppText>
        </View>
    } else {
      loadView =
        <ListView
          enableEmptySections={true}
          ref={ref => this.listView = ref}
          dataSource={this.state.isAddMore ? this.state.dataSource : dataSource.cloneWithRows([])}
          renderRow={this.renderRow.bind(this)}
        />
    }

    let bottomContainer =
      <View style={styles.bottomContainer}>
        <AppText style={styles.stepCounter}> </AppText>
        <View style={{alignSelf: "flex-end", flexDirection: 'row'}}>
          {this.props.isNoFlow ?
            <TouchableHighlight
              onPress={this.selectAction.bind(this, 'end')}
              underlayColor='#dddddd'
              style={styles.noFlowBtn}>
              <View>
                <AppText style={{color: '#000000'}}>
                  {STRINGS.SUBMIT_CHOOSE_CONTACT.BTN_END_FLOW}
                </AppText>
              </View>
            </TouchableHighlight> : null
          }

          <TouchableHighlight
            onPress={this._sendContacts.bind(this)}
            underlayColor='#dddddd'
            style={this._styleBtn()}>
            <View>
              <AppText style={{color: '#ffffff'}}>
                {STRINGS.SUBMIT_CHOOSE_CONTACT.BTN_SEND}
              </AppText>
            </View>
          </TouchableHighlight>
        </View>
      </View>;

    let assignList = [];
    for (let i = 0; i < mainUserGroupList.length; i++) {
      if (mainUserGroupList[i].selected) {
        let icon = null;
        if (mainUserGroupList[i].type === 'group') {
          icon = <Ionicons name="md-people" size={18} color="black"
                           style={{justifyContent: 'center', alignItems: 'center'}}/>
        } else if (mainUserGroupList[i].type === 'emails') {
          icon = <Ionicons name="md-at" size={18} color="black"
                           style={{justifyContent: 'center', alignItems: 'center'}}/>;
        } else if (mainUserGroupList[i].type === 'customers') {
          icon = <Ionicons name="md-bookmarks" size={18} color="black"
                           style={{justifyContent: 'center', alignItems: 'center'}}/>;
        } else {
          icon = <Ionicons name="md-person" size={18} color="black"
                           style={{justifyContent: 'center', alignItems: 'center'}}/>;
        }
        assignList.push(
          <View key={"list" + i} style={styles.dropdownBtn}>
            <View style={{marginRight: 3}}>
              {icon}
            </View>
            <View>
              <Text numberOfLines={1} style={styles.dropdownBtnTxt}>{mainUserGroupList[i].name}</Text>
            </View>
            <View>
              <TouchableHighlight
                underlayColor='transparent'
                onPress={this.removeAssign.bind(this, mainUserGroupList[i]._id)}>
                <Ionicons name="md-close" size={18} color="black"
                          style={{justifyContent: 'center', alignItems: 'center'}}/>
              </TouchableHighlight>
            </View>
          </View>)
      }
    }

    let topContainer =
      <View style={styles.topOuterContainer}>
        <View style={styles.topContainer}>
          <View style={{alignSelf: 'flex-start', padding: 10}}>
            <AppText style={{fontSize: 15, color: '#757575'}}>To: </AppText>
          </View>
          <ScrollView>
            <View style={styles.listAssign}>
              {assignList}
            </View>
          </ScrollView>
        </View>
        <View style={{
          paddingTop: 10, flexDirection: 'row', justifyContent: 'flex-end'
        }}>
          <TouchableHighlight
            underlayColor='#dddddd'
            style={{
              width: 80,
              height: 36,
              backgroundColor: '#B2D234',
              justifyContent: 'center',
              alignItems: 'center',
              borderColor: '#B2D234',
              borderWidth: 1,
              borderRadius: 4,
            }}
            onPress={() => this.addMore()}>
            <View>
              <AppText style={{color: '#ffffff'}}>
                {this.state.isAddMore ? STRINGS.COMMON.CLOSE : STRINGS.COMMON.ADD_MORE}
              </AppText>
            </View>
          </TouchableHighlight>
        </View>
        {this.state.isAddMore ?
          <View style={{
            paddingLeft: 10, paddingRight: 10, flexDirection: 'row',
            flexWrap: 'wrap', alignItems: 'flex-start'
          }}>
            <TextInput
              ref='assignInput'
              returnKeyType={"done"}
              style={styles.input}
              placeholder={'Input user'}
              blurOnSubmit={false}
              onChange={(event) => this.inputChange(event.nativeEvent.text)}
              onSubmitEditing={(event) => this.addEmail(event.nativeEvent.text)}
              underlineColorAndroid='rgba(0,0,0,0)'
            />
            {this.state.canAddEmail ?
              <TouchableHighlight
                underlayColor='transparent'
                onPress={() => this.addEmailIcon()}>
                <Ionicons name="ios-checkmark-circle" size={25} color="#B2D234"
                          style={{justifyContent: 'center', alignItems: 'center'}}/>
              </TouchableHighlight>
              : null
            }
          </View> : null}
        {this.state.isAddMore ?
          <View style={styles.separator}/> : null}
      </View>;

    return (
      <View style={styles.container}>
        <View style={styles.outer_innerContainer}>
          <View style={styles.headerModal}>
            <AppText style={styles.textHeaderModal}>
              {this.state.headerTitle}
            </AppText>
            <TouchableHighlight
              underlayColor='transparent'
              style={{marginRight: 13}}
              onPress={() => this.props.closeModalFail()}>
              <Ionicons name="md-close" size={25} color="black"
                        style={{justifyContent: 'center', alignItems: 'center'}}/>
            </TouchableHighlight>
          </View>

          <View style={{flexGrow: 9}}>
            <View style={{
              flexDirection: 'column',
              height: height - 180,
              flexGrow: 1,
              paddingTop: 13, paddingLeft: 13, paddingRight: 13
            }}>
              {!this.state.isLoading ? topContainer : null}
              {loadView}
              {!this.state.isLoading ? bottomContainer : null}
            </View>
          </View>
        </View>
      </View>
    );
  }

  addMore() {
    this.setState({
      isAddMore: !this.state.isAddMore
    });
  }

  selectAction(action) {
    this.props.noFlowAction(action);
  }

  _sendContacts() {
    let users = [];
    let groups = [];
    let emails = [];
    let customers = [];
    let isSelected;
    for (let i = 0; i < mainUserGroupList.length; i++) {
      if (mainUserGroupList[i].selected) {
        isSelected = true;
        break;
      } else {
        isSelected = false;
      }
    }

    if (isSelected) {
      for (let i = 0; i < mainUserGroupList.length; i++) {
        if (mainUserGroupList[i].selected) {
          if (mainUserGroupList[i].type === 'user') {
            users.push(mainUserGroupList[i]._id)
          } else if (mainUserGroupList[i].type === 'group') {
            groups.push(mainUserGroupList[i]._id)
          } else if (mainUserGroupList[i].type === 'emails') {
            emails.push(mainUserGroupList[i]._id)
          } else if (mainUserGroupList[i].type === 'customers') {
            customers.push(mainUserGroupList[i]._id)
          }
        }
      }
      let data = {"users": users, "groups": groups, "emails": emails, "customers": customers};
      this.props.closeModal(true, data);
    }
  }

  _setUsersGroupsList() {
    //console.log(this.usersPermissionList);
    if ('users' in this.usersPermissionList &&
      'groups' in this.usersPermissionList &&
      'customers' in this.usersPermissionList &&
      'emails' in this.usersPermissionList) {
      let userList = [];
      for (let i = 0; i < this.usersPermissionList.users.length; i++) {
        let tmp = this.usersPermissionList.users[i];
        let isNext = this._checkUserInList(tmp._id);
        let obj = {
          _id: tmp._id,
          type: 'user',
          name: tmp.displayName,
          selected: isNext
        };
        if (isNext) {
          mainUserGroupList.push(obj)
        } else {
          userList.push(obj)
        }
      }
      for (let i = 0; i < this.usersPermissionList.groups.length; i++) {
        let tmp = this.usersPermissionList.groups[i];
        let isNext = this._checkUserInList(tmp._id);
        let obj = {
          _id: tmp._id,
          type: 'group',
          name: tmp.name,
          selected: isNext
        };
        if (isNext) {
          mainUserGroupList.push(obj)
        } else {
          userList.push(obj)
        }
      }
      for (let i = 0; i < this.usersPermissionList.customers.length; i++) {
        let tmp = this.usersPermissionList.customers[i];
        let isNext = this._checkUserInList(tmp._id);
        let obj = {
          _id: tmp._id,
          type: 'customers',
          name: tmp.name,
          selected: isNext
        };
        if (isNext) {
          mainUserGroupList.push(obj)
        } else {
          userList.push(obj)
        }
      }

      for (let i = 0; i < this.usersPermissionList.emails.length; i++) {
        let tmp = this.usersPermissionList.emails[i];
        let isNext = this._checkUserInList(tmp);
        let obj = {
          _id: tmp,
          type: 'emails',
          name: tmp,
          selected: isNext
        };
        if (isNext) {
          mainUserGroupList.push(obj)
        } else {
          userList.push(obj)
        }
      }

      mainUserGroupList.push(...userList);
    }

    this.setState({
      isLoading: false,
      dataSource: dataSource.cloneWithRows(mainUserGroupList)
    });
  }

  _checkUserInList(user_id) {
    for (let i = 0; i < this.nextUsersList.length; i++) {
      if (this.nextUsersList[i]._id === user_id) {
        return true
      }
    }
    return false
  }

  _getUsersPermissions() {
    let apiData = {};
    let config = JSON.parse(JSON.stringify(apiConfig.getUsersPermissions));
    config.url += this.props.formID;
    DFService.callApiWithHeader(apiData, config, function (error, response) {

      if (response) {
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          self.usersPermissionList = response.data;
          if (self.props.isNoFlow) {
            self._setUsersGroupsList();
          } else {
            self._getNextUsers();
          }
        } else {
          // Failed Response
          self.props.openMainModal(response.resMessage)
        }
      } else {
        // Error from application calling service
        console.log("error = ", error);
      }

    });
  }

  _getNextUsers() {
    let apiData = {};
    let config = JSON.parse(JSON.stringify(apiConfig.getNextUsers));
    config.url += this.props.formJobID + '/' + this.props.currentStep;

    DFService.callApiWithHeader(apiData, config, function (error, response) {

      if (response) {
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          let userList = [];
          if (response.data.users) {
            for (let i = 0; i < response.data.users.length; i++) {
              userList.push(response.data.users[i])
            }
          }

          if (response.data.groups) {
            for (let i = 0; i < response.data.groups.length; i++) {
              userList.push(response.data.groups[i])
            }
          }

          if (response.data.customers) {
            for (let k = 0; k < response.data.customers.length; k++) {
              userList.push({
                "type": "customers",
                "_id": response.data.customers[k]._id,
                "name": response.data.customers[k].name,
                selected: false
              });
            }
          }

          if (response.data.emails) {
            for (let k = 0; k < response.data.emails.length; k++) {
              userList.push({
                "type": "emails",
                "_id": response.data.emails[k],
                "name": response.data.emails[k],
                selected: false
              });
            }
          }

          self.nextUsersList = userList;
          self._setUsersGroupsList();
        } else {
          // Failed Response
          self.props.openMainModal(response.resMessage)
        }
      } else {
        // Error from application calling service
        console.log("error = ", error);
      }
    });
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent: 'center',
    padding: 20,
    zIndex: 99
  },
  headerModal: {
    paddingTop:10,
    paddingBottom:10,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#efeff1'
  },
  textHeaderModal: {
    marginLeft: 20,
    fontWeight: 'bold',
    fontSize: 18,
  },
  outer_innerContainer: {
    height: height - 180,
    width: width - 40,
    // alignItems: 'center',
    backgroundColor: '#ffffff',
    flexGrow: 1,
    flexDirection: 'column'
  },
  spinner: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  circle: {
    width: 40,
    height: 40,
    // flex: 0.15,
    borderRadius: 40 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: '#ADB0B1',
    marginLeft: 3,
    marginRight: 3
  },
  rowContainer: {
    backgroundColor: '#ffffff'
  },
  innerContainer: {
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'transparent'
  },
  bottomContainer: {
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'transparent'
  },
  checkbox: {
    width: 35,
    height: 35,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  nameText: {
    fontSize: 20,
    width: width - 35 - 40 - 70,
    fontWeight: 'bold',
    textAlign: 'left',
    alignSelf: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    fontFamily: 'DB Heavent'
  },
  sendBtn: {
    backgroundColor: "#B2D234",
    borderColor: "#B2D234",
    alignSelf: "flex-end",
    flexGrow: undefined,
    paddingLeft: 30,
    paddingRight: 30
  },
  inActiveSendBtn: {
    backgroundColor: "#dddddd",
    borderColor: "#dddddd",
    alignSelf: "flex-end",
    flexGrow: undefined,
    paddingLeft: 30,
    paddingRight: 30
  },
  stepCounter: {
    justifyContent: 'center',
  },
  topContainer: {
    maxHeight: 100,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'transparent',
  },
  topOuterContainer: {
    paddingLeft: 5,
    paddingRight: 5,
  },
  userListText: {
    fontSize: 14,
    color: '#90969D'
  },
  separator: {
    flexGrow: 1,
    height: 1,
    backgroundColor: '#CCCCCC'
  },
  listAssign: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  dropdownBtn: {
    height: 30,
    flexDirection: 'row',
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 2,
    marginRight: 2,
    marginLeft: 2,
    paddingRight: 8,
    paddingLeft: 8,
    borderRadius: 15,
  },
  dropdownBtnTxt: {
    marginRight: 4,
    fontSize: 18,
    maxWidth: 140,
    fontFamily: 'DB Heavent'
  },
  input: {
    flexGrow: 0.9,
    height: 35,
    fontSize: 18,
    borderWidth: 0,
    color: '#333435'
  },
  noFlowBtn: {
    width: 80,
    height: 36,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#000000',
    borderWidth: 1,
    borderRadius: 4,
    marginRight: 10,
  }
});
