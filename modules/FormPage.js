import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableHighlight,
  ActivityIndicator,
  ScrollView,
  Keyboard,
  Dimensions,
  Platform,
  NativeModules,
  AsyncStorage,
  Alert,
  NetInfo,
  AppState,
  InteractionManager,
  LayoutAnimation
} from 'react-native';

import DFLabel from './FormComponents/DFLabel'
import DFErrorLabel from './FormComponents/DFErrorLabel'
import DFTextBox from './FormComponents/DFTextBox'
import DFNumber from './FormComponents/DFNumber'
import DFTextArea from './FormComponents/DFTextArea'
import DFDatePicker from './FormComponents/DFDatePicker'
import DFRadio from './FormComponents/DFRadio'
import DFCheckBox from './FormComponents/DFCheckBox'
import DFDropDown from './FormComponents/DFDropDown'
import DFSeparator from './FormComponents/DFSeparator'
import DFButton from './FormComponents/DFButton'
import DFPhoto from './FormComponents/DFPhoto'
import DFLocation from './FormComponents/DFLocation'
import DFAddress from './FormComponents/DFAddress'
import DFFile from './FormComponents/DFFile'
import DFBarcode from './FormComponents/DFBarcode'
import DFSignature from './FormComponents/DFSignature'
import DFAutoComplete from './FormComponents/DFAutoComplete'
import DFDownloadPDF from './FormComponents/DFDownloadPDF'
import CancelModel from './CustomViews/CancelModel'
import ActionModel from './CustomViews/ActionModel'
import NoFlowModel from './CustomViews/NoFlowModel'
import Device from './Util/Util'
import ProgressSpinner from './Animation/ProgressSpinner'
import DFService from './Service/DFService';
import apiConfig from './Util/Config';
import DFGPS from './FormComponents/DFGPS'
import DFTime from './FormComponents/DFTime'
import SubmitChooseContact from './SubmitChooseContact'
import SaveFormDialog from './Util/SaveFormDialog'
import AlertConfirmDialog from './Util/AlertConfirmDialog';


import Picker from 'react-native-picker'

const Timer = require('react-native-timer');
var Ionicons = require('react-native-vector-icons/Ionicons');
import config from './Util/Config'
import AppText from './Util/AppText';
import STRINGS from './Util/strings'
import RNFetchBlob from 'react-native-fetch-blob';

console.ignoredYellowBox = [
  'Warning: Failed propType',
  'Warning: ScrollView'
  // Other warnings you don't want like 'jsSchedulingOverhead',
];

var {height, width} = Dimensions.get('window');

var self;

class FormPage extends Component {

  constructor(props) {
    // console.log('CONSTRUCTOR : Form Page')
    super(props);
    this.state = {
      message: '',
      errorMessage: '',
      formComponents: null,
      formData: this.props.formData,
      visibleHeight: Dimensions.get('window').height,
      scrollViewAdjustment: 0,
      indexToFocus: 0,
      isLoadingTemp: false,
      isLoading: true,
      success: false,
      showModal: false,
      doneSaving: false,
      saveSuccess: false,
      spinnerText: STRINGS.COMMON.LOADING,
      confirmMessage: STRINGS.FORM_PAGE.CONFIRM_SUBMIT,
      rejectModel: false,
      cancelModel: false,
      actionModel: false,
      noFlowModel: false,
      alertModel: false,
      selectedUserContacts: [],
      errorArray: [],
      province: null,
      timeValue: ["00", "00", "00"],
      isConnected: false,
      appState: AppState.currentState,
      previousAppState: "active",
      showAlertDialog: false,
      showAlertPopup: false,
      alertDialogType: '',
      alertDialogMsg: '',
      alertMsg: '',
      endFlowConfirm: false,
      renderPlaceHolderOnly: true,
      renderPlaceHolderText: STRINGS.FORM_PAGE.LOADING_FORM,
      fileForGenerate: '',
      isTwoColumns: false,
      isNextUser: false
    };

    //console.log('form data on load : ' + JSON.stringify(this.state.formData))

    self = this;
    this.isNoFlow = false;
    this.isDraft = false;
    this.isActualDraft = false;
    this.hasAutoSaveData = false;
    this.pageIndex = 0;
    this.pageSize = 0;
    this.dataRender = {};
    this.formObject = {};
    this.formObjectTemp = {};
    this.fieldsEnableList = [];
    this.textBoxData = {};
    this.timeKey = -1;
    this.fileUri = new Map();
    this.fileUpload = [];
    this.formQueue = null;

    this.usersPermissionList = {};
    this.nextUsersList = [];

    this.CURRENT_DATE_TIME = new Date();

    // this.userid = this.props.userid
    if ('draft_form_id' in this.props) {
      this.draftFormId = this.props.draft_form_id
    } else {
      this.draftFormId = this.state.formData.draft_form_id
    }

    let formStatus = this.state.formData.status;
    this.IS_VIEW_MODE = (formStatus === "EXPIRED" || formStatus === "INPROGRESS" || formStatus === "COMPLETED" || formStatus === "CANCELED")
  }

  _closeModalFail = () => {
    console.log('Closing Working Detail Page. Error Occurred');
    self.props.refreshWorkingList();
    self.props.navigator.popToTop(0);
  };

  onBackPressed(popToTop) {
    if (this.IS_VIEW_MODE) {
      if (popToTop) {
        this.props.navigator.popToTop(0)
      } else {
        this.props.navigator.pop()
      }
    } else {
      if (!this.state.renderPlaceHolderOnly) {
        this.setState({renderPlaceHolderOnly: true, renderPlaceHolderText: STRINGS.FORM_PAGE.SAVING_DATA})

        let finishAutoSave = () => {
          if (popToTop) {
            this.props.navigator.popToTop(0)
          } else {
            this.props.navigator.pop()
          }
        };

        this._autoSaveForm(finishAutoSave)
      }
    }

  }

  _getUsersPermissions() {
    let apiData = {};
    let config = JSON.parse(JSON.stringify(apiConfig.getUsersPermissions));
    config.url += this.props.formData.user_form_id._id;
    DFService.callApiWithHeader(apiData, config, function (error, response) {
      if (response) {

        // console.log('GOT USERS PERMISSION!!', response);

        if (response.resCode === 'DF2000000') {
          // Successfull Response
          self.usersPermissionList = response.data;
          if (self.isNoFlow) {
            // self._setUsersGroupsList();
            //self.setState({isLoading: false, renderPlaceHolderOnly: false})
            self._getNextUsers();
          } else {
            self._getNextUsers();
          }
        } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
          let callback = self._closeModalFail;
          self.props.openMainModal(response.resMessage, callback)
        } else {
          // Failed Response
          self.props.openMainModal(response.resMessage)
        }
      } else {
        // Error from application calling service
        self.props.openMainModal();
        console.log("_getUsersPermissions error = ", error);
      }
    });
  }

  _getNextUsers() {
    let apiData = {};
    let config = JSON.parse(JSON.stringify(apiConfig.getNextUsers));
    config.url += this.props.formData.form_job_id._id + '/' + this.props.formData.form_job_id.current_step;

    DFService.callApiWithHeader(apiData, config, function (error, response) {
      // console.log('_getNextUsers Response: ', response)
      if (response) {
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          if (response.data) {
            let userList = [];
            for (let i = 0; i < response.data.users.length; i++) {
              userList.push(response.data.users[i])
            }
            for (let i = 0; i < response.data.groups.length; i++) {
              userList.push(response.data.groups[i])
            }
            for (let i = 0; i < response.data.customers.length; i++) {
              userList.push(response.data.customers[i])
            }
            for (let i = 0; i < response.data.emails.length; i++) {
              let tmp_obj = {
                _id: response.data.emails[i],
                email: response.data.emails[i]
              };
              userList.push(tmp_obj);
            }
            self.nextUsersList = userList;
          }
          self.setState({isLoading: false, renderPlaceHolderOnly: false, isNextUser: true})
        } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
          self.setState({isLoading: false, renderPlaceHolderOnly: false});
          let callback = self._closeModalFail;
          self.props.openMainModal(response.resMessage, callback)
        } else {
          // Failed Response
          self.props.openMainModal(response.resMessage)
        }
      } else {
        // Error from application calling servicex
        self.props.openMainModal()
        console.log("_getNextUsers error = ", error);
      }
    });
  }

  _getFormDetail() {
    console.log('Get Form Detail');
    let apiData = {};
    let config = {
      method: apiConfig.getFormList.method,
      url: apiConfig.getFormList.url,
      action_name: apiConfig.getFormList.action_name
    };
    config.url += this.props.formData.user_form_id._id;

    DFService.callApiWithHeader(apiData, config, function (error, response) {
      if (response) {
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          self.formObject = response.data;
          // console.log(self.formObject.header);
          // console.log(self.formObject.footer);
          self.formObjectTemp = JSON.parse(JSON.stringify(self.formObject));
          //console.log(' aoo object : ' + JSON.stringify(self.formObject))
          if (self.formObject.form_flow_id === undefined) {
            self.isNoFlow = true;
          } else {
            self.fieldsEnableList = self.formObject.form_flow_id.steps[self.props.formData.form_job_id.current_step - 1].field_keys;
            //console.log("fieldsEnableList = ", self.fieldsEnableList);
          }
          self.getDraftFormDetail();
        } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
          self.setState({isLoading: false, renderPlaceHolderOnly: false});
          let callback = self._closeModalFail;
          self.props.openMainModal(response.resMessage, callback)
        } else {
          // Failed Response
          self.setState({isLoading: false, renderPlaceHolderOnly: false});
          self.props.openMainModal(response.resMessage)
        }
      } else {
        // Error from application calling service
        self.setState({isLoading: false, renderPlaceHolderOnly: false});
        console.log("_getFormDetail error = ", error);
      }

    });
  }

  _getFormData() {
    console.log('Get Form Data');
    let apiData = {};
    let config = {
      method: apiConfig.getFormData.method,
      url: apiConfig.getFormData.url,
      action_name: apiConfig.getFormData.action_name
    };
    config.url += this.props.formData.form_job_id.form_data_id._id;

    DFService.callApiWithHeader(apiData, config, function (error, response) {
      if (response) {
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          self.formObject = response.data;

          self.formObjectTemp = JSON.parse(JSON.stringify(self.formObject));
          if (self.formObject.form_flow_id === undefined) {
            self.isNoFlow = true;
          } else {
            self.fieldsEnableList = self.formObject.form_flow_id.steps[self.props.formData.form_job_id.current_step - 1].field_keys;
          }
          self.getDraftFormDetail();
        } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
          self.setState({isLoading: false, renderPlaceHolderOnly: false});
          let callback = self._closeModalFail;
          self.props.openMainModal(response.resMessage, callback)
        } else {
          // Failed Response
          self.setState({isLoading: false, renderPlaceHolderOnly: false});
          self.props.openMainModal(response.resMessage)
        }
      } else {
        // Error from application calling service
        self.setState({isLoading: false, renderPlaceHolderOnly: false});
        console.log("_getFormData error = ", error);
      }
    });
  }

  setPageIndex() {
    console.log("setPageIndex ");
    this.dataRender = this.formObject.pages[this.pageIndex];

    let fieldColumn = this.dataRender.layout;
    let isTwoColumns = false;
    if (fieldColumn === "2-column") {
      if (!Device.isPhone()) {
        isTwoColumns = true;
      }
    }

    this.setState({
      isLoadingTemp: true,
      isTwoColumns: isTwoColumns
    });

    if (Device.isPhone()) {
      if (this.refs.scrollView) {
        this.refs.scrollView.scrollTo({x: 0, y: 0, animated: false})
      }
    } else {
      if (this.refs.scrollViewLeft) {
        this.refs.scrollViewLeft.scrollTo({x: 0, y: 0, animated: false})
      }
      if (this.refs.scrollViewRight) {
        this.refs.scrollViewRight.scrollTo({x: 0, y: 0, animated: false})
      }

    }
  }

  nextPage() {
    if (this.pageIndex < this.formObject.pages.length - 1) {
      this.setState({
        isLoadingTemp: false
      });
      this.pageIndex += 1;
      this.setPageIndex();
    }
  }

  prevPage() {
    if (this.pageIndex > 0) {
      this.setState({
        isLoadingTemp: false,
        dataRenderLoaded: false
      });
      this.pageIndex -= 1;
      this.setPageIndex();
    }
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow)
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide)
  }

  componentDidMount() {
    // console.log('COMPONENT DID MOUNT : Form Page')

    AppState.addEventListener('change', this._handleAppStateChange);

    NetInfo.isConnected.addEventListener(
      'change',
      this._handleConnectivityChange
    );
    NetInfo.isConnected.fetch().done(
      (isConnected) => {
        this.setState({isConnected});
      }
    );

    // console.log("formData = ", this.props.formData);
    InteractionManager.runAfterInteractions(() => {
      if (this.props.formData.form_job_id.form_data_id === undefined || this.props.formData.form_job_id.form_data_id === null) {
        this._getFormDetail();
      } else {
        this._getFormData();
      }
    });

  }

  componentWillUnmount() {
    Timer.clearTimeout(this);

    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();

    AppState.removeEventListener('change', this._handleAppStateChange);

    NetInfo.isConnected.removeEventListener(
      'change',
      this._handleConnectivityChange
    );
  }

  _handleConnectivityChange = (isConnected) => {
    console.log("isConnected: " + isConnected)
    this.setState({
      isConnected,
    });
  };

  _keyboardDidShow = (e) => {
    let newSize = Dimensions.get('window').height - e.endCoordinates.height
    let keyboardHeight = this.state.visibleHeight - newSize
    this.setState({
      visibleHeight: newSize,
      scrollViewAdjustment: keyboardHeight
    })
  }

  _keyboardDidHide = (e) => {
    this.setState({
      visibleHeight: Dimensions.get('window').height,
      scrollViewAdjustment: 0
    })
  }

  isFieldEnabled(fieldKey) {
    if (this.IS_VIEW_MODE) {
      return false
    } else {
      if (this.isNoFlow) {
        return true;
      } else {
        return this.fieldsEnableList.indexOf(fieldKey) !== -1;
      }
    }
  }

  closeDialog() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      noFlowModel: false,
      rejectModel: false,
      cancelModel: false
    })
  }

  rejectModal() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      rejectModel: true,
      cancelModel: false
    })
  }

  cancelModal() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      rejectModel: false,
      cancelModel: true
    })
  }

  backPage() {
    this.setState({
      rejectModel: false,
      cancelModel: false
    });
    this.props.refreshWorkingList();
    this.props.navigator.popToTop(0);
  }

  async errorCancelReject(saveData, type) {
    var value = await AsyncStorage.getItem("Profile");
    var profile = await JSON.parse(value);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      rejectModel: false,
      cancelModel: false
    });

    this.formQueue = {
      user_id: profile.user.id,
      url: this.props.formData.user_form_id._id,
      request_type: type,
      form_data: this.props.formData,
      form_save: saveData,
      uploads: [],
      form_draft_id: this.draftFormId
    };

    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      alertModel: true
    });
  }

  actionModal() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      actionModel: true
    })
  }

  closeAction(num) {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      actionModel: false
    });
    if (num === 0) {
      this.cancelModal();
    } else if (num === 1) {
      this.rejectModal()
    } else if (num === 2) {
      //noinspection JSAnnotator
      Timer.setTimeout(self, 'closeModal', () => {
        self.clearData()
      }, 500)

    } else if (num === 3) { // Save Draft
      this.saveDraftForm()
    } else if (num === 4) { // Cancel Draft
      this.showAlertDialog('CANCEL DRAFT')
    } else if (num === 5) { // Cancel Draft
      this.generatePDF();
    }
  }

  saveDraftForm() {
    let self = this;
    var saveData = JSON.parse(JSON.stringify(this.formObject));

    this.setState({isLoading: true});

    for (let i = 0; i < saveData.pages.length; i++) {
      for (let j = 0; j < saveData.pages[i].fields.length; j++) {
        saveData.pages[i].fields[j].field_id = field_id = saveData.pages[i].fields[j].field_id._id
      }
    }

    var config = JSON.parse(JSON.stringify(apiConfig.saveFormDraft));
    config.url += self.state.formData.draft_form_id;

    DFService.callApiWithHeader(saveData.pages, config, function (error, response) {

      if (response) {
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          self.success(response, false)
        } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
          self.setState({
            isLoading: false
          });
          let callback = self.forceBack;
          self.props.openMainModal(response.resMessage, callback)
        } else {
          // Failed Response
          self.setState({isLoading: false});
          self.props.openMainModal(response.resMessage)
        }
      } else {
        // Error from application calling service
        self.submitError();
        console.log("saveDraftForm error = ", error);
      }

    });
  }

  cancelDraftForm(_needPopUp, silent = false) {
    let self = this;

    if (!silent) {
      this.setState({isLoading: true})
    }
    var apiData = {};
    var config = {
      method: apiConfig.cancelDraftForm.method,
      url: apiConfig.cancelDraftForm.url,
      action_name: apiConfig.cancelDraftForm.action_name
    };
    config.url += self.state.formData._id;

    var configNewCancel = {
      method: apiConfig.cancelDraftFormByUser.method,
      url: apiConfig.cancelDraftFormByUser.url,
      action_name: apiConfig.cancelDraftFormByUser.action_name
    };
    configNewCancel.url += self.state.formData._id;
    if (_needPopUp) {
      DFService.callApiWithHeader(apiData, configNewCancel, function (error, response) {
        if (response) {
          if (response.resCode === 'DF2000000') {
            // Successfull Response
            self._clearAutoSaveData();
            self.success(response, true);
          } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
            self.setState({
              isLoading: false
            });
            let callback = self.forceBack;
            self.props.openMainModal(response.resMessage, callback)
          } else {
            // Failed Response
            self.setState({isLoading: false})
            self.props.openMainModal(response.resMessage)
          }
        } else {
          // Error from application calling service
          self.cancelDraftError(apiData, self.state.formData._id);
          console.log("cancelDraftForm _needPopUp error = ", error);
        }
      });
    } else {
      DFService.callApiWithHeader(apiData, config, function (error, response) {
        if (response) {
          if (response.resCode === 'DF2000000') {
            // Successfull Response
            self._clearAutoSaveData();
            if (silent) {
              self.props.refreshWorkingList();
              self.props.navigator.popToTop(0);
            } else {
              self.success(response);
            }
          } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
            self.setState({
              isLoading: false
            });
            let callback = self.forceBack;
            self.props.openMainModal(response.resMessage, callback)
          } else {
            // Failed Response
            self.setState({isLoading: false})
            self.props.openMainModal(response.resMessage)
          }
        } else {
          // Error from application calling service
          self.submitError();
          console.log("cancelDraftForm error = ", error);
        }
      });
    }
  }

  getDraftFormDetail() {
    let self = this;
    console.log('getDraftFormDetail :', self.state.formData.draft_form_id);
    if (this.state.formData.draft_form_id) {

      let config = JSON.parse(JSON.stringify(apiConfig.getFormDraftDetail));
      if (typeof self.state.formData.draft_form_id === "object") {
        config.url += self.state.formData.draft_form_id._id;
      } else {
        config.url += self.state.formData.draft_form_id;
      }

      DFService.callApiWithHeader(null, config, function (error, response) {

        if (response) {
          if (response.resCode === 'DF2000000') {
            // Successfull Response
            if (response.data !== undefined && response.data.pages.length > 0) {
              if (response.data.createdDate !== response.data.updatedDate) {
                console.log('SET IS DRAFT TRUE >>> 1');
                self.isDraft = true;
                self.isActualDraft = true;
              }
              self.formObject.pages = response.data.pages
            } else {
              // console.log('No page data')
            }
            self.pageSize = self.formObject.pages.length;
            self._loadAutoSaveData();
          } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
            self.setState({isLoading: false, renderPlaceHolderOnly: false});
            let callback = self._closeModalFail;
            self.props.openMainModal(response.resMessage, callback)
          } else {
            // Failed Response
            console.log('Failed getDraftFormDetail');

            self.setState({isLoading: false, renderPlaceHolderOnly: false});
            self.props.openMainModal(response.resMessage)
          }
        } else {
          // Error from application calling service
          self.setState({isLoading: false, renderPlaceHolderOnly: false});
          console.log("getDraftFormDetail error = ", error);
        }

      });
    } else {
      this.pageSize = this.formObject.pages.length;
      this.setPageIndex();

      if (this.isNoFlow) {
        // console.log('GETING: _getUsersPermissions')
        this._getUsersPermissions();
      } else {
        if (this.props.formData.form_job_id.current_step === this.props.formData.form_job_id.form_flow_id.steps.length) {
          // On the last step
          this.setState({isLoading: false, renderPlaceHolderOnly: false})
        } else {
          // console.log('GETING WITH FLOW: _getUsersPermissions')
          this._getUsersPermissions();
        }
      }

    }
  }

  noFlowAction(action) {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      showModal: true,
      endFlowConfirm: true
    });
  }

  endFlowConfirmDialogSuccess() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      endFlowConfirm: false,
      isLoading: true
    });
    this._processFileUploads('submitFormNoFlow')
  }

  endFlowConfirmDialogFail() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      endFlowConfirm: false
    });
  }

  clearData() {
    this._removeFileFromDownloadQueueForPage(this.pageIndex);

    this.setState({
      isLoadingTemp: false,
      errorArray: []
    });
    this.isDraft = false;
    this.formObject.pages[this.pageIndex] = JSON.parse(JSON.stringify(this.formObjectTemp.pages[this.pageIndex]));
    let CURRENT_DATE = new Date();

    let PAGE = this.formObject.pages[this.pageIndex];
    for (let j = 0; j < PAGE.fields.length; j++) {
      let FIELD = PAGE.fields[j];
      if (this.isFieldEnabled(FIELD.field_key)) {
        if (FIELD.field_id.type === 'calendar' || FIELD.field_id.type === 'time') {
          if (FIELD.properties !== null && FIELD.properties !== undefined) {
            let propertiesList = FIELD.properties;
            let property = null;
            for (let k = 0; k < propertiesList.length; k++) {
              if (propertiesList[k].checked) {
                property = propertiesList[k].name
              }
            }

            if (property !== null && (property === 'Current Time' || property === 'Current Date')) {
              let formatStyleList = FIELD.style;
              let formatStyle = null;
              if (formatStyleList !== null || formatStyleList !== undefined) {
                for (let k = 0; k < formatStyleList.length; k++) {
                  if (formatStyleList[k].checked) {
                    formatStyle = formatStyleList[k].name
                  }
                }
              }

              /// Set Time Value
              if (FIELD.field_id.type === 'time') {
                FIELD.value = Device.fnFormateTime('', formatStyle, CURRENT_DATE);
              }

              // Set Date Value
              if (FIELD.field_id.type === 'calendar') {
                FIELD.value = Device.fnFormateDate(CURRENT_DATE);
              }
            }
          }
        }
      }
    }

    this.setPageIndex();
  }

  _renderPlaceHolderView() {
    return (
      <View style={styles.spinner}>
        <ActivityIndicator
          hidden='true'
          size='large'/>
        <AppText>{this.state.renderPlaceHolderText}</AppText>
      </View>
    );
  }

  render() {

    if (this.state.renderPlaceHolderOnly) {
      return this._renderPlaceHolderView();
    }

    var renderForm = [];
    var formComponents = [];
    var formComponentsLeft = [];
    var formComponentsRight = [];
    var fieldColumn = "";


    var max_length_text = 200;
    var max_length_textArea = 1000;
    var header = null;

    if (this.state.isLoadingTemp) {
      if (this.formObject.header !== undefined) {
        var headerAlign = 'center';
        let styleHeader = {textAlign: 'left', justifyContent: 'flex-start'};
        if (this.formObject.header.text_align === 'left') {
          headerAlign = 'flex-start';
        } else if (this.formObject.header.text_align === 'right') {
          headerAlign = 'flex-end';
          styleHeader.textAlign = 'right';
          styleHeader.justifyContent = 'flex-end';
        }
        let headerImage = null;
        if (this.formObject.header.img_path !== '' && this.formObject.header.img_path !== null) {
          headerImage = <Image resizeMode='stretch'
                               source={this.getImagePath(this.formObject.header.img_path)} style={styles.imgHeader}/>

        }
        // console.log(this.formObject.header);
        if (this.formObject.header.enable) {

          if (this.formObject.header.background_type === 'image') {
            if (this.formObject.header.background_image === '') {
              header =
                <View>
                  <View
                    style={[styles.rowContainer, {
                      justifyContent: headerAlign
                    }]}>
                    {(this.formObject.header.text_align !== 'right') ? headerImage : null}
                    <View style={{
                      flex: 1,
                      flexDirection: 'column',
                      paddingLeft: 10,
                      paddingRight: 10,
                    }}>
                      <AppText numberOfLines={1}
                               style={[{
                                 fontSize: 14, color: '#666666'
                               }, styleHeader]}>{this.formObject.header.title}</AppText>
                      <AppText numberOfLines={2}
                               style={[{
                                 fontSize: 10, color: 'gray'
                               }, styleHeader]}>{this.formObject.header.description}</AppText>
                    </View>
                    {(this.formObject.header.text_align === 'right') ? headerImage : null}
                  </View>
                  <View style={styles.separator}/>
                </View>;
            } else {
              header =
                <View>
                  <Image
                    resizeMode='cover'
                    source={this.getImagePath(this.formObject.header.background_image)}
                    style={[styles.rowContainer, {
                      justifyContent: headerAlign
                    }]}>
                    {(this.formObject.header.text_align !== 'right') ? headerImage : null}
                    <View style={{
                      flex: 1,
                      flexDirection: 'column',
                      paddingLeft: 10,
                      paddingRight: 10,
                    }}>
                      <AppText numberOfLines={1}
                               style={[{
                                 fontSize: 14, color: '#666666'
                               }, styleHeader]}>{this.formObject.header.title}</AppText>
                      <AppText numberOfLines={2}
                               style={[{
                                 fontSize: 10, color: 'gray'
                               }, styleHeader]}>{this.formObject.header.description}</AppText>
                    </View>
                    {(this.formObject.header.text_align === 'right') ? headerImage : null}
                  </Image>
                  <View style={styles.separator}/>
                </View>;
            }
          } else {
            if (this.formObject.header.background_color !== '') {
              header =
                <View>
                  <View
                    style={[styles.rowContainer, {
                      backgroundColor: this.formObject.header.background_color,
                      justifyContent: headerAlign
                    }]}>
                    {(this.formObject.header.text_align !== 'right') ? headerImage : null}
                    <View style={{
                      flex: 1,
                      flexDirection: 'column',
                      paddingLeft: 10,
                      paddingRight: 10,
                    }}>
                      <AppText numberOfLines={1}
                               style={[{
                                 fontSize: 14, color: '#666666'
                               }, styleHeader]}>{this.formObject.header.title}</AppText>
                      <AppText numberOfLines={2}
                               style={[{
                                 fontSize: 10, color: 'gray'
                               }, styleHeader]}>{this.formObject.header.description}</AppText>
                    </View>
                    {(this.formObject.header.text_align === 'right') ? headerImage : null}
                  </View>
                  <View style={styles.separator}/>
                </View>;
            } else {
              header =
                <View>
                  <View
                    style={[styles.rowContainer, {
                      justifyContent: headerAlign
                    }]}>
                    {(this.formObject.header.text_align !== 'right') ? headerImage : null}
                    <View style={{
                      flex: 1,
                      flexDirection: 'column',
                      paddingLeft: 10,
                      paddingRight: 10,
                    }}>
                      <AppText numberOfLines={1}
                               style={[{
                                 fontSize: 14, color: '#666666'
                               }, styleHeader]}>{this.formObject.header.title}</AppText>
                      <AppText numberOfLines={2}
                               style={[{
                                 fontSize: 10, color: 'gray'
                               }, styleHeader]}>{this.formObject.header.description}</AppText>
                    </View>
                    {(this.formObject.header.text_align === 'right') ? headerImage : null}
                  </View>
                  <View style={styles.separator}/>
                </View>;
            }

          }
        }
      }

      var footer = null;
      if (this.formObject.footer !== undefined) {
        var footerAlign = 'center';
        if (this.formObject.footer.text_align === 'left') {
          footerAlign = 'flex-start';
        } else if (this.formObject.footer.text_align === 'right') {
          footerAlign = 'flex-end';
        }
        if (this.formObject.footer.enable) {
          let title = (this.formObject.footer.title !== null && this.formObject.footer.title !== undefined && this.formObject.footer.title !== '') ? this.formObject.footer.title : ' ';
          let desc = (this.formObject.footer.description !== null && this.formObject.footer.description !== undefined && this.formObject.footer.title !== '') ? this.formObject.footer.description : ' ';
          if (this.formObject.footer.background_type === 'image') {
            if (this.formObject.footer.background_image === '') {
              footer =
                <View>
                  {/*<View style={styles.separator}/>*/}
                  <View
                    style={[styles.rowContainer, {
                      justifyContent: footerAlign,
                    }]}>
                    <View style={{flexGrow: 1, flexDirection: 'column', alignItems: footerAlign}}>
                      <AppText numberOfLines={1}
                               style={{
                                 fontSize: 14,
                                 color: '#666666',
                                 textAlign: this.formObject.footer.text_align,
                                 justifyContent: footerAlign
                               }}>{title}</AppText>
                      <AppText numberOfLines={2}
                               style={{
                                 fontSize: 10,
                                 color: 'gray',
                                 textAlign: this.formObject.footer.text_align,
                                 justifyContent: footerAlign
                               }}>{desc}</AppText>
                    </View>
                  </View>
                </View>;
            } else {
              footer =
                <View>
                  {/*<View style={styles.separator}/>*/}
                  <Image
                    resizeMode='cover'
                    source={this.getImagePath(this.formObject.footer.background_image)}
                    style={[styles.rowContainer, {justifyContent: footerAlign}]}>
                    <View style={{flexGrow: 1, flexDirection: 'column', alignItems: footerAlign}}>
                      <AppText numberOfLines={1}
                               style={{
                                 fontSize: 14,
                                 color: '#666666',
                                 textAlign: this.formObject.footer.text_align,
                                 justifyContent: footerAlign
                               }}>{title}</AppText>
                      <AppText numberOfLines={2}
                               style={{
                                 fontSize: 10,
                                 color: 'gray',
                                 textAlign: this.formObject.footer.text_align,
                                 justifyContent: footerAlign
                               }}>{desc}</AppText>
                    </View>
                  </Image>
                </View>;
            }
          } else {
            if (this.formObject.footer.background_color !== '') {
              footer =
                <View>
                  {/*<View style={styles.separator}/>*/}
                  <View
                    style={[styles.rowContainer, {
                      justifyContent: footerAlign,
                      backgroundColor: this.formObject.footer.background_color
                    }]}>
                    <View style={{flexGrow: 1, flexDirection: 'column', alignItems: footerAlign}}>
                      <AppText numberOfLines={1}
                               style={{
                                 fontSize: 14,
                                 color: '#666666',
                                 textAlign: this.formObject.footer.text_align,
                                 justifyContent: footerAlign
                               }}>{title}</AppText>
                      <AppText numberOfLines={2}
                               style={{
                                 fontSize: 10,
                                 color: 'gray',
                                 textAlign: this.formObject.footer.text_align,
                                 justifyContent: footerAlign
                               }}>{desc}</AppText>
                    </View>
                  </View>
                </View>;
            } else {
              footer =
                <View>
                  {/*<View style={styles.separator}/>*/}
                  <View
                    style={[styles.rowContainer, {
                      justifyContent: footerAlign
                    }]}>
                    <View style={{flexGrow: 1, flexDirection: 'column', alignItems: footerAlign}}>
                      <AppText numberOfLines={1}
                               style={{
                                 fontSize: 14,
                                 color: '#666666',
                                 textAlign: this.formObject.footer.text_align,
                                 justifyContent: footerAlign
                               }}>{title}</AppText>
                      <AppText numberOfLines={2}
                               style={{
                                 fontSize: 10,
                                 color: 'gray',
                                 textAlign: this.formObject.footer.text_align,
                                 justifyContent: footerAlign
                               }}>{desc}</AppText>
                    </View>
                  </View>
                </View>;
            }
          }
        }
      }

      renderForm = [];
      formComponents = [];
      formComponentsLeft = [];
      formComponentsRight = [];
      fieldColumn = "";

      var theFields = this.dataRender.fields;
      fieldColumn = this.dataRender.layout;

      var page = <View style={{
        height: 40,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
        <AppText numberOfLines={1}
                 style={{
                   padding: 8,
                   fontSize: 14,
                   fontWeight: 'bold',
                   color: 'black'
                 }}>{this.dataRender.page_name}</AppText>
      </View>;

      for (let i = 0; i < theFields.length; i++) {
        // {
        //   theFields.map(function (field, i) {
        var field = theFields[i];
        var fieldType = field.field_id.type;

        //alert(field.field_id.type)
        // console.log('render : ', field.field_key);

        if (fieldType === 'label') {
          // console.log('label : ', field);
          let fontSize = 14;
          if (!field.properties) {
            field.properties = {};
            field.properties.useTextColor = false;
            field.properties.useBackgroundColor = false;
            field.properties.style = {};
            field.properties.style.fontSize = '14px';
          } else {
            try {
              fontSize = parseInt(field.properties.style.fontSize.substring(0, 2));
            } catch (error) {

            }
          }
          if (field.column == '1') {
            formComponentsLeft.push(<DFLabel fontWeight="bold" key={field.field_key + "-a-comLeft"} text={field.value}
                                             isTwoColumns={this.state.isTwoColumns}
                                             color={(field.properties.useTextColor) ? field.properties.style.color : "black"}
                                             backgroundColor={(field.properties.useBackgroundColor) ? field.properties.style.backgroundColor : "transparent"}
                                             fontSize={fontSize}
                                             isRequired={false}/>)
            // formComponentsLeft.push(<DFSeparator key={field.field_key + "-c"}/>)

          } else if (field.column == '2') {
            formComponentsRight.push(<DFLabel fontWeight="bold" key={field.field_key + "-a-comRight"} text={field.value}
                                              isTwoColumns={this.state.isTwoColumns}
                                              color={(field.properties.useTextColor) ? field.properties.style.color : "black"}
                                              backgroundColor={(field.properties.useBackgroundColor) ? field.properties.style.backgroundColor : "transparent"}
                                              fontSize={fontSize}
                                              isRequired={false}/>)
            // formComponentsRight.push(<DFSeparator key={field.field_key + "-c"}/>)

          }

          formComponents.push(<DFLabel fontWeight="bold" key={field.field_key + "-a"} text={field.value}
                                       isTwoColumns={this.state.isTwoColumns}
                                       color={(field.properties.useTextColor) ? field.properties.style.color : "black"}
                                       backgroundColor={(field.properties.useBackgroundColor) ? field.properties.style.backgroundColor : "transparent"}
                                       fontSize={fontSize}
                                       isRequired={false}/>);
          // formComponents.push(<DFSeparator key={field.field_key + "-c"}/>)


        }


        if (fieldType == 'textbox' || fieldType == 'textarea' || fieldType == 'number') {
          let nextIndex = i + 1;
          var nextItem = '';
          for (var x = nextIndex; x < theFields.length; x++) {
            if (x < theFields.length) {
              if (this.isFieldEnabled(theFields[x].field_key)) {
                if (!(theFields[x].read_only)) {
                  if (theFields[x].field_id.type == 'textbox') {
                    nextItem = 'textInput-' + x
                    break;
                  } else if (theFields[x].field_id.type == 'number') {
                    nextItem = 'textNumber-' + x
                    break;
                  } else if (theFields[x].field_id.type == 'textarea') {
                    nextItem = 'textArea-' + x
                    break;
                  }
                }
              }
            }
          }

          let field_name = <DFLabel fontWeight="bold" key={field.field_key + "-a"} text={field.name} color="black"
                                    isTwoColumns={this.state.isTwoColumns}
                                    isRequired={field.required}/>


          if (field.column == '1') {
            formComponentsLeft.push(field_name)
          } else if (field.column == '2') {
            formComponentsRight.push(field_name)
          }

          formComponents.push(field_name);

          if (fieldType == 'number') {

            let field_comp = <DFNumber
              key={field.field_key + "-b"}
              componentIndex={i}
              nextItem={nextItem}
              ref={'textNumber-' + i}
              refName={'textNumber-' + i}
              value={field.value}
              numStyle={field.style}
              readOnly={field.read_only}
              maxLength={field.field_id.max_length}
              editable={this.isFieldEnabled(field.field_key)}
              viewMode={this.IS_VIEW_MODE}
              next={this.nextTapped.bind(this)}
              addTextData={this.insertData.bind(this, field.field_key)}
              pageIndex={this.pageIndex}
              fieldIndex={i}
              isTwoColumns={this.state.isTwoColumns}/>

            formComponents.push(field_comp)

            if (field.column == '1') {
              formComponentsLeft.push(field_comp)
            } else if (field.column == '2') {
              formComponentsRight.push(field_comp)
            }

            if (this.state.errorArray[field.field_key + "_Msg"]) {
              var textPlaceholder = (this.state.errorArray[field.field_key + "_Msg"]) ? this.state.errorArray[field.field_key + "_Msg"] : ""
              let error_label = <DFErrorLabel key={field.field_key + "-is-required"}
                                              isShow={this.state.showErr}
                                              text={textPlaceholder}
                                              ref={field.field_key + "_is_required"}
                                              refName={field.field_key + "_is_required"}
                                              isTwoColumns={this.state.isTwoColumns}/>
              formComponents.push(error_label)
              if (field.column == '1') {
                formComponentsLeft.push(error_label)
              } else if (field.column == '2') {
                formComponentsRight.push(error_label)
              }
            }

          } else if (fieldType == 'textbox') {
            let field_comp = <DFTextBox
              key={field.field_key + "-b"}
              componentIndex={i}
              nextItem={nextItem}
              ref={'textInput-' + i}
              refName={'textInput-' + i}
              placeholder=''
              value={field.value}
              readOnly={field.read_only}
              maxLength={field.field_id.max_length}
              editable={this.isFieldEnabled(field.field_key)}
              viewMode={this.IS_VIEW_MODE}
              next={this.nextTapped.bind(this)}
              addTextData={this.insertData.bind(this, field.field_key)}
              pageIndex={this.pageIndex}
              fieldIndex={i}
              openMainConfirm={this.props.openMainConfirm.bind(this)}
              openMainModal={this.props.openMainModal.bind(this)}
              isTwoColumns={this.state.isTwoColumns}/>

            formComponents.push(field_comp)

            if (field.column == '1') {
              formComponentsLeft.push(field_comp)
            } else if (field.column == '2') {
              formComponentsRight.push(field_comp)
            }

            if (this.state.errorArray[field.field_key + "_Msg"]) {
              var textPlaceholder = (this.state.errorArray[field.field_key + "_Msg"]) ? this.state.errorArray[field.field_key + "_Msg"] : ""
              let error_label = <DFErrorLabel key={field.field_key + "-is-required"}
                                              isShow={this.state.showErr}
                                              text={textPlaceholder}
                                              ref={field.field_key + "_is_required"}
                                              refName={field.field_key + "_is_required"}
                                              isTwoColumns={this.state.isTwoColumns}/>

              formComponents.push(error_label)
              if (field.column == '1') {
                formComponentsLeft.push(error_label)
              } else if (field.column == '2') {
                formComponentsRight.push(error_label)
              }
            }

          } else {

            let field_comp = <DFTextArea
              key={field.field_key + "-b"}
              componentIndex={i}
              nextItem={nextItem}
              ref={'textArea-' + i}
              refName={'textArea-' + i}
              editable={this.isFieldEnabled(field.field_key)}
              placeholder=''
              readOnly={field.read_only}
              value={field.value}
              maxLength={field.field_id.max_length}
              viewMode={this.IS_VIEW_MODE}
              addTextData={this.insertData.bind(this, field.field_key)}
              pageIndex={this.pageIndex}
              fieldIndex={i}
              openMainConfirm={this.props.openMainConfirm.bind(this)}
              openMainModal={this.props.openMainModal.bind(this)}
              isTwoColumns={this.state.isTwoColumns}/>

            formComponents.push(field_comp)

            if (field.column == '1') {
              formComponentsLeft.push(field_comp)
            } else if (field.column == '2') {
              formComponentsRight.push(field_comp)
            }

            if (field.required) {
              var textPlaceholder = (this.state.errorArray[field.field_key + "_Msg"]) ? this.state.errorArray[field.field_key + "_Msg"] : ""
              let error_label = <DFErrorLabel key={field.field_key + "-is-required"}
                                              isShow={this.state.showErr}
                                              text={textPlaceholder}
                                              ref={field.field_key + "_is_required"}
                                              refName={field.field_key + "_is_required"}
                                              isTwoColumns={this.state.isTwoColumns}/>

              formComponents.push(error_label)
              if (field.column == '1') {
                formComponentsLeft.push(error_label)
              } else if (field.column == '2') {
                formComponentsRight.push(error_label)
              }
            }

          }
        }

        if (fieldType == 'calendar') {

          let field_label = <DFLabel fontWeight="bold" key={field.field_key + "-a"} text={field.name} color="black"
                                     isTwoColumns={this.state.isTwoColumns}
                                     isRequired={field.required}/>

          let field_comp = <DFDatePicker
            key={field.field_key + "-b"}
            viewMode={this.IS_VIEW_MODE}
            value={field.value}
            formatDate={field.style}
            properties={field.properties}
            readOnly={field.read_only}
            isDraft={this.isDraft}
            currentDateTime={this.CURRENT_DATE_TIME}
            editable={this.isFieldEnabled(field.field_key)}
            insertData={this.insertData.bind(this, field.field_key)}
            pageIndex={this.pageIndex}
            fieldIndex={i}
            isTwoColumns={this.state.isTwoColumns}/>

          formComponents.push(field_label)
          formComponents.push(field_comp)

          if (field.column == '1') {
            formComponentsLeft.push(field_label)
            formComponentsLeft.push(field_comp)
          } else if (field.column == '2') {
            formComponentsRight.push(field_label)
            formComponentsRight.push(field_comp)
          }

          if (field.required) {
            var textPlaceholder = (this.state.errorArray[field.field_key + "_Msg"]) ? this.state.errorArray[field.field_key + "_Msg"] : ""
            let error_label = <DFErrorLabel key={field.field_key + "-is-required"}
                                            isShow={this.state.showErr}
                                            text={textPlaceholder}
                                            ref={field.field_key + "_is_required"}
                                            refName={field.field_key + "_is_required"}
                                            isTwoColumns={this.state.isTwoColumns}/>

            formComponents.push(error_label)
            if (field.column == '1') {
              formComponentsLeft.push(error_label)
            } else if (field.column == '2') {
              formComponentsRight.push(error_label)
            }
          }

        }

        if (fieldType == 'time') {

          let field_label = <DFLabel fontWeight="bold" key={field.field_key + "-a"} text={field.name} color="black"
                                     isTwoColumns={this.state.isTwoColumns}
                                     isRequired={field.required}/>

          let field_comp = <DFTime
            key={field.field_key + "-b"}
            value={field.value}
            formatTime={field.style}
            properties={field.properties}
            currentDateTime={this.CURRENT_DATE_TIME}
            editable={this.isFieldEnabled(field.field_key)}
            readOnly={field.read_only}
            insertData={this.insertData.bind(this, field.field_key)}
            isDraft={this.isDraft}
            viewMode={this.IS_VIEW_MODE}
            openMainModal={(message) => self.props.openMainModal(message)}
            ref={"dftime-" + field.field_key}
            pageIndex={this.pageIndex}
            fieldIndex={i}
            isTwoColumns={this.state.isTwoColumns}/>


          formComponents.push(field_label)
          formComponents.push(field_comp)

          if (field.column == '1') {
            formComponentsLeft.push(field_label)
            formComponentsLeft.push(field_comp)
          } else if (field.column == '2') {
            formComponentsRight.push(field_label)
            formComponentsRight.push(field_comp)
          }


          if (field.required) {
            var textPlaceholder = (this.state.errorArray[field.field_key + "_Msg"]) ? this.state.errorArray[field.field_key + "_Msg"] : ""
            let error_label = <DFErrorLabel key={field.field_key + "-is-required"}
                                            isShow={this.state.showErr}
                                            text={textPlaceholder}
                                            ref={field.field_key + "_is_required"}
                                            refName={field.field_key + "_is_required"}
                                            isTwoColumns={this.state.isTwoColumns}/>

            formComponents.push(error_label)
            if (field.column == '1') {
              formComponentsLeft.push(error_label)
            } else if (field.column == '2') {
              formComponentsRight.push(error_label)
            }

          }

        }


        if (fieldType == 'checkbox') {

          let field_label = <DFLabel fontWeight="bold" key={field.field_key + "-a"} text={field.name} color="black"
                                     isTwoColumns={this.state.isTwoColumns}
                                     isRequired={field.required}/>
          // formComponents.push(<DFSeparator key={field.field_key + "-c"}/>)
          let field_comp = <DFCheckBox
            key={field.field_key + "-b"} options={field.option}
            editable={this.isFieldEnabled(field.field_key)}
            viewMode={this.IS_VIEW_MODE}
            insertData={this.selectCheckBox.bind(this, field.field_key)}
            pageIndex={this.pageIndex}
            fieldIndex={i}
            isTwoColumns={this.state.isTwoColumns}/>

          formComponents.push(field_label)
          formComponents.push(field_comp)

          if (field.column == '1') {
            formComponentsLeft.push(field_label)
            formComponentsLeft.push(field_comp)
          } else if (field.column == '2') {
            formComponentsRight.push(field_label)
            formComponentsRight.push(field_comp)
          }

          if (field.required) {
            var textPlaceholder = (this.state.errorArray[field.field_key + "_Msg"]) ? this.state.errorArray[field.field_key + "_Msg"] : ""
            let error_label = <DFErrorLabel key={field.field_key + "-is-required"}
                                            isShow={this.state.showErr}
                                            text={textPlaceholder}
                                            ref={field.field_key + "_is_required"}
                                            refName={field.field_key + "_is_required"}
                                            isTwoColumns={this.state.isTwoColumns}/>

            formComponents.push(error_label)
            if (field.column == '1') {
              formComponentsLeft.push(error_label)
            } else if (field.column == '2') {
              formComponentsRight.push(error_label)
            }

          }

        }

        if (fieldType == 'dropdown') {
          let field_label = <DFLabel fontWeight="bold" key={field.field_key + "-a"} text={field.name} color="black"
                                     isRequired={field.required}
                                     isTwoColumns={this.state.isTwoColumns}/>

          // formComponents.push(<DFSeparator key={field.field_key + "-c" }/>)
          let field_comp = <DFDropDown
            key={field.field_key + "-b"}
            value={field.value}
            options={field.option}
            viewMode={this.IS_VIEW_MODE}
            editable={this.isFieldEnabled(field.field_key)}
            insertData={this.insertData.bind(this, field.field_key)}
            pageIndex={this.pageIndex}
            fieldIndex={i}
            isTwoColumns={this.state.isTwoColumns}/>

          formComponents.push(field_label)
          formComponents.push(field_comp)

          if (field.column == '1') {
            formComponentsLeft.push(field_label)
            formComponentsLeft.push(field_comp)
          } else if (field.column == '2') {
            formComponentsRight.push(field_label)
            formComponentsRight.push(field_comp)
          }

          if (field.required) {
            var textPlaceholder = (this.state.errorArray[field.field_key + "_Msg"]) ? this.state.errorArray[field.field_key + "_Msg"] : ""
            let error_label = <DFErrorLabel key={field.field_key + "-is-required"}
                                            isShow={this.state.showErr}
                                            text={textPlaceholder}
                                            ref={field.field_key + "_is_required"}
                                            refName={field.field_key + "_is_required"}
                                            isTwoColumns={this.state.isTwoColumns}/>

            formComponents.push(error_label)
            if (field.column == '1') {
              formComponentsLeft.push(error_label)
            } else if (field.column == '2') {
              formComponentsRight.push(error_label)
            }
          }
        }

        if (fieldType == 'radio') {

          let field_label = <DFLabel fontWeight="bold" key={field.field_key + "-a"} text={field.name} color="black"
                                     isRequired={field.required}
                                     isTwoColumns={this.state.isTwoColumns}/>
          // formComponents.push(<DFSeparator key={field.field_key + "-c" }/>)
          let field_comp = <DFRadio
            key={field.field_key + "-b"} options={field.option}
            editable={this.isFieldEnabled(field.field_key)}
            viewMode={this.IS_VIEW_MODE}
            insertData={this.selectRadio.bind(this, field.field_key)}
            pageIndex={this.pageIndex}
            fieldIndex={i}
            isTwoColumns={this.state.isTwoColumns}/>

          formComponents.push(field_label)
          formComponents.push(field_comp)

          if (field.column == '1') {
            formComponentsLeft.push(field_label)
            formComponentsLeft.push(field_comp)
          } else if (field.column == '2') {
            formComponentsRight.push(field_label)
            formComponentsRight.push(field_comp)
          }

          if (field.required) {
            var textPlaceholder = (this.state.errorArray[field.field_key + "_Msg"]) ? this.state.errorArray[field.field_key + "_Msg"] : ""
            let error_label = <DFErrorLabel key={field.field_key + "-is-required"}
                                            isShow={this.state.showErr}
                                            text={textPlaceholder}
                                            ref={field.field_key + "_is_required"}
                                            refName={field.field_key + "_is_required"}
                                            isTwoColumns={this.state.isTwoColumns}/>

            formComponents.push(error_label)
            if (field.column == '1') {
              formComponentsLeft.push(error_label)
            } else if (field.column == '2') {
              formComponentsRight.push(error_label)
            }
          }

        }


        if (fieldType == 'image') {

          let field_label = <DFLabel fontWeight="bold" key={field.field_key + "-a"} text={field.name} color="black"
                                     isRequired={field.required}
                                     isTwoColumns={this.state.isTwoColumns}/>
          let field_comp = <DFPhoto
            key={field.field_key + "-b"}
            valueUri={field.value}
            image_src={field.image_src}
            image_size={field.image_size}
            draw_on_photo={field.properties.draw_on_photo}
            value={this.getFileTempUri(field.field_key)}
            editable={this.isFieldEnabled(field.field_key)}
            viewMode={this.IS_VIEW_MODE}
            insertData={this.selectImage.bind(this, field.field_key)}
            removeData={this.removeUri.bind(this, field.field_key)}
            pageIndex={this.pageIndex}
            fieldIndex={i}
            label_name={field.name}
            openImageViewer={this.props.openImageViewer.bind(this)}
            isTwoColumns={this.state.isTwoColumns}/>

          formComponents.push(field_label)
          formComponents.push(field_comp)

          if (field.column == '1') {
            formComponentsLeft.push(field_label)
            formComponentsLeft.push(field_comp)
          } else if (field.column == '2') {
            formComponentsRight.push(field_label)
            formComponentsRight.push(field_comp)
          }

          if (this.state.errorArray[field.field_key + "_Msg"]) {
            var textPlaceholder = (this.state.errorArray[field.field_key + "_Msg"]) ? this.state.errorArray[field.field_key + "_Msg"] : ""
            let error_label = <DFErrorLabel key={field.field_key + "-is-required"}
                                            isShow={this.state.showErr}
                                            text={textPlaceholder}
                                            ref={field.field_key + "_is_required"}
                                            refName={field.field_key + "_is_required"}
                                            isTwoColumns={this.state.isTwoColumns}/>

            formComponents.push(error_label)
            if (field.column == '1') {
              formComponentsLeft.push(error_label)
            } else if (field.column == '2') {
              formComponentsRight.push(error_label)
            }
          }
        }

        if (fieldType == 'signature') {

          let field_label = <DFLabel fontWeight="bold" key={field.field_key + "-a"} text={field.name} color="black"
                                     isRequired={field.required}
                                     isTwoColumns={this.state.isTwoColumns}/>
          let field_comp = <DFSignature
            key={field.field_key + "-b"}
            valueUri={field.value}
            image_src={field.image_src}
            image_size={field.image_size}
            navigator={this.props.navigator}
            value={this.getFileTempUri(field.field_key)}
            editable={this.isFieldEnabled(field.field_key)}
            viewMode={this.IS_VIEW_MODE}
            insertData={this.selectSignature.bind(this, field.field_key)}
            removeData={this.removeUri.bind(this, field.field_key)}
            pageIndex={this.pageIndex}
            fieldIndex={i}
            isTwoColumns={this.state.isTwoColumns}/>

          formComponents.push(field_label)
          formComponents.push(field_comp)

          if (field.column == '1') {
            formComponentsLeft.push(field_label)
            formComponentsLeft.push(field_comp)
          } else if (field.column == '2') {
            formComponentsRight.push(field_label)
            formComponentsRight.push(field_comp)
          }

          if (this.state.errorArray[field.field_key + "_Msg"]) {
            var textPlaceholder = (this.state.errorArray[field.field_key + "_Msg"]) ? this.state.errorArray[field.field_key + "_Msg"] : ""
            let error_label = <DFErrorLabel key={field.field_key + "-is-required"}
                                            isShow={this.state.showErr}
                                            text={textPlaceholder}
                                            ref={field.field_key + "_is_required"}
                                            refName={field.field_key + "_is_required"}
                                            isTwoColumns={this.state.isTwoColumns}/>

            formComponents.push(error_label)
            if (field.column == '1') {
              formComponentsLeft.push(error_label)
            } else if (field.column == '2') {
              formComponentsRight.push(error_label)
            }
          }
        }

        if (fieldType == 'checkin') {
          let field_label = <DFLabel fontWeight="bold" key={field.field_key + "-a"} text={field.name} color="black"
                                     isRequired={field.required}
                                     isTwoColumns={this.state.isTwoColumns}/>
          let field_comp = <DFLocation
            key={field.field_key + "-b"}
            value={field.value}
            properties={field.properties}
            editable={this.isFieldEnabled(field.field_key)}
            insertData={this.insertData.bind(this, field.field_key)}
            viewMode={this.IS_VIEW_MODE}
            isConnected={this.state.isConnected}
            showSpinner={this.showSpinner.bind(this)}
            showAlertPopup={this.showAlertPopup.bind(this)}
            pageIndex={this.pageIndex}
            fieldIndex={i}
            isTwoColumns={this.state.isTwoColumns}/>

          formComponents.push(field_label)
          formComponents.push(field_comp)

          if (field.column == '1') {
            formComponentsLeft.push(field_label)
            formComponentsLeft.push(field_comp)
          } else if (field.column == '2') {
            formComponentsRight.push(field_label)
            formComponentsRight.push(field_comp)
          }

          if (field.required == true) {
            var textPlaceholder = (this.state.errorArray[field.field_key + "_Msg"]) ? this.state.errorArray[field.field_key + "_Msg"] : ""
            let error_label = <DFErrorLabel key={field.field_key + "-is-required"}
                                            isShow={this.state.showErr}
                                            text={textPlaceholder}
                                            ref={field.field_key + "_is_required"}
                                            refName={field.field_key + "_is_required"}
                                            isTwoColumns={this.state.isTwoColumns}/>

            formComponents.push(error_label)
            if (field.column == '1') {
              formComponentsLeft.push(error_label)
            } else if (field.column == '2') {
              formComponentsRight.push(error_label)
            }
          }
        }

        if (fieldType == 'address') {
          let field_label = <DFLabel fontWeight="bold" key={field.field_key + "-a"} text={field.name} color="black"
                                     isRequired={field.required}
                                     isTwoColumns={this.state.isTwoColumns}/>
          let field_comp = <DFAddress
            key={field.field_key + "-b"}
            maxLength={field.field_id.max_length}
            properties={field.properties}
            editable={this.isFieldEnabled(field.field_key)}
            viewMode={this.IS_VIEW_MODE}
            insertData={this.addAddressData.bind(this, field.field_key)}
            openMainModal={(message) => self.props.openMainModal(message)}
            pageIndex={this.pageIndex}
            fieldIndex={i}
            isTwoColumns={this.state.isTwoColumns}
          />

          formComponents.push(field_label)
          formComponents.push(field_comp)

          if (field.column == '1') {
            formComponentsLeft.push(field_label)
            formComponentsLeft.push(field_comp)
          } else if (field.column == '2') {
            formComponentsRight.push(field_label)
            formComponentsRight.push(field_comp)
          }

          if (field.required == true) {
            var textPlaceholder = (this.state.errorArray[field.field_key + "_Msg"]) ? this.state.errorArray[field.field_key + "_Msg"] : ""
            let error_label = <DFErrorLabel
              key={field.field_key + "-is-required"}
              isShow={this.state.showErr}
              text={textPlaceholder}
              ref={field.field_key + "_is_required"}
              refName={field.field_key + "_is_required"}
              isTwoColumns={this.state.isTwoColumns}/>

            formComponents.push(error_label)
            if (field.column == '1') {
              formComponentsLeft.push(error_label)
            } else if (field.column == '2') {
              formComponentsRight.push(error_label)
            }
          }
        }

        if (fieldType == 'gps') {
          let field_label = <DFLabel fontWeight="bold" key={field.field_key + "-a"} text={field.name} color="black"
                                     isRequired={field.required}
                                     isTwoColumns={this.state.isTwoColumns}/>
          let field_comp = <DFGPS
            key={field.field_key + "-b"}
            value={field.value}
            properties={field.properties}
            navigator={this.props.navigator}
            editable={this.isFieldEnabled(field.field_key)}
            viewMode={this.IS_VIEW_MODE}
            isConnected={this.state.isConnected}
            setLocation={this.setLocationData.bind(this, field.field_key)}
            showAlertPopup={this.showAlertPopup.bind(this)}
            pageIndex={this.pageIndex}
            fieldIndex={i}
            isTwoColumns={this.state.isTwoColumns}/>

          formComponents.push(field_label)
          formComponents.push(field_comp)

          if (field.column == '1') {
            formComponentsLeft.push(field_label)
            formComponentsLeft.push(field_comp)
          } else if (field.column == '2') {
            formComponentsRight.push(field_label)
            formComponentsRight.push(field_comp)
          }

          if (field.required == true) {
            var textPlaceholder = (this.state.errorArray[field.field_key + "_Msg"]) ? this.state.errorArray[field.field_key + "_Msg"] : ""
            let error_label = <DFErrorLabel key={field.field_key + "-is-required"}
                                            isShow={this.state.showErr}
                                            text={textPlaceholder}
                                            ref={field.field_key + "_is_required"}
                                            refName={field.field_key + "_is_required"}
                                            isTwoColumns={this.state.isTwoColumns}/>

            formComponents.push(error_label)
            if (field.column == '1') {
              formComponentsLeft.push(error_label)
            } else if (field.column == '2') {
              formComponentsRight.push(error_label)
            }
          }
        }

        if (fieldType == 'file') {

          let field_label = <DFLabel fontWeight="bold" key={field.field_key + "-a"} text={field.name} color="black"
                                     isRequired={field.required}
                                     isTwoColumns={this.state.isTwoColumns}/>
          let field_comp = <DFFile
            key={field.field_key + "-b"}
            valueUri={field.value}
            ref={"DFFile_" + field.field_key}
            showSpinner={this.showSpinner.bind(this)}
            value={this.getFileTempUri(field.field_key)}
            editable={this.isFieldEnabled(field.field_key)}
            viewMode={this.IS_VIEW_MODE}
            insertData={this.selectFile.bind(this, field.field_key)}
            removeData={this.removeUriFile.bind(this, field.field_key)}
            pageIndex={this.pageIndex}
            fieldIndex={i}
            isTwoColumns={this.state.isTwoColumns}/>

          formComponents.push(field_label)
          formComponents.push(field_comp)

          if (field.column == '1') {
            formComponentsLeft.push(field_label)
            formComponentsLeft.push(field_comp)
          } else if (field.column == '2') {
            formComponentsRight.push(field_label)
            formComponentsRight.push(field_comp)
          }
          // console.log('Reload: ' + this.state.errorArray[field.field_key + "_Msg"]);
          if (this.state.errorArray[field.field_key + "_Msg"]) {

            var textError = (this.state.errorArray[field.field_key + "_Msg"]) ? this.state.errorArray[field.field_key + "_Msg"] : "";
            let error_label = <DFErrorLabel key={field.field_key + "-is-required"}
                                            isShow={this.state.showErr}
                                            text={textError}
                                            ref={field.field_key + "_is_required"}
                                            refName={field.field_key + "_is_required"}
                                            isTwoColumns={this.state.isTwoColumns}/>;

            formComponents.push(error_label);
            if (field.column == '1') {
              formComponentsLeft.push(error_label)
            } else if (field.column == '2') {
              formComponentsRight.push(error_label)
            }
          }
        }

        if (fieldType == 'barcode') {

          let field_label = <DFLabel fontWeight="bold" key={field.field_key + "-a"} text={field.name} color="black"
                                     isRequired={field.required}
                                     isTwoColumns={this.state.isTwoColumns}/>
          // formComponents.push(<DFSeparator key={field.field_key + "-c"}/>)
          let field_comp = <DFBarcode
            key={field.field_key + "-b"}
            componentIndex={i}
            ref={'barcode-' + i}
            refName={'barcode-' + i}
            placeholder=''
            value={field.value}
            navigator={this.props.navigator}
            readOnly={field.read_only}
            editable={this.isFieldEnabled(field.field_key)}
            viewMode={this.IS_VIEW_MODE}
            addTextData={this.insertData.bind(this, field.field_key)}
            pageIndex={this.pageIndex}
            fieldIndex={i}
            isTwoColumns={this.state.isTwoColumns}/>;

          formComponents.push(field_label);
          formComponents.push(field_comp);

          if (field.column == '1') {
            formComponentsLeft.push(field_label);
            formComponentsLeft.push(field_comp);
          } else if (field.column == '2') {
            formComponentsRight.push(field_label);
            formComponentsRight.push(field_comp);
          }

          if (field.required) {
            let textPlaceholder = (this.state.errorArray[field.field_key + "_Msg"]) ? this.state.errorArray[field.field_key + "_Msg"] : ""
            let error_label = <DFErrorLabel key={field.field_key + "-is-required"}
                                            isShow={this.state.showErr}
                                            text={textPlaceholder}
                                            ref={field.field_key + "_is_required"}
                                            refName={field.field_key + "_is_required"}
                                            isTwoColumns={this.state.isTwoColumns}/>

            formComponents.push(error_label);
            if (field.column == '1') {
              formComponentsLeft.push(error_label)
            } else if (field.column == '2') {
              formComponentsRight.push(error_label)
            }

          }

        }

        if (fieldType == 'autocomplete') {

          let field_label = <DFLabel fontWeight="bold" key={field.field_key + "-a"} text={field.name} color="black"
                                     isRequired={field.required}
                                     isTwoColumns={this.state.isTwoColumns}/>
          // formComponents.push(<DFSeparator key={field.field_key + "-c"}/>)
          let field_comp = <DFAutoComplete
            key={field.field_key + "-b"}
            componentIndex={i}
            ref={'auto-' + i}
            refName={'auto-' + i}
            text={field.name}
            properties={field.properties}
            mapped_values={field.field_id.mapped_values}
            placeholder=''
            value={field.value}
            navigator={this.props.navigator}
            readOnly={field.read_only}
            editable={this.isFieldEnabled(field.field_key)}
            viewMode={this.IS_VIEW_MODE}
            setAutoComplete={this.setAutoComplete.bind(this, field.field_key)}
            pageIndex={this.pageIndex}
            fieldIndex={i}
            openMainConfirm={this.props.openMainConfirm.bind(this)}
            openMainModal={this.props.openMainModal.bind(this)}
            isTwoColumns={this.state.isTwoColumns}/>;

          formComponents.push(field_label);
          formComponents.push(field_comp);

          if (field.column == '1') {
            formComponentsLeft.push(field_label);
            formComponentsLeft.push(field_comp);
          } else if (field.column == '2') {
            formComponentsRight.push(field_label);
            formComponentsRight.push(field_comp);
          }

          if (field.required) {
            let textPlaceholder = (this.state.errorArray[field.field_key + "_Msg"]) ? this.state.errorArray[field.field_key + "_Msg"] : ""
            let error_label = <DFErrorLabel key={field.field_key + "-is-required"}
                                            isShow={this.state.showErr}
                                            text={textPlaceholder}
                                            ref={field.field_key + "_is_required"}
                                            refName={field.field_key + "_is_required"}
                                            isTwoColumns={this.state.isTwoColumns}/>

            formComponents.push(error_label);
            if (field.column == '1') {
              formComponentsLeft.push(error_label)
            } else if (field.column == '2') {
              formComponentsRight.push(error_label)
            }

          }

        }

      } // END OF ADDING ALL FORM COMPONENTS
      // let field_comp = <DFAutoComplete  editable={true} navigator={this.props.navigator}/>;
      //
      // formComponents.push(field_comp);

    }

    var styleSubmit = this.state.isNextUser ? {
      backgroundColor: "#B2D234",
      borderColor: "#B2D234"
    } : {backgroundColor: "#90A3AE", borderColor: "#90A3AE"};

    var submitBtn = !this.IS_VIEW_MODE ? <DFButton
      text={STRINGS.POPUP_MSG.BTN_SUBMIT}
      onPress={this._submitForm.bind(this)}
      style={{backgroundColor: "#B2D234", borderColor: "#B2D234"}}
      underlayColor='#B2D234'/> : <View/>;

    var cancelBtn = !this.IS_VIEW_MODE ? <DFButton
      text={STRINGS.POPUP_MSG.BTN_CANCEL}
      onPress={() => this.actionModal()}
      style={{backgroundColor: "#DC6565", borderColor: "#DC6565"}}/> : <View/>;

    var pageView = <View style={{
      height: 40,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      backgroundColor: "#B2D234"
    }}>
      <TouchableHighlight
        underlayColor='transparent'
        onPress={() => this.prevPage()}
        style={{
          paddingLeft: 7,
          paddingRight: 7,
          borderRadius: 4,
          marginLeft: 10,
          height: 35,
          justifyContent: 'center',
          alignItems: 'center'
        }}>
        <Ionicons name="ios-arrow-back" size={30} color="white"/>
      </TouchableHighlight>
      <TouchableHighlight
        underlayColor='transparent'
        style={{
          paddingLeft: 7,
          paddingRight: 7,
          borderRadius: 4,
          marginLeft: 10,
          height: 35,
          justifyContent: 'center',
          alignItems: 'center'
        }}>
        <View>
          <AppText style={styles.title}>{this.pageIndex + 1}/{this.pageSize}</AppText>
        </View>
      </TouchableHighlight>
      <TouchableHighlight
        underlayColor='transparent'
        onPress={() => this.nextPage()}
        style={{
          paddingLeft: 7,
          paddingRight: 7,
          borderRadius: 4,
          marginRight: 10,
          height: 35,
          justifyContent: 'center',
          alignItems: 'center'
        }}>
        <Ionicons name="ios-arrow-forward" size={30} color="white"
                  style={{justifyContent: 'center', alignItems: 'center'}}/>
      </TouchableHighlight>
    </View>;

    var mainSpinner = this.state.isLoading ?
      <ProgressSpinner
        setSuccess={this.state.doneSaving}
        isSuccessfull={this.state.saveSuccess}
        spinnerText={this.state.spinnerText}
      /> : <View/>;

    var alertDialog = this.state.showAlertDialog ?
      <AlertConfirmDialog confrimMessage={this.state.alertDialogMsg}
                          closeModalFail={this.closeModalFail.bind(this)}
                          closeModalSuccess={this.closeModalSuccess.bind(this)}/> : null

    var alertPopup = this.state.showAlertPopup ?
      <AlertConfirmDialog alertMessage={this.state.alertMsg}
                          closeModalFail={this.closeModalFail.bind(this)}/> : null

    var endFlowConfirmDialog = this.state.endFlowConfirm ?
      <AlertConfirmDialog confrimMessage={STRINGS.POPUP_MSG.END_FLOW}
                          closeModalFail={this.endFlowConfirmDialogFail.bind(this)}
                          closeModalSuccess={this.endFlowConfirmDialogSuccess.bind(this)}/> : null

    if (Device.isPhone()) {
      // console.log('1Column Phone');
      renderForm.push(
        <ScrollView
          key="scr-form"
          ref="scrollView"
          style={{flex: 1, paddingBottom: 10, width: width}}
          automaticallyAdjustContentInsets={false}
          // keyboardShouldPer sistTaps = {true}
          contentInset={{bottom: this.state.scrollViewAdjustment}}>
          {header}
          {page}
          <View style={styles.separator}/>
          {formComponents}
          {footer}

        </ScrollView>)
    } else {
      if (fieldColumn == "2-column") {
        // console.log('2Column');
        renderForm.push(
          <ScrollView
            key="scr-form-ipad-2"
            ref="scrollViewRight"
            style={{flex: 1, paddingBottom: 10}}
            automaticallyAdjustContentInsets={false}
            // keyboardShouldPersistTaps = {true}
            contentInset={{bottom: this.state.scrollViewAdjustment}}>
            {header}
            {page}
            <View style={styles.separator}/>
            <View style={styles.containerColumn}>
              <View style={styles.columnLeft}>
                {formComponentsLeft}
              </View>
              <View style={styles.columnRight}>
                {formComponentsRight}
              </View>
            </View>
            {footer}

          </ScrollView>)
      } else {
        // console.log('1Column Tablet');
        renderForm.push(
          <ScrollView
            key="scr-form-ipad"
            ref="scrollViewLeft"
            style={{flex: 1, paddingTop: 10, width: width}}
            automaticallyAdjustContentInsets={false}
            // keyboardSh ouldPersistTaps = {true}
            contentInset={{bottom: this.state.scrollViewAdjustment}}>
            {formComponents}
            {submitBtn}
          </ScrollView>)
      }
    }

    return (
      <View style={styles.container}>

        {renderForm}

        <View style={styles.separator}/>
        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          {cancelBtn}
          {submitBtn}
        </View>
        {pageView}
        {this.state.fileForGenerate !== '' ?
          <DFDownloadPDF
            fileName={this.state.fileForGenerate}
            showSpinner={this.showSpinner.bind(this)}
          /> : null
        }
        {this.state.rejectModel ?
          <CancelModel headerTitle={STRINGS.POPUP_MSG.REASON_OF_REJECT} isReject={true}
                       formJobId={this.props.formData.form_job_id._id}
                       closeDialog={this.closeDialog.bind(this)} step={this.state.formData.form_job_id.current_step}
                       backPage={this.backPage.bind(this)}
                       errorCancelReject={this.errorCancelReject.bind(this)}
                       TranID={this.props.formData._id}
                       openMainModal={this.closeModalWithErrMsg.bind(this)}/> : null}
        {this.state.cancelModel ?
          <CancelModel headerTitle={STRINGS.POPUP_MSG.REASON_OF_DELETE} isReject={false}
                       formJobId={this.props.formData.form_job_id._id}
                       closeDialog={this.closeDialog.bind(this)} step={this.state.formData.form_job_id.current_step}
                       backPage={this.backPage.bind(this)}
                       errorCancelReject={this.errorCancelReject.bind(this)}
                       TranID={this.props.formData._id}
                       openMainModal={(message) => self.props.openMainModal(message)}/> : null}
        {this.state.actionModel ?
          <ActionModel headerTitle={STRINGS.POPUP_MSG.SELECT_ACTION} status={this.state.formData.status}
                       step={this.state.formData.form_job_id.current_step}
                       type='FORM'
                       closeAction={this.closeAction.bind(this)}/> : null}
        {this.state.noFlowModel ?
          <NoFlowModel headerTitle={STRINGS.POPUP_MSG.SELECT_ACTION}
                       closeDialog={this.closeDialog.bind(this)}
                       noFlowAction={this.noFlowAction.bind(this)}/> : null}
        {this.state.alertModel ?
          <SaveFormDialog confrimMessage={STRINGS.POPUP_MSG.SAVE_IN_OFFLINE_MODE}
                          data={this.formQueue}
                          closeSaveForm={this.closeSaveForm.bind(this)}
                          cancelSaveForm={this.cancelSaveForm.bind(this)}/> : null}
        <Picker
          ref={picker => this.picker = picker}
          showMask={true}
          style={{flex: 1, height: 320}}
          showDuration={300}
          pickerData={this.createTimeData()}
          selectedValue={this.state.timeValue}
          onPickerDone={(pickedValue) => {
            // console.log('pickedValue', pickedValue)
            this.selectTime(pickedValue)
          }}
        />

        {this.state.showModal ?
          <SubmitChooseContact
            closeModal={this.closeModal.bind(this)}
            closeModalFail={this.closeModal.bind(this, false)}
            formID={this.props.formData.user_form_id._id}
            usersPermissionList={this.usersPermissionList}
            nextUsersList={this.nextUsersList}
            currentStep={this.props.formData.form_job_id.current_step}
            isNoFlow={this.isNoFlow}
            formJobID={this.props.formData.form_job_id._id}
            noFlowAction={this.noFlowAction.bind(this)}
            totalSteps={this.isNoFlow ? 'n' : this.props.formData.form_job_id.form_flow_id.steps.length}
            openMainModal={(message) => self.props.openMainModal(message)}/> : null}

        {endFlowConfirmDialog}
        {mainSpinner}
        {alertDialog}
        {alertPopup}
      </View>);
  }

  closeModalWithErrMsg(msg = '') {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      noFlowModel: false,
      rejectModel: false,
      cancelModel: false
    })
    if (msg !== '') {
      this.props.openMainModal(msg)
    }

  }

  onPressHandle(field_key, field_value, value) {

    // console.log('Field Value: ', field_value)
    // console.log('Value: ', value)

    if (value == '') {
      this.setState({
        timeValue: ["00", "00", "00"]
      })
    } else {
      this.setState({
        timeValue: value.split(':')
      })
    }

    // this.state.timeValue = value.split(':')

    this.picker.toggle();
    this.timeKey = field_key;
  }

  selectTime(value) {
    this.setState({
      isLoadingTemp: false,
    });
    // this.timeValue = value;
    this.insertData(this.timeKey, value[0] + ":" + value[1] + ":" + value[2]);
    this.setState({
      isLoadingTemp: true,
      timeValue: value
    });

    this.refs['dftime-' + this.timeKey].updateComponent(value[0] + ":" + value[1] + ":" + value[2])
  }

  createTimeData() {
    let hour = [];
    for (let i = 0; i < 24; i++) {
      hour.push(this.pad(i));
    }
    let minute = [];
    for (let j = 0; j < 60; j++) {
      minute.push(this.pad(j));
    }
    let second = [];
    for (let k = 0; k < 60; k++) {
      second.push(this.pad(k));
    }
    let time = [];
    time.push(hour);
    time.push(minute);
    time.push(second);
    return time;
  };

  pad(num) {
    if (num < 10) {
      return "0" + num;
    } else {
      return "" + num;
    }
  }

  closeModal(submitForm, data = null) {
    // console.log('SELECTED USER LIST', data);
    //LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      showModal: false,
      isLoading: submitForm,
      selectedUserContacts: data
    });

    if (submitForm) {
      this._processFileUploads()
    }
  }

  formValidation() {
    var blReturn = true;

    var tmpArrMsg = {};

    for (let i = 0; i < this.formObject.pages.length; i++) {
      for (let j = 0; j < this.formObject.pages[i].fields.length; j++) {
        if (this.formObject.pages[i].fields[j].required == true && this.isFieldEnabled(this.formObject.pages[i].fields[j].field_key)) {
          if (this.formObject.pages[i].fields[j].field_id.type == 'image' || this.formObject.pages[i].fields[j].field_id.type == 'signature') {
            if (this.formObject.pages[i].fields[j].value == '') {
              tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = "This is required";
              blReturn = false;
            }
            //Validate checkbox / radio
          } else if (this.formObject.pages[i].fields[j].field_id.type == 'file') {
            if (this.formObject.pages[i].fields[j].value == '') {
              tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = "This is required";
              blReturn = false;
            }
            //Validate checkbox / radio
          } else if (this.formObject.pages[i].fields[j].field_id.type == 'checkbox' || this.formObject.pages[i].fields[j].field_id.type == 'radio') {
            var countChk = 0;
            let options = this.formObject.pages[i].fields[j].option;
            for (let k = 0; k < options.length; k++) {
              if (this.formObject.pages[i].fields[j].option[k].checked) {
                countChk = countChk + 1;
              }
            }

            if (countChk == 0) {
              tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = "This is required";
              blReturn = false;
            }

          } else if (this.formObject.pages[i].fields[j].field_id.type == 'checkin') {
            if (this.formObject.pages[i].fields[j].value == undefined || this.formObject.pages[i].fields[j].value == '') {
              tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = "This is required";
              blReturn = false;
            }
          } else if (this.formObject.pages[i].fields[j].field_id.type == 'gps') {
            if (this.formObject.pages[i].fields[j].properties.lat == undefined || this.formObject.pages[i].fields[j].properties.lat == '') {
              tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = "This is required";
              blReturn = false;
            }
          } else if (this.formObject.pages[i].fields[j].field_id.type == 'address') {
            var isPassAddress = true;
            var errorMsg = '';
            if (this.formObject.pages[i].fields[j].properties.address.value == '') {
              errorMsg += 'address, ';
              isPassAddress = false;
              blReturn = false;
            }
            if (this.formObject.pages[i].fields[j].properties.province.value == '') {
              errorMsg += 'province, ';
              isPassAddress = false;
              blReturn = false;
            }
            if (this.formObject.pages[i].fields[j].properties.district.value == '') {
              errorMsg += 'district, ';
              isPassAddress = false;
              blReturn = false;
            }
            if (this.formObject.pages[i].fields[j].properties.subdistrict.value == '') {
              errorMsg += 'subdistrict, ';
              isPassAddress = false;
              blReturn = false;
            }

            if (!isPassAddress) {
              tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = "Please input " + errorMsg.substring(0, errorMsg.length - 2);
            }

          } else {
            if (this.formObject.pages[i].fields[j].value == '' || this.formObject.pages[i].fields[j].value == undefined) {
              tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = "This is required";
              blReturn = false;
            }
          }

        }
      }
    }

    if (blReturn) {


      for (let i = 0; i < this.formObject.pages.length; i++) {
        for (let j = 0; j < this.formObject.pages[i].fields.length; j++) {
          if (this.isFieldEnabled(this.formObject.pages[i].fields[j].field_key)) {
            if (this.formObject.pages[i].fields[j].field_id.type === 'number') {

              var check_min_max = false;

              if (this.formObject.pages[i].fields[j].value != '') {
                var styleList = this.formObject.pages[i].fields[j].style;
                if (styleList.length > 0) {
                  for (var k = 0; k < styleList.length; k++) {
                    //console.log('Control Name: ' + this.formObject.pages[i].fields[j].name + ', control value: '+ this.formObject.pages[i].fields[j].value +',   Check: ' + styleList[k].checked + ' , Type: ' + styleList[k].name);


                    if (this.formObject.pages[i].fields[j].value != undefined && this.formObject.pages[i].fields[j].value != '') {

                      if (styleList[k].checked && styleList[k].name === 'Integer') {
                        check_min_max = true;
                        var num = parseFloat(this.formObject.pages[i].fields[j].value);
                        if (!this.isInt(this.formObject.pages[i].fields[j].value)) {
                          tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = STRINGS.VALIDATION_MESSAGES.INT_FORMAT;
                          // console.log('Error isInt: ', this.formObject.pages[i].fields[j].name);
                          blReturn = false;
                        } else if (num > parseFloat(2000000000)) {
                          tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = STRINGS.VALIDATION_MESSAGES.INT_MAX_VAL;
                          blReturn = false;
                        } else if (num < parseFloat(-2000000000)) {
                          tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = STRINGS.VALIDATION_MESSAGES.INT_MIN_VAL;
                          blReturn = false;
                        } else {
                          var number = parseInt(this.formObject.pages[i].fields[j].value);
                          if (number == undefined || number == null) {
                            number = '';
                          } else {
                            number = number + '';
                          }
                          this.formObject.pages[i].fields[j].value = number;
                        }
                      } else if (styleList[k].checked && styleList[k].name === 'Decimal') {
                        check_min_max = true;
                        if (!this.isDecimal(this.formObject.pages[i].fields[j].value)) {
                          tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = STRINGS.VALIDATION_MESSAGES.DEC_FORMAT;
                          blReturn = false;
                        } else {
                          if (parseFloat(this.formObject.pages[i].fields[j].value) > parseFloat(10000000000)) {
                            tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = STRINGS.VALIDATION_MESSAGES.DEC_MAX_VAL;
                            blReturn = false;
                          } else if (parseFloat(this.formObject.pages[i].fields[j].value) < parseFloat(-10000000000)) {
                            tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = STRINGS.VALIDATION_MESSAGES.DEC_MIN_VAL;
                            blReturn = false;
                          } else if (this.formObject.pages[i].fields[j].value.indexOf('.') != -1) {
                            var getDec = this.formObject.pages[i].fields[j].value.substring(this.formObject.pages[i].fields[j].value.indexOf('.') + 1);
                            if (getDec.length > 8) {
                              tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = STRINGS.VALIDATION_MESSAGES.DEC_MAX_DIGITS;
                              blReturn = false;
                            } else if (getDec.length > 2) {
                              this.formObject.pages[i].fields[j].value = this.floorFigure(this.formObject.pages[i].fields[j].value, getDec.length)
                            } else {
                              this.formObject.pages[i].fields[j].value = this.floorFigure(this.formObject.pages[i].fields[j].value)
                            }
                          } else {
                            this.formObject.pages[i].fields[j].value = this.floorFigure(this.formObject.pages[i].fields[j].value)
                          }
                        }
                      } else if (styleList[k].checked && styleList[k].name === 'Currency') {
                        check_min_max = true;
                        if (!this.isCurrency(this.formObject.pages[i].fields[j].value)) {
                          tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = STRINGS.VALIDATION_MESSAGES.CURRENCY_FORMAT;
                          blReturn = false;
                        } else {
                          if (parseFloat(this.formObject.pages[i].fields[j].value) > parseFloat(10000000000)) {
                            tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = STRINGS.VALIDATION_MESSAGES.DEC_MAX_VAL;
                            blReturn = false;
                          } else if (parseFloat(this.formObject.pages[i].fields[j].value) < parseFloat(-10000000000)) {
                            tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = STRINGS.VALIDATION_MESSAGES.DEC_MIN_VAL;
                            blReturn = false;
                          } else if (this.formObject.pages[i].fields[j].value.indexOf('.') !== -1) {
                            var getDec = this.formObject.pages[i].fields[j].value.substring(this.formObject.pages[i].fields[j].value.indexOf('.') + 1);
                            if (getDec.length > 8) {
                              tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = STRINGS.VALIDATION_MESSAGES.CURRENCY_MAX_DIGITS;
                              blReturn = false;
                            } else if (getDec.length > 2) {
                              this.formObject.pages[i].fields[j].value = this.floorFigure(this.formObject.pages[i].fields[j].value, getDec.length)
                            } else {
                              this.formObject.pages[i].fields[j].value = this.floorFigure(this.formObject.pages[i].fields[j].value)
                            }
                          } else {
                            this.formObject.pages[i].fields[j].value = this.floorFigure(this.formObject.pages[i].fields[j].value)
                          }
                        }
                      } else if (styleList[k].checked && styleList[k].name === 'Citizen Card') {
                        if (this.formObject.pages[i].fields[j].value != '' && !Device.checkThaiCitizenIdCard(this.formObject.pages[i].fields[j].value)) {
                          tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = STRINGS.VALIDATION_MESSAGES.CITIZEN_FORMAT;
                          //console.log('Error isInt: ', this.formObject.pages[i].fields[j].name);
                          blReturn = false;
                        }
                      } else if (styleList[k].checked && styleList[k].name === 'Telephone Number') {
                        if (!Device.isInternationalPhoneNoCorrect(this.formObject.pages[i].fields[j].value)) {
                          tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = STRINGS.VALIDATION_MESSAGES.INVALID_PHONE;
                          //console.log('Error isInt: ', this.formObject.pages[i].fields[j].name);
                          blReturn = false;
                        }
                      }

                    }

                  }
                }

                if (check_min_max) {
                  var propertiesList = this.formObject.pages[i].fields[j].properties;
                  if (propertiesList.minimum != undefined && propertiesList.maximum != undefined) {
                    if (propertiesList.minimum != '' && propertiesList.maximum != '') {
                      if (parseFloat(this.formObject.pages[i].fields[j].value) < propertiesList.minimum || parseFloat(this.formObject.pages[i].fields[j].value) > propertiesList.maximum) {
                        tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = STRINGS.formatString(STRINGS.VALIDATION_MESSAGES.MIN_MAX_ERR, propertiesList.minimum, propertiesList.maximum);
                        // console.log('Error is not must between')
                        blReturn = false;
                      }
                    }
                    if (propertiesList.minimum == '' && propertiesList.maximum != '') {
                      if (parseFloat(this.formObject.pages[i].fields[j].value) > propertiesList.maximum) {
                        tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = STRINGS.formatString(STRINGS.VALIDATION_MESSAGES.MAX_ERR, propertiesList.maximum);
                        // console.log('Error is not must not more than')
                        blReturn = false;
                      }
                    }
                    if (propertiesList.minimum != '' && propertiesList.maximum == '') {
                      if (parseFloat(this.formObject.pages[i].fields[j].value) < propertiesList.minimum) {
                        tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = STRINGS.formatString(STRINGS.VALIDATION_MESSAGES.MIN_ERR, propertiesList.minimum);
                        // console.log('Error is not must not less than')
                        blReturn = false;
                      }
                    }
                  }
                }

              }

            } else if (this.formObject.pages[i].fields[j].field_id.type == 'textbox') {
              if (this.formObject.pages[i].fields[j].value !== '' && this.formObject.pages[i].fields[j].value !== null && this.formObject.pages[i].fields[j].value !== undefined) {
                var formateTextList = this.formObject.pages[i].fields[j].style;
                if (formateTextList != null) {
                  for (var k = 0; k < formateTextList.length; k++) {
                    if (formateTextList[k].name == "Email" && formateTextList[k].checked === true) {

                      var txt = this.formObject.pages[i].fields[j].value;

                      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                      if (!re.test(txt)) {
                        tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = STRINGS.VALIDATION_MESSAGES.EMAIL_INCORRECT_FORMAT;
                        blReturn = false;
                      }
                    }
                  }
                }
              }
            } else if (this.formObject.pages[i].fields[j].field_id.type == 'file') {
              var isPassFile = false;
              var errorMsg = '';
              var field = this.formObject.pages[i].fields[j];
              var file = this.getFileUri(field.field_key);

              if (file != undefined) {
                let fileImageExt = file.name.substring(file.name.lastIndexOf('.') + 1, file.name.length);
                for (let i = 0; i < field.file_types.length; i++) {
                  if (field.file_types[i].ext === fileImageExt.toUpperCase()) {
                    isPassFile = true;
                  }
                }

                if (!isPassFile) {
                  errorMsg = STRINGS.VALIDATION_MESSAGES.FILE_TYPE
                }

                if (errorMsg == '') {
                  if (field.properties.limit != 0) {
                    if (file.size > field.properties.limit) {
                      var megaBytes = (field.properties.limit / (1024 * 1024)).toFixed(1);
                      errorMsg = STRINGS.formatString(STRINGS.VALIDATION_MESSAGES.FILE_SIZE, megaBytes);
                      isPassFile = false;
                    }
                  }
                }

                if (!isPassFile) {
                  blReturn = false;
                  tmpArrMsg[this.formObject.pages[i].fields[j].field_key + "_Msg"] = errorMsg
                }
                console.log('Error', errorMsg)
              }
            }
          }

        }
      }
    }

    this.setState({
      errorArray: tmpArrMsg,
      showErr: true
    });

    if (!blReturn) {
      this.props.openMainModal(STRINGS.VALIDATION_MESSAGES.ERR_MESSAGE)
    }

    return blReturn;
  }

  isInt(value) {
    if (value == undefined)
      return false;

    if (isNaN(value)) {
      return false;
    }
    return value.indexOf(".") == -1;

  }

  isDecimal(value) {
    var n2 = value;
    if (value.split('.').length == 1) {
      value = parseFloat(value);
      return (value != 'NaN' && n2 == value);
    } else if (value.split('.').length == 2) {
      var chk = value.substring(value.indexOf('.') + 1);
      if (chk == '' || chk == undefined) {
        return false
      } else {
        value = parseFloat(value);
        return (value != 'NaN' && n2 == value);
      }
    } else {
      return false
    }
  }

  isCurrency(value) {
    var n2 = value;
    if (value.split('.').length == 1) {
      value = parseFloat(value);
      return (value != 'NaN' && n2 == value);
    } else if (value.split('.').length == 2) {
      var chk = value.substring(value.indexOf('.') + 1);
      if (chk == '' || chk == undefined) {
        return false
      } else {
        value = parseFloat(value);
        return (value != 'NaN' && n2 == value);
      }
    } else {
      return false
    }
  }

  floorFigure(figure, decimals = 2) {
    return (parseFloat(figure)).toFixed(decimals);
  }

  _processFileUploads(submitType = 'submitForm') {
    if (self.state.isConnected && self.fileUpload.length > 0) {
      self._uploadOfflineFile(self.fileUpload[0], submitType)
    } else {
      if (submitType === 'submitForm') {
        self.submitForm()
      } else {
        self.submitFormNoFlow()
      }
    }
  }

  async uploadFile(field_key, pageIndex, uploadObj, type = 'image') {
    self = this;
    // console.log('Upload Object: ', uploadObj);

    if (!self.state.isConnected) {
      self._storeFileForOfflineMode(field_key, uploadObj, type)
    } else {
      self.setState({isLoading: true});

      try {
        let formJobID = self.props.formData.form_job_id._id;

        let formData = new FormData();
        formData.append(type, uploadObj);
        let config = null;

        if (type === 'image') {
          config = JSON.parse(JSON.stringify(apiConfig.uploadFileImage));
        } else {
          config = JSON.parse(JSON.stringify(apiConfig.uploadFileFile));
        }
        config.url += formJobID;

        DFService.callApiWithHeaderObj(formData, config, function (error, response) {

          if (response) {
            if (response.resCode === 'DF2000000') {
              // Successfull Response
              self._removeFileFromDownloadQueue(field_key);
              let file_url = response.data;
              self.insertData(field_key, file_url);
              if (type === 'file') {
                self.refs['DFFile_' + field_key].updateUrl(file_url)
              }
              self.setState({
                isLoading: false
              });
            } else {
              // Failed Response
              self.setState({
                isLoading: false
              });
              self.props.openMainModal(response.resMessage)
            }
          } else {
            // Error from application calling service
            self._storeFileForOfflineMode(field_key, uploadObj, type);

            self.setState({
              isLoading: false
            });
          }
        });
      } catch (error) {
        self._storeFileForOfflineMode(field_key, uploadObj, type);
        self.setState({isLoading: false});
        console.log(error)
      }
    }

  }

  async _uploadOfflineFile(uploadObj, submitType) {
    self = this;

    if (self.state.isConnected) {
      try {

        let formJobID = self.props.formData.form_job_id._id;
        let type = uploadObj.uploadFileType;
        let field_key = uploadObj.field_key;

        let formData = new FormData();
        formData.append(type, uploadObj);

        console.log('uploadObj :' + JSON.stringify(uploadObj));

        let config = null;
        if (type === 'image') {
          config = JSON.parse(JSON.stringify(apiConfig.uploadFileImage));
        } else {
          config = JSON.parse(JSON.stringify(apiConfig.uploadFileFile));
        }
        config.url += formJobID;

        DFService.callApiWithHeaderObj(formData, config, function (error, response) {

          if (response) {
            if (response.resCode === 'DF2000000') {
              // Successfull Response
              self._removeFileFromDownloadQueue(field_key);

              let file_url = response.data;
              self.insertData(field_key, file_url);
              if (uploadObj.uploadType === 'file') {
                self.refs['DFFile_' + field_key].updateUrl(file_url)
              }
              self._processFileUploads(submitType)
            } else {
              // Failed Response
              self.setState({
                isLoading: false
              });
              self.props.openMainModal(response.resMessage)
            }
          } else {
            // Error from application calling service
            console.log('uploadOfflineFile Error :' + error);
            self.setState({
              isLoading: false
            });
            self.props.openMainModal(STRINGS.COMMON.UNEXPECTED_ERROR);
          }

        });
      } catch (error) {
        self.setState({isLoading: false});
        self.props.openMainModal(STRINGS.COMMON.UNEXPECTED_ERROR_CATCH);
      }
    } else {
      self._processFileUploads(submitType)
    }
  }

  async submitForm() {
    try {
      var value = await AsyncStorage.getItem("Profile");
      var profile = await JSON.parse(value);

      var saveData = JSON.parse(JSON.stringify(this.formObject));

      for (let i = 0; i < saveData.pages.length; i++) {
        for (let j = 0; j < saveData.pages[i].fields.length; j++) {
          saveData.pages[i].fields[j].field_id = saveData.pages[i].fields[j].field_id._id;
        }
      }

      saveData.company_id = this.formObject.company_id._id;

      saveData.updatedDate = new Date();
      saveData.updatedBy = profile.user.id;
      if (!this.isNoFlow) {
        saveData.form_flow_id = this.formObject.form_flow_id._id;
      }

      saveData.next_user = this.state.selectedUserContacts;

      saveData.form_transaction_id = this.props.formData._id;
      saveData.status = "SUBMIT";

      if (this.props.formData.form_job_id.current_step === 1) {
        delete saveData._id;
        saveData.createdDate = new Date();
        saveData.createdBy = profile.user.id;
      }

      self = this;

      // console.log('saveData saveData :', saveData)

      var config = JSON.parse(JSON.stringify(apiConfig.saveFormData));
      config.url += this.props.formData.user_form_id._id;

      DFService.callApiWithHeader(saveData, config, function (error, response) {

        if (response) {
          if (response.resCode === 'DF2000000') {
            // Successfull Response
            // console.log("before cancelDraftForm");
            self.cancelDraftForm(false);
            // console.log("after cancelDraftForm");
            self.success(response);
          } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
            // console.log("error concurrent");
            self.setState({
              isLoading: false
            });
            let callback = self.forceBack;
            self.props.openMainModal(response.resMessage, callback)
          } else {
            // Failed Response
            self.setState({
              isLoading: false
            });

            let callback = null
            self.props.openMainModal(response.resMessage, callback)
          }
        } else {
          // Error from application calling service
          self.saveSubmitError(saveData, profile.user.id);
          console.log("error = ", error);
        }

      });

    } catch (error) {
      self.setState({
        isLoading: false
      });
      console.log(error);
    }

  }

  _transactionExpiredBack = () => {
    // console.log('Trans Expired')
    self.cancelDraftForm(false, true);
  }

  async submitFormNoFlow() {

    try {
      let value = await AsyncStorage.getItem("Profile");
      let profile = await JSON.parse(value);

      let saveData = JSON.parse(JSON.stringify(this.formObject));
      // console.log('Submit Form Data ===> : ', saveData)

      for (let i = 0; i < saveData.pages.length; i++) {
        for (let j = 0; j < saveData.pages[i].fields.length; j++) {
          saveData.pages[i].fields[j].field_id = saveData.pages[i].fields[j].field_id._id;
        }
      }

      saveData.company_id = this.formObject.company_id._id;
      delete saveData.form_flow_id;
      saveData.updatedDate = new Date();
      saveData.updatedBy = profile.user.id;

      saveData.form_transaction_id = this.props.formData._id;
      saveData.status = "COMPLETED";

      if (this.props.formData.form_job_id.current_step === 1) {
        delete saveData._id;
        saveData.createdDate = new Date();
        saveData.createdBy = profile.user.id;
      }

      // console.log("saveData", saveData);

      self = this;
      var config = JSON.parse(JSON.stringify(apiConfig.submitEndForm));
      config.url += this.props.formData.user_form_id._id;

      DFService.callApiWithHeader(saveData, config, function (error, response) {

        if (response) {
          if (response.resCode === 'DF2000000') {
            // Successfull Response
            self.cancelDraftForm(false);
          } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
            self.setState({
              isLoading: false
            });
            let callback = self.forceBack;
            self.props.openMainModal(response.resMessage, callback)
          } else {
            // Failed Response
            self.setState({
              isLoading: false
            });

            let callback = null;
            if (response.resCode === 'DF5000022') { // Transaction expired
              callback = self.forceBack
              // callback = self._transactionExpiredBack
            }
            self.props.openMainModal(response.resMessage, callback)

          }
        } else {
          // Error from application calling service
          self.saveEndSubmitError(saveData, profile.user.id);
          console.log("submitFormNoFlow error = ", error);
        }
      });

    } catch (error) {
      self.setState({
        isLoading: false
      });
      console.log(error);
    }

  }

  _removeFileFromDownloadQueue(field_key) {
    for (let i = self.fileUpload.length; i--;) {
      if (self.fileUpload[i].field_key === field_key) {
        if (self.fileUpload[i].uploadType === 'file') {
          this.fileUri.delete(field_key);
        }
        self.fileUpload.splice(i, 1);
      }
    }
  }

  _removeFileFromDownloadQueueForPage(pageIndex) {
    for (let i = self.fileUpload.length; i--;) {
      if (self.fileUpload[i].pageIndex === pageIndex) {
        if (self.fileUpload[i].uploadType === 'file') {
          this.fileUri.delete(self.fileUpload[i].field_key);
        }
        self.fileUpload.splice(i, 1);
      }
    }
  }

  _storeFileForOfflineMode = (field_key, uploadObj, type) => {
    for (let i = self.fileUpload.length; i--;) {
      if (self.fileUpload[i].field_key === field_key) {
        self.fileUpload.splice(i, 1);
      }
    }

    uploadObj.field_key = field_key;
    uploadObj.uploadFileType = type;
    self.fileUpload.push(uploadObj);
    self.insertData(field_key, uploadObj.uri);
  };

  success(response, goBack = true) {
    if (response.resCode === 'DF2000000') {
      //noinspection JSAnnotator
      Timer.setTimeout(self, 'success', () => {
        self.setState({
          doneSaving: true,
          saveSuccess: true,
          spinnerText: 'Success',
        });
        if (goBack) {
          self._goBack(1500)
        } else {
          //noinspection JSAnnotator
          Timer.setTimeout(self, 'closeLoading', () => {
            self.setState({isLoading: false})
          }, 1500)
        }
      }, 2000);

    } else if (response.resCode === 'DF5000038' || response.resCode === 'DF5000022') { // Concurrent Action Occured
      self.setState({
        doneSaving: false,
        saveSuccess: false,
        spinnerText: STRINGS.COMMON.SAVING_FAILED,
        isLoading: false
      });
      let callback = self.forceBack;
      self.props.openMainModal(response.resMessage, callback)
    } else {
      //noinspection JSAnnotator
      Timer.setTimeout(self, 'errorOnSuccess', () => {
        self.setState({
          doneSaving: true,
          saveSuccess: false,
          spinnerText: STRINGS.COMMON.SAVING_FAILED,
          isLoading: false
        });
        self._dismissSpiner(1500);
      }, 2000)
    }
  }

  submitError() {
    //noinspection JSAnnotator
    Timer.setTimeout(self, 'submitError', () => {
      self.setState({
        doneSaving: true,
        saveSuccess: false,
        spinnerText: STRINGS.COMMON.SAVING_FAILED
      });
      self._dismissSpiner(1500);
    }, 500)
  }

  _dismissSpiner(delay) {
    //noinspection JSAnnotator
    Timer.setTimeout(self, '_dismissSpiner', () => {
      self.setState({
        isLoading: false,
        doneSaving: false,
        spinnerText: STRINGS.COMMON.SAVING,
      })
    }, delay)
  }

  _goBack(delay) {
    //noinspection JSAnnotator
    Timer.setTimeout(self, '_goBack', () => {
      self.props.refreshWorkingList();
      self.props.navigator.popToTop(0);
    }, delay)

  }

  forceBack = () => {
    // console.log('Force Back concurrent or expired')
    self.props.refreshWorkingList();
    self.props.navigator.popToTop(0);
  }

  nextTapped(nextItem) {
    // console.log("nextTapped", nextItem);
    if (nextItem !== '') {
      this.refs[nextItem].focusNow()
    }
  }

  _submitForm() {
    // console.log('Submit Form Pressed');
    if (!this.state.isNextUser) {
      return;
    }

    if (this.isNoFlow) {
      if (this.formValidation()) {
        this.setState({
          showModal: true
        })
      }
      return;
    }

    if (this.props.formData.form_job_id.current_step === this.props.formData.form_job_id.form_flow_id.steps.length) {
      this.setState({isLoading: true});
      if (this.formValidation()) {
        this._processFileUploads()
      } else {
        this.setState({isLoading: false});
      }
    } else {
      if (this.formValidation()) {
        this.setState({
          showModal: true
        })
      }
    }
  }

  insertData(field_key, data, pageIndex, fieldIndex) {
    // console.log('insertData', data);

    this.state.errorArray[field_key + "_Msg"] = null;
    this.setState({
      showErr: true
    });

    if (pageIndex && fieldIndex) {
      this.formObject.pages[pageIndex].fields[fieldIndex].value = data;
    } else {
      for (let i = 0; i < this.formObject.pages.length; i++) {
        for (let j = 0; j < this.formObject.pages[i].fields.length; j++) {
          if (this.formObject.pages[i].fields[j].field_key === field_key) {
            this.formObject.pages[i].fields[j].value = data;
            return;
          }
        }
      }
    }
  }

  addTextData(field_key, data) {
    this.textBoxData['key_' + field_key] = data;
  }

  setLocationData(field_key, properties, pageIndex, fieldIndex) {
    // console.log('GPS LOCATION PROPERTIES', properties)
    this.state.errorArray[field_key + "_Msg"] = null;

    // for (let i = 0; i < this.formObject.pages.length; i++) {
    //   for (let j = 0; j < this.formObject.pages[i].fields.length; j++) {
    //     if (this.formObject.pages[i].fields[j].field_key == field_key) {
    //       this.formObject.pages[i].fields[j].properties = properties;
    //       return;
    //     }
    //   }
    // }
    this.formObject.pages[pageIndex].fields[fieldIndex].properties = properties;
  }

  addAddressData(field_key, properties, pageIndex, fieldIndex) {
    // console.log("Address properties: ", properties);
    this.state.errorArray[field_key + "_Msg"] = null;
    this.setState({
      showErr: true
    });
    // for (let i = 0; i < this.formObject.pages.length; i++) {
    //   for (let j = 0; j < this.formObject.pages[i].fields.length; j++) {
    //     if (this.formObject.pages[i].fields[j].field_key == field_key) {
    //       this.formObject.pages[i].fields[j].properties = properties;
    //       return;
    //     }
    //   }
    // }
    this.formObject.pages[pageIndex].fields[fieldIndex].properties = properties;

  }

  setAutoComplete(field_key, value, mapped_values, pageIndex, fieldIndex) {
    this.state.errorArray[field_key + "_Msg"] = null;
    this.setState({
      showErr: true
    });
    this.formObject.pages[pageIndex].fields[fieldIndex].value = value;
    this.formObject.pages[pageIndex].fields[fieldIndex].field_id.mapped_values = mapped_values;
  }

  selectRadio(field_key, newOptions, pageIndex, fieldIndex) {
    this.state.errorArray[field_key + "_Msg"] = null;
    this.setState({
      showErr: true
    });
    this.formObject.pages[pageIndex].fields[fieldIndex].option = newOptions
  }

  selectCheckBox(field_key, checkedItems, pageIndex, fieldIndex) {
    // console.log("selectCheckBox", field_key + " : ", checkedItems);
    this.state.errorArray[field_key + "_Msg"] = null;
    this.setState({
      showErr: true
    });
    this.formObject.pages[pageIndex].fields[fieldIndex].option = checkedItems

  }

  getFileExt(filename) {
    return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : 'undefined';
  }

  selectImage(field_key, imgUri, pageIndex) {

    let isPassFile = true;
    let errorMsg = '';

    // Checks if the image is greater than 10 MB
    if (imgUri.fileSize > ( 10 * (1024 * 1024) )) {
      errorMsg = STRINGS.formatString(STRINGS.VALIDATION_MESSAGES.FILE_SIZE, 10);
      isPassFile = false;
    }

    if (!isPassFile) {
      this.insertData(field_key, "");
      this.state.errorArray[field_key + "_Msg"] = errorMsg;
      this.setState({
        showErr: true
      });

      return;
    }

    var extToMimes = {
      'jpg': 'image/jpeg',
      'png': 'image/png',
      'jpeg': 'image/jpeg',
      'gif': 'image/gif'
    }

    var ext = this.getFileExt(imgUri.uri);
    console.log("ext : ", ext);
    var mime = extToMimes.hasOwnProperty(ext) ? extToMimes[ext] : 'undefined';
    console.log("mime : ", mime);
    //Upload image here
    let uploadObj = {};
    if (Platform.OS === 'ios') {
      uploadObj = {uri: imgUri.uri, name: 'IMAGE-IOS.' + ext, type: mime, pageIndex}
    } else {
      uploadObj = {uri: imgUri.uri, name: imgUri.name, type: imgUri.type, pageIndex}
    }

    console.log('Upload Obj: ', uploadObj);

    self.uploadFile(field_key, pageIndex, uploadObj);
    self.state.errorArray[field_key + "_Msg"] = null;
    self.setState({
      showErr: true
    });
  }

  selectSignature(field_key, imgUri, pageIndex) {

    //Upload image here
    let uploadObj = {uri: imgUri.uri, name: 'signature.png', type: 'image/png', pageIndex}

    self.uploadFile(field_key, pageIndex, uploadObj);
    self.state.errorArray[field_key + "_Msg"] = null;
    self.setState({
      showErr: true
    });
  }

  getFileUri(field_key) {
    return this.fileUri.get(field_key)
  }

  getFileTempUri(field_key) {
    for (let i = 0; i < this.fileUpload.length; i++) {
      if (this.fileUpload[i].field_key === field_key) {
        return this.fileUpload[i]
      }
    }

    return this.getFileUri(field_key);
  }

  removeUri(field_key, pageIndex, fieldIndex) {

    this.formObject.pages[pageIndex].fields[fieldIndex].value = "";

    for (var i = this.fileUpload.length; i--;) {
      if (this.fileUpload[i].field_key === field_key) {
        this.fileUpload.splice(i, 1);
      }
    }

    this.state.errorArray[field_key + "_Msg"] = null;
    this.setState({
      showErr: true
    });
  }

  selectFile(field_key, fileUri, pageIndex, fieldIndex) {

    this.state.errorArray[field_key + "_Msg"] = null;
    let isPassFile = false;
    let errorMsg = '';
    let field = this.getField(field_key);

    if (!fileUri) {
      this.insertData(field_key, "");
      this.state.errorArray[field_key + "_Msg"] = STRINGS.VALIDATION_MESSAGES.FILE_ACCESS;
      this.setState({
        showErr: true
      });
      return;
    }

    let file = fileUri;

    let fileImageExt = file.name.substring(file.name.lastIndexOf('.') + 1, file.name.length);
    for (let i = 0; i < field.file_types.length; i++) {
      if (field.file_types[i].ext === fileImageExt.toUpperCase()) {
        isPassFile = true;
      }
    }

    if (!isPassFile) {
      errorMsg = STRINGS.VALIDATION_MESSAGES.FILE_TYPE
    }

    if (errorMsg === '') {
      if (field.properties.limit === 0) {
        field.properties.limit = 10 * (1024 * 1024)
      }
      if (field.properties.limit !== 0) {
        if (file.size > field.properties.limit) {
          let megaBytes = (field.properties.limit / (1024 * 1024)).toFixed(1);
          errorMsg = STRINGS.formatString(STRINGS.VALIDATION_MESSAGES.FILE_SIZE, megaBytes);
          isPassFile = false;
        }
      }
    }

    if (!isPassFile) {
      this.insertData(field_key, "");
      this.state.errorArray[field_key + "_Msg"] = errorMsg;
      this.setState({
        showErr: true
      });
      console.log(errorMsg);
      return;
    }

    this.fileUri.set(field_key, fileUri);
    //Upload file here
    let uploadObj = {};
    if (Platform.OS === 'ios') {
      uploadObj = {uri: fileUri.uri, name: 'file.' + fileUri.fileType, type: fileUri.type, pageIndex}
    } else {
      uploadObj = {uri: fileUri.uri, name: encodeURIComponent(fileUri.name), type: fileUri.type, pageIndex}
    }

    this.uploadFile(field_key, pageIndex, uploadObj, 'file');

  }

  getField(field_key) {
    for (let i = 0; i < this.formObject.pages.length; i++) {
      for (let j = 0; j < this.formObject.pages[i].fields.length; j++) {
        if (this.formObject.pages[i].fields[j].field_key == field_key) {
          return this.formObject.pages[i].fields[j];
        }
      }
    }
    return null;
  }

  removeUriFile(field_key, pageIndex, fieldIndex) {

    this.formObject.pages[pageIndex].fields[fieldIndex].value = "";

    for (var i = this.fileUpload.length; i--;) {
      if (this.fileUpload[i].field_key === field_key) {
        this.fileUpload.splice(i, 1);
      }
    }

    this.fileUri.delete(field_key);

    this.state.errorArray[field_key + "_Msg"] = null;
    this.setState({
      showErr: true
    });
  }

  getImagePath(path) {
    if (path === '') {
      return null;
    }
    return {uri: config.upload_url + path};
  }

  saveSubmitError(saveData, userId) {

    // console.log("saveSubmitError >>>> OFFLINE MODE")

    this.formQueue = {
      user_id: userId,
      url: this.props.formData.user_form_id._id,
      request_type: 'submit',
      form_data: this.props.formData,
      form_save: saveData,
      uploads: this.fileUpload,
      form_draft_id: this.draftFormId
    };

    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      isLoading: false,
      doneSaving: false,
      spinnerText: STRINGS.COMMON.SAVING,
      alertModel: true
    });
  }

  saveEndSubmitError(saveData, userId) {
    this.formQueue = {
      user_id: userId,
      url: this.props.formData.user_form_id._id,
      request_type: 'end',
      form_data: this.props.formData,
      form_save: saveData,
      uploads: this.fileUpload,
      form_draft_id: this.draftFormId
    };

    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      isLoading: false,
      doneSaving: false,
      spinnerText: STRINGS.COMMON.SAVING,
      showModal: false,
      alertModel: true
    });
  }

  async cancelDraftError(saveData, formId) {
    var value = await AsyncStorage.getItem("Profile");
    var profile = await JSON.parse(value);

    this.formQueue = {
      user_id: profile.user.id,
      url: formId,
      request_type: 'cancel_draft',
      form_data: this.props.formData,
      form_save: saveData,
      uploads: [],
      form_draft_id: this.draftFormId
    };

    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({
      isLoading: false,
      doneSaving: false,
      spinnerText: STRINGS.COMMON.SAVING,
      alertModel: true
    });
  }

  closeModalSuccess() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({
      alertModel: false
    });
  }

  closeSaveForm() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({
      alertModel: false
    });
    //this.props.logout();
    this._autoSaveForm()
    this.props.navigator.popToTop(0);
  }

  cancelSaveForm() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({
      alertModel: false
    });
  }

  // HANDLING APP STATE
  // AUTO SAVE FEATURE
  _handleAppStateChange(appState) {
    //var self = this;
    // console.log(appState);
    // console.log('Prev State: ', self.state.appState);
    // console.log('Draft Form ID: ', self.draftFormId);
    if (self.draftFormId !== undefined) {
      let previousAppState = self.state.appState;

      if (appState === 'active') {
        // console.log('AutoSave: App is active and in foreground');
        if (previousAppState === "background" || previousAppState === "inactive") {
          // console.log('AutoSave: Just came back from a background or inactive state')
        }
      }

      if (appState === 'background') {
        if (previousAppState !== 'inactive') {
          // console.log('AutoSave: - Background Mode');
          self._autoSaveForm()
        }
      }

      if (appState === 'inactive') {
        if (previousAppState === 'active') {
          // console.log('AutoSave: - Inactive Mode');
          self._autoSaveForm()
        }
      }

      self.setState({
        appState,
        previousAppState
      });
    }
  }

  _loadAutoSaveData() {
    let STORAGE_KEY = 'AutoSaveData:' + self.draftFormId;
    // console.log('AutoSave: Storage Key - ', STORAGE_KEY);
    AsyncStorage.getItem(STORAGE_KEY, (err, result) => {
      if (err) {
        // console.log('AutoSave: ERROR IN GETTING AUTO SAVE DATA');
        self._setDefaultValues();
      } else {
        // console.log(result)
        // console.log('AutoSave: LOAD SAVE DATA SUCCESS');
        let response = JSON.parse(result);
        if (result !== null && response !== null && result !== undefined && response !== undefined) {
          // console.log("AutoSave: value= ", response);
          // console.log('AutoSave: Pages ', response.pages);
          if (response.pages) {
            if (response.pages.length > 0) {
              self.formObject.pages = response.pages;
              self.isDraft = true;
              self.hasAutoSaveData = true;
              // console.log('SET IS DRAFT TRUE >>> 2')
            }
          }

          if (response.fileUpload) {
            self.fileUpload = response.fileUpload
          }

          self._setDefaultValues();
        } else {
          // console.log('AutoSave: value is null!!');
          self._setDefaultValues();
        }
      }
    });

    if (this.isNoFlow) {
      // console.log('GETING: _getUsersPermissions')
      this._getUsersPermissions();
    } else {
      if (this.props.formData.form_job_id.current_step === this.props.formData.form_job_id.form_flow_id.steps.length) {
        // On the last step
        // this.setState({isLoading: false, renderPlaceHolderOnly: false})
        this.setState({isLoading: false, isNextUser: true})
      } else {
        // console.log('GETING WITH FLOW: _getUsersPermissions')
        this._getUsersPermissions();
      }
    }
  }

  _setDefaultValues() {
    let self = this;
    // console.log('self.isDraft: ', self.isDraft);
    // console.log('self.isActualDraft: ', self.isActualDraft);
    // console.log('self.hasAutoSaveData: ', self.hasAutoSaveData);
    // console.log('self.IS_VIEW_MODE: ', self.IS_VIEW_MODE);
    if (!self.isActualDraft && !self.IS_VIEW_MODE) {
      self._processDefaultValues(() => {
        self.setState({isLoading: false, renderPlaceHolderOnly: false})
        self.setPageIndex();
      })
    } else {
      self.setState({isLoading: false, renderPlaceHolderOnly: false})
      self.setPageIndex();
    }
  }

  _processDefaultValues(callback) {
    // self.isFieldEnabled(FieldKey)
    // console.log('_processDefaultValues')
    let self = this;
    let CURRENT_DATE = new Date();

    for (var i = 0; i < self.formObject.pages.length; i++) {
      let PAGE = self.formObject.pages[i];
      for (var j = 0; j < PAGE.fields.length; j++) {
        let FIELD = PAGE.fields[j];
        if (self.isFieldEnabled(FIELD.field_key)) {
          if (FIELD.field_id.type == 'calendar' || FIELD.field_id.type == 'time') {
            if (FIELD.properties != null && FIELD.properties != undefined) {
              let propertiesList = FIELD.properties;
              let property = null;
              for (var k = 0; k < propertiesList.length; k++) {
                if (propertiesList[k].checked) {
                  property = propertiesList[k].name
                }
              }

              if (property !== null && (property === 'Current Time' || property === 'Current Date')) {

                let formatStyleList = FIELD.style;
                let formatStyle = null;
                if (formatStyleList !== null || formatStyleList !== undefined) {
                  for (let k = 0; k < formatStyleList.length; k++) {
                    if (formatStyleList[k].checked) {
                      formatStyle = formatStyleList[k].name
                    }
                  }
                }

                /// Set Time Value
                if (FIELD.field_id.type === 'time') {
                  FIELD.value = Device.fnFormateTime('', formatStyle, CURRENT_DATE);
                }

                // Set Date Value
                if (FIELD.field_id.type === 'calendar') {
                  FIELD.value = Device.fnFormateDate(CURRENT_DATE, formatStyle);
                }
              }
            }
          }
        }
      }
    }

    callback();
  }

  _autoSaveForm(callback = null) {
    if (!this.IS_VIEW_MODE) {
      var STORAGE_KEY = 'AutoSaveData:' + self.draftFormId;
      // console.log('AutoSave: Storage Key - ', STORAGE_KEY)
      // console.log('AutoSave: Saving data: ', self.formObject)
      self.formObject.fileUpload = self.fileUpload
      AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(self.formObject), () => {
        // console.log('AutoSave: FINISHED SAVING DATA!')
        if (callback != null) {
          callback()
        }
      });
    }
  }

  _clearAutoSaveData() {
    var STORAGE_KEY = 'AutoSaveData:' + self.draftFormId;
    AsyncStorage.removeItem(STORAGE_KEY, (err) => {
      if (err) {
        conole.log(err)
      }
    });
  }

  showSpinner(show) {
    if (this.state.fileForGenerate !== '') {
      this.setState({
        isLoading: show,
        fileForGenerate: ''
      });
    } else {
      this.setState({
        isLoading: show,
      });
    }

  }

  showAlertDialog(type) {
    let msg = '';
    if (type == 'CANCEL DRAFT') {
      msg = STRINGS.POPUP_MSG.CANCEL_DRAFT
    }

    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({
      alertDialogType: type,
      alertDialogMsg: msg,
      showAlertDialog: true
    })
  }

  showAlertPopup(msg) {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({
      alertMsg: msg,
      showAlertPopup: true
    })
  }

  closeModalFail = () => {
    //LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    self.setState({showAlertDialog: false, showAlertPopup: false});
    // let type = this.state.alertDialogType
    // if(type == ''){

    // }
  };

  closeModalSuccess() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({showAlertDialog: false});
    let type = this.state.alertDialogType;
    if (type == 'CANCEL DRAFT') {
      this.cancelDraftForm(true)
    }
  }

  generatePDF() {
    self = this;
    this.setState({
      isLoading: true
    });

    let config = JSON.parse(JSON.stringify(apiConfig.genPdfPath));
    config.url += this.props.formData.form_job_id.form_data_id._id;
    DFService.callApiWithHeader({}, config, function (error, response) {
      if (error) {
        self.setState({
          isLoading: false
        });
        self.props.openMainModal(STRINGS.COMMON.UNEXPECTED_ERROR_TRY_AGAIN);
      } else {
        if (response.resCode === 'DF2000000') {
          // console.log("generatePDF = ", response);
          self.setState({
            fileForGenerate: response.data
          })
        } else {
          console.log("generatePDF error = ", error);
          self.setState({
            isLoading: false
          });
        }
      }
    });
  }

}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  spinner: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  containerColumn: {
    flexGrow: 1,
    width: width,
    flexDirection: 'row',
  },
  columnLeft: {
    flexGrow: 1,
    width: width / 2,
    paddingLeft: 10,
    paddingRight: 5
  },
  columnRight: {
    flexGrow: 1,
    width: width / 2,
    paddingLeft: 5,
    paddingRight: 10
  },
  title: {
    fontSize: 18,
    color: 'white',
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowContainer: {
    flexGrow: 1,
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center'
  },
  rowContain: {
    flexDirection: 'row'
  },
  imgHeader: {
    width: 40,
    height: 40
  },
  circle: {
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  separator: {
    height: 1,
    backgroundColor: '#8e8e8e',
  }

});

export default FormPage;
