import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Platform,
  Image,
  Text,
  TouchableHighlight,
  Linking
} from 'react-native';
import DFButton from '../FormComponents/DFButton'
import MapPage from '../MapPage'
import STRINGS from '../Util/strings'
import AppText from '../Util/AppText'
var Icon = require('react-native-vector-icons/FontAwesome');

class DFGPS extends Component {

  constructor(props) {
    super(props);
    //console.log(props);

    var properties = null;

    if ("properties" in this.props) {
      if (this.props.properties != undefined)
        properties = this.props.properties
    }

    if (properties == null) {
      properties = {
        lat: "",
        lng: ""
      }
    }
    //console.log('PROP PROPERTIES GPS: ', this.props.properties)

    this.state = {
      lat: properties.lat,
      lng: properties.lng,
      msgError: null,
      isConnected: this.props.isConnected,
      editable: this.props.editable
    };

  }

  handleClick(lat, lng) {
    var urlMap = "https://www.google.com/maps/place/" + lat + "," + lng + "/@" + lat + "," + lng + ",14z"
    Linking.canOpenURL(urlMap).then(supported => {
      if (supported) {
        Linking.openURL(urlMap);
      } else {
        console.log('Don\'t know how to open URI: ' + urlMap);
      }
    });
  };

  componentWillReceiveProps(nextProps) {
    var properties = null;

    if ("properties" in nextProps) {
      if (nextProps.properties != undefined)
        properties = nextProps.properties
    }

    if (properties == null) {
      properties = {
        lat: "",
        lng: ""
      }
    }
    //console.log('PROP PROPERTIES GPS: ', this.props.properties)

    this.state = {
      lat: properties.lat,
      lng: properties.lng,
      msgError: null,
      isConnected: nextProps.isConnected,
      editable: nextProps.editable
    };
  }

  openMap() {

    if (this.state.isConnected) {
      this.props.navigator.push({
        name: 'GPS',
        title: STRINGS.COMMON.GPS_COMPONENT,
        component: MapPage,
        passProps: {
          title: STRINGS.COMMON.GPS_COMPONENT,
          navigator: this.props.navigator,
          properties: { lat: this.state.lat, lng: this.state.lng },
          pageIndex: this.props.pageIndex,
          fieldIndex: this.props.fieldIndex,
          setLocation: this.setLocation.bind(this)
        }
      });
    } else {
      this.props.showAlertPopup(STRINGS.COMMON.INTERNET_REQUIRED)
    }

  }

  setLocation(properties) {
    console.log('dfGPS PROPS', properties)
    var value = {
      lat: properties.lat,
      lng: properties.lng
    }
    this.setState({
      lat: properties.lat,
      lng: properties.lng
    })
    this.props.setLocation(value,properties.pageIndex,properties.fieldIndex)
  }

  render() {

    var btnLocation = (this.state.editable) ? <TouchableHighlight
      style={styles.locationBtn}
      underlayColor='#9FB159'
      onPress={() => this.openMap()}>
      <View style={styles.locationBtnContainer}>
        <View>
          <AppText style={styles.labelBtn}>{STRINGS.COMMON.GPS_COMPONENT}</AppText>
        </View>
        <View>
          <Icon
            name={'map-pin'}
            size={20}
            color='white'
            style={{ width: 20, height: 20 }}
            />
        </View>
      </View>
    </TouchableHighlight> : <TouchableHighlight
      style={styles.locationBtnDisable}>
        <View style={styles.locationBtnContainer}>
          <View>
            <AppText style={styles.labelBtn}>GPS</AppText>
          </View>
          <View>
            <Icon
              name={'map-pin'}
              size={20}
              color='white'
              style={{ width: 20, height: 20 }}
              />
          </View>
        </View>
      </TouchableHighlight>;

    if(this.props.viewMode){
      btnLocation = null;
    }

    var locationValues = (this.state.lat != "") ?

      <View style={{ marginBottom: 5 }}>
        <AppText style={styles.label}>
          {this.state.lat}, {this.state.lng}
        </AppText>

        <TouchableHighlight
          style={styles.locationBtn}
          underlayColor='#9FB159'
          onPress={() => this.handleClick(this.state.lat, this.state.lng)}>
          <View style={styles.locationBtnContainer}>
            <View>
              <AppText style={styles.labelBtn}>{STRINGS.COMMON.BTN_OPEN_MAPS}</AppText>
            </View>
            <View>
              <Icon
                name={'external-link'}
                size={20}
                color='white'
                style={{ width: 20, height: 20 }}
                />
            </View>
          </View>
        </TouchableHighlight>
      </View> : null;
    var renderView = <View style={{ marginLeft: 10, marginRight:10 }}>
      {locationValues}
      <View style={{ marginBottom: 5 }}>
        {btnLocation}
      </View>
    </View>;

    if (this.props.viewMode && this.state.lat == '') {
      renderView = <View
        style={styles.borderView }>
        <AppText style={styles.viewMode}>{STRINGS.CUSTOM_MODALS.NO_COORDINATES}</AppText>
      </View>;
    }
    return (renderView);
  }
}

const styles = StyleSheet.create({
  label: {
    marginLeft: 10,
    marginRight: 5,
    marginBottom: 3,
    fontSize: 14,
    color: 'black'
  },
  labelBtn: {
    marginLeft: 10,
    marginRight: 5,
    marginBottom: 3,
    fontSize: 14,
    color: 'white'
  },
  labelError: {
    marginLeft: 10,
    marginRight: 5,
    marginBottom: 3,
    fontSize: 14,
    color: 'red'
  },
  locationBtn: {
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    marginRight: 10,
    marginLeft: 10,
    padding: 10,
    borderRadius: 8,
  },
  locationBtnDisable: {
    backgroundColor: '#cdcdcd',
    borderColor: '#cdcdcd',
    marginRight: 10,
    marginLeft: 10,
    padding: 10,
    borderRadius: 8,
  },
  locationBtnContainer: {
    flexGrow: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  borderView: {
    borderColor: '#cdcdcd',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  viewMode: {
    minHeight: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#606060',
    textAlignVertical: 'center',
    paddingTop: 5,
    paddingBottom: 5
  },
});

export default DFGPS;
