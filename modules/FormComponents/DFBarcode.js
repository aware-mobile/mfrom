import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  TouchableHighlight,
  Linking,
  PermissionsAndroid,
  Platform,
  Alert,
} from 'react-native';

let Icon = require('react-native-vector-icons/FontAwesome');
import QRPage from '../QRPage'
import STRINGS from '../Util/strings'
import AppText from '../Util/AppText'

import Permissions from 'react-native-permissions'

export default class DFBarcode extends Component {

  constructor(props) {
    super(props);
    //console.log(props);
    let placeText = '';
    if ("placeholder" in this.props) {
      placeText = this.props.placeholder
    }

    let valueText = '';
    if ("value" in this.props) {
      valueText = this.props.value
    }

    let type = 'default';

    let read_only = false;
    if ("readOnly" in this.props) {
      read_only = this.props.readOnly
    }

    this.state = {
      placeholderText: placeText,
      text: valueText,
      validationError: '',
      inputType: type,
      readOnly: read_only
    };
  }

  render() {
    var output = (this.props.readOnly || !this.props.editable) ?
      <View>
        <View style={styles.borderViewDisable}>
          <AppText style={(this.state.text == '') ? styles.DisableInputNoText : styles.DisableInput}>{this.state.text}</AppText>
        </View>
        <TouchableHighlight
          underlayColor='#9FB159'
          style={styles.locationBtnDisable}>
          <View style={styles.locationBtnContainer}>
            <View style={{ width: 20, height: 20 }} />
            <View>
              <AppText style={styles.labelBtn}>{STRINGS.COMMON.SCAN_BARCODE}</AppText>
            </View>
            <View>
              <Icon
                name={'barcode'}
                size={20}
                color='white'
                style={{ width: 20, height: 20 }}
              />
            </View>
          </View>
        </TouchableHighlight>
      </View>
      :
      <View>
        <View style={styles.borderView}>
          <TextInput
            ref={this.props.refName}
            style={styles.input}
            keyboardType={this.state.inputType}
            value={this.state.text}
            placeholder={this.state.placeholderText}
            editable={this.props.editable}
            onChangeText={(text) => {
              this.props.addTextData(text, this.props.pageIndex, this.props.fieldIndex);
              this.setState({ text })
            }}
            underlineColorAndroid='rgba(0,0,0,0)'
          />
        </View>
        <TouchableHighlight
          underlayColor='#9FB159'
          style={styles.locationBtn}
          onPress={() => this.tryOpenCamera()}>
          <View style={styles.locationBtnContainer}>
            <View style={{ width: 20, height: 20 }} />
            <View>
              <AppText style={styles.labelBtn}>{STRINGS.COMMON.SCAN_BARCODE}</AppText>
            </View>
            <View>
              <Icon
                name={'barcode'}
                size={20}
                color='white'
                style={{ width: 20, height: 20 }}
              />
            </View>
          </View>
        </TouchableHighlight>
      </View>;

    if (this.props.viewMode) {
      output = <View style={styles.borderViewDisable}>
        <AppText style={styles.viewMode}>{this.state.text}</AppText>
      </View>
    }
    return (output);
  }

  tryOpenCamera() {
    if (Platform.OS === 'ios') {

      // Permissions.getPermissionStatus('camera').then(response => {
      Permissions.requestPermission('camera').then(response => {
        if (response === 'authorized') {
          this.openBarcode();
          
        } else {

          var settings = 'app-settings:';

          Linking.canOpenURL(settings)
            .then(supported => {
              if (supported) {
                Alert.alert(STRINGS.COMMON.OPEN_CAMERA_SERVICES, STRINGS.SETTINGS.SETTINGS_CAMERA,
                  [
                    { text: 'Cancel', onPress: () => "", style: 'cancel' },
                    { text: 'Settings', onPress: () => { Linking.openURL(settings) } },
                  ],
                  { cancelable: false }
                );
              } else {
                Alert.alert('', STRINGS.COMMON.OPEN_CAMERA_SERVICES_MANUALLY,
                  [
                    { text: 'OK' },
                  ],
                  { cancelable: false }
                );
              }
            });
        }
      })
    } else {
      let self = this;
      PermissionsAndroid.checkPermission(PermissionsAndroid.PERMISSIONS.CAMERA)
        .then(function (granted) {
          if (granted) {
            self.openBarcode();
          } else {
            PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.CAMERA)
              .then(function (userGranted) {
                if (userGranted) {
                  self.openBarcode();
                }
              });
          }
        });
    }
  }

  openBarcode() {
    this.props.navigator.push({
      name: 'barcode',
      title: STRINGS.COMMON.SCAN_BARCODE,
      component: QRPage,
      passProps: {
        title: STRINGS.COMMON.SCAN_BARCODE,
        navigator: this.props.navigator,
        setBarcode: this.setBarcode.bind(this)
      }
    });
  }

  setBarcode(value) {
    this.setState({
      text: value,
    });
    this.props.addTextData(value, this.props.pageIndex, this.props.fieldIndex);
  }
}


const styles = StyleSheet.create({
  input: {
    height: 30,
    fontSize: 20,
    borderWidth: 0,
    color: '#333435',
    paddingTop: 2,
    paddingBottom: 2
  },
  labelBtn: {
    marginLeft: 10,
    marginRight: 5,
    marginBottom: 3,
    fontSize: 14,
    color: 'white'
  },
  borderView: {
    borderColor: '#95989A',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 20,
    marginLeft: 20,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  borderViewDisable: {
    borderColor: '#95989A',
    backgroundColor: '#eee',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  DisableInput: {
    height: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#c0c0c0',
    paddingTop: 5,
    paddingBottom: 5,
  },
  DisableInputNoText: {
    height: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#c0c0c0',
    paddingTop: 5,
    paddingBottom: 5,
  },
  locationBtn: {
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    marginRight: 20,
    marginLeft: 20,
    padding: 10,
    borderRadius: 8,
  },
  locationBtnDisable: {
    backgroundColor: '#cdcdcd',
    borderColor: '#cdcdcd',
    marginRight: 20,
    marginLeft: 20,
    padding: 10,
    borderRadius: 8,
  },
  viewMode: {
    minHeight: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#606060',
    textAlignVertical: 'center',
    paddingTop: 5,
    paddingBottom: 5,
  },
  locationBtnContainer: {
    flexGrow: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
