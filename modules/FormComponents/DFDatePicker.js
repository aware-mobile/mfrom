import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  DatePickerIOS,
  TouchableHighlight,
  DatePickerAndroid,
  Platform
} from 'react-native';
import AppText from '../Util/AppText'
let Icon = require('react-native-vector-icons/FontAwesome');

import FadeInView from '../Animation/FadeInView'
import STRINGS from '../Util/strings'

class DFDatePicker extends Component {
  constructor(props) {
    super(props);
    //console.log(props);

    let date = ('date' in this.props) ? this.props.date : new Date();

    let value = ('value' in this.props) ? this.props.value : 'Select a date';
    let isDraft = ('isDraft' in this.props) ? this.props.isDraft : false;

    let formatDateList = ('formatDate' in this.props) ? this.props.formatDate : null;
    let propertiesList = ('properties' in this.props) ? this.props.properties : null;

    this.formatDate = null;
    this.property = null;

    if (formatDateList !== null) {
      for (let i = 0; i < formatDateList.length; i++) {
        if (formatDateList[i].checked) {
          this.formatDate = formatDateList[i].name
        }
      }
    }

    if (propertiesList !== null) {
      for (let i = 0; i < propertiesList.length; i++) {
        if (propertiesList[i].checked) {
          this.property = propertiesList[i].name
        }
      }
    }

    let pickable = ('editable' in this.props) ? this.props.editable : true;

    let read_only = false;
    if ('readOnly' in this.props) {
      if (this.props.readOnly === '' || this.props.readOnly === undefined) {
        read_only = false;
      } else {
        read_only = this.props.readOnly
      }
    }

    let defaultDate;

    if (this.property === 'Current Date' && pickable && !this.props.viewMode) {
      if (value === '') {
        defaultDate = this.fnFormateDate(this.props.currentDateTime);
        this.props.insertData(defaultDate, this.props.pageIndex, this.props.fieldIndex);
      } else {
        defaultDate = value;
      }
    } else {
      if (value === '' || value === null || value === undefined) {
        value = STRINGS.COMMON.PLEASE_SELECT_DATE;
        defaultDate = this.fnFormateDate(this.props.currentDateTime);
      } else {
        defaultDate = value;
      }
    }
    let selectable = true;
    if (read_only || !pickable) {
      selectable = false;
    }

    this.state = {
      show: false,
      date: this.checkErrFormatIOS(defaultDate),
      pickable: selectable,
      dateSelectString: this.property === 'Current Date' && (pickable) ? defaultDate : value
    };
  }

  checkErrFormatIOS(defaultDate) {
    let strDate = defaultDate;

    if (this.formatDate === 'DD/MM/YYYY') {
      let arrayOfStrings = defaultDate.split('/');
      strDate = arrayOfStrings[1] + '/' + arrayOfStrings[0] + '/' + arrayOfStrings[2]
    }

    return new Date(strDate)

  }

  pickDate() {
    if (this.state.pickable) {
      if (Platform.OS === 'ios') {
        this.setState({show: !this.state.show});
        this.onDateChange(this.state.date);
      } else {
        this.showPicker('simple', {date: this.state.date})
      }
    }
  }

  render() {

    let picker = <View style={{marginBottom: 10}}/>;
    if (this.state.show) {
      console.log('SHOW DATE PICKER: ', this.state.date);
      picker =
        <FadeInView style={{marginBottom: 10}}>
          <DatePickerIOS
            date={this.state.date}
            mode="date"
            onDateChange={this.onDateChange.bind(this)}
          />
        </FadeInView>
    }

    let iconName = this.state.show ? 'arrow-up' : 'calendar';
    let icon = <Icon
      name={iconName}
      size={20}
      color='white'
      style={{width: 20, height: 20}}
    />;

    let returnView = (this.state.pickable) ?
      <View style={styles.container}>
        <TouchableHighlight
          style={styles.datePickerBtn}
          underlayColor='#9FB159'
          onPress={() => this.pickDate()}>
          <View style={styles.datePickerBtnContainer}>
            <View>
              <AppText style={styles.datePickerBtnTxt}>{this.state.dateSelectString}</AppText>
            </View>
            <View>
              {icon}
            </View>
          </View>
        </TouchableHighlight>
        {picker}
      </View>
      :
      <View style={styles.container}>
        <TouchableHighlight
          style={styles.datePickerBtnDisable}
          underlayColor='#9FB159'
          onPress={() => this.pickDate()}>
          <View style={styles.datePickerBtnContainer}>
            <View>
              <AppText style={styles.datePickerBtnTxt}>{this.state.dateSelectString}</AppText>
            </View>
            <View>
              {icon}
            </View>
          </View>
        </TouchableHighlight>
        {picker}
      </View>;

    if (this.props.viewMode) {
      returnView = <View
        style={styles.borderView}>
        <AppText
          style={styles.viewMode}>{this.state.dateSelectString === STRINGS.COMMON.PLEASE_SELECT_DATE ? '' : this.state.dateSelectString}</AppText>
      </View>
    }
    return ( returnView )
  }

  onDateChange(date) {

    var dateString = this.fnFormateDate(date);

    this.props.insertData(dateString, this.props.pageIndex, this.props.fieldIndex);
    this.setState({
      date: date,
      dateSelectString: dateString
    })
  }

  pad(num) {
    if (num < 10) {
      return "0" + num;
    } else {
      return "" + num;
    }
  }


  fnFormateDate(date) {

    const day = date.getDate();
    const monthIndex = date.getMonth() + 1;
    const year = date.getFullYear();
    let dateString = "";

    if (this.formatDate === 'DD/MM/YYYY') {
      dateString = this.pad(day) + '/' + this.pad(monthIndex) + '/' + year;
    }
    if (this.formatDate === 'MM/DD/YYYY') {
      dateString = this.pad(monthIndex) + '/' + this.pad(day) + '/' + year;
    }
    if (this.formatDate === 'YYYY/MM/DD') {
      dateString = year + '/' + this.pad(monthIndex) + '/' + this.pad(day);
    }

    return dateString;
  }


  async showPicker(stateKey, options) {
    try {
      console.log('showPicker options: ' + options);
      let dateAndroid = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dateSetAction) {
        let date = new Date(year, month, day);

        let dateString = this.fnFormateDate(date);

        dateAndroid['dateSelectString'] = dateString;
        dateAndroid['date'] = date;
        this.props.insertData(dateString, this.props.pageIndex, this.props.fieldIndex);
        this.setState({
          date: date,
          dateSelectString: dateString,
          dateAndroid,
        })
      }
      this.setState(dateAndroid);
    } catch (error) {
      console.warn(`Error in example '${stateKey}': `, error.message);
    }
  }

}

const styles = StyleSheet.create({
  container: {
    // flexGrow: 1
    marginLeft: 10,
    marginRight: 10
  },
  datePickerBtn: {
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    marginRight: 10,
    marginLeft: 10,
    padding: 10,
    borderRadius: 8,
  },
  datePickerBtnDisable: {
    backgroundColor: '#cdcdcd',
    borderColor: '#cdcdcd',
    marginRight: 10,
    marginLeft: 10,
    padding: 10,
    borderRadius: 8,
  },
  datePickerBtnContainer: {
    flexGrow: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  datePickerBtnTxt: {
    color: 'black'
  },
  borderView: {
    borderColor: '#cdcdcd',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
    justifyContent: 'center',
  },
  viewMode: {
    minHeight: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#606060',
    textAlignVertical: 'center',
    paddingTop: 5,
    paddingBottom: 5
  },
});

const monthNames = [
  "January", "February", "March",
  "April", "May", "June", "July",
  "August", "September", "October",
  "November", "December"
];


export default DFDatePicker;
