import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Platform,
  Linking,
  Image,
  TouchableHighlight,
  NativeModules,
  Alert
} from 'react-native';

import config from '../Util/Config'

let Ionicons = require('react-native-vector-icons/Ionicons');
let Icon = require('react-native-vector-icons/FontAwesome');
import ImagePicker from 'react-native-image-picker';

import STRINGS from '../Util/strings'
import AppText from '../Util/AppText'

let NativeAwareAPI = NativeModules.RNAwareModule;

class DFPhoto extends Component {

  constructor(props) {
    super(props);
    //console.log(props);
    let imgPath = null;

    if ("valueUri" in this.props) {
      console.log('PHOTO valueUri: ', this.props.valueUri);
      let uri = this.props.valueUri;
      if (uri !== '') {
        if (uri.includes('file://')) {
          imgPath = { uri: this.props.valueUri }
        } else {
          imgPath = { uri: config.upload_url + this.props.valueUri }
        }
      }
    }

    if ("value" in this.props) {
      console.log('PHOTO value: ', this.props.value);
      if (this.props.value !== undefined) {
        imgPath = this.props.value
      }
    }

    if ("draw_on_photo" in this.props) {
      if (this.props.draw_on_photo === null || this.props.draw_on_photo === '') {
        this.props.draw_on_photo = false;
      }
    } else {
      this.props.draw_on_photo = false;
    }
    console.log('PHOTO imgPath: ', imgPath);
    this.state = {
      imgPath: imgPath,
      fullScreenMode: false
    };
  }

  pickImage() {

    console.log("this.props.draw_on_photo: ", this.props.draw_on_photo);
    let options = {
      title: STRINGS.IMAGE_PICKER.TITLE_SELECT_PHOTO, // specify null or empty string to remove the title
      cancelButtonTitle: STRINGS.IMAGE_PICKER.CANCEL,
      // takePhotoButtonTitle: 'Take Photo...', // specify null or empty string to remove this button
      // chooseFromLibraryButtonTitle: 'Choose from Library...', // specify null or empty string to remove this button
      cameraType: 'back', // 'front' or 'back'
      mediaType: 'photo', // 'photo' or 'video'
      // maxWidth: 1000, // photos only
      // maxHeight: 1000, // photos only
      aspectX: 2, // android only - aspectX:aspectY, the cropping image's ratio of width to height
      aspectY: 1, // android only - aspectX:aspectY, the cropping image's ratio of width to height
      quality: 0.2, // 0 to 1, photos only
      angle: 0, // android only, photos only
      allowsEditing: false, // Built in functionality to resize/reposition the image after selection
      noData: true, // photos only - disables the base64 `data` field from being generated (greatly improves performance on large photos)
      storageOptions: { // if this key is provided, the image will get saved in the documents directory on ios, and the pictures directory on android (rather than a temporary directory)
        skipBackup: true, // ios only - image will NOT be backed up to icloud
        path: 'images' // ios only - will save image at /Documents/images rather than the root
      },
      drawOnPhoto: this.props.draw_on_photo,
      colors: ["#ffffff", "#000000", "#ace629", "#e64629", "#1264df", "#ffe608", "#ff1717", "#ff980f", "#3af602", "#00ffff", "#8316ff", "#fb00ea"]
    };

    for (let i = 0; i < this.props.image_src.length; i++) {
      if (this.props.image_src[i].name === 'Any' && this.props.image_src[i].checked) {
        options.takePhotoButtonTitle = STRINGS.IMAGE_PICKER.LBL_TAKE_PHOTO;
        options.chooseFromLibraryButtonTitle = STRINGS.IMAGE_PICKER.LBL_PHOTO_LIBRARY;
        console.log("Any")
      }
      if (this.props.image_src[i].name === 'Camera Only' && this.props.image_src[i].checked) {
        options.takePhotoButtonTitle = STRINGS.IMAGE_PICKER.LBL_TAKE_PHOTO;
        options.chooseFromLibraryButtonTitle = null;
        console.log("Camera")
      }
      if (this.props.image_src[i].name === 'Gallery Only' && this.props.image_src[i].checked) {
        options.takePhotoButtonTitle = null;
        options.chooseFromLibraryButtonTitle = STRINGS.IMAGE_PICKER.LBL_PHOTO_LIBRARY;
        console.log("Gallery")
      }
    }

    for (let i = 0; i < this.props.image_size.length; i++) {
      if (this.props.image_size[i].size === 'S' && this.props.image_size[i].checked) {
        options.maxWidth = 400;
        options.maxHeight = 400;
      }
      if (this.props.image_size[i].size === 'M' && this.props.image_size[i].checked) {
        options.maxWidth = 800;
        options.maxHeight = 800;
      }
      if (this.props.image_size[i].size === 'L' && this.props.image_size[i].checked) {
        options.maxWidth = 1200;
        options.maxHeight = 1200;
      }
      if (this.props.image_size[i].size === 'XL' && this.props.image_size[i].checked) {
        options.maxWidth = 1800;
        options.maxHeight = 1800;
      }
      if (this.props.image_size[i].size === 'THUMB' && this.props.image_size[i].checked) {
        options.maxWidth = 300;
        options.maxHeight = 300;
      }

    }

    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);
      
      // if (response.customButton){
      //   console.log('customButton', response.customButton);
      // }

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);

        if (response.error === 'Camera not available on simulator' || response.error === 'Camera permissions not granted') {
          this.showPopup(STRINGS.COMMON.OPEN_CAMERA_SERVICES, STRINGS.COMMON.OPEN_CAMERA_SERVICES_MANUALLY);
        } else {
          this.showPopup(STRINGS.COMMON.OPEN_PHOTOS_SERVICES, STRINGS.COMMON.OPEN_PHOTOS_SERVICES_MANUALLY);
        }

      } else {
        // You can display the image using either data...
        // const source = {uri: 'data:image/jpeg;base64,' + response.data, isStatic: true};

        // or a reference to the platform specific asset location
        let source;
        console.log('PHOTO RESPONSE: ', response);
        source = {
          uri: response.uri,
          isStatic: true,
          name: response.fileName,
          type: response.type,
          uploadType: 'image',
          fileSize: response.fileSize
        };

        console.log("IMG SOURCE: ", source);

        // this.setState({
        //   imgPath: source
        // });
        // this.props.insertData(this.state.imgPath, this.props.pageIndex)

        if (this.props.draw_on_photo && Platform.OS !== 'ios') {
          NativeAwareAPI.editImage(response.path, (response) => {
            source.uri = response.uri;
            this.setState({
              imgPath: source
            });
            this.props.insertData(this.state.imgPath, this.props.pageIndex)
          });
        } else {
          this.setState({
            imgPath: source
          });
          this.props.insertData(this.state.imgPath, this.props.pageIndex)
        }

      }
    });

  }

  showPopup(ToSettings, ToSettingsManually) {
    // show popup allow Camera and Photos 
    Linking.canOpenURL('app-settings:')
        .then(supported => {
          if (supported) {
            Alert.alert('', ToSettings,
              [
                { text: 'Cancel', onPress: () => "", style: 'cancel' },
                {
                  text: 'OK', onPress: () => {
                    // Linking.openURL("App-Prefs:root=Privacy&path=Photos&path=app")

                    Linking.openURL('app-settings:')
                  }
                },
              ],
              { cancelable: false }
            );
          } else {
            Alert.alert(ToSettingsManually, "",
              [
                { text: 'OK' },
              ],
              { cancelable: false }
            );
          }
        });
  }

  render() {

    let btnImage = this.props.editable ? <View style={{ marginBottom: 5 }}>
      <TouchableHighlight
        style={styles.locationBtn}
        underlayColor='#9FB159'
        onPress={() => this.pickImage()}>
        <View style={styles.locationBtnContainer}>
          <View style={{ width: 20, height: 20 }}></View>
          <AppText style={styles.labelBtn}>{STRINGS.IMAGE_PICKER.UPLOAD_A_PHOTO}</AppText>
          <View>
            <Icon
              name={'file-photo-o'}
              size={20}
              color='white'
              style={{ width: 20, height: 20 }}
            />
          </View>
        </View>
      </TouchableHighlight>
    </View> : <View style={{ marginBottom: 5 }}>
        <TouchableHighlight
          style={styles.locationBtnDisable}>
          <View style={styles.locationBtnContainer}>
            <View style={{ width: 20, height: 20 }}></View>
            <AppText style={styles.labelBtn}>{STRINGS.IMAGE_PICKER.UPLOAD_A_PHOTO}</AppText>
            <View>
              <Icon
                name={'file-photo-o'}
                size={20}
                color='white'
                style={{ width: 20, height: 20 }}
              />
            </View>
          </View>
        </TouchableHighlight>
      </View>;
    if (this.props.viewMode) {
      btnImage = null;
    }
    let btnRemove = this.state.imgPath !== null && this.props.editable ? <TouchableHighlight
      underlayColor='transparent'
      onPress={() => this.removeImage()}
      style={{ alignItems: 'flex-end' }}>
      <Ionicons name="md-close" size={25} color="#B2D234"
        style={{ justifyContent: 'center', alignItems: 'center', marginRight: 5 }} />
    </TouchableHighlight> : null;

    let renderView =
      <View style={{ marginLeft: 10, marginRight: 10 }}>
        <View style={{ marginBottom: 5 }}>
          {btnRemove}
          {this.state.imgPath !== null ?
            <View style={{
              borderWidth: 2,
              borderStyle: 'dashed',
              marginLeft: 20,
              marginRight: 20,
              borderColor: '#c0c0c0'
            }}>
              <TouchableHighlight
                underlayColor='transparent'
                onPress={this.fullScreen.bind(this)}>
                <Image resizeMode={Image.resizeMode.contain} source={this.state.imgPath} style={styles.imgView} />
              </TouchableHighlight>
            </View>
            : null}
        </View>
        {btnImage}
      </View>;

    if (this.props.viewMode && this.state.imgPath === null) {
      renderView = <View
        style={styles.borderView}>
        <AppText style={styles.viewMode}>{STRINGS.IMAGE_PICKER.NO_IMAGE_UPLOADED}</AppText>
      </View>;
    }

    return (renderView);
  }

  fullScreen() {
    console.log('DFPhoto imgPath: ', this.state.imgPath);

    if ("valueUri" in this.props) {
      let uri = this.props.valueUri;
      let imgPath = this.state.imgPath;
      if (uri !== '') {
        if (uri.includes('file://')) {
          imgPath = { uri: this.props.valueUri };
          this.props.openImageViewer(this.props.label_name, imgPath);
        } else {
          imgPath = { uri: config.upload_url + this.props.valueUri };
          this.props.openImageViewer(this.props.label_name, imgPath);
        }
      }
    }
  }

  removeImage() {
    this.setState({
      imgPath: null
    });
    this.props.removeData(this.props.pageIndex, this.props.fieldIndex);
  }
}

const styles = StyleSheet.create({
  imgView: {
    height: 200,
    margin: 5,
  },
  label: {
    marginLeft: 10,
    marginRight: 5,
    marginBottom: 3,
    fontSize: 14,
    color: 'black'
  },
  labelBtn: {
    marginLeft: 10,
    marginRight: 5,
    marginBottom: 3,
    fontSize: 14,
    color: 'white'
  },
  locationBtn: {
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    marginRight: 10,
    marginLeft: 10,
    padding: 10,
    borderRadius: 8,
  },
  locationBtnDisable: {
    backgroundColor: '#cdcdcd',
    borderColor: '#cdcdcd',
    marginRight: 10,
    marginLeft: 10,
    padding: 10,
    borderRadius: 8,
  },
  locationBtnContainer: {
    flexGrow: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  borderView: {
    borderColor: '#cdcdcd',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  viewMode: {
    minHeight: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#606060',
    textAlignVertical: 'center',
    paddingTop: 5,
    paddingBottom: 5
  },
});

export default DFPhoto;
