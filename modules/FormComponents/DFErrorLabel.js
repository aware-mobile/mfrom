import React, { Component } from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';

import AppText from '../Util/AppText'

class DFErrorLabel extends Component {
  constructor(props) {
    super(props);

    var fontColor = 'red'

    this.state = {
      fontColor: fontColor
    };

    this._createStylesheet();
  }

  render() {
    var output = (this.props.isShow && this.props.text.length > 0) ?
      <AppText
        style={styles.label}
        ref={this.props.refName}>
        {this.props.text}
      </AppText>
      : null;

    return (output);

  }

  _createStylesheet() {
    styles = StyleSheet.create({
      label: {
        marginLeft: 10,
        marginRight: 5,
        marginBottom: 3,
        fontSize: 16,
        color: this.state.fontColor
      },
    });
  }

}

var styles

export default DFErrorLabel;
