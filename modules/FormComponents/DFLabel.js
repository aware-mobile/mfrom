import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';

class DFLabel extends Component {
  constructor(props) {
    super(props);

    let labelText = 'Label Title';
    if ("text" in this.props) {
      labelText = this.props.text
    }

    let fontWeight = 'normal';
    if ('fontWeight' in this.props) {
      fontWeight = this.props.fontWeight
    }

    let fontColor = '#000000';
    if ("color" in this.props) {
      fontColor = this.props.color
    }

    let backgroundColor = 'transparent';
    if ("backgroundColor" in this.props) {
      backgroundColor = this.props.backgroundColor
    }

    let fontSize = 20;
    if ("fontSize" in this.props) {
      fontSize = this.props.fontSize
    }

    let marginLeft = 15;
    if ("marginLeft" in this.props) {
      marginLeft = this.props.marginLeft
    }

    this.state = {
      labelText: labelText,
      fontColor: fontColor,
      marginLeft: marginLeft,
      backgroundColor: backgroundColor,
      fontSize: fontSize,
      fontWeight: fontWeight
    };

    this._createStylesheet();
  }

  render() {

    let outputText = null;
    let outputRequired = null;

    outputText =
      <Text
        style={{
          fontFamily: 'DB Heavent',
          fontSize: this.state.fontSize, color: this.state.fontColor,
          fontWeight: this.state.fontWeight
        }}
        ref={this.props.refName}>
        {this.state.labelText}
      </Text>;

    outputRequired = (this.props.isRequired) ?
      <Text
        style={[styles.required, {fontFamily: 'DB Heavent', fontSize: this.state.fontSize}]}>*
      </Text> : null;

    return (
      <View style={[styles.container,{ backgroundColor: this.state.backgroundColor, marginLeft: this.state.marginLeft }]}>
        <Text>
          {outputText}{outputRequired}
        </Text>
      </View>
    );
  }

  _createStylesheet() {
    styles = StyleSheet.create({
      label: {
        color: this.state.fontColor,
        fontWeight: this.state.fontWeight,
      },
      required: {
        color: 'red'
      },
      container: {
        alignItems: 'flex-start',
        marginTop: 5,
        marginLeft: 15,
        marginRight: 15,
        marginBottom: 5
      }
    });
  }

}

let styles;

export default DFLabel;
