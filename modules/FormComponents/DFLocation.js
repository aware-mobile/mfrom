import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Platform,
  Image,
  TouchableHighlight,
  Linking,
  PermissionsAndroid,
  Alert
} from 'react-native';
import DFButton from '../FormComponents/DFButton'
import STRINGS from '../Util/strings'
import AppText from '../Util/AppText'
import Permissions from 'react-native-permissions'
var Icon = require('react-native-vector-icons/FontAwesome');

class DFLocation extends Component {

  constructor(props) {
    super(props);
    //console.log(props);

    this.watchID = null;
    var valueLocation = null;

    if ("value" in this.props) {
      if (this.props.value != undefined)
        valueLocation = this.props.value
    }

    this.state = {
      value: valueLocation,
      msgError: null,
      isConnected: this.props.isConnected
    };

    this.isFindLocation = false;
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }

  componentWillReceiveProps(nextProps) {

    let valueLocation = null;
    if ("value" in nextProps) {
      if (nextProps.value != undefined)
        valueLocation = nextProps.value
    }
    //console.log('PROP PROPERTIES GPS: ', this.props.properties)

    this.state = {
      value: valueLocation,
      isConnected: nextProps.isConnected
    };
  }

  tryPickLocation() {
    if (Platform.OS === 'ios') {
      this.pickLocation();
    } else {
      let self = this;
      PermissionsAndroid.checkPermission(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
        .then(function (granted) {
          if (granted) {
            self.pickLocation();
          } else {
            PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
              .then(function (userGranted) {
                if (userGranted) {
                  self.pickLocation();
                }
              });
          }
        });
    }

  }

  pickLocation() {
    if (this.state.isConnected) {
      var mysrclat = 0;
      var mysrclong = 0;
      var self = this;
      if (this.isFindLocation) {
        return;
      }

      this.props.showSpinner(true)
      this.isFindLocation = true;

      if (Platform.OS === 'ios') {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            console.log('Position = ', position);
            mysrclat = position.coords.latitude;
            mysrclong = position.coords.longitude;

            var value = mysrclat.toFixed(7) + ',' + mysrclong.toFixed(7);

            if (this.props.properties.choose_location) {

              var distance = self.getDistanceFromLatLonInM(mysrclat, mysrclong, this.props.properties.lat, this.props.properties.lng)

              if (distance < this.props.properties.radius) {
                this.setState({
                  value: value,
                  msgError: ''
                });
                this.props.insertData(this.state.value, this.props.pageIndex, this.props.fieldIndex)
                this.props.showSpinner(false);
              } else {
                this.props.showSpinner(false);
                this.setState({
                  msgError: STRINGS.COMMON.CANNOT_CHECKIN
                });
              }

            } else {
              this.setState({
                value: value,
                msgError: ''
              });
              this.props.insertData(this.state.value, this.props.pageIndex, this.props.fieldIndex)
              this.props.showSpinner(false)
            }

            self.isFindLocation = false;
          },
          (error) => {
            console.log('error', error);
              self.isFindLocation = false;
              this.props.showSpinner(false)

              Linking.canOpenURL('app-settings:')
              .then(supported => {
                if (supported) {

                  Alert.alert(STRINGS.COMMON.OPEN_LOCATION_SERVICES, STRINGS.SETTINGS.SETTINGS_LOCATION,
                    [
                      { text: 'Cancel', onPress: () => "", style: 'cancel' },
                      {
                        text: 'Settings', onPress: () => {
                          Linking.openURL('app-settings:')
                        }
                      },
                    ],
                    { cancelable: false }
                  );
                } else { 
                  Alert.alert('', STRINGS.COMMON.OPEN_LOCATION_SERVICES_MANUALLY,
                    [
                      { text: 'OK' },
                    ],
                    { cancelable: false }
                  );
                }
              });
          },
          { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
      } else {
        this.watchID = navigator.geolocation.watchPosition((position) => {
          console.log('Position = ', position);
          mysrclat = position.coords.latitude;
          mysrclong = position.coords.longitude;

          var value = mysrclat.toFixed(7) + ',' + mysrclong.toFixed(7);

          if (this.props.properties.choose_location) {

            var distance = self.getDistanceFromLatLonInM(mysrclat, mysrclong, this.props.properties.lat, this.props.properties.lng)

            if (distance < this.props.properties.radius) {
              this.setState({
                value: value,
                msgError: ''
              });
              this.props.insertData(this.state.value, this.props.pageIndex, this.props.fieldIndex)
              this.props.showSpinner(false)
            } else {
              this.props.showSpinner(false);
              this.setState({
                msgError: STRINGS.COMMON.CANNOT_CHECKIN
              });
            }
          } else {
            this.setState({
              value: value,
              msgError: ''
            });
            this.props.insertData(this.state.value, this.props.pageIndex, this.props.fieldIndex)
            this.props.showSpinner(false)
          }
          self.isFindLocation = false;
          navigator.geolocation.clearWatch(this.watchID);
        }, (error) => {
          console.log('watchPositionError = ', error);
          self.setState({
            msgError: error.message
          });
          self.isFindLocation = false;
          navigator.geolocation.clearWatch(self.watchID);
          self.props.showSpinner(false);
          self.setState({
            msgError: STRINGS.COMMON.LOCATION_ERR
          });
        });
      }
    } else {
      this.props.showAlertPopup(STRINGS.COMMON.INTERNET_REQUIRED)
    }
  }

  getDistanceFromLatLonInM(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - lat1); // deg2rad below
    var dLon = this.deg2rad(lon2 - lon1);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c * 1000; // Distance in m
    console.log('distance: ' + d);
    return d;
  }

  deg2rad(deg) {
    return deg * (Math.PI / 180)
  }

  handleClickLatLng(position) {
    var urlMap = "https://www.google.com/maps/place/" + position + "/@" + position + ",14z"
    Linking.canOpenURL(urlMap).then(supported => {
      if (supported) {
        Linking.openURL(urlMap);
      } else {
        console.log('Don\'t know how to open URI: ' + urlMap);
      }
    });
  };

  render() {

    var btnLocation = this.props.editable ? <TouchableHighlight
      style={styles.locationBtn}
      underlayColor='#9FB159'
      onPress={() => this.tryPickLocation()}>
      <View style={styles.locationBtnContainer}>
        <View>
          <AppText style={styles.labelBtn}>{STRINGS.COMMON.BTN_GET_LOCATION}</AppText>
        </View>
        <View>
          <Icon
            name={'map-pin'}
            size={20}
            color='white'
            style={{ width: 20, height: 20 }}
          />
        </View>
      </View>
    </TouchableHighlight> : <TouchableHighlight
      style={styles.locationBtnDisable}>
        <View style={styles.locationBtnContainer}>
          <View>
            <AppText style={styles.labelBtn}>{STRINGS.COMMON.BTN_GET_LOCATION}</AppText>
          </View>
          <View>
            <Icon
              name={'map-pin'}
              size={20}
              color='white'
              style={{ width: 20, height: 20 }}
            />
          </View>
        </View>
      </TouchableHighlight>;
    if (this.props.viewMode) {
      btnLocation = null;
    }

    // var btnLocation = this.props.editable ? <DFButton
    //   text="Get Location"
    //   onPress={ () => this.pickLocation() }
    //   style={{backgroundColor: "#B2D234", borderColor: "#B2D234"}}/> : <DFButton
    //   text="Get Location"
    //   style={{backgroundColor: 'gray', borderColor: 'gray'}}/>;

    var viewError = this.state.msgError != null ? <AppText style={styles.labelError}>
      {this.state.msgError}
    </AppText> : null;

    var renderView = <View style={{ marginLeft: 10, marginRight: 10 }}>
      {this.state.value !== null ?
        <View style={{ marginBottom: 5 }}>
          <AppText style={styles.label}>
            {this.state.value}
          </AppText>

          <TouchableHighlight
            style={styles.locationBtn}
            underlayColor='#9FB159'
            onPress={() => this.handleClickLatLng(this.state.value)}>
            <View style={styles.locationBtnContainer}>
              <View>
                <AppText style={styles.labelBtn}>{STRINGS.COMMON.BTN_OPEN_MAPS}</AppText>
              </View>
              <View>
                <Icon
                  name={'external-link'}
                  size={20}
                  color='white'
                  style={{ width: 20, height: 20 }}
                />
              </View>
            </View>
          </TouchableHighlight>
        </View>
        :
        null
      }
      <View style={{ flexGrow: 1, marginBottom: 5 }}>
        {btnLocation}
      </View>
      {viewError}
    </View>

    if (this.props.viewMode && this.state.value == null) {
      renderView = <View
        style={styles.borderView}>
        <AppText style={styles.viewMode}>{STRINGS.CUSTOM_MODALS.NO_LOCATION_DATA}</AppText>
      </View>;
    }

    return (renderView);
  }
}

const styles = StyleSheet.create({
  label: {
    marginLeft: 10,
    marginRight: 5,
    marginBottom: 3,
    fontSize: 14,
    color: 'black'
  },
  labelBtn: {
    marginLeft: 10,
    marginRight: 5,
    marginBottom: 3,
    fontSize: 14,
    color: 'white'
  },
  labelError: {
    marginLeft: 10,
    marginRight: 5,
    marginBottom: 3,
    fontSize: 14,
    color: 'red'
  },
  container: {
    flexGrow: 1
  },
  locationBtn: {
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    marginRight: 10,
    marginLeft: 10,
    padding: 10,
    borderRadius: 8,
  },
  locationBtnDisable: {
    backgroundColor: '#cdcdcd',
    borderColor: '#cdcdcd',
    marginRight: 10,
    marginLeft: 10,
    padding: 10,
    borderRadius: 8,
  },
  locationBtnContainer: {
    flexGrow: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  borderView: {
    borderColor: '#cdcdcd',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  viewMode: {
    minHeight: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#606060',
    textAlignVertical: 'center',
    paddingTop: 5,
    paddingBottom: 5
  },
});

export default DFLocation;
