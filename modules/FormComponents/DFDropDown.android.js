import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Picker,
  TouchableHighlight,
  Platform
} from 'react-native';

import STRINGS from '../Util/strings'
import AppText from '../Util/AppText'

let PickerItem = Picker.Item;

class DFDropDown extends Component {
  constructor(props) {
    super(props);
    let listItemNames = [];
    let DropDownOptions = [];

    let firstSelectVal = '';
    let firstSelectLabel = STRINGS.COMMON.PLEASE_SELECT;
    listItemNames.push(firstSelectVal);
    DropDownOptions.push({label: firstSelectLabel, value: firstSelectVal});
    if ("options" in this.props) {

      for (let i = 0; i < this.props.options.length; i++) {
        listItemNames.push(this.props.options[i].value);
        DropDownOptions.push({label: this.props.options[i].label, value: this.props.options[i].value});
        if (this.props.options[i].value === this.props.value) {
          firstSelectVal = this.props.options[i].value;
          firstSelectLabel = this.props.options[i].label
        }
      }
    }

    let pickable = ('editable' in this.props) ? this.props.editable : true;
    this.state = {
      options: DropDownOptions,
      labels: listItemNames,
      show: false,
      pickable: pickable,
      selectedValue: firstSelectVal,
      selectedLabel: firstSelectLabel
    };

  }

  _valueChanged(value) {
    let index = this.state.labels.indexOf(value);
    this.setState({
      selectedValue: this.state.options[index].value,
      selectedLabel: this.state.options[index].label
    });

    this.props.insertData(value, this.props.pageIndex, this.props.fieldIndex);
  }

  render() {
    let styleView = this.state.pickable ? styles.dropdownBtn : styles.dropdownBtnDisable;

    let returnView = <View style={styleView}>
      <Picker style={styles.picker}
              enabled={this.state.pickable}
              selectedValue={this.state.selectedValue}
              onValueChange={this._valueChanged.bind(this)}>
        {Object.keys(this.state.options).map((key) => (
          <PickerItem
            key={key}
            style={styles.dropdownBtnTxt}
            value={this.state.options[key].value}
            label={this.state.options[key].label}
          />
        ))}

      </Picker>
    </View>;
    if (this.props.viewMode) {
      returnView = <View
        style={styles.borderView}>
        <AppText
          style={styles.viewMode}>{this.state.selectedValue == '' ? STRINGS.COMMON.NO_OPTION_SELECTED : this.state.selectedLabel}</AppText>
      </View>
    }
    return (returnView);
  }

}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    marginLeft: 5,
    marginRight: 5
  },
  dropdownBtn: {
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    marginTop: 0,
    marginRight: 20,
    marginLeft: 20,
    borderRadius: 8,
  },
  picker: {
    backgroundColor: 'transparent',
    height: 40,
    color: 'black',
  },
  dropdownBtnDisable: {
    backgroundColor: '#cdcdcd',
    borderColor: '#cdcdcd',
    marginTop: 0,
    marginRight: 20,
    marginLeft: 20,
    borderRadius: 8,
  },
  dropdownBtnContainer: {
    flexGrow: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  dropdownBtnTxt: {
    fontSize: 10,
    color: 'black'
  },
  borderView: {
    borderColor: '#cdcdcd',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
    justifyContent: 'center',
  },
  viewMode: {
    minHeight: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#606060',
    textAlignVertical: 'center',
    paddingTop: 5,
    paddingBottom: 5
  },
});

export default DFDropDown;
