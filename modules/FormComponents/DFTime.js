'use strict';

import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  DatePickerIOS,
  TouchableHighlight,
  TimePickerAndroid,
  Platform
} from 'react-native';

import AppText from '../Util/AppText'

let Icon = require('react-native-vector-icons/FontAwesome');

import FadeInView from '../Animation/FadeInView'
import STRINGS from '../Util/strings'

let self;

class DFTime extends Component {
  constructor(props) {
    super(props);

    self = this;

    let value = ('value' in this.props) ? this.props.value : null;
    let isDraft = ('isDraft' in this.props) ? this.props.isDraft : false;

    let formatTimeList = ('formatTime' in this.props) ? this.props.formatTime : null;
    let propertiesList = ('properties' in this.props) ? this.props.properties : null;

    this.formatTime = null;
    this.property = null;

    if (formatTimeList !== null) {
      for (let i = 0; i < formatTimeList.length; i++) {
        if (formatTimeList[i].checked) {
          this.formatTime = formatTimeList[i].name
        }
      }
    }

    if (propertiesList !== null) {
      for (let i = 0; i < propertiesList.length; i++) {
        if (propertiesList[i].checked) {
          this.property = propertiesList[i].name
        }
      }
    }

    let pickable = ('editable' in this.props) ? this.props.editable : true;

    let read_only = false;
    if ('readOnly' in this.props) {
      if (this.props.readOnly === '' || this.props.readOnly === undefined) {
        read_only = false;
      } else {
        read_only = this.props.readOnly
      }
    }

    let defaultTime = STRINGS.COMMON.PLEASE_SELECT_TIME;
    let hour = 0;
    let minute = 0;

    if (this.property === 'Current Time' && pickable && !this.props.viewMode) {
      if (value !== '' && value !== null && value !== undefined) {
        defaultTime = value;
        hour = Number(value.match(/^(\d\d?)/)[1]);
        minute = Number(value.match(/:(\d\d?)/)[1]);

        let result = value.match(/\s?([AaPp][Mm]?)$/);
        let AMPM;
        if (result && result.length >= 2) {
          AMPM = result[1];
        } else {
          AMPM = '';
        }

        if (AMPM === 'PM' && hour < 12) {
          hour = hour + 12;
        } else if (AMPM === 'AM' && hour === 12) {
          hour = hour - 12;
        }
      }
    } else {
      if (value !== '' && value !== null && value !== undefined) {
        defaultTime = value;
        hour = Number(value.match(/^(\d\d?)/)[1]);
        minute = Number(value.match(/:(\d\d?)/)[1]);

        let result = value.match(/\s?([AaPp][Mm]?)$/);
        let AMPM;
        if (result && result.length >= 2) {
          AMPM = result[1];
        } else {
          AMPM = '';
        }

        if (AMPM === 'PM' && hour < 12) {
          hour = hour + 12;
        } else if (AMPM === 'AM' && hour === 12) {
          hour = hour - 12;
        }
      }
    }

    if (read_only) {
      pickable = false;
    }

    this.isClick = false;
    this.state = {
      show: false,
      time: defaultTime,
      pickable: pickable,
      readOnly: read_only,
      value,
      hour,
      minute,
      is24Hour: (this.formatTime !== 'AM/PM'),
      showIOSPicker: false,
      iosDate: this.props.currentDateTime
    };

  }

  onPressHandle() {
    if (Platform.OS === 'ios') {
      if (this.state.showIOSPicker) {
        this.setState({showIOSPicker: false})
      } else {
        let date = this.state.iosDate;
        date.setHours(this.state.hour);
        date.setMinutes(this.state.minute);
        this.setState({
          iosDate: date,
          showIOSPicker: true
        })
      }
    } else {
      this.showAndroidPicker({
        hour: this.state.hour,
        minute: this.state.minute,
        is24Hour: this.state.is24Hour
      })
    }
  }

  showAndroidPicker = async (options) => {
    try {
      const {action, minute, hour} = await TimePickerAndroid.open(options);
      let newState = {};
      if (action === TimePickerAndroid.timeSetAction) {
        // this.props.openMainModal('Set Time: ' + hour + ':' + minute)
        let timeString = this.fomateTime([hour, minute]);
        this.props.insertData(timeString, this.props.pageIndex, this.props.fieldIndex);
        this.setState({hour, minute, time: timeString})
      } else if (action === TimePickerAndroid.dismissedAction) {
        // this.props.openMainModal('Dissmissed The Time Picker')
      }

    } catch ({code, message}) {
      this.props.openMainModal(STRINGS.CUSTOM_MODALS.TIME_PICKER_ERR + message)
      console.log('Cannot open time picker: ', message);
    }
  };

  // IOS ONLY
  onTimeChange(date) {
    let hour = date.getHours();
    let minute = date.getMinutes();
    let timeString = this.fomateTime([hour, minute]);
    this.props.insertData(timeString, this.props.pageIndex, this.props.fieldIndex);
    this.setState({hour, minute, iosDate: date, time: timeString})
  }

  updateComponent(time) {
    console.log('Update Component');
    console.log(time);
    let defaultTime;
    if (time === '') {
      defaultTime = this.fnFormateTime('');
    } else {
      defaultTime = this.fomateTime(time.split(':'));
    }
    this.isClick = false;
    this.setState({
      time: defaultTime,
      value: time
    })
  }

  fomateTime(time) {

    let hour = time[0];
    let minute = time[1];

    let timeString = "";

    if (this.formatTime === 'AM/PM') {
      let num = parseInt(hour);
      let format = (num < 12) ? 'AM' : 'PM';
      if (num > 12) {
        num = num - 12;
      }
      if (num === 0) {
        num = 12;
      }
      timeString = this.pad(num) + ':' + this.pad(minute) + ' ' + format
    }
    if (this.formatTime === 'HH:MM') {
      timeString = this.pad(hour) + ':' + this.pad(minute)
    }

    return timeString;
  }

  render() {

    let iconName = this.state.show ? 'arrow-up' : 'clock-o';
    let icon = <Icon
      name={iconName}
      size={20}
      color='white'
      style={{width: 20, height: 20}}/>;

    let picker = null;
    if (this.state.showIOSPicker) {
      picker =
        <FadeInView style={{marginBottom: 10}}>
          <DatePickerIOS
            date={this.state.iosDate}
            mode="time"
            onDateChange={this.onTimeChange.bind(this)}
          />
        </FadeInView>
    }

    let returnView = (this.state.pickable) ?

      <View style={styles.container}>

        <TouchableHighlight style={styles.datePickerBtn} underlayColor='#9FB159'
                            onPress={this.onPressHandle.bind(this)}>
          <View style={styles.datePickerBtnContainer}>
            <View>
              <AppText style={styles.datePickerBtnTxt}>{this.state.time}</AppText>
            </View>
            <View>
              {icon}
            </View>
          </View>
        </TouchableHighlight>
        {picker}
      </View> :
      <View style={styles.container}>

        <TouchableHighlight style={styles.datePickerBtnDisable} underlayColor='#9FB159'>
          <View style={styles.datePickerBtnContainer}>
            <View>
              <AppText style={styles.datePickerBtnTxt}>{this.state.time}</AppText>
            </View>
            <View>
              {icon}
            </View>
          </View>
        </TouchableHighlight>
      </View>;

    if (this.props.viewMode) {
      returnView = <View
        style={styles.borderView}>
        <AppText
          style={styles.viewMode}>{this.state.time === STRINGS.COMMON.PLEASE_SELECT_TIME ? '' : this.state.time}</AppText>
      </View>
    }
    return (returnView);
  }

  pad(num) {
    num = parseInt(num);
    if (num < 10) {
      return "0" + num;
    } else {
      return num;
    }
  }

  fnFormateTime(time) {

    let hour = '';
    let minute = '';

    if (time === '') {
      let date = new Date();
      hour = date.getHours();
      minute = date.getMinutes();
    }

    time = this.pad(hour) + ':' + this.pad(minute);

    this.props.insertData(time, this.props.pageIndex, this.props.fieldIndex);

    let timeString = "";

    if (this.formatTime === 'AM/PM') {
      let num = parseInt(hour);
      let format = (num < 12) ? 'AM' : 'PM';
      if (num > 12) {
        num = num - 12;
      }
      if (num === 0) {
        num = 12;
      }
      timeString = this.pad(num) + ':' + this.pad(minute) + ' ' + format
    }

    if (this.formatTime === 'HH:MM') {
      timeString = this.pad(hour) + ':' + this.pad(minute)
    }
    return timeString;
  }

}

const styles = StyleSheet.create({
  container: {
    marginLeft: 15,
    marginRight: 15
    // flexGrow: 1
  },
  datePickerBtn: {
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    marginRight: 5,
    marginLeft: 5,
    marginBottom: 10,
    padding: 10,
    borderRadius: 8,
  },
  datePickerBtnDisable: {
    backgroundColor: '#cdcdcd',
    borderColor: '#cdcdcd',
    marginRight: 5,
    marginLeft: 5,
    marginBottom: 10,
    padding: 10,
    borderRadius: 8,
  },
  datePickerBtnContainer: {
    flexGrow: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  datePickerBtnTxt: {
    color: 'black'
  },
  borderView: {
    borderColor: '#cdcdcd',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
    justifyContent: 'center',
  },
  viewMode: {
    minHeight: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#606060',
    textAlignVertical: 'center',
    paddingTop: 5,
    paddingBottom: 5
  },
});

export default DFTime;
