import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    TextInput,
    Platform,
    Image,
    CameraRoll,
    TouchableHighlight,
    NativeModules
} from 'react-native';
const Timer = require('react-native-timer');
import Device from '../Util/Util'

import config from '../Util/Config'
var Ionicons = require('react-native-vector-icons/Ionicons');
var Icon = require('react-native-vector-icons/FontAwesome');
import ImagePicker from 'react-native-image-picker';

import RNFetchBlob from 'react-native-fetch-blob';
import FileOpenIOS from 'react-native-open-file';

import STRINGS from '../Util/strings'

class DFDownloadPDF extends Component {

    constructor(props) {
        super(props);
        this.downloadFile(this.props.fileName)
    }

    render(){
        return null
    }

    getMimeByExt(ext) {
        if (ext == 'PDF') {
            return 'application/pdf'
        }
        return false;
    }

    downloadFile(fileName) {
        let self = this

        console.log('downloadedFile: ', fileName)
        let filePath = { uri: config.download_url + '/' + fileName, name: fileName };
        console.log('File Path', filePath.uri)
        let dl_file = filePath.uri.split('/')
        let dl_file_name = dl_file[dl_file.length - 1]
        let file_ext_arr = dl_file_name.split('.')
        let file_ext = file_ext_arr[file_ext_arr.length - 1]
        let dl_file_mime = this.getMimeByExt(file_ext.toUpperCase())
        if (dl_file_mime) {
            // let dirs = RNFetchBlob.fs.dirs
            let encodedURI = encodeURI(filePath.uri)
            console.log('send uri: ', encodedURI)

            RNFetchBlob
                .config({
                    fileCache: true,
                    appendExt: file_ext
                })
                .fetch('GET', encodedURI, {
                    //some headers ..
                })
                .then((res) => {
                    // the path should be dirs.DocumentDir + 'path-to-file.anything'
                    console.log('The file saved to ', res.path())
                    // this.props.showSpinner(false)
                    Timer.setTimeout(self, 'closeLoading', () => {
                        self.props.showSpinner(false)
                    }, 500)
                    FileOpenIOS.open(res.path());

                })
        } else {
            this.props.showSpinner(false)
            console.log('DF-ERROR: Unknown Mime Type for extension', file_ext[file_ext.length - 1])
            alert(STRINGS.VALIDATION_MESSAGES.UNKNOWN_FILE_TYPE)
        }

    }
}

export default DFDownloadPDF;
