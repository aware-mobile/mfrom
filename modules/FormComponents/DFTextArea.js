import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Linking
} from 'react-native';

import AppText from '../Util/AppText'

import Hyperlink from 'react-native-hyperlink'
import STRINGS from '../Util/strings'

var OutputComponent;
var self;

class DFTextArea extends Component {

  constructor(props) {
    super(props);

    self = this;

    var placeText = ''
    if ("placeholder" in this.props) {
      placeText = this.props.placeholder
    }

    var valueText = ''
    if ("value" in this.props) {
      valueText = this.props.value
    }

    var read_only = false;
    if ("readOnly" in this.props) {
      read_only = this.props.readOnly
    }

    var max_length = this.props.maxLength;

    this.state = {
      placeholderText: placeText,
      text: valueText,
      validationError: '',
      maxLength: max_length,
      readOnly: read_only
    };
  }

  render() {
    var output = (this.state.readOnly || !this.props.editable) ?
      <Hyperlink
        onPress={ url => this.clickedLink(url) }
        linkStyle={{ color: '#2980b9' }}>
        <AppText style={(this.state.text === '') ? styles.disabletextArea : styles.disabletextArea}>{this.state.text}</AppText>
      </Hyperlink>

      // <TextInput
      //   multiline={true}
      //   ref={this.props.refName}
      //   returnKeyType = {"default"}
      //   style={styles.disabletextArea}
      //   value={this.state.text}
      //   placeholder={this.state.placeholderText}
      //   maxLength={this.state.maxLength}
      //   editable={false}
      //   underlineColorAndroid='rgba(0,0,0,0)'
      //   />
      :
      <TextInput
        multiline={true}
        ref={this.props.refName}
        returnKeyType = {"default"}
        style={styles.textArea}
        value={this.state.text}
        placeholder={this.state.placeholderText}
        maxLength={this.state.maxLength}
        editable={this.props.editable}
        onChangeText={(text) => {
          this.props.addTextData(text,this.props.pageIndex,this.props.fieldIndex)
          this.setState({ text })
        } }
        underlineColorAndroid='rgba(0,0,0,0)'
        />


    if(this.props.viewMode){
      output =  <Hyperlink onPress={ url => this.clickedLink(url) } linkStyle={{ color: '#2980b9' }}><AppText style={styles.viewMode}>{this.state.text}</AppText></Hyperlink>
    }


    return (
      <View style={(this.state.readOnly || !this.props.editable) ? styles.borderViewDisable : styles.borderView }>
        {output}
      </View>
    );
  }

  focusNow() {
    this.refs[this.props.refName].focus()
  }

  clickedLink(url){
    this.props.openMainConfirm(STRINGS.formatString(STRINGS.POPUP_MSG.OPEN_LINK, url), function () {
      Linking.canOpenURL(url).then(supported => {
        if (supported) {
          Linking.openURL(url);
        } else {
          self.props.openMainModal(STRINGS.POPUP_MSG.CANNOT_OPEN_LINK);
        }
      });
    });
  }

}

const styles = StyleSheet.create({
  textArea: {
    height: 110,
    flexGrow: 4,
    fontSize: 20,
    borderWidth: 0,
    borderRadius: 8,
    color: '#333435',
    textAlignVertical: 'top',
    paddingTop: 2,
    paddingBottom: 2
  },
  borderView: {
    borderColor: '#95989A',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  borderViewDisable: {
    borderColor: '#95989A',
    backgroundColor:'#eee',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  disabletextArea: {
    minHeight: 110,
    flexGrow: 4,
    fontSize: 14,
    borderWidth: 0,
    borderRadius: 8,
    color: '#c0c0c0',
    textAlignVertical: 'top'
  },
  viewMode: {
    minHeight: 110,
    flexGrow: 4,
    fontSize: 14,
    borderWidth: 0,
    borderRadius: 8,
    textAlignVertical: 'top',
    color: '#606060',
    paddingTop: 5,
    paddingBottom: 5,
  },
  DisableInput: {
    minHeight: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#c0c0c0',
    paddingTop: 5,
    paddingBottom: 5,
  },
  DisableInputNoText: {
    minHeight: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#c0c0c0',
    paddingTop: 5,
    paddingBottom: 5,
  },
});

export default DFTextArea;
