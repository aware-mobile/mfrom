import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Picker,
  Platform
} from 'react-native';

import DFService from '../Service/DFService';
import apiConfig from '../Util/Config';
import STRINGS from '../Util/strings'
import AppText from '../Util/AppText'
import AddressUtil from '../Util/AddressUtil';

let PickerItem = Picker.Item;

class DFAddress extends Component {

  constructor(props) {
    super(props);
    //console.log(props);
    let read_only = false;
    if ("readOnly" in this.props) {
      read_only = this.props.readOnly
    }

    let city = STRINGS.USER_DATA.PROVINCE;
    if ("city" in this.props) {
      city = STRINGS.USER_DATA.CITY;
    }

    let properties = null;
    if ("properties" in this.props) {
      properties = this.props.properties;

      if (properties.province.value === undefined || properties.province.value === null) {
        properties.province.value = '';
      }
      if (properties.district.value === undefined || properties.district.value === null) {
        properties.district.value = '';
      }
      if (properties.subdistrict.value === undefined || properties.subdistrict.value === null) {
        properties.subdistrict.value = '';
      }
      //console.log(properties);
    }

    let select_province = {province_code: '', province: properties.province.placeholder};
    let province_list = AddressUtil.getProvinces();
    if (province_list[0].province_code !== '') {
      province_list.unshift(select_province);
    }

    let tempProvince = this.props.properties.province.value;
    let selectedProvinceLabel = properties.province.placeholder;
    this.select_district = {amphur_code: '', amphur: properties.district.placeholder};
    let district_list = [];
    if (tempProvince !== '') {
      let selectedObj = AddressUtil.findProvince(tempProvince);
      selectedProvinceLabel = selectedObj.province;
      console.log(selectedProvinceLabel);
      district_list = AddressUtil.getDistricts(tempProvince)
    }
    district_list.unshift(this.select_district);

    let tempDistrict = this.props.properties.district.value;
    let selectedDistrictLabel = properties.district.placeholder;
    this.select_subdistrict = {district_code: '', district: properties.subdistrict.placeholder}
    let sub_district_list = []
    if (tempDistrict !== '') {
      let selectedObj = AddressUtil.findDistrict(tempDistrict)
      selectedDistrictLabel = selectedObj.amphur
      sub_district_list = AddressUtil.getSubDistricts(tempDistrict)
    }
    sub_district_list.unshift(this.select_subdistrict)

    let tempSubDistrict = this.props.properties.subdistrict.value
    let selectedSubDistrictLabel = properties.subdistrict.placeholder
    if (tempSubDistrict !== '') {
      let selectedObj = AddressUtil.findSubDistrict(tempSubDistrict)
      selectedSubDistrictLabel = selectedObj.district
    }

    // this.initProvince = [{province_code: '', province: properties.province.placeholder}];
    // this.initDistrict = [{amphur_code: '', amphur: properties.district.placeholder}];
    // this.initSubdistricte = [{district_code: '', district: properties.subdistrict.placeholder}];

    this.state = {
      provinces: province_list,
      district: district_list,
      subdistrict: sub_district_list,
      properties: properties,
      validationError: '',
      maxLength: this.props.maxLength,
      showProvince: false,
      showDistrict: false,
      showSubdistrict: false,
      selectedProvinceLabel,
      selectedDistrictLabel,
      selectedSubDistrictLabel,
      readOnly: read_only,
      tempProvince,
      tempDistrict,
      tempSubDistrict,
      enableDistrict: tempDistrict !== "",
      enableSubDistrict: tempSubDistrict !== ""
    };

  }

  // getProvince() {
  //   var self = this;
  //   DFService.callApiWithHeader(null, apiConfig.getProvince, function (error, response) {

  //     if (response) {
  //       if (response.resCode == 'DF2000000') {
  //         // Successfull Response
  //         self.state.provinces = self.initProvince;
  //         self.setState({
  //           provinces: self.state.provinces.concat(response.data),
  //           enableDistrict: false,
  //           enableSubDistrict: false,
  //         });

  //         if (self.state.tempProvince != '') {
  //           self.provinceChanged(self.state.tempProvince);
  //           self.state.tempProvince = '';
  //         }

  //         if (self.state.tempDistrict != '') {
  //           self.districtChanged(self.state.tempDistrict);
  //           self.state.tempDistrict = '';
  //         }
  //       } else {
  //         // Failed Response
  //         self.props.openMainModal(response.resMessage)
  //       }
  //     } else {
  //       // Error from application calling service
  //       console.log("error = ", error);
  //     }

  //   });
  // }

  // getDistrict(code) {
  //   var self = this;
  //   var config = {
  //     url: apiConfig.getDistrict.url + code,
  //     method: apiConfig.getDistrict.method
  //   };

  //   DFService.callApiWithHeader(null, config, function (error, response) {

  //     if (response) {
  //       if (response.resCode == 'DF2000000') {
  //         // Successfull Response
  //         self.state.subdistrict = self.initSubdistricte;
  //         self.setState({
  //           subdistrict: self.state.subdistrict.concat(response.data),
  //           enableDistrict: true,
  //           enableSubDistrict: true,
  //         });

  //         if (self.state.tempSubDistrict != '') {
  //           self.subDistrictChanged(self.state.tempSubDistrict);
  //           self.state.tempSubDistrict = '';
  //         }
  //       } else {
  //         // Failed
  //         self.props.openMainModal(response.resMessage)
  //       }
  //     } else {
  //       // Error from application calling service
  //       console.log("error = ", error);
  //     }

  //   });
  // }

  provinceChanged(code) {
    if (code === '') {

      this.setState({
        enableDistrict: false,
        enableSubDistrict: false,
        district: [this.select_district],
        subdistrict: [this.select_subdistrict],
      })

      this.state.properties.province.value = code;
      this.state.properties.district.value = '';
      this.state.properties.subdistrict.value = ''

      this.valueChanged()

      return;
    }

    this.setState({showDistrict: false, showSubdistrict: false});
    console.log("provinceChanged Code: ", code);

    let district_list = AddressUtil.getDistricts(code)
    district_list.unshift(this.select_district)
    let selectedProviceObj = AddressUtil.findProvince(code)

    this.setState({
      district: district_list,
      subdistrict: [this.select_subdistrict],
      enableDistrict: true,
      enableSubDistrict: false,
      selectedProvinceLabel: selectedProviceObj.province,
      selectedDistrictLabel: this.state.properties.district.placeholder,
      selectedSubDistrictLabel: this.state.properties.subdistrict.placeholder,
    });
    this.state.properties.province.value = code;
    this.state.properties.district.value = '';
    this.state.properties.subdistrict.value = ''

    this.valueChanged()
  }

  districtChanged(code) {

    if (code === '') {

      this.setState({
        enableSubDistrict: false,
        subdistrict: [this.select_subdistrict],
      })

      this.state.properties.district.value = code;
      this.state.properties.subdistrict.value = '';
      this.valueChanged()

      return
    }

    this.setState({showSubdistrict: false});
    console.log("districtChanged", code);

    let sub_district_list = AddressUtil.getSubDistricts(code)
    sub_district_list.unshift(this.select_subdistrict)
    let selectedDistrictObj = AddressUtil.findDistrict(code)

    this.setState({
      subdistrict: sub_district_list,
      enableDistrict: true,
      enableSubDistrict: true,
      selectedDistrictLabel: selectedDistrictObj.amphur,
      selectedSubDistrictLabel: this.state.properties.subdistrict.placeholder,
    });
    this.state.properties.district.value = code;
    this.state.properties.subdistrict.value = '';

    this.valueChanged()
  }

  subDistrictChanged(code) {

    if (code === '') {
      this.state.properties.subdistrict.value = code;
      this.valueChanged()
      return
    }

    console.log("subDistrictChanged", code);

    let selectedSubDistrictObj = AddressUtil.findSubDistrict(code)

    this.setState({
      selectedSubDistrictLabel: selectedSubDistrictObj.district
    });
    this.state.properties.subdistrict.value = code;

    this.valueChanged()
  }

  addressChanged(value) {
    this.state.properties.address.value = value;
    this.valueChanged()
  }

  valueChanged() {

    let tmp_properties = JSON.parse(JSON.stringify(this.state.properties));
    tmp_properties.district.value = "" + tmp_properties.district.value;
    tmp_properties.province.value = "" + tmp_properties.province.value;
    tmp_properties.subdistrict.value = "" + tmp_properties.subdistrict.value;

    this.props.insertData(tmp_properties, this.props.pageIndex, this.props.fieldIndex);
  }

  render() {
    var output = !this.props.editable ?

      <TextInput multiline={true}
                 returnKeyType={"default"}
                 style={styles.disabletextArea}
                 value={this.state.properties.address.value}
                 placeholder={this.state.properties.address.placeholder}
                 maxLength={this.state.maxLength}
                 editable={false}
                 underlineColorAndroid='rgba(0,0,0,0)'
      /> :
      <TextInput multiline={true}
                 returnKeyType={"default"}
                 style={styles.textArea}
                 value={this.state.properties.address.value}
                 placeholder={this.state.properties.address.placeholder}
                 maxLength={this.state.maxLength}
                 editable={this.props.editable}
                 onChangeText={(text) => {
                   this.addressChanged(text);
                 }}
                 underlineColorAndroid='rgba(0,0,0,0)'
      />;

    if (this.props.viewMode) {
      output = <AppText
        style={styles.viewModeAddress}>{this.state.properties.address.value === '' ? STRINGS.COMMON.NO_ADDRESS_ENTERED : this.state.properties.address.value}</AppText>
    }

    var styleView = this.props.editable ? styles.dropdownBtn : styles.dropdownBtnDisable;

    var provinceView = <View style={styleView}>
      <Picker
        style={styles.picker}
        itemStyle={styles.dropdownBtnTxt}
        enabled={this.props.editable}
        selectedValue={parseInt(this.state.properties.province.value)}
        onValueChange={this.provinceChanged.bind(this)}>

        {Object.keys(this.state.provinces).map((key) => (
          <PickerItem
            key={key}
            style={styles.dropdownBtnTxt}
            value={this.state.provinces[key].province_code}
            label={this.state.provinces[key].province}
          />
        ))}

      </Picker>
    </View>;
    if (this.props.viewMode) {
      provinceView = <View style={styles.borderViewDisable}>
        <AppText
          style={styles.viewMode}>{this.state.properties.province.value === '' ? STRINGS.COMMON.NO_PROVINCE_SELECTED : this.state.selectedProvinceLabel}</AppText>
      </View>
    }
    var enableDistrict = this.props.editable && this.state.enableDistrict;
    var districtView = <View style={styleView}>
      <Picker
        style={styles.picker}
        itemStyle={styles.dropdownBtnTxt}
        enabled={enableDistrict}
        selectedValue={parseInt(this.state.properties.district.value)}
        onValueChange={this.districtChanged.bind(this)}>
        {Object.keys(this.state.district).map((key) => (
          <PickerItem
            key={key}
            style={styles.dropdownBtnTxt}
            value={this.state.district[key].amphur_code}
            label={this.state.district[key].amphur}
          />
        ))}
      </Picker>
    </View>;
    if (this.props.viewMode) {
      districtView = <View style={styles.borderViewDisable}>
        <AppText
          style={styles.viewMode}>{this.state.properties.district.value === '' ? STRINGS.COMMON.NO_DISTRICT_SELECTED : this.state.selectedDistrictLabel}</AppText>
      </View>
    }
    var enableSubDistrict = this.props.editable && this.state.enableSubDistrict;
    var subDistrictView = <View style={styleView}>
      <Picker
        style={styles.picker}
        itemStyle={styles.dropdownBtnTxt}
        enabled={enableSubDistrict}
        selectedValue={parseInt(this.state.properties.subdistrict.value)}
        onValueChange={this.subDistrictChanged.bind(this)}>
        {Object.keys(this.state.subdistrict).map((key) => (
          <PickerItem
            key={key}
            style={styles.dropdownBtnTxt}
            value={this.state.subdistrict[key].district_code}
            label={this.state.subdistrict[key].district}
          />
        ))}

      </Picker>
    </View>;
    if (this.props.viewMode) {
      subDistrictView = <View style={styles.borderViewDisable}>
        <AppText
          style={styles.viewMode}>{this.state.properties.subdistrict.value === '' ? STRINGS.COMMON.NO_SUB_DISTRICT_SELECTED : this.state.selectedSubDistrictLabel}</AppText>
      </View>
    }
    return (
      <View>
        <View style={(this.state.readOnly || !this.props.editable) ? styles.borderViewDisable : styles.borderView}>
          {output}
        </View>
        <AppText style={styles.label}>
          {this.state.properties.province.name}
        </AppText>
        {provinceView}
        <AppText style={styles.label}>
          {this.state.properties.district.name}
        </AppText>
        {districtView}
        <AppText style={styles.label}>
          {this.state.properties.subdistrict.name}
        </AppText>
        {subDistrictView}
      </View>
    );
  }

}

const styles = StyleSheet.create({
  textArea: {
    height: 80,
    flexGrow: 4,
    fontSize: 20,
    borderWidth: 0,
    borderRadius: 8,
    color: '#333435',
    textAlignVertical: 'top',
    paddingLeft: 5,
    paddingRight: 5
  },
  borderView: {
    borderColor: '#95989A',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 20,
    marginLeft: 20,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  borderViewDisable: {
    borderColor: '#95989A',
    backgroundColor: '#eee',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 20,
    marginLeft: 20,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  disabletextArea: {
    height: 110,
    flexGrow: 4,
    fontSize: 20,
    borderWidth: 0,
    borderRadius: 8,
    color: '#cdcdcd',
    textAlignVertical: 'top'
  },
  container: {
    flexGrow: 1
  },
  dropdownBtn: {
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    marginTop: 3,
    marginRight: 20,
    marginLeft: 20,
    borderRadius: 8,
  },
  dropdownBtnDisable: {
    backgroundColor: '#cdcdcd',
    borderColor: '#cdcdcd',
    marginTop: 3,
    marginRight: 20,
    marginLeft: 20,
    borderRadius: 8,
  },
  dropdownBtnContainer: {
    flexGrow: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  dropdownBtnTxt: {
    fontSize: 10,
  },
  picker: {
    backgroundColor: 'transparent',
    height: 40,
    color: 'black',
  },
  label: {
    marginLeft: 20,
    marginRight: 2,
    fontSize: 14,
    color: 'black'
  },
  viewModeAddress: {
    minHeight: 30,
    fontSize: 14,
    borderWidth: 0,
    borderRadius: 8,
    textAlignVertical: 'top',
    color: '#606060',
    paddingTop: 5,
    paddingBottom: 5
  },
  viewMode: {
    minHeight: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#606060',
    textAlignVertical: 'center',
    paddingTop: 5,
    paddingBottom: 5
  },
});

export default DFAddress;
