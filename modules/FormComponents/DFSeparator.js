import React, { Component } from 'react';
import {
  StyleSheet,
  View
} from 'react-native';

class DFSeparator extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }
  render() {
    return (
      <View style={styles.separator} />
    )
  }
}

const styles = StyleSheet.create({
  separator: {
    height: 1,
    backgroundColor: '#8e8e8e',
    margin: 5
  }
})

export default DFSeparator;
