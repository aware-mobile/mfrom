import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Platform,
  Image,
  CameraRoll,
  TouchableHighlight,
  NativeModules,
  Linking,
  Alert
} from 'react-native';

import Device from '../Util/Util'
import AppText from '../Util/AppText'
import config from '../Util/Config'

var Ionicons = require('react-native-vector-icons/Ionicons');
var Icon = require('react-native-vector-icons/FontAwesome');
import ImagePicker from 'react-native-image-picker';
import STRINGS from '../Util/strings'

import RNFetchBlob from 'react-native-fetch-blob';
import FileOpenIOS from 'react-native-open-file';

const extToMimes = {
  'IMG': 'image/jpeg',
  'JPG': 'image/jpeg',
  'JPEG': 'image/jpeg',
  'PNG': 'image/png',
  'DOC': 'application/msword',
  'DOCX': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  'PDF': 'application/pdf',
  'TXT': 'text/plain',
  'XLS': 'application/vnd.ms-excel',
  'XLSX': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  'CSV': 'application/vnd.ms-excel',
  'PPSX': 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
  'PPT': 'application/vnd.ms-powerpoint',
  'PPTX': 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
  'XML': 'text/xml',
  'BMP': 'image/bmp',
  'GIF': 'image/gif'
}

class DFFile extends Component {

  constructor(props) {
    super(props);
    //console.log(props);
    let filePath = null;
    let shortName = null;
    let isDownload = false;
    if ("valueUri" in this.props) {
      if (this.props.valueUri !== '') {
        let res = this.props.valueUri.split("/");
        filePath = {uri: config.upload_url + this.props.valueUri, name: res[res.length - 1]};
        isDownload = true
      }
    }

    // if ("value" in this.props) {
    //   if (this.props.value !== undefined && this.props.value !== '') {
    //     let res = this.props.value.split("/");
    //     filePath = {uri: config.upload_url + this.props.value, name: res[res.length - 1]};
    //     isDownload = true
    //   }
    // }

    if (filePath !== null) {
      shortName = this.setShortName(decodeURIComponent(filePath.name))
    }

    this.state = {
      filePath: filePath,
      shortName,
      isDownload: isDownload,
      downloadedFile: ''
    };
  }

  setShortName(fileName) {
    let shortName = null
    let char_count = 20
    if (Device.isPhone()) {
      char_count = 35
    }

    if (fileName.length > char_count) {
      let newShortName = fileName.substring(fileName.length - char_count, fileName.length)
      newShortName = '...' + newShortName;
      return newShortName
    } else {
      return fileName
    }
  }

  updateUrl(file_url) {
    let new_url = decodeURIComponent(file_url);
    let shortName = this.setShortName(new_url);
    this.setState({shortName, isDownload: true})
  }

  pickFile() {

    var options = {
      title: STRINGS.IMAGE_PICKER.TITLE_SELECT_FILE, // specify null or empty string to remove the title
      cancelButtonTitle: STRINGS.IMAGE_PICKER.CANCEL,
      takePhotoButtonTitle: null, // specify null or empty string to remove this button
      chooseFromLibraryButtonTitle: STRINGS.IMAGE_PICKER.LBL_PHOTO_LIBRARY, // specify null or empty string to remove this button
      cameraType: 'back', // 'front' or 'back'
      mediaType: 'mixed', // 'photo' or 'video'
      // maxWidth: 1000, // photos only
      // maxHeight: 1000, // photos only
      quality: 1, // 0 to 1, photos only
      angle: 0, // android only, photos only
      allowsEditing: false, // Built in functionality to resize/reposition the image after selection
      noData: false, // photos only - disables the base64 `data` field from being generated (greatly improves performance on large photos)
      storageOptions: { // if this key is provided, the image will get saved in the documents directory on ios, and the pictures directory on android (rather than a temporary directory)
        skipBackup: true, // ios only - image will NOT be backed up to icloud
        path: 'images' // ios only - will save image at /Documents/images rather than the root
      }
    }

    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);

        Linking.canOpenURL('app-settings:')
          .then(supported => {
              if (supported) {
                  Alert.alert('', STRINGS.COMMON.OPEN_PHOTOS_SERVICES,
                      [
                          { text: 'Cancel', onPress: () => "", style: 'cancel' },
                          { text: 'OK', onPress: () => { 
                            
                            Linking.openURL('app-settings:') 
                          }},
                      ],
                      { cancelable: false }
                  );
              } else {
                  Alert.alert('', STRINGS.COMMON.OPEN_PHOTOS_SERVICES_MANUALLY,
                      [
                          { text: 'OK' },
                      ],
                      { cancelable: false }
                  );
              }
          });

      } else {
        // You can display the image using either data...
        // const source = {uri: 'data:image/jpeg;base64,' + response.data, isStatic: true};

        // or a reference to the platform specific asset location
        var mimeType
        var fileType
        if ((response.uri.indexOf('.jpg') != -1) || (response.uri.indexOf('.JPG') != -1)) {
          mimeType = 'image/jpeg'
          fileType = 'jpg'
        }
        if ((response.uri.indexOf('.png') != -1) || (response.uri.indexOf('.PNG') != -1)) {
          mimeType = 'image/png'
          fileType = 'png'
        }
        if ((response.uri.indexOf('.gif') != -1) || (response.uri.indexOf('.GIF') != -1)) {
          mimeType = 'image/gif'
          fileType = 'gif'
        }
        if ((response.uri.indexOf('.MOV') != -1) || (response.uri.indexOf('.mov') != -1)) {
          mimeType = 'video/quicktime'
          fileType = 'mov'
        }
        if ((response.uri.indexOf('.mp4') != -1) || (response.uri.indexOf('.mpeg4') != -1) || (response.uri.indexOf('.mpeg') != -1) || (response.uri.indexOf('.MPEG4') != -1) || (response.uri.indexOf('.MP4') != -1)) {
          mimeType = 'video/mpeg'
          fileType = 'mp4'
        }

        var filename = response.uri.substring(response.uri.lastIndexOf("/") + 1, response.uri.length);
        console.log('typeFile : ', mimeType)
        var source = {
          uri: response.uri,
          isStatic: true,
          name: filename,
          type: mimeType,
          fileType: fileType,
          size: response.fileSize,
          pageIndex: this.props.pageIndex,
          uploadType: 'file'
        };

        let shortName = this.setShortName(source.name)

        this.setState({
          filePath: source,
          shortName,
          downloadedFile: response.uri
        });

        this.props.insertData(this.state.filePath, this.props.pageIndex, this.props.fieldIndex)
      }
    });
  }

  render() {

    var btnFile = this.props.editable ? <TouchableHighlight
      style={styles.locationBtn}
      underlayColor='#9FB159'
      onPress={() => this.pickFile()}>
      <View style={styles.locationBtnContainer}>
        <View style={{width: 20, height: 20}}>
        </View>
        <View>
          <AppText style={styles.labelBtn}>{STRINGS.IMAGE_PICKER.UPLOADE_A_FILE}</AppText>
        </View>
        <View>
          <Icon
            name={'file'}
            size={20}
            color='white'
            style={{width: 20, height: 20}}
          />
        </View>
      </View>
    </TouchableHighlight> : <TouchableHighlight
      style={styles.locationBtnDisable}>
      <View style={styles.locationBtnContainer}>
        <View style={{width: 20, height: 20}}>
        </View>
        <View>
          <AppText style={styles.labelBtn}>{STRINGS.IMAGE_PICKER.UPLOADE_A_FILE}</AppText>
        </View>
        <View>
          <Icon
            name={'file'}
            size={20}
            color='white'
            style={{width: 20, height: 20}}
          />
        </View>
      </View>
    </TouchableHighlight>;
    if (this.props.viewMode) {
      btnFile = null;
    }

    var lbInfo = this.props.editable&& !this.props.viewMode? <AppText style={styles.labelInfo}>{STRINGS.IMAGE_PICKER.FILE_SELECT_INFO}</AppText>:null;

    var btnRemove = this.state.filePath !== null && this.props.editable ? <TouchableHighlight
      underlayColor='transparent'
      onPress={() => this.removeFile()}
      style={{alignItems: 'flex-end'}}>
      <Ionicons name="md-close" size={25} color="#B2D234"
                style={{justifyContent: 'center', alignItems: 'center', marginRight: 5}}/>
    </TouchableHighlight> : null;

    var btnDownload = this.state.isDownload ? <TouchableHighlight
      onPress={() => this.downloadFile()}
      underlayColor='transparent'
      style={{alignItems: 'flex-end', backgroundColor: 'transparent'}}>
      <Ionicons name="md-download" size={30} color="#B2D234"
                style={{justifyContent: 'center', alignItems: 'center', marginRight: 5, marginLeft: 5}}/>
    </TouchableHighlight> : null;

    var renderView = <View style={{marginLeft: 10, marginRight: 10}}>
      <View style={{marginBottom: 5}}>
        {btnRemove}
        {this.state.filePath !== null ?
          <View style={{
            borderWidth: 2,
            marginLeft: 20,
            marginRight: 20,
            padding: 5,
            flexDirection: 'row',
            borderStyle: 'dashed',
            borderColor: '#c0c0c0',
            alignItems: 'center',
          }}>
            <AppText style={styles.label}>{this.state.shortName}</AppText>
            {btnDownload}
          </View> : null}

      </View>
      <View>
        {btnFile}
      </View>
      <View style={{marginBottom: 5}}>
        {lbInfo}
      </View>
    </View>

    if (this.props.viewMode && this.state.filePath == null) {
      renderView = <View
        style={styles.borderView}>
        <AppText style={styles.viewMode}>{STRINGS.IMAGE_PICKER.NO_FILE_UPLOADED}</AppText>
      </View>;
    }

    return (renderView);
  }

  removeFile() {
    this.setState({
      filePath: null,
      isDownload: false
    });
    this.props.removeData(this.props.pageIndex, this.props.fieldIndex);
  }

  downloadFile() {
    let self = this

    console.log('downloadedFile: ', this.state.downloadedFile)

    if (this.state.downloadedFile !== '') {
      console.log('Downloaded File already: ', this.state.downloadedFile)
      FileOpenIOS.open(this.state.downloadedFile);
    } else {
      this.props.showSpinner(true)
      console.log('File Path', this.state.filePath.uri)
      let dl_file = this.state.filePath.uri.split('/')
      let dl_file_name = dl_file[dl_file.length - 1]
      let file_ext_arr = dl_file_name.split('.')
      let file_ext = file_ext_arr[file_ext_arr.length - 1]
      let dl_file_mime = this.getMimeByExt(file_ext.toUpperCase())
      if (dl_file_mime) {
        // let dirs = RNFetchBlob.fs.dirs
        let encodedURI = encodeURI(this.state.filePath.uri)
        console.log('send uri: ', encodedURI)

        RNFetchBlob
          .config({
            fileCache: true,
            appendExt: file_ext
          })
          .fetch('GET', encodedURI, {
            //some headers ..
          })
          .then((res) => {
            // the path should be dirs.DocumentDir + 'path-to-file.anything'
            console.log('The file saved to ', res.path())
            self.setState({downloadedFile: res.path()})
            this.props.showSpinner(false)
            FileOpenIOS.open(res.path());

          })
      } else {
        console.log('DF-ERROR: Unknown Mime Type for extension', file_ext[file_ext.length - 1])
        alert(STRINGS.VALIDATION_MESSAGES.UNKNOWN_FILE_TYPE)
      }

    }
  }

  getMimeByExt(ext) {
    if (extToMimes.hasOwnProperty(ext)) {
      return extToMimes[ext];
    }
    return false;
  }

}

const styles = StyleSheet.create({
  imgView: {
    height: 200,
    margin: 5,
  },
  label: {
    marginLeft: 10,
    marginRight: 5,
    marginBottom: 3,
    fontSize: 12,
    color: 'black'
  },
  labelInfo: {
    marginLeft: 10,
    marginRight: 5,
    marginBottom: 3,
    fontSize: 12,
    color: 'grey',
    fontWeight: 'bold'
  },
  labelBtn: {
    marginLeft: 10,
    marginRight: 5,
    marginBottom: 3,
    fontSize: 14,
    color: 'white'
  },
  locationBtn: {
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    marginRight: 10,
    marginLeft: 10,
    padding: 10,
    borderRadius: 8,
  },
  locationBtnDisable: {
    backgroundColor: '#cdcdcd',
    borderColor: '#cdcdcd',
    marginRight: 10,
    marginLeft: 10,
    padding: 10,
    borderRadius: 8,
  },
  locationBtnContainer: {
    flexGrow: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  borderView: {
    borderColor: '#cdcdcd',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  viewMode: {
    minHeight: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#606060',
    textAlignVertical: 'center',
    paddingTop: 5,
    paddingBottom: 5
  },
});

export default DFFile;
