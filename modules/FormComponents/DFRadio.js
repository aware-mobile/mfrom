import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TouchableHighlight,
  Dimensions
} from 'react-native';
import AppText from '../Util/AppText'

var {height, width} = Dimensions.get('window');

import DFLabel from './DFLabel'
// import ItemCheckbox from './custom/ItemCheckbox'

var Ionicons = require('react-native-vector-icons/Ionicons');

class DFRadio extends Component {
  constructor(props) {
    super(props);

    var colorDisable = (this.props.editable) ? '#B2D234' : '#cdcdcd'
    var colorLabelDisable = (this.props.editable) ? '#000000' : '#cdcdcd'
    if (this.props.viewMode) {
      colorLabelDisable = '#606060'
    }
    this.state = {
      clickedIndex: -1,
      colorDisable: colorDisable,
      colorLabelDisable: colorLabelDisable,
      valFromFormPage: this.props.options
    };

  }

  render() {

    var optionList = [];
    var options = this.state.valFromFormPage
    for (var i = 0; i < options.length; i++) {
      let item = options[i]
      let isChecked = false
      if (item.checked) {
        this.state.clickedIndex = i
        isChecked = true
      }

      let icon = (isChecked) ?
        <Ionicons style={styles.icon} name="ios-radio-button-on-outline" size={35} color={this.state.colorDisable}/> :
        <Ionicons style={styles.icon} name="ios-radio-button-off-outline" size={35} color={this.state.colorDisable}/>

      let output = (this.props.editable) ?
        <TouchableHighlight
          key={i + "-DFCheckBox-" + isChecked}
          onPress={this.checkItem.bind(this, i)}
          underlayColor="transparent">
          <View style={[styles.checkBoxContainer, this.setContainerWidth(20)]}>
            {icon}
            <AppText style={[styles.label, {color: this.state.colorLabelDisable}, this.setContainerWidth(65)]}>
              {item.label}
            </AppText>
          </View>
        </TouchableHighlight>
        :
        <View style={[styles.checkBoxContainer, this.setContainerWidth(20)]} key={i + "-DFCheckBox-" + isChecked}>
          {icon}
          <AppText style={[styles.label, {color: this.state.colorLabelDisable}, this.setContainerWidth(65)]}>
            {item.label}
          </AppText>
        </View>

      optionList.push(output)

    }

    return (
      <View style={{marginLeft: 10, marginRight: 10}}>
        {optionList}
      </View>
    )
  }

  checkItem(index) {
    // console.log('Check Item: ', index)
    var options = this.state.valFromFormPage
    let currentIndex = this.state.clickedIndex

    if (currentIndex != index) {
      if (currentIndex !== -1) {
        options[currentIndex].checked = false
      }
      options[index].checked = true
      this.setState({
        valFromFormPage: options,
        clickedIndex: index
      })
      this.props.insertData(this.state.valFromFormPage, this.props.pageIndex, this.props.fieldIndex);
    }
  }

  setContainerWidth(padding) {
    let containerWidth = width;
    if (this.props.isTwoColumns) {
      containerWidth = containerWidth / 2 - 20;
    }
    return {
      width: containerWidth - padding
    }
  }

}

const styles = StyleSheet.create({
  checkBoxContainer: {
    flexDirection: 'row',
    paddingTop: 0,
    paddingBottom: 0,
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 10,
    alignItems: 'center'
  },
  icon: {
    alignSelf: 'center'
  },
  label: {
    marginLeft: 10,
    fontSize: 14
  }
});

export default DFRadio;
