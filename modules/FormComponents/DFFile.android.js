import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Platform,
  Image,
  CameraRoll,
  TouchableHighlight,
  NativeModules,
  PermissionsAndroid
} from 'react-native';

import Device from '../Util/Util'
import config from '../Util/Config'
import STRINGS from '../Util/strings'
import AppText from '../Util/AppText'

var Ionicons = require('react-native-vector-icons/Ionicons');
var Icon = require('react-native-vector-icons/FontAwesome');
var NativeAwareAPI = NativeModules.RNAwareModule;

const extToMimes = {
  'IMG': 'image/jpeg',
  'JPG': 'image/jpeg',
  'JPEG': 'image/jpeg',
  'PNG': 'image/png',
  'DOC': 'application/msword',
  'DOCX': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  'PDF': 'application/pdf',
  'TXT': 'text/plain',
  'XLS': 'application/vnd.ms-excel',
  'XLSX': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  'CSV': 'application/vnd.ms-excel',
  'PPSX': 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
  'PPT': 'application/vnd.ms-powerpoint',
  'PPTX': 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
  'XML': 'text/xml',
  'BMP': 'image/bmp',
  'GIF': 'image/gif'
};

class DFFile extends Component {

  constructor(props) {
    super(props);
    //console.log(props);
    let filePath = null;
    let shortName = null;
    let isDownload = false;
    if ("valueUri" in this.props) {
      if (this.props.valueUri !== '') {
        let res = this.props.valueUri.split("/");
        filePath = { uri: config.upload_url + this.props.valueUri, name: res[res.length - 1] };
        isDownload = true
      }
    }

    // if ("value" in this.props) {
    //   if (this.props.value !== undefined && this.props.value !== '') {
    //     let res = this.props.value.split("/");
    //     filePath = {uri: config.upload_url + this.props.value, name: res[res.length - 1]};
    //     isDownload = true
    //   }
    // }

    if (filePath !== null) {
      shortName = this.setShortName(decodeURIComponent(filePath.name))
    }

    this.state = {
      filePath: filePath,
      shortName,
      isDownload: isDownload,
      downloadedFile: ''
    };
  }

  setShortName(fileName) {
    let shortName = null;
    let char_count = 20;
    if (Device.isPhone()) {
      char_count = 35
    }

    if (fileName.length > char_count) {
      let newShortName = fileName.substring(fileName.length - char_count, fileName.length)
      newShortName = '...' + newShortName;
      return newShortName
    } else {
      return fileName
    }
  }

  updateUrl(file_url) {
    let new_url = decodeURIComponent(file_url);
    let shortName = this.setShortName(new_url);
    let res = file_url.split("/");
    let filePath = { uri: config.upload_url + file_url, name: res[res.length - 1] };
    this.setState({ shortName, filePath, isDownload: true })
  }

  tryPickFile() {
    let self = this;
    PermissionsAndroid.checkPermission(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
      .then(function (granted) {
        if (granted) {
          self.pickFileWithPermission();
        } else {
          PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
            .then(function (userGranted) {
              if (userGranted) {
                self.pickFileWithPermission();
              }
            });
        }
      });
  }

  pickFileWithPermission() {
    NativeAwareAPI.browseFile((response) => {
      if (response.fileName) {
        let file_ext_arr = response.fileName.split('.');
        let file_ext = file_ext_arr[file_ext_arr.length - 1];
        let dl_file_mime = this.getMimeByExt(file_ext.toUpperCase());

        const source = {
          uri: response.path,
          isStatic: true,
          name: response.fileName,
          type: dl_file_mime,
          fileType: file_ext,
          size: response.fileSize,
          pageIndex: this.props.pageIndex,
          uploadType: 'file'
        };

        let shortName = this.setShortName(source.name);

        this.setState({
          filePath: source,
          shortName
        });

        this.props.insertData(this.state.filePath, this.props.pageIndex, this.props.fieldIndex)
      } else {
        this.props.insertData(null, this.props.pageIndex, this.props.fieldIndex)
      }
    });
  }

  render() {

    let btnFile = this.props.editable ? <TouchableHighlight
      style={styles.locationBtn}
      underlayColor='#9FB159'
      onPress={() => this.tryPickFile()}>
      <View style={styles.locationBtnContainer}>
        <View style={{ width: 20, height: 20 }}>
        </View>
        <View>
          <AppText style={styles.labelBtn}>{STRINGS.IMAGE_PICKER.UPLOADE_A_FILE}</AppText>
        </View>
        <View>
          <Icon
            name={'file'}
            size={20}
            color='white'
            style={{ width: 20, height: 20 }}
          />
        </View>
      </View>
    </TouchableHighlight> : <TouchableHighlight
      style={styles.locationBtnDisable}>
        <View style={styles.locationBtnContainer}>
          <View style={{ width: 20, height: 20 }}>
          </View>
          <View>
            <AppText style={styles.labelBtn}>{STRINGS.IMAGE_PICKER.UPLOADE_A_FILE}</AppText>
          </View>
          <View>
            <Icon
              name={'file'}
              size={20}
              color='white'
              style={{ width: 20, height: 20 }}
            />
          </View>
        </View>
      </TouchableHighlight>;
    if (this.props.viewMode) {
      btnFile = null;
    }

    let btnRemove = this.state.filePath !== null && this.props.editable ? <TouchableHighlight
      underlayColor='transparent'
      onPress={() => this.removeFile()}
      style={{ alignItems: 'flex-end' }}>
      <Ionicons name="md-close" size={25} color="#B2D234"
        style={{ justifyContent: 'center', alignItems: 'center', marginRight: 5 }} />
    </TouchableHighlight> : null;

    let btnDownload = this.state.isDownload ? <TouchableHighlight
      onPress={() => this.checkPermissionFile()}
      style={{ alignItems: 'flex-end' }}>
      <Ionicons name="md-download" size={30} color="#B2D234"
        style={{ justifyContent: 'center', alignItems: 'center', marginRight: 5, marginLeft: 5 }} />
    </TouchableHighlight> : null;

    let renderView = <View style={{ marginLeft: 10, marginRight: 10 }}>
      <View style={{ marginBottom: 5 }}>
        {btnRemove}
        {this.state.filePath !== null ?
          <View style={{
            borderWidth: 2,
            marginLeft: 20,
            marginRight: 20,
            padding: 5,
            flexDirection: 'row',
            borderStyle: 'dashed',
            borderColor: '#c0c0c0',
            alignItems: 'center',
          }}>
            <AppText style={styles.label}>{this.state.shortName}</AppText>
            {btnDownload}
          </View> : null}

      </View>
      <View style={{ marginBottom: 5 }}>
        {btnFile}
      </View>
    </View>;

    if (this.props.viewMode && this.state.filePath === null) {
      renderView = <View
        style={styles.borderView}>
        <AppText style={styles.viewMode}>{'No File Uploaded'}</AppText>
      </View>;
    }

    return (renderView);
  }

  removeFile() {
    this.setState({
      filePath: null,
      isDownload: false
    });
    this.props.removeData(this.props.pageIndex, this.props.fieldIndex);
  }


  checkPermissionFile(){
    let self = this;
    PermissionsAndroid.checkPermission(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
      .then(function (granted) {
        if (granted) {
          self.downloadFile();
        } else {
          PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
            .then(function (userGranted) {
              if (userGranted) {
                self.downloadFile();
              }
            });
        }
      });
  }

  downloadFile() {
    console.log('downloadedFile: ', this.state.downloadedFile);
    console.log('File to download: ', this.state.filePath.uri);
    let encodedURI = encodeURI(this.state.filePath.uri);
    NativeAwareAPI.downloadFile(encodedURI, this.state.filePath.name);
  }

  getMimeByExt(ext) {
    if (extToMimes.hasOwnProperty(ext)) {
      return extToMimes[ext];
    }
    return false;
  }
}

const styles = StyleSheet.create({
  imgView: {
    height: 200,
    margin: 5,
  },
  label: {
    marginLeft: 10,
    marginRight: 5,
    marginBottom: 3,
    fontSize: 12,
    color: 'black'
  },
  labelBtn: {
    marginLeft: 10,
    marginRight: 5,
    marginBottom: 3,
    fontSize: 14,
    color: 'white'
  },
  locationBtn: {
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    marginRight: 10,
    marginLeft: 10,
    padding: 10,
    borderRadius: 8,
  },
  locationBtnDisable: {
    backgroundColor: '#cdcdcd',
    borderColor: '#cdcdcd',
    marginRight: 10,
    marginLeft: 10,
    padding: 10,
    borderRadius: 8,
  },
  locationBtnContainer: {
    flexGrow: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  borderView: {
    borderColor: '#cdcdcd',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  viewMode: {
    minHeight: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#606060',
    textAlignVertical: 'center',
    paddingTop: 5,
    paddingBottom: 5
  },
});

export default DFFile;
