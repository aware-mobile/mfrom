import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TouchableHighlight
} from 'react-native';

import AppText from '../Util/AppText'

class DFButton extends Component {
  constructor(props) {
    super(props);

    let btnText = ('text' in this.props) ? this.props.text : ''
    let customStyle = ('style' in this.props) ? this.props.style : {}
    let btnFontSize = ('btnFontSize' in this.props) ? this.props.btnFontSize : 18
    let underlayColor = ('underlayColor' in this.props) ? this.props.underlayColor : '#9FB159'

    this.state = {
      text: btnText,
      customStyle: customStyle,
      btnFontSize: btnFontSize,
      underlayColor: underlayColor
    }

  }

  render() {
    return (
      <TouchableHighlight
        style={[styles.button, this.props.style]}
        underlayColor={this.props.underlayColor}
        onPress={this.props.onPress}>
        <View style={styles.view}>
          <AppText style={[styles.buttonText, {fontSize: this.state.btnFontSize}]}>{this.state.text}</AppText>
        </View>
      </TouchableHighlight>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    flexGrow: 1,
    height: 36,
    flexDirection: 'row',
    backgroundColor: '#83aa3a',
    borderColor: '#83aa3a',
    borderWidth: 1,
    borderRadius: 4,
    marginTop: 10,
    marginBottom: 10,
    marginRight: 15,
    marginLeft: 15,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  view: {
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export default DFButton;
