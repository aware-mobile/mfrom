import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Platform,
  Image,
  CameraRoll,
  TouchableHighlight,
  NativeModules
} from 'react-native';
const Timer = require('react-native-timer');
import Device from '../Util/Util'

import config from '../Util/Config'
let NativeAwareAPI = NativeModules.RNAwareModule;

class DFDownloadPDF extends Component {

  constructor(props) {
    super(props);
    this.downloadFile(this.props.fileName)
  }

  render() {
    return null
  }

  downloadFile(fileName) {
    let self = this;
    let file_name = fileName.split("/");
    let filePath = {uri: config.download_url + "/" + fileName, name: file_name[file_name.length - 1]};
    console.log("PDF Path = ", filePath.uri);
    let encodedURI = encodeURI(filePath.uri);
    Timer.setTimeout(self, 'closeLoading', () => {
      self.props.showSpinner(false);
      NativeAwareAPI.downloadFile(encodedURI, filePath.name);
    }, 500);

  }
}

export default DFDownloadPDF;
