import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  PickerIOS,
  TouchableHighlight,
  Platform
} from 'react-native';

import DFLabel from './DFLabel';
import DFService from '../Service/DFService';
import apiConfig from '../Util/Config';
var Icon = require('react-native-vector-icons/FontAwesome');
import FadeInView from '../Animation/FadeInView'
import PickerAndroid from 'react-native-picker-android';
import STRINGS from '../Util/strings'
import AppText from '../Util/AppText'
import AddressUtil from '../Util/AddressUtil';

let Picker = Platform.OS === 'ios' ? PickerIOS : PickerAndroid;
let PickerItem = Picker.Item;

class DFAddress extends Component {

  constructor(props) {
    super(props);
    //console.log(props);
    var read_only = false;
    if ("readOnly" in this.props) {
      read_only = this.props.readOnly
    }

    let city = STRINGS.USER_DATA.PROVINCE;
    if ("city" in this.props) {
      city = STRINGS.USER_DATA.CITY;
    }

    var properties = null;
    if ("properties" in this.props) {
      properties = this.props.properties;

      if(properties.province.value == undefined || properties.province.value == null ){
        properties.province.value = '';
      }if(properties.district.value == undefined || properties.district.value == null ){
        properties.district.value = '';
      }if(properties.subdistrict.value == undefined || properties.subdistrict.value == null ){
        properties.subdistrict.value = '';
      }
    }
    var tmp_properties = JSON.parse(JSON.stringify(properties));

    let province_list = AddressUtil.getProvinces()

    let tempProvince = this.props.properties.province.value
    let selectedProvinceLabel = properties.province.placeholder
    let district_list = []
    if (tempProvince != '') {
      let selectedObj = AddressUtil.findProvince(tempProvince)
      selectedProvinceLabel = selectedObj.province
      district_list = AddressUtil.getDistricts(tempProvince)
    }

    let tempDistrict = this.props.properties.district.value
    let selectedDistrictLabel = properties.district.placeholder
    let sub_district_list = []
    if (tempDistrict != '') {
      let selectedObj = AddressUtil.findDistrict(tempDistrict)
      selectedDistrictLabel = selectedObj.amphur
      sub_district_list = AddressUtil.getSubDistricts(tempDistrict)
    }

    let tempSubDistrict = this.props.properties.subdistrict.value
    let selectedSubDistrictLabel = properties.subdistrict.placeholder
    if (tempSubDistrict != '') {
      let selectedObj = AddressUtil.findSubDistrict(tempSubDistrict)
      selectedSubDistrictLabel = selectedObj.district
    }

    this.state = {
      properties: properties,
      tmp_properties,
      provinces: province_list,
      district: district_list,
      subdistrict: sub_district_list,
      validationError: '',
      maxLength: this.props.maxLength,
      showProvince: false,
      showDistrict: false,
      showSubdistrict: false,
      selectedProvinceLabel,
      selectedDistrictLabel,
      selectedSubDistrictLabel,
      readOnly: read_only,
      tempProvince,
      tempDistrict,
      tempSubDistrict
    };

  }

  provinceChanged(code) {
    this.setState({showDistrict: false, showSubdistrict: false});
    console.log("provinceChanged Code: ", code);

    let district_list = AddressUtil.getDistricts(code)
    let selectedProviceObj = AddressUtil.findProvince(code)

    this.setState({
      district: district_list,
      subdistrict: [],
      selectedProvinceLabel: selectedProviceObj.province,
      selectedDistrictLabel: this.state.properties.district.placeholder,
      selectedSubDistrictLabel: this.state.properties.subdistrict.placeholder,
    });
    this.state.properties.province.value = code;
    this.state.properties.district.value = '';
    this.state.properties.subdistrict.value = ''

    this.valueChanged()
  }

  districtChanged(code) {
    this.setState({showSubdistrict: false});
    console.log("districtChanged", code);

    let sub_district_list = AddressUtil.getSubDistricts(code)
    let selectedDistrictObj = AddressUtil.findDistrict(code)

    this.setState({
      subdistrict: sub_district_list,
      selectedDistrictLabel: selectedDistrictObj.amphur,
      selectedSubDistrictLabel: this.state.properties.subdistrict.placeholder,
    });
    this.state.properties.district.value = code;
    this.state.properties.subdistrict.value = '';

    this.valueChanged()
  }

  subDistrictChanged(code) {
    console.log("subDistrictChanged", code);

    let selectedSubDistrictObj = AddressUtil.findSubDistrict(code)

    this.setState({
      selectedSubDistrictLabel: selectedSubDistrictObj.district
    });
    this.state.properties.subdistrict.value = code;

    this.valueChanged()
  }

  addressChanged(value) {
    this.state.properties.address.value = value;
    this.valueChanged()
  }

  valueChanged() {
    let tmp_properties = JSON.parse(JSON.stringify(this.state.properties));
    tmp_properties.district.value = ""+tmp_properties.district.value;
    tmp_properties.province.value = ""+tmp_properties.province.value;
    tmp_properties.subdistrict.value = ""+tmp_properties.subdistrict.value;

    this.props.insertData(tmp_properties,this.props.pageIndex,this.props.fieldIndex);
  }

  pickProvince() {
    if (this.state.provinces.length > 0){
      this.setState({showProvince: !this.state.showProvince})
      if(this.state.selectedProvinceLabel == this.state.properties.province.placeholder && !this.state.showProvince){
        this.provinceChanged(10)
      }
    }
  }

  pickDistrict() {
    if (this.state.district.length > 0){
      this.setState({showDistrict: !this.state.showDistrict})
      if(this.state.selectedDistrictLabel == this.state.properties.district.placeholder && !this.state.showDistrict){
        this.districtChanged(this.state.district[0].amphur_code)
      }
    }
  }

  pickSubDistrict() {
    if (this.state.subdistrict.length > 0){
      this.setState({showSubdistrict: !this.state.showSubdistrict})
      if(this.state.selectedSubDistrictLabel == this.state.properties.subdistrict.placeholder && !this.state.showSubdistrict){
        this.subDistrictChanged(this.state.subdistrict[0].district_code)
      }
    }
  }

  render() {
    var output = !this.props.editable ?

      <TextInput multiline={true}
                 returnKeyType={"default"}
                 style={styles.disabletextArea}
                 value={this.state.properties.address.value}
                 placeholder={this.state.properties.address.placeholder}
                 maxLength={this.state.maxLength}
                 editable={false}
                 underlineColorAndroid='rgba(0,0,0,0)'
      /> :
      <TextInput multiline={true}
                 returnKeyType={"default"}
                 style={styles.textArea}
                 value={this.state.properties.address.value}
                 placeholder={this.state.properties.address.placeholder}
                 maxLength={this.state.maxLength}
                 editable={this.props.editable}
                 onChangeText={(text) => {
                   this.addressChanged(text);
                 } }
                 underlineColorAndroid='rgba(0,0,0,0)'
      />;

    if (this.props.viewMode) {
      output = <AppText
        style={styles.viewModeAddress}>{this.state.properties.address.value == '' ? STRINGS.COMMON.NO_ADDRESS_ENTERED : this.state.properties.address.value}</AppText>
    }

    var picker = <View style={{marginBottom: 10}}/>;
    if (this.state.showProvince) {
      picker =
        <FadeInView style={{marginBottom: 10}}>
          <Picker
            selectedValue={parseInt(this.state.properties.province.value)}
            onValueChange={this.provinceChanged.bind(this) }>

            {Object.keys(this.state.provinces).map((key) => (
              <PickerItem
                key={key}
                value={this.state.provinces[key].province_code}
                label={this.state.provinces[key].province}
              />
            )) }

          </Picker>
        </FadeInView>
    }

    var iconName = this.state.showProvince ? 'arrow-up' : 'list';
    var icon = <Icon
      name={iconName}
      size={20}
      color='white'
      style={{width: 20, height: 20}}
    />;

    var provinceView = this.props.editable ?
      <View style={styles.container}>
        <AppText style={styles.label}>
          {this.state.properties.province.name}
        </AppText>
        <TouchableHighlight
          style={styles.dropdownBtn}
          underlayColor='#9FB159'
          onPress={() => this.pickProvince() }>
          <View style={styles.dropdownBtnContainer}>
            <View>
              <AppText style={styles.dropdownBtnTxt}>{this.state.selectedProvinceLabel}</AppText>
            </View>
            <View>
              {icon}
            </View>
          </View>
        </TouchableHighlight>
        {picker}
      </View> :
      <View style={styles.container}>
        <AppText style={styles.label}>
          {this.state.properties.province.name}
        </AppText>
        <TouchableHighlight
          style={styles.dropdownBtnDisable}>
          <View style={styles.dropdownBtnContainer}>
            <View>
              <AppText style={styles.dropdownBtnTxt}>{this.state.selectedProvinceLabel}</AppText>
            </View>
            <View>
              {icon}
            </View>
          </View>
        </TouchableHighlight>
      </View>;

    var pickerDistrict = <View style={{marginBottom: 10}}/>;
    if (this.state.showDistrict) {
      pickerDistrict =
        <FadeInView style={{marginBottom: 10}}>
          <Picker
            selectedValue={parseInt(this.state.properties.district.value)}
            onValueChange={this.districtChanged.bind(this) }>

            {Object.keys(this.state.district).map((key) => (
              <PickerItem
                key={key}
                fontSize={14}
                value={this.state.district[key].amphur_code}
                label={this.state.district[key].amphur}
              />
            )) }

          </Picker>
        </FadeInView>
    }

    if (this.props.viewMode) {
      provinceView =
      <View>
        <DFLabel text={this.state.properties.province.name} fontSize={10} marginLeft={5} color="black" />
        <View style={styles.borderViewDisable }>
          <AppText
            style={styles.viewMode}>{this.state.properties.province.value === '' ? STRINGS.COMMON.NO_PROVINCE_SELECTED : this.state.selectedProvinceLabel}</AppText>
        </View>
      </View>
    }

    var iconDis = this.state.showDistrict ? 'arrow-up' : 'list';
    var dis = <Icon
      name={iconDis}
      size={20}
      color='white'
      style={{width: 20, height: 20}}
    />;

    var districtView = this.props.editable ?
      <View style={styles.container}>
        <AppText style={styles.label}>
          {STRINGS.USER_DATA.DISTRICT}
        </AppText>
        <TouchableHighlight
          style={styles.dropdownBtn}
          underlayColor='#9FB159'
          onPress={() => this.pickDistrict() }>
          <View style={styles.dropdownBtnContainer}>
            <View>
              <AppText style={styles.dropdownBtnTxt}>{this.state.selectedDistrictLabel}</AppText>
            </View>
            <View>
              {dis}
            </View>
          </View>
        </TouchableHighlight>
        {pickerDistrict}
      </View> :
      <View style={styles.container}>
        <AppText style={styles.label}>
          {STRINGS.USER_DATA.DISTRICT}
        </AppText>
        <TouchableHighlight
          style={styles.dropdownBtnDisable}>
          <View style={styles.dropdownBtnContainer}>
            <View>
              <AppText style={styles.dropdownBtnTxt}>{this.state.selectedDistrictLabel}</AppText>
            </View>
            <View>
              {dis}
            </View>
          </View>
        </TouchableHighlight>
      </View>;

    if (this.props.viewMode) {
      districtView =
      <View>
        <DFLabel text={STRINGS.USER_DATA.DISTRICT} fontSize={10} marginLeft={5} color="black" />
        <View style={styles.borderViewDisable }>
          <AppText
            style={styles.viewMode}>{this.state.properties.district.value === '' ? STRINGS.COMMON.NO_DISTRICT_SELECTED : this.state.selectedDistrictLabel}</AppText>
        </View>
      </View>
    }

    var pickerSubDistrict = <View style={{marginBottom: 10}}/>;
    if (this.state.showSubdistrict) {
      pickerSubDistrict =
        <FadeInView style={{marginBottom: 10}}>
          <Picker
            selectedValue={parseInt(this.state.properties.subdistrict.value)}
            onValueChange={this.subDistrictChanged.bind(this) }>

            {Object.keys(this.state.subdistrict).map((key) => (
              <PickerItem
                key={key}
                fonSize={10}
                value={this.state.subdistrict[key].district_code}
                label={this.state.subdistrict[key].district}
              />
            )) }

          </Picker>
        </FadeInView>
    }

    var iconSub = this.state.showSubdistrict ? 'arrow-up' : 'list';
    var sub = <Icon
      name={iconSub}
      size={20}
      color='white'
      style={{width: 20, height: 20}}
    />;

    var subDistrictView = this.props.editable ?
      <View style={styles.container}>
        <AppText style={styles.label}>
          {STRINGS.USER_DATA.SUB_DISTRICT}
        </AppText>
        <TouchableHighlight
          style={styles.dropdownBtn}
          underlayColor='#9FB159'
          onPress={() => this.pickSubDistrict() }>
          <View style={styles.dropdownBtnContainer}>
            <View>
              <AppText style={styles.dropdownBtnTxt}>{this.state.selectedSubDistrictLabel}</AppText>
            </View>
            <View>
              {sub}
            </View>
          </View>
        </TouchableHighlight>
        {pickerSubDistrict}
      </View> : <View style={styles.container}>
      <AppText style={styles.label}>
        {STRINGS.USER_DATA.DISTRICT}
      </AppText>
      <TouchableHighlight
        style={styles.dropdownBtnDisable}>
        <View style={styles.dropdownBtnContainer}>
          <View>
            <AppText style={styles.dropdownBtnTxt}>{this.state.selectedSubDistrictLabel}</AppText>
          </View>
          <View>
            {sub}
          </View>
        </View>
      </TouchableHighlight>
    </View>;

    if (this.props.viewMode) {
      subDistrictView =
      <View>
        <DFLabel text={STRINGS.USER_DATA.SUB_DISTRICT} fontSize={10} marginLeft={5} color="black" />
        <View style={styles.borderViewDisable }>
          <AppText
            style={styles.viewMode}>{this.state.properties.subdistrict.value === '' ? STRINGS.COMMON.NO_SUB_DISTRICT_SELECTED : this.state.selectedSubDistrictLabel}</AppText>
        </View>
      </View>
    }
    return (
      <View style={{paddingLeft:10 , paddingRight:10}}>
        <View style={(this.state.readOnly || !this.props.editable) ? styles.borderViewDisable : styles.borderView }>
          {output}
        </View>

        <View>
          {provinceView}
        </View>

        <View>
          {districtView}
        </View>

        <View>
          {subDistrictView}
        </View>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  textArea: {
    height: 80,
    flexGrow: 4,
    fontSize: 20,
    borderWidth: 0,
    borderRadius: 8,
    color: '#333435',
    textAlignVertical: 'top',
    paddingLeft: 5,
    paddingRight: 5
  },
  borderView: {
    borderColor: '#95989A',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 5,
    marginLeft: 5,
    marginBottom: 5,
    paddingLeft: 5,
    paddingRight: 5
  },
  borderViewDisable: {
    borderColor: '#95989A',
    backgroundColor:'#eee',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 10,
    marginLeft: 10,
    marginBottom: 5,
    paddingLeft: 5,
    paddingRight: 5
  },
  disabletextArea: {
    height: 110,
    flexGrow: 4,
    fontSize: 20,
    borderWidth: 0,
    borderRadius: 8,
    color: '#cdcdcd',
    textAlignVertical: 'top'
  },
  container: {
    flexGrow: 1
  },
  dropdownBtn: {
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    marginTop: 3,
    marginRight: 10,
    marginLeft: 10,
    padding: 10,
    borderRadius: 8,
  },
  dropdownBtnDisable: {
    backgroundColor: '#cdcdcd',
    borderColor: '#cdcdcd',
    marginTop: 3,
    marginRight: 10,
    marginLeft: 10,
    padding: 10,
    borderRadius: 8,
  },
  dropdownBtnContainer: {
    flexGrow: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  dropdownBtnTxt: {
    fontSize: 14,
  },
  label: {
    marginLeft: 10,
    marginRight: 2,
    fontSize: 14,
    color: 'black'
  },
  viewModeAddress: {
    minHeight: 30,
    fontSize: 14,
    borderWidth: 0,
    borderRadius: 8,
    textAlignVertical: 'top',
    color: '#606060',
    paddingTop: 5,
    paddingBottom: 5
  },
  viewMode: {
    minHeight: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#606060',
    textAlignVertical: 'center',
    paddingTop: 5,
    paddingBottom: 5
  },
});

export default DFAddress;
