import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Platform,
  Image,
  TouchableHighlight,
  Linking,
  PermissionsAndroid
} from 'react-native';

import config from '../Util/Config'
import STRINGS from '../Util/strings'
import AppText from '../Util/AppText'
import SignaturePad from '../SignaturePad'
let Ionicons = require('react-native-vector-icons/Ionicons');
let Icon = require('react-native-vector-icons/FontAwesome');

class DFSignature extends Component {

  constructor(props) {
    super(props);
    // console.log(props);
    let imgPath = null;

    if ("valueUri" in this.props) {
      console.log('Signature valueUri: ', this.props.valueUri);
      let uri = this.props.valueUri;
      if (uri !== '') {
        if (uri.includes('file://')) {
          imgPath = { uri: this.props.valueUri }
        } else {
          imgPath = { uri: config.upload_url + this.props.valueUri }
        }
      }
    }

    if ("value" in this.props) {
      console.log('Signature value: ', this.props.value);
      if (this.props.value !== undefined)
        imgPath = this.props.value
    }
    this.state = {
      imgPath: imgPath
    };
  }

  tryOpenSignPad() {
    if (Platform.OS === 'ios') {
      this.openSignPad();

    } else {
      let self = this;
      PermissionsAndroid.checkPermission(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
        .then(function (granted) {
          if (granted) {
            self.openSignPad();
          } else {
            PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
              .then(function (userGranted) {
                if (userGranted) {
                  self.openSignPad();
                }
              });
          }
        });
    }
  }

  openSignPad() {
    this.props.navigator.push({
      name: 'Signature',
      title: STRINGS.CUSTOM_MODALS.SIGNATURE_TITLE,
      component: SignaturePad,
      passProps: {
        navigator: this.props.navigator,
        setPathUri: this.setPathUri.bind(this)
      }
    });
  }

  setPathUri(imgPathUrl) {
    // if (Platform.OS == 'android') {
    //   imgPathUrl = 'file:/' + imgPathUrl;
    // }
    let dl_file = imgPathUrl.split('/');
    let dl_file_name = dl_file[dl_file.length - 1];
    let source = { uri: imgPathUrl, isStatic: true, name: dl_file_name, type: 'image/png', uploadType: 'image' };

    console.log('imgPathUrl = ', imgPathUrl);
    this.setState({
      imgPath: source
    });

    this.props.insertData(source, this.props.pageIndex)
  }

  render() {

    let btnImage = this.props.editable ? <View style={{ marginBottom: 5 }}>
      <TouchableHighlight
        style={styles.locationBtn}
        underlayColor='#9FB159'
        onPress={() => this.tryOpenSignPad()}>
        <View style={styles.locationBtnContainer}>
          <View style={{ width: 20, height: 20 }}></View>
          <AppText style={styles.labelBtn}>{STRINGS.CUSTOM_MODALS.SIGNATURE_TITLE}</AppText>
          <View>
            <Image source={require('../img/signature_icon.png')} style={{ width: 20, height: 20 }} />
          </View>
        </View>
      </TouchableHighlight>
    </View> : <View style={{ marginBottom: 5 }}>
        <TouchableHighlight
          style={styles.locationBtnDisable}>
          <View style={styles.locationBtnContainer}>
            <View style={{ width: 20, height: 20 }}></View>
            <AppText style={styles.labelBtn}>{STRINGS.CUSTOM_MODALS.SIGNATURE_TITLE}</AppText>
            <View>
              <Image source={require('../img/signature_icon.png')} style={{ width: 20, height: 20 }} />
            </View>
          </View>
        </TouchableHighlight>
      </View>;
    if (this.props.viewMode) {
      btnImage = null;
    }
    let btnRemove = this.state.imgPath != null && this.props.editable ? <TouchableHighlight
      underlayColor='transparent'
      onPress={() => this.removeImage()}
      style={{ alignItems: 'flex-end' }}>
      <Ionicons name="md-close" size={25} color="#B2D234"
        style={{ justifyContent: 'center', alignItems: 'center', marginRight: 5 }} />
    </TouchableHighlight> : null;

    let renderView =
      <View style={{ marginLeft: 10, marginRight: 10 }}>
        <View style={{ marginBottom: 5 }}>
          {btnRemove}
          {this.state.imgPath !== null ?
            <View style={{
              borderWidth: 2,
              borderStyle: 'dashed',
              marginLeft: 20,
              marginRight: 20,
              borderColor: '#c0c0c0'
            }}>
              <Image resizeMode={Image.resizeMode.contain} source={this.state.imgPath} style={styles.imgView} />
            </View>
            : null}
        </View>
        {btnImage}
      </View>;

    if (this.props.viewMode && this.state.imgPath === null) {
      renderView = <View
        style={styles.borderView}>
        <AppText style={styles.viewMode}>{STRINGS.CUSTOM_MODALS.NO_SIGNATURE}</AppText>
      </View>;
    }

    return (renderView);
  }

  removeImage() {
    this.setState({
      imgPath: null
    });
    this.props.removeData(this.props.pageIndex, this.props.fieldIndex);
  }
}

const styles = StyleSheet.create({
  imgView: {
    height: 200,
    margin: 5,
  },
  label: {
    marginLeft: 10,
    marginRight: 5,
    marginBottom: 3,
    fontSize: 14,
    color: 'black'
  },
  labelBtn: {
    marginLeft: 10,
    marginRight: 5,
    marginBottom: 3,
    fontSize: 14,
    color: 'white'
  },
  locationBtn: {
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    marginRight: 10,
    marginLeft: 10,
    padding: 10,
    borderRadius: 8,
  },
  locationBtnDisable: {
    backgroundColor: '#cdcdcd',
    borderColor: '#cdcdcd',
    marginRight: 10,
    marginLeft: 10,
    padding: 10,
    borderRadius: 8,
  },
  locationBtnContainer: {
    flexGrow: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  borderView: {
    borderColor: '#cdcdcd',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  viewMode: {
    minHeight: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#606060',
    textAlignVertical: 'center',
    paddingTop: 5,
    paddingBottom: 5
  },
});

export default DFSignature;
