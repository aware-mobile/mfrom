import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableHighlight,
  Dimensions
} from 'react-native';

import AppText from '../Util/AppText'

var {height, width} = Dimensions.get('window');

import DFLabel from './DFLabel'
var Ionicons = require('react-native-vector-icons/Ionicons');

class DFCheckBox extends Component {
  constructor(props) {
    super(props);

    var colorDisable = (this.props.editable) ? '#B2D234' : '#cdcdcd'
    var colorLabelDisable = (this.props.editable) ? '#000000' : '#cdcdcd'
    if (this.props.viewMode) {
      colorLabelDisable = '#606060'
    }
    this.state = {
      colorDisable: colorDisable,
      colorLabelDisable: colorLabelDisable,
      valFromFormPage: this.props.options,
    };
    this.checkedItems = []
  }


  render() {
    // console.log('valFromFormPage Render : ', this.state.valFromFormPage)
    var optionList = [];
    // this.checkedItems = [];
      var options = this.state.valFromFormPage
      for(let i = 0 ; i < options.length ; i++) {
        // if (item.checked) {
          var item = options[i]
        //   this.checkedItems.push(item.option_key);
        // }

        let icon = (item.checked) ?
        <View style={[styles.boxBorder,{borderColor: this.state.colorDisable}]}>
         <Ionicons style={styles.icon} name="md-checkmark" size={24} color={this.state.colorDisable}/>
         </View>
        : <View style={[styles.boxBorder,{borderColor: this.state.colorDisable}]}>
         </View>
        let output = (this.props.editable) ?
          <TouchableHighlight
            key={i + "-DFCheckBox-" + item.checked}
            onPress={this.checkItem.bind(this, i)}
            style={{}}
            underlayColor="transparent" >
            <View style={[styles.checkBoxContainer, this.setContainerWidth(20)]}>
              {icon}
              <AppText style={[styles.label, { color: this.state.colorLabelDisable }, this.setContainerWidth(65) ]}>
                {item.label}
              </AppText>
            </View>
          </TouchableHighlight >
          :
          <View style={[styles.checkBoxContainer, this.setContainerWidth(20)]} key={i + "-DFCheckBox-" + item.checked}>
            {icon}
            <AppText style={[styles.label, { color: this.state.colorLabelDisable }, this.setContainerWidth(65) ]}>
              {item.label}
            </AppText>
          </View>

        optionList.push(output)
    }


    return (
      <View style={{ paddingLeft: 10, paddingRight:10 }}>
        {optionList}
      </View>
    );
  }

  checkItem(i) {
    var options = this.state.valFromFormPage

    if (options[i].checked) {
      options[i].checked = false
    } else {
      options[i].checked = true
    }
    this.setState({
      valFromFormPage: options,
    })
    this.props.insertData(this.state.valFromFormPage, this.props.pageIndex, this.props.fieldIndex);
  }

  setContainerWidth(padding) {
    let containerWidth = width;
    if(this.props.isTwoColumns){
      containerWidth = containerWidth / 2 - 20;
    }
    return {
      width: containerWidth - padding
    }
  }

}

const styles = StyleSheet.create({
  checkBoxContainer: {
    flexDirection: 'row',
    paddingTop: 0,
    paddingBottom:0,
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 10
  },
  boxBorder:{
    borderWidth: 2,
    width:25,
    height:25,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  icon: {
    paddingTop: 2,
    alignSelf: 'center',
  },
  label: {
    marginLeft: 10,
    fontSize: 14
  }
});

export default DFCheckBox;
