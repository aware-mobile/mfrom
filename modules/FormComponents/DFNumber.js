import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Image,
  Platform
} from 'react-native';

import AppText from '../Util/AppText'

var OutputComponent;

class DFNumber extends Component {

  constructor(props) {
    super(props);

    let thaiCurSymbol = 'THB';  //&#3647;'
    let max_length = this.props.maxLength;

    let valueText = '';
    if ("value" in this.props) {
      valueText = this.props.value
    }

    let styleList = null;
    let styleNum = '';
    if ("numStyle" in this.props) {
      styleList = this.props.numStyle
    }

    for (let k = 0; k < styleList.length; k++) {
      if (styleList[k].checked) {
        styleNum = styleList[k].name;
        console.log('styleNum:' + styleNum)
      }
    }

    if (styleNum === 'Citizen Card') {
      max_length = 13;
    }
    this.keyboardType = 'default';
    if (styleNum === 'Telephone Number') {
      this.keyboardType = 'default'
    }

    let read_only = false;
    if ("readOnly" in this.props) {
      read_only = this.props.readOnly
    }
    // console.log('styleNum:' + styleNum)

    this.state = {
      validationError: '',
      text: valueText,
      maxLength: max_length,
      numberStyle: styleNum,
      symbolBath: thaiCurSymbol,
      readOnly: read_only
    };
  }

  render() {

    var output = null;
    var outputCur = null;

    output = (this.props.readOnly || !this.props.editable) ?
      <AppText style={(this.state.text === '') ? styles.DisableInputNoText : styles.DisableInput}>{this.state.text}</AppText>
      :
      <TextInput
        ref={this.props.refName}
        keyboardType={this.keyboardType}
        returnKeyType={"next"}
        style={styles.input}
        value={this.state.text}
        maxLength={this.state.maxLength}
        onChangeText={(text) => {
          this.props.addTextData(text, this.props.pageIndex, this.props.fieldIndex);
          this.setState({text})
        } }
        onSubmitEditing={this.props.next.bind(this, this.props.nextItem) }
        underlineColorAndroid='rgba(0,0,0,0)'/>

    outputCur = (this.state.numberStyle === 'Currency') ?
      ((this.props.readOnly || !this.props.editable) ?
        <Image source={require('../img/thbaht-gray16.png') } style={styles.curIcon}/>
        :
        <Image source={require('../img/thbaht-black16.png') } style={styles.curIcon}/>)
      : null;
    let value = '';
    if (this.state.numberStyle === 'Currency') {
      value = this.formatNumber(this.state.text);
    } else {
      value = this.state.text;
    }
    if (this.props.viewMode) {
      output = <AppText style={styles.viewMode}>{value}</AppText>
    }
    return (
      <View style={(this.state.readOnly || !this.props.editable) ? styles.borderViewDisable : styles.borderView }>
        {outputCur}
        {output}
      </View>
    );
  }

  focusNow() {
    this.refs[this.props.refName].focus()
  }

  formatNumber(value) {
    if (value) {
      let parts = value.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts.join(".");
    } else {
      return '';
    }
  }
}

const styles = StyleSheet.create({
  input: {
    flexGrow: 1,
    height: 30,
    fontSize: 20,
    borderWidth: 0,
    color: '#333435',
    marginRight: 10,
    textAlign: 'left',
    paddingTop: 2,
    paddingBottom: 2,
  },
  borderView: {
    borderColor: '#95989A',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
    flexDirection: 'row'
  },
  borderViewDisable: {
    borderColor: '#95989A',
    backgroundColor: '#eee',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
    flexDirection: 'row'
  },
  label: {
    flexGrow: 1,
    marginLeft: 10,
    marginRight: 5,
    marginBottom: 3,
    fontSize: 14,
    color: '#000000'
  },
  DisableInput: {
    marginRight: 15,
    marginLeft: 15,
    height: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#c0c0c0',
    textAlign: 'left',
    paddingTop: 5,
    paddingBottom: 5,
  },
  DisableInputNoText: {
    marginRight: 15,
    marginLeft: 15,
    height: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#c0c0c0',
    textAlign: 'left',
    paddingTop: 5,
    paddingBottom: 5,
  },
  viewMode: {
    minHeight: 30,
    flexGrow: 1,
    marginLeft: 10,
    marginRight: 10,
    fontSize: 14,
    borderWidth: 0,
    color: '#606060',
    textAlign: 'left',
    textAlignVertical: 'center',
    paddingTop: 5,
    paddingBottom: 5,
  },
  curIcon: {
    marginTop: 7,
    width: 15,
    height: 15,
  }

});

export default DFNumber;
