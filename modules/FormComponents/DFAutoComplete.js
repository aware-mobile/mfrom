import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  TouchableHighlight,
  AsyncStorage,
  Linking
} from 'react-native';
import AutoCompletePage from '../AutoCompletePage'
import Hyperlink from 'react-native-hyperlink'
import STRINGS from '../Util/strings'
import AppText from '../Util/AppText'

class DFAutoComplete extends Component {

  constructor(props) {
    super(props);
    //console.log(props);
    let placeText = '';
    if ("placeholder" in this.props) {
      placeText = this.props.placeholder
    }

    let valueText = '';
    if ("value" in this.props) {
      valueText = this.props.value
    }

    let type = 'default';
    if ("keyboardType" in this.props) {
      type = this.props.keyboardType
    }

    let read_only = false;
    if ("readOnly" in this.props) {
      read_only = this.props.readOnly
    }

    let placeholder = '';

    let selectedProp = this.getSelectedProperty(this.props.properties);
    if (selectedProp == 'Current User') {
      read_only = true;
    }

    let max_length = this.props.maxLength;

    this.state = {
      placeholderText: placeholder,
      text: valueText,
      validationError: '',
      inputType: type,
      maxLength: max_length,
      readOnly: read_only
    };

    if (selectedProp == 'Current User' && this.props.editable) {
      this.getCurrentUser();
    }
  }

  getSelectedProperty(properties){
    for (var i = 0; i < properties.length; i++) {
      if('checked' in properties[i]){
        if(properties[i].checked){
          return properties[i].name;
        }
      }
    }

    return null;
  }

  async getCurrentUser() {
    let value = await AsyncStorage.getItem("Profile");
    let profile = await JSON.parse(value);
    this.setValue(profile.user.displayName, '', '');
  }

  render() {
    let output = (this.state.readOnly || !this.props.editable) ?
      <Hyperlink
        onPress={ url => this.clickedLink(url) }
        linkStyle={{ color: '#2980b9' }}>

        <View>
          <AppText placeholder={this.state.placeholderText}
                style={(this.state.text === '') ? styles.DisableInputNoText : styles.DisableInput}>
            {this.state.text}
          </AppText>
        </View>

      </Hyperlink>
      :
      <TouchableHighlight
        underlayColor={'transparent'}
        onPress={() => this.openAutoComplete(this.props.text)}>
        <View>
          <AppText multiline={false} placeholder={this.state.placeholderText} numberOfLines={1} style={styles.input}>
            {this.state.text}
          </AppText>
        </View>
      </TouchableHighlight>;
    if (this.props.viewMode) {
      output = <Hyperlink
        onPress={ url => this.clickedLink(url) }
        linkStyle={{ color: '#2980b9' }}>
          <AppText placeholder={this.state.placeholderText} style={styles.viewMode}>{this.state.text}</AppText>
        </Hyperlink>
    }
    return (
      <View style={(this.state.readOnly || !this.props.editable) ? styles.borderViewDisable : styles.borderView}>
        {output}
      </View>);
  }

  clickedLink(url){
    this.props.openMainConfirm(STRINGS.formatString(STRINGS.POPUP_MSG.OPEN_LINK, url), function () {
      Linking.canOpenURL(url).then(supported => {
        if (supported) {
          Linking.openURL(url);
        } else {
          self.props.openMainModal(STRINGS.POPUP_MSG.CANNOT_OPEN_LINK);
        }
      });
    });
  }

  focusNow() {
    this.refs[this.props.refName].focus()
  }

  openAutoComplete(title) {
    this.props.navigator.push({
      name: 'autocomplete',
      title: title,
      component: AutoCompletePage,
      passProps: {
        title: title,
        properties: this.props.properties,
        navigator: this.props.navigator,
        text: this.state.text,
        setValue: this.setValue.bind(this)
      }
    });
  }

  setValue(value, userId, customerId) {
    this.setState({
      text: value,
    });
    this.props.mapped_values.user_id = userId;
    this.props.mapped_values.customer_id = customerId;
    this.props.setAutoComplete(value, this.props.mapped_values, this.props.pageIndex, this.props.fieldIndex);
  }

}

const styles = StyleSheet.create({
  input: {
    height: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#333435',
    paddingTop: 5,
    paddingBottom: 5
  },
  borderView: {
    borderColor: '#95989A',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  borderViewDisable: {
    borderColor: '#95989A',
    backgroundColor:'#eee',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  DisableInput: {
    fontSize: 14,
    borderWidth: 0,
    color: '#c0c0c0',
    paddingTop: 5,
    paddingBottom: 5,
  },
  DisableInputNoText: {
    height: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#c0c0c0',
    paddingTop: 5,
    paddingBottom: 5,
  },
  viewMode: {
    minHeight: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#606060',
    textAlignVertical: 'center',
    paddingTop: 5,
    paddingBottom: 5,
  }
});

export default DFAutoComplete;
