import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Picker,
  TouchableHighlight,
  Platform,
  Dimensions
} from 'react-native';

var {height, width} = Dimensions.get('window');

var Icon = require('react-native-vector-icons/FontAwesome');
import FadeInView from '../Animation/FadeInView'
import STRINGS from '../Util/strings'
import AppText from '../Util/AppText'

let PickerItem = Picker.Item;

class DFDropDown extends Component {
  constructor(props) {
    super(props);

    var DropDownOptions = null;
    if ("options" in this.props) {
      DropDownOptions = this.props.options
    }
    var firstSelectVal = '';
    var firstSelectLabel = STRINGS.COMMON.PLEASE_SELECT;
    var listItemNames = [];
    for (var i = 0; i < DropDownOptions.length; i++) {
      listItemNames.push(DropDownOptions[i].value);
      if (DropDownOptions[i].value == this.props.value) {
        firstSelectVal = DropDownOptions[i].value
        firstSelectLabel = DropDownOptions[i].label
      }
    }
    var pickable = ('editable' in this.props) ? this.props.editable : true;
    this.state = {
      options: DropDownOptions,
      labels: listItemNames,
      show: false,
      pickable: pickable,
      selectedValue: firstSelectVal,
      selectedLabel: firstSelectLabel
    };

  }

  pickDate() {
    if (this.state.pickable) {
      if(this.state.selectedValue == '' && !this.state.show){
        this.setState({
          selectedValue: this.state.options[0].value,
          selectedLabel: this.state.options[0].label,
          show: !this.state.show
        });
        this.props.insertData(this.state.options[0].value, this.props.pageIndex,this.props.fieldIndex);
      }else{
        this.setState({ show: !this.state.show });
      }
    }
  }

  _valueChanged(value) {
    var index = this.state.labels.indexOf(value);
    this.setState({
      selectedValue: this.state.options[index].value,
      selectedLabel: this.state.options[index].label
    });

    this.props.insertData(value,this.props.pageIndex,this.props.fieldIndex);
  }

  render() {

    var picker = <View style={{ marginBottom: 10 }}/>;
    if (this.state.show) {
      picker =
        <FadeInView style={{ marginBottom: 10 }}>
          <Picker
            selectedValue={this.state.selectedValue}
            onValueChange={this._valueChanged.bind(this) }>

            {Object.keys(this.state.options).map((key) => (
              <PickerItem
                key={key}
                value={this.state.options[key].value}
                label={this.state.options[key].label}
                />
            )) }

          </Picker>
        </FadeInView>
    }

    var iconName = this.state.show ? 'arrow-up' : 'list';
    var icon = <Icon
      name={iconName}
      size={20}
      color='white'
      style={{ width: 20, height: 20 }}
      />;

    var returnView = this.state.pickable ?
      <View style={[styles.container, this.setContainerWidth()]}>
        <TouchableHighlight
          style={[styles.dropdownBtn, this.setButtonStyle()]}
          underlayColor='#9FB159'
          onPress={() => this.pickDate() }>
          <View style={[styles.dropdownBtnContainer]}>
            <AppText style={[styles.dropdownBtnTxt, this.setButtonTextWidth()]}>{this.state.selectedLabel}</AppText>
            <View style={styles.iconContainer}>
              {icon}
            </View>
          </View>
        </TouchableHighlight>
        {picker}
      </View> : <View style={[styles.container, this.setContainerWidth()]}>
        <TouchableHighlight
          style={[styles.dropdownBtnDisable, this.setButtonStyle()]}>
          <View style={styles.dropdownBtnContainer}>
            <AppText style={[styles.dropdownBtnTxt, this.setButtonTextWidth()]}>{this.state.selectedLabel}</AppText>
            <View style={styles.iconContainer}>
              {icon}
            </View>
          </View>
        </TouchableHighlight>
        {picker}
      </View>;
    if (this.props.viewMode) {
      returnView = <View
        style={styles.borderView}>
        <AppText style={styles.viewMode}>{this.state.selectedValue == '' ? STRINGS.COMMON.NO_OPTION_SELECTED : this.state.selectedLabel}</AppText>
      </View>
    }
    return (returnView);
  }

  setContainerWidth() {
    let containerWidth = width - 20;
    if(this.props.isTwoColumns){
      containerWidth = containerWidth / 2 - 20;
    }
    return {
      width: containerWidth
    }
  }

  setButtonStyle() {
    if(this.props.isTwoColumns){
      return {width: width/2 - 60}
    }else{
      return {width: width - 40}
    }
  }

  setButtonTextWidth() {
    if(this.props.isTwoColumns){
      return {width: width/2 - 100}
    }else{
      return {width: width-85}
    }
  }

}

const styles = StyleSheet.create({
  container: {
    paddingLeft: 5,
    paddingRight: 5,
    alignSelf: 'center',
    // alignItems: 'center',
  },
  iconContainer: {
    width: 30,
    alignItems: 'center',
    // backgroundColor: '#887b22'
  },
  dropdownBtn: {
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    marginTop: 0,
    padding: 10,
    borderRadius: 8,
    alignSelf: 'center'
  },
  dropdownBtnDisable: {
    backgroundColor: '#cdcdcd',
    borderColor: '#cdcdcd',
    marginTop: 0,
    padding: 10,
    borderRadius: 8,
  },
  dropdownBtnContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  dropdownBtnTxt: {
    fontSize: 14,
    color:'black'
  },
  borderView: {
    borderColor: '#cdcdcd',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  viewMode: {
    minHeight: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#606060',
    textAlignVertical: 'center',
    paddingTop: 5,
    paddingBottom: 5
  },
});

export default DFDropDown;
