import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Linking
} from 'react-native';

import Hyperlink from 'react-native-hyperlink'
import STRINGS from '../Util/strings'
import AppText from '../Util/AppText'
var OutputComponent;
var self;

class DFTextBox extends Component {

  constructor(props) {
    super(props);

    self = this;
    //console.log(props);
    var placeText = '';
    if ("placeholder" in this.props) {
      placeText = this.props.placeholder
    }

    var valueText = '';
    if ("value" in this.props) {
      valueText = this.props.value
    }

    var type = 'default';
    if ("keyboardType" in this.props) {
      type = this.props.keyboardType
    }

    var read_only = false;
    if ("readOnly" in this.props) {
      read_only = this.props.readOnly
    }

    var max_length = this.props.maxLength;

    //alert('max_length : ' + max_length)

    this.state = {
      placeholderText: placeText,
      text: valueText,
      validationError: '',
      inputType: type,
      maxLength: max_length,
      readOnly: read_only
    };
  }

  render() {
    var output = (this.props.readOnly || !this.props.editable) ?
      <Hyperlink
        onPress={ url => this.clickedLink(url) }
        linkStyle={{color: '#2980b9'}}>
        <AppText style={(this.state.text == '') ? styles.DisableInputNoText : styles.DisableInput}>{this.state.text}</AppText>
      </Hyperlink>
      :
      <TextInput
        ref={this.props.refName}
        returnKeyType={"next"}
        style={styles.input}
        keyboardType={this.state.inputType}
        value={this.state.text}
        placeholder={this.state.placeholderText}
        maxLength={this.state.maxLength}
        editable={this.props.editable}
        onChangeText={(text) => {
          this.props.addTextData(text, this.props.pageIndex, this.props.fieldIndex)
          this.setState({text})
        }}
        onSubmitEditing={this.props.next.bind(this, this.props.nextItem)}
        underlineColorAndroid='rgba(0,0,0,0)'
      />
    if (this.props.viewMode) {
      output = <Hyperlink
        onPress={ url => this.clickedLink(url) }
        linkStyle={{color: '#2980b9'}}>
        <AppText style={styles.viewMode}>{this.state.text}</AppText>
      </Hyperlink>
    }
    return (
      <View style={(this.state.readOnly || !this.props.editable) ? styles.borderViewDisable : styles.borderView }>
        {output}
      </View>
    );
  }

  focusNow() {
    this.refs[this.props.refName].focus()
  }

  clickedLink(url) {
    this.props.openMainConfirm(STRINGS.formatString(STRINGS.POPUP_MSG.OPEN_LINK, url), function () {
      Linking.canOpenURL(url).then(supported => {
        if (supported) {
          Linking.openURL(url);
        } else {
          self.props.openMainModal(STRINGS.POPUP_MSG.CANNOT_OPEN_LINK);
        }
      });
    });
  }


}

const styles = StyleSheet.create({
  input: {
    height: 30,
    fontSize: 20,
    borderWidth: 0,
    color: '#333435',
    paddingTop: 2,
    paddingBottom: 2
  },
  borderView: {
    borderColor: '#95989A',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  borderViewDisable: {
    borderColor: '#95989A',
    backgroundColor: '#eee',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  DisableInput: {
    height: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#c0c0c0',
    paddingTop: 5,
    paddingBottom: 5,
  },
  DisableInputNoText: {
    height: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#c0c0c0',
    paddingTop: 5,
    paddingBottom: 5,
  },
  viewMode: {
    minHeight: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#606060',
    textAlignVertical: 'center',
    paddingTop: 5,
    paddingBottom: 5,
  }
});

export default DFTextBox;
