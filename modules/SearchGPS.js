import React, {Component} from 'react';
import {
  View,
  Image,
  TouchableHighlight,
  Dimensions
} from 'react-native';

var {height, width} = Dimensions.get('window');

import Icon from 'react-native-vector-icons/Ionicons';
import Util from './Util/Util';
import STRINGS from './Util/strings';

var self;

var {GooglePlacesAutocomplete} = require('react-native-google-places-autocomplete');

class SearchGPS extends Component {
  constructor(props) {
    super(props);
    self = this
    this.state = {
    };
  }

  render() {

    return (

      <GooglePlacesAutocomplete
        style={{width: width}}
        placeholder={STRINGS.COMMON.SEARCH}
        minLength={2} // minimum length of text to search
        autoFocus={false}
        listViewDisplayed='auto'
        fetchDetails={true}
        renderDescription={(row) => row.description} // custom description render
        onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
          let position = details.geometry.location
          self.props.selectedPlace(position)
          self.props.navigator.pop();
        }}
        getDefaultValue={() => {
          return ''; // text input default value
        }}
        query={{
          // available options: https://developers.google.com/places/web-service/autocomplete
          // key: 'AIzaSyASOWaHBXM8tBQ0iX40RI9Myx1YHzTjV9Q',
          // language: 'en', // language of the results
          // types: '(cities)', // default: 'geocode'

          // available options: https://developers.google.com/places/web-service/autocomplete
          key: 'AIzaSyASOWaHBXM8tBQ0iX40RI9Myx1YHzTjV9Q',
          language: 'en', // language of the results
          types: 'establishment', // default: 'geocode'
          componentRestrictions: {country: 'th'}
        }}
        styles={{
          description: {
            fontWeight: 'bold',
            width: width - 30
          },
          predefinedPlacesDescription: {
            color: '#1faadb',
          },
        }}

        currentLocation={false}
        nearbyPlacesAPI='GooglePlacesSearch'
        GoogleReverseGeocodingQuery={{
          // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
        }}
        GooglePlacesSearchQuery={{
          // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
          rankby: 'distance',
          types: 'food',
        }}


        filterReverseGeocodingByTypes={[]}

        debounce={200}
      />

    )
  }

}

/*

<View style={{width:width}}>
  <GooglePlacesAutocomplete
    placeholder={STRINGS.COMMON.SEARCH}
    minLength={2} // minimum length of text to search
    autoFocus={false}
    fetchDetails={true}
    onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
      let position = details.geometry.location
      self.props.selectedPlace(position)
      self.props.navigator.pop();
    }}
    getDefaultValue={() => {
      return ''; // text input default value
    }}
    query={{
      // available options: https://developers.google.com/places/web-service/autocomplete
      key: 'AIzaSyASOWaHBXM8tBQ0iX40RI9Myx1YHzTjV9Q',
      language: 'en', // language of the results
      types: 'establishment', // default: 'geocode'
      componentRestrictions: {country: 'th'}
    }}
    styles={{
      description: {
        fontWeight: 'bold',
      },
      predefinedPlacesDescription: {
        color: '#1faadb',
      },
    }}

    currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
    nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
    GoogleReverseGeocodingQuery={{
      // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
    }}
    GooglePlacesSearchQuery={{
      // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
      rankby: 'distance',
      types: 'food',
    }}

    filterReverseGeocodingByTypes={[]} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
    predefinedPlaces={[]}
  />
</View>

*/

export default SearchGPS;
