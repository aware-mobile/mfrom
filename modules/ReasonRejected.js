import React, { Component } from 'react';
import {
    ScrollView,
    StyleSheet,
    View,
    Image,
    TouchableHighlight,
    ListView,
    ActivityIndicator,
    AsyncStorage,
    Dimensions,
    Platform
} from 'react-native';
import DFService from './Service/DFService';
// import DFService2 from './Service/DFService';
import apiConfig from './Util/Config';
import Util from './Util/Util';
import STRINGS from './Util/strings';
import FormPage from './FormPage'
import AppText from './Util/AppText';

var self;

var Ionicons = require('react-native-vector-icons/Ionicons');
var {height, width} = Dimensions.get('window');

export class ReasonRejected extends Component {
    constructor(props) {
        super(props)

    }

    render() {

        return (
            <View style={styles.container}>
                <View style={styles.innerContainer}>
                    <View style={styles.headerModal}>
                        <AppText style={styles.textHeaderModal}>
                            {this.props.title}
                        </AppText>

                        <TouchableHighlight
                            underlayColor='transparent'
                            style={{ width: 30, height: 30 }}
                            onPress={() => this.props.closeModal() }>
                            <Ionicons name="md-close" size={30} color="black"
                              style={{ justifyContent: 'center', alignItems: 'center' }}/>
                        </TouchableHighlight>

                    </View>
                    <View style={{ flexGrow: 1, padding:10 }}>
                        <AppText style={{fontWeight:'bold'}}>
                            {STRINGS.COMMON.DETAIL_LBL}
                        </AppText>
                        <AppText style={{paddingTop:5, color:'grey'}}>
                            {this.props.message}
                        </AppText>
                    </View>
                </View>
            </View>
        )

    }

}

var styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        zIndex: 999,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerModal: {
        height: 60,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#efeff1'
    },
    textHeaderModal: {
        marginLeft: 20,
        fontWeight: 'bold',
        fontSize: 18,
    },
    innerContainer: {
        width: width - 60,
        backgroundColor: '#ffffff',
        borderRadius: 4,
    },
    row: {
        alignItems: 'center',
        flexGrow: 1,
        flexDirection: 'row',
        marginBottom: 20,
    },
    rowTitle: {
        flexGrow: 1,
        fontWeight: 'bold',
    },
    button: {
        borderRadius: 5,
        flexGrow: 1,
        height: 44,
        alignSelf: 'stretch',
        justifyContent: 'center',
        overflow: 'hidden',
    },
    buttonText: {
        fontSize: 18,
        margin: 5,
        textAlign: 'center',
    },
    modalButton: {
        marginTop: 10,
    },
});

export default ReasonRejected;
