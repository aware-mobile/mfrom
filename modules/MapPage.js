import React, {Component} from 'react';
import {Dimensions, StyleSheet, TouchableOpacity, View, Platform} from 'react-native';

import Topbar from './Topbar'
import MapView from 'react-native-maps';
import SearchGPS from './SearchGPS'
import STRINGS from './Util/strings';
import AppText from './Util/AppText';

var Timer = require('react-native-timer');

var self;

const {width, height} = Dimensions.get('window');
const timeout = 1000;
let animationTimeout;

const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 9.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const LATITUDE_DELTA_CLOSEUP = 0.06764637641092541;
const LONGITUDE_DELTA_CLOSEUP = LATITUDE_DELTA_CLOSEUP * ASPECT_RATIO;

export default class MapPage extends Component {
  constructor(props) {
    super(props);
    self = this;

    let properties = this.props.properties;
    console.log('MAP VIEW PROPS: ', properties);
    let selectedLocation = (properties.lat !== "");

    // var longitude = -122.4324
    // var latitude = 37.78825

    var longitude = 101.35359096676508;
    var latitude = 13.88149759150059;

    if (selectedLocation) {
      longitude = parseFloat(properties.lng);
      latitude = parseFloat(properties.lat);
    }

    let mapRegion = {
      latitude: latitude,
      longitude: longitude,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
    }

    this.state = {
      isFirstLoad: true,
      selectedLocation,
      mapRegion,
      region: {
        latitude: latitude,
        longitude: longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      mapRegionInput: undefined,
      marker: {
        latlng: {
          longitude: longitude,
          latitude: latitude
        },
        title: STRINGS.MAP_PAGE.HEADER,
        description: STRINGS.MAP_PAGE.DESCRIPTION
      },
      showMarker: false
    };
  }

  componentWillUnmount() {
    Timer.clearTimeout(this);
  }

  _onMapPress(e) {
    let coordinates = e.nativeEvent.coordinate;
    this.setState({
      marker: {
        latlng: coordinates
      },
      region: {
        latitude: coordinates.latitude,
        longitude: coordinates.longitude,
        latitudeDelta: this.state.region.latitudeDelta,
        longitudeDelta: this.state.region.longitudeDelta
      },
      selectedLocation: true,
      showMarker: true
    })
  }

  getMarkerCoord() {
    let coordinates = this.state.marker.latlng;
    console.log('THe Coordinates: ', coordinates);
    return {
      latitude: coordinates.latitude,
      longitude: coordinates.longitude
    };
  }

  onRegionChange(region) {
    console.log('On Region Changed!');
    let markerCoord = this.getMarkerCoord();
    this.setState({
      region: {
        latitude: region.latitude,
        longitude: region.longitude,
        latitudeDelta: region.latitudeDelta,
        longitudeDelta: region.longitudeDelta
      }
    });
    if (this.state.isFirstLoad) {
      this.setState({isFirstLoad: false});
      if (this.state.selectedLocation) {
        animationTimeout = setTimeout(() => {
          this.map.animateToCoordinate(markerCoord);
          console.log('moved map to marker');
          this.setState({
            showMarker: true,
            selectedLocation: true,
            marker: {
              latlng: markerCoord
            }
          })
        }, timeout);
      }
    }
  }

  _doneBtn() {
    let coordinates = this.state.marker.latlng;
    this.props.setLocation({
      lat: coordinates.latitude.toFixed(7),
      lng: coordinates.longitude.toFixed(7),
      pageIndex: this.props.pageIndex,
      fieldIndex: this.props.fieldIndex
    });
    this.props.navigator.pop();
  }

  openSearchGPS() {

    this.props.navigator.push({
      name: 'SearchGPS',
      title: STRINGS.MAP_PAGE.SEARCH_PLACE_TITLE,
      component: SearchGPS,
      passProps: {
        title: STRINGS.MAP_PAGE.SEARCH_PLACE_TITLE,
        navigator: this.props.navigator,
        selectedPlace: this.selectedPlace.bind(this)
      }
    });
  }

  selectedPlace(position) {
    console.log('POSITION SET: ', position);
    let coord = {
      latitude: position.lat,
      longitude: position.lng
    };
    let newRegion = {
      latitude: position.lat,
      longitude: position.lng,
      latitudeDelta: LATITUDE_DELTA_CLOSEUP,
      longitudeDelta: LONGITUDE_DELTA_CLOSEUP
    };
    console.log('long delta: ', self.state.region.latitudeDelta);
    if (Platform.OS === 'ios') {
      self.setState({
        marker: {
          latlng: coord
        },
        selectedLocation: true,
        showMarker: true
      }, () => Timer.setTimeout(self, 'setSearchPosition', () => self.map.animateToRegion(newRegion), 500));
    } else {
      self.map.animateToRegion(newRegion);
      self.setState({
        marker: {
          latlng: coord
        },
        selectedLocation: true,
        showMarker: true
      });
    }
  }

  render() {

    var btnDone = (this.state.selectedLocation) ? <TouchableOpacity
      onPress={() => this._doneBtn()}
      style={[styles.bubble, styles.button]}>
      <View>
        <AppText>{STRINGS.COMMON.BTN_DONE}</AppText>
      </View>
    </TouchableOpacity> : null

    var btnJump = (this.state.selectedLocation) ? <TouchableOpacity
      onPress={() => this.map.animateToCoordinate(this.getMarkerCoord())}
      style={[styles.bubble, styles.button]}>
      <View>
        <AppText>{STRINGS.MAP_PAGE.JUMP_TO_MARKER}</AppText>
      </View>
    </TouchableOpacity> : null

    var pleseSelect = (!this.state.selectedLocation) ? <View style={[styles.bubble, styles.latlng]}>
      <AppText style={{textAlign: 'center'}}>
        {STRINGS.MAP_PAGE.SELECT_LOCATION_LBL}
      </AppText>
    </View> : null

    var mapMarker = (this.state.showMarker) ?
      <MapView.Marker
        identifier="Marker1"
        coordinate={this.state.marker.latlng}/> : null

    return (
      <View>
        <Topbar {...this.props} searchGPS={this.openSearchGPS.bind(this)} navigator={this.props.navigator}
                title={STRINGS.MAP_PAGE.SELECT_LOCATION_TITLE}/>
        <View style={styles.container}>
          <MapView
            ref={ref => {
              this.map = ref;
            }}
            style={styles.map}
            region={this.state.region}
            initialRegion={this.state.mapRegion}
            loadingEnabled
            loadingIndicatorColor={"#666666"}
            loadingBackgroundColor={"#eeeeee"}
            onRegionChangeComplete={region => this.onRegionChange(region)}
            onPress={(e) => this._onMapPress(e)}>
            {mapMarker}
          </MapView>
          {pleseSelect}
          <View style={styles.buttonContainer}>
            {btnJump}
            {btnDone}
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  bubble: {
    flexGrow: 1,
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 10,
    height: 40,
    borderRadius: 20,
    marginVertical: 20,
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 0,
    backgroundColor: 'transparent',
  },
  container: {
    flexGrow: 1,
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: height - 135
  },
  latlng: {
    width: 200,
    alignItems: 'stretch',
  },
});
