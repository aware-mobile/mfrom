import React, {Component} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Image,
  Dimensions,
  AsyncStorage,
  TouchableHighlight,
  ActivityIndicator,
  LayoutAnimation,
  Keyboard,
  UIManager,
  Platform,
  ScrollView,
  TouchableWithoutFeedback,
  Linking
} from 'react-native';

import FCM from 'react-native-fcm';

import dismissKeyboard from 'react-native-dismiss-keyboard';
import DFService from './Service/DFService';
import md5 from 'md5';
import apiConfig from './Util/Config';
import DeviceInfo from 'react-native-device-info';

import Util from './Util/Util';
import AppText from './Util/AppText';
import STRINGS from './Util/strings';

let self;

let {height, width} = Dimensions.get('window');

let Ionicons = require('react-native-vector-icons/Ionicons');
//var {Icon,} = require('react-native-icons');

let TextLines = React.createClass({
  render: function () {
    let lines = this.props.lines;

    let br = lines.map(function (line) {
      return (<span>{line}<br/></span>);
    });
    return (<div>{br}</div>);
  }
});

class ChangePasswordPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      currentPasswordInput: '',
      currentPassword: '',
      newPasswordInput: '',
      newPassword: '',
      conNewPasswordInput: '',
      conNewPassword: '',
      message: '',
      errorMessage: this.props.errorMessage,
      deviceToken: this.props.deviceToken,
      keyboardHeight: 0,
      logo: {},
      containerFooter: {},
    };
    this.email = '';
    self = this;

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental &&
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow)
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide)
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = (e) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    self.setState({
      keyboardHeight: 100,
      containerFooter: {paddingTop: 5},
      logo: {width: 80, height: 94}
    })
  };

  _keyboardDidHide = (e) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    self.setState({
      keyboardHeight: 0,
      containerFooter: {},
      logo: {}
    })
  };

  render() {
    return (
      <View style={{flexGrow: 1}}>
        <TouchableWithoutFeedback onPress={() => {
          dismissKeyboard();
        }}>
          <Image source={require('./img/backgroup.png')} style={styles.containerBG}>
            <View style={styles.container}>

              <View style={[styles.containerFooter, this.state.containerFooter]}>

                <View style={styles.textLoginContainer}>

                  <Image source={require('./img/login_lock.png')} style={styles.textLoginIcon}/>
                  <TextInput
                    style={styles.input}
                    value={this.state.currentPassword}
                    onChangeText={(text) => this.setState({
                      currentPasswordInput: text,
                      currentPassword: text,
                      message: ''
                    })}
                    placeholder={STRINGS.USER_DATA.CURRENT_PASSWORD}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    secureTextEntry={true}/>
                </View>

                <View style={styles.textInputUnderline}/>

                <AppText style={styles.messageErr}>
                  {this.state.curPassMsg}
                </AppText>

                <View style={styles.textLoginContainer}>

                  <Image source={require('./img/login_lock.png')} style={styles.textLoginIcon}/>
                  <TextInput
                    style={styles.input}
                    placeholder={STRINGS.USER_DATA.NEW_PASSWORD}
                    value={this.state.newPassword}
                    onChangeText={(text) => this.setState({newPasswordInput: text, newPassword: text, message: ''})}
                    secureTextEntry={true}
                    underlineColorAndroid='rgba(0,0,0,0)'/>
                </View>

                <View style={styles.textInputUnderline}/>

                <AppText style={styles.messageErr}>
                  {this.state.newPasswordMsg}
                </AppText>

                <View style={styles.textLoginContainer}>

                  <Image source={require('./img/login_lock.png')} style={styles.textLoginIcon}/>
                  <TextInput
                    style={styles.input}
                    placeholder={STRINGS.USER_DATA.CONFIRM_PASSWORD}
                    value={this.state.conNewPassword}
                    onChangeText={(text) => this.setState({
                      conNewPasswordInput: text,
                      conNewPassword: text,
                      message: ''
                    })}
                    secureTextEntry={true}
                    underlineColorAndroid='rgba(0,0,0,0)'/>
                </View>

                <View style={styles.textInputUnderline}/>

                <AppText style={styles.messageErr}>
                  {this.state.conNewPasswordMsg}
                </AppText>

                <TouchableHighlight
                  style={styles.button}
                  underlayColor='#B2D234'
                  onPress={this.onSavePressed.bind(this)}>
                  <View>
                    <AppText style={styles.buttonText}>{STRINGS.POPUP_MSG.BTN_SAVE}</AppText>
                  </View>
                </TouchableHighlight>

                <AppText style={styles.messageErr}>
                  {this.state.message}
                </AppText>
              </View>

              <View style={{height: this.state.keyboardHeight}}/>

            </View>
            <View style={{height: this.state.keyboardHeight}}/>
          </Image>
        </TouchableWithoutFeedback>
      </View>)
  }

  onSavePressed() {

    this.setState({errorMessage: ''});

    dismissKeyboard();
    // console.log('Save Pressed');
    // console.log('current password: ' + this.state.currentPasswordInput);
    // console.log('new password: ' + this.state.newPasswordInput);
    // console.log('confirm new password: ' + this.state.conNewPasswordInput);

    // console.log('Save Pressed');
    // console.log('current password: ' + md5(this.state.currentPasswordInput));
    // console.log('new password: ' + md5(this.state.newPasswordInput));
    // console.log('confirm new password: ' + md5(this.state.conNewPasswordInput));

    let apiData = {
      oldPwd: md5(this.state.currentPasswordInput),
      newPwd: md5(this.state.newPasswordInput),
      channel: "app"
    };

    if (this.state.currentPasswordInput === '' || this.state.currentPasswordInput === undefined
      || this.state.newPasswordInput === '' || this.state.newPasswordInput === undefined
      || this.state.conNewPasswordInput === '' || this.state.conNewPasswordInput === undefined) {
      if (this.state.currentPasswordInput === '' || this.state.currentPasswordInput === undefined) {
        this.setState({curPassMsg: STRINGS.VALIDATION_MESSAGES.PASS_CURRENT_EMPTY});
      } else {
        this.setState({curPassMsg: ""});
      }
      if (this.state.newPasswordInput === '' || this.state.newPasswordInput === undefined) {
        this.setState({newPasswordMsg: STRINGS.VALIDATION_MESSAGES.PASS_NEW_EMPTY});
      } else {
        this.setState({newPasswordMsg: ""});
      }

      if (this.state.conNewPasswordInput === '' || this.state.conNewPasswordInput === undefined) {
        this.setState({conNewPasswordMsg: STRINGS.VALIDATION_MESSAGES.PASS_CONFIRM_EMPTY});
      } else {
        this.setState({conNewPasswordMsg: ""});
      }
    } else {

      this.setState({curPassMsg: ""});
      this.setState({newPasswordMsg: ""});
      this.setState({conNewPasswordMsg: ""});
      this.setState({message: ""});

      if (!Util.validatePassword(this.state.newPasswordInput)) {
        this.setState({message: STRINGS.VALIDATION_MESSAGES.PASS_INVALID_FORMAT});
      } else if (this.state.newPasswordInput !== this.state.conNewPasswordInput) {
        this.setState({message: STRINGS.VALIDATION_MESSAGES.PASS_NOT_MATCH});
      } else {
        self.props.toggleMainSpinner(true);

        var config = JSON.parse(JSON.stringify(apiConfig.changePasswordMyself));

        DFService.callApiWithHeader(apiData, config, function (error, response) {
          if (response) {
            console.log(response);
            if (response.resCode === 'DF2000000') {
              // Successfull Response
              self.props.toggleMainSpinner(false);
              // have go to login page

              self.props.openMainModal(STRINGS.POPUP_MSG.CHANGE_PASSWORD_SUCCESS, function () {
                self.props.navigator.refs['dash'].refreshList();
                self.props.navigator.pop();
              });
            } else {
              // Failed Response
              self.props.toggleMainSpinner(false);
              self.props.openMainModal(response.resMessage)
            }
          } else {
            // Error from application calling service
            self.props.toggleMainSpinner(false);
            console.log("error = ", error);
          }
        });

      }

    }
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1
  },
  containerFooter: {
    flexGrow: 1,
    //alignItems: 'center',
    paddingTop: 30,
    marginTop: 30
  },
  containerBG: {
    flexGrow: 1,
    width: undefined,
    height: undefined,
    backgroundColor: 'transparent',
    //justifyContent: 'center',
    //alignItems: 'center'
  },
  description: {
    marginBottom: 14,
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#8cbb39'
  },

  input: {
    height: 36,
    padding: 4,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    paddingLeft: 10,
    fontSize: 21,
    borderWidth: 0,
    borderColor: '#B2D234',
    borderRadius: 8,
    color: '#49621c',
    flexGrow: 3
  },

  button: {
    width: width - 60,
    height: 36,
    flexDirection: 'row',
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    borderWidth: 1,
    borderRadius: 2,
    marginTop: 10,
    marginRight: 30,
    marginLeft: 30,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    justifyContent: 'center'
  },
  messageText: {
    textDecorationLine: 'underline',
    textDecorationStyle: 'solid',
    marginTop: 5,
    fontSize: 14,
    textAlign: 'center',
    color: 'black'
  },
  messageErr: {
    margin: 10,
    fontSize: 14,
    textAlign: 'center',
    color: 'red'
  },
  bottomImage: {
    bottom: 0,
    position: 'absolute',
    width: 350,
    height: 80
  },
  topImage: {
    top: 0,
    left: 0,
    position: 'absolute',
    width: 160,
    height: 95
  },
  logo: {
    top: 3,
    bottom: 10,
    //right: 5,
    //position: 'absolute',
    // width: 150,
    // height: 71,
    alignSelf: 'center'
  },

  textInputUnderline: {
    width: width - 60,
    height: 1.5,
    backgroundColor: '#B2D234',
    marginLeft: 30,
    marginRight: 30
  },

  textLoginContainer: {
    flexDirection: 'row',
    justifyContent: "space-between",
    alignItems: 'center'
  },
  textLoginIcon: {
    //flexGrow: 1
    width: 25,
    height: 32,
    marginTop: 8,
    marginLeft: 30

  }

});

export default ChangePasswordPage;
