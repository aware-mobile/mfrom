import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  ScrollView,
  Image,
  Text,
  TouchableHighlight,
  AsyncStorage
} from 'react-native';

import DFTextBox from './FormComponents/DFTextBox'
import Topbar from './Topbar'
import DFLabel from './FormComponents/DFLabel'
import DFErrorLabel from './FormComponents/DFErrorLabel'
import DFButton from './FormComponents/DFButton'
import ImagePicker from 'react-native-image-picker';
import apiConfig from './Util/Config';
import AppText from './Util/AppText';
import DFService from './Service/DFService';
import CreateCustomerContact from './CreateCustomerContact'
import STRINGS from './Util/strings'

let Ionicons = require('react-native-vector-icons/Ionicons');
let Icon = require('react-native-vector-icons/FontAwesome');

export default class CustomerDetailPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDelete: false
    };
    //console.log(this.props.data);
    this.checkUser();
  }

  render() {
    let phoneList = [];
    let phones = this.props.data.telephone.join(", ");

    if (this.props.data.telephone.length > 0) {
      phoneList.push(<AppText key={"phone"} style={styles.dropdownBtnTxt}>{phones}</AppText>);
    } else {
      phoneList.push(<AppText style={styles.dropdownBtnTxt}>-</AppText>);
    }

    let editView = null;
    if (this.state.isDelete) {
      editView =
        <View style={{
          flexDirection: 'row', justifyContent: 'flex-end',
          alignItems: 'flex-end', marginTop: 5, marginRight: 5
        }}>
          <TouchableHighlight
            style={styles.button}
            onPress={() => this.openEditCustomer(this.props.data)}
            underlayColor={'#B2D234'}>
            <Icon name="edit" size={20} color="white"
                  style={{justifyContent: 'center', alignItems: 'center'}}/>
          </TouchableHighlight>
          <TouchableHighlight
            style={styles.buttonDelete}
            onPress={this.deleteCustomer.bind(this, this.props.data._id)}
            underlayColor={'#DC6565'}>
            <Ionicons name="md-trash" size={20} color="white"
                      style={{justifyContent: 'center', alignItems: 'center'}}/>
          </TouchableHighlight>
        </View>;
    }
    let formView =
      <View>
        {editView}
        <DFLabel fontWeight={'bold'} key={"name"} text={STRINGS.USER_DATA.NAME_CUSTOMER} color="black" isRequired={false}/>
        <View style={styles.borderView}>
          <AppText style={styles.input}>
            {this.props.data.name}
          </AppText>
        </View>
        <DFLabel fontWeight={'bold'} key={"email"} text={STRINGS.USER_DATA.EMAIL} color="black" isRequired={false}/>
        <View style={styles.borderView }>
          <AppText style={styles.input}>
            {(this.props.data.email) ? this.props.data.email : '-'}
          </AppText>
        </View>
        <DFLabel fontWeight={'bold'} key={"phone"} text={STRINGS.USER_DATA.TELEPHONE} color="black" isRequired={false}/>
        <View style={{paddingLeft: 20, paddingBottom: 5}}>
          {phoneList}
        </View>

        <DFLabel fontWeight={'bold'} key={"address"} text={STRINGS.USER_DATA.ADDRESS} color="black" isRequired={false}/>
        <View style={styles.borderView}>
          <AppText style={styles.input}>
            {(this.props.data.address) ? this.props.data.address : '-'}
          </AppText>
        </View>

        <DFLabel fontWeight={'bold'} key={"city"} text={STRINGS.USER_DATA.CITY} color="black" isRequired={false}/>
        <View style={styles.borderView}>
          <AppText style={styles.input}>
            {(this.props.data.province) ? this.props.data.province.province : '-'}
          </AppText>
        </View>
        <DFLabel fontWeight={'bold'} key={"district"} text={STRINGS.USER_DATA.DISTRICT} color="black" isRequired={false}/>
        <View style={styles.borderView}>
          <AppText style={styles.input}>
            {(this.props.data.district) ? this.props.data.district.amphur : '-'}
          </AppText>
        </View>
        <DFLabel fontWeight={'bold'} key={"subdistrict"} text={STRINGS.USER_DATA.SUB_DISTRICT} color="black" isRequired={false}/>
        <View style={styles.borderView}>
          <AppText style={styles.input}>
            {(this.props.data.sub_district) ? this.props.data.sub_district.district : '-'}
          </AppText>
        </View>
        <DFLabel fontWeight={'bold'} key={"postalcode"} text={STRINGS.USER_DATA.POSTAL_CODE} color="black" isRequired={false}/>
        <View style={styles.borderView}>
          <AppText style={styles.input}>
            {(this.props.data.postal_code) ? this.props.data.postal_code : '-'}
          </AppText>
        </View>

      </View>;

    return (
      <View style={{flexGrow: 1, backgroundColor: '#F5F4F7'}}>
        <ScrollView
          contentContainerStyle={{paddingVertical: 20}}
          ref={(scrollView) => {
            _scrollView = scrollView;
          }}
          automaticallyAdjustContentInsets={true}
          onScroll={() => {
            console.log('onScroll!');
          }}
          scrollEventThrottle={200}>

          <View>
            {formView}

            <View style={{height: 40}}>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }


  async checkUser() {
    let value = await AsyncStorage.getItem("Profile");
    let profile = await JSON.parse(value);
    console.log(profile.user.capabilities);
    if (profile.user.capabilities.indexOf("deleteCustomers") !== -1) {
      this.setState({
        isDelete: true
      });
    }
  }

  deleteCustomer(id) {
    let self = this;
    this.props.openMainConfirm(STRINGS.POPUP_MSG.DELETE_CUSTOMER, function () {
      self.deleteCustomerRequest(id);
    });
  }


  deleteCustomerRequest(id) {
    let self = this;
    let custIDObj = {
      custID: []
    };
    custIDObj.custID.push(id);
    DFService.callApiWithHeader(custIDObj, apiConfig.deleteCustomerContact, function (error, response) {
      if (response) {
        if (response.resCode === 'DF2000000') {
          //self.props.toggleMainSpinner(false);
          self.props.openMainModal(STRINGS.POPUP_MSG.DELETE_SUCCESS, function () {
            self.props.navigator.pop();
            self.props.reloadCustomer(0);
          });
        } else {
          //self.props.toggleMainSpinner(false);
          self.props.openMainModal(response.resMessage)
        }
      } else {
        // self.props.toggleMainSpinner(false);
        self.props.openMainModal(STRINGS.COMMON.UNEXPECTED_ERROR);
      }
    });
  }

  openEditCustomer(data) {
    this.props.navigator.push({
      name: "edit_customer_contact",
      title: STRINGS.CUSTOM_MODALS.EDIT_CUSTOMER_HEADER,
      component: CreateCustomerContact,
      passProps: {
        navigator: this.props.navigator,
        // onBack: this.props.onBack.bind(this),
        isEdit: true,
        data: data,
        // logout: this.logout.bind(this),
        refreshCustomer: this.props.refreshCustomer.bind(this),
        openMainModal: this.props.openMainModal.bind(this),
        openMainConfirm: this.props.openMainConfirm.bind(this)
      }
    });
  }
}

const styles = StyleSheet.create({
  input: {
    fontSize: 14,
    borderWidth: 0,
    color: '#333435',
    paddingTop: 2,
    paddingBottom: 2
  },
  inputPhone: {
    width: 150,
    height: 30,
    fontSize: 14,
    borderWidth: 0,
    color: '#333435',
    paddingTop: 2,
    paddingBottom: 2
  },
  borderView: {
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  borderViewDisable: {
    borderColor: '#c0c0c0',
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  disableInput: {
    fontSize: 14,
    borderWidth: 0,
    color: '#cdcdcd',
    paddingTop: 5,
    paddingBottom: 5,
  },
  disableInputNoText: {
    height: 25,
    fontSize: 14,
    borderWidth: 0,
    color: '#cdcdcd',
    paddingTop: 5,
    paddingBottom: 5,
  },
  dropdownBtn: {
    height: 30,
    flexDirection: 'row',
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 2,
    marginLeft: 2,
    paddingLeft: 8,
    paddingRight: 8,
    borderRadius: 18,
  },
  dropdownBtnTxt: {
    color: '#333435',
    marginRight: 4,
    fontSize: 14
  },
  viewMode: {
    minHeight: 25,
    fontSize: 14,
    borderWidth: 0,
    color: '#606060',
    textAlignVertical: 'center',
    paddingTop: 5,
    paddingBottom: 5,
  },
  circle: {
    width: 120,
    height: 120
  },
  circleImage: {
    margin: 10,
    width: 100,
    height: 100,
    borderRadius: 100 / 2,
    position: 'absolute',
  },
  image: {
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  listAssign: {
    paddingLeft: 8,
    paddingRight: 8,
    flexGrow: 1,
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  button: {
    height: 30,
    width: 30,
    paddingTop: 2,
    paddingBottom: 2,
    margin: 5,
    backgroundColor: '#B2D234',
    borderColor: '#B2D234',
    borderWidth: 1,
    borderRadius: 4,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonDelete: {
    height: 30,
    width: 30,
    paddingTop: 2,
    paddingBottom: 2,
    margin: 5,
    backgroundColor: '#DC6565',
    borderColor: '#DC6565',
    borderWidth: 1,
    borderRadius: 4,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonClose: {
    height: 30,
    width: 30,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center'
  },
});
