import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableHighlight,
  ActivityIndicator,
  Platform,
  NativeModules,
  Alert,
  AppState,
  NetInfo,
  Keyboard,
  LayoutAnimation,
  UIManager
} from 'react-native';

import Topbar from './Topbar'
import CustomerContact from './CustomerContact'
import UserContact from './UserContact'
import CreateCustomerContact from './CreateCustomerContact'
import STRINGS from './Util/strings'
import AppText from './Util/AppText';

class MainContact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoad: true,
      isRefresh: false,
      isCustomerContact: true,
      isReloadCustomerContact: false
    }
  }

  render() {
    var renderContact = null
    var renderHeaderContact = null
    if (this.state.isLoad) {
      renderHeaderContact =
        <View style={styles.tabbar}>
          <TouchableHighlight
            onPress={() => this.tabPress(0)}
            style={styles.tabView}
            underlayColor='#dddddd'>
            <View style={{flexGrow: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
              <Image source={require('./img/tobbarUser.png')} style={styles.navBtnImage}/>
              <View style={{flexDirection: 'column', alignItems: 'flex-start', paddingLeft: 5}}>
                <AppText style={{fontSize: 14, color: 'white'}}>{STRINGS.CUSTOMER_CONTACT_PAGE.CUSTOMER}</AppText>
                <AppText style={{fontSize: 14, color: 'white'}}>{STRINGS.CUSTOMER_CONTACT_PAGE.CONTACT}</AppText>
              </View>
            </View>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => this.tabPress(1)}
            style={styles.tabView}
            underlayColor='#dddddd'>
            <View style={{flexGrow: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
              <Image source={require('./img/tobbarUser.png')} style={styles.navBtnImage}/>
              <View style={{flexDirection: 'column', alignItems: 'flex-start', paddingLeft: 5}}>
                <AppText style={{fontSize: 14, color: 'white'}}>{STRINGS.CUSTOMER_CONTACT_PAGE.USER}</AppText>
                <AppText style={{fontSize: 14, color: 'white'}}>{STRINGS.CUSTOMER_CONTACT_PAGE.CONTACT}</AppText>
              </View>
            </View>
          </TouchableHighlight>
        </View>

      renderContact = this.state.isCustomerContact ?
        <CustomerContact toggleMainSpinner={this.props.toggleMainSpinner.bind(this)} navigator={this.props.navigator}
                         ref="CustomerContact" refreshCustomer={this.refreshCustomer.bind(this)}
                         openMainModal={this.props.openMainModal} openMainConfirm={this.props.openMainConfirm}/> :
        <UserContact ref="UserContact" refreshUser={this.refreshUser.bind(this)}
                     toggleMainSpinner={this.props.toggleMainSpinner.bind(this)} navigator={this.props.navigator}
                     openMainModal={this.props.openMainModal} openMainConfirm={this.props.openMainConfirm}/>
    }

    var renderTopBar = this.state.isCustomerContact ? <Topbar
      {...this.props}
      isCustomerContact={true}
      title={STRINGS.CUSTOMER_CONTACT_PAGE.HEADER}
      ref="changeStateTopBar"
      refreshCustomer={this.refreshCustomer.bind(this)}
      customerContact={this.addCustomerContact.bind(this)}/>
      : <Topbar
        {...this.props}
        isCustomerContact={false}
        refreshUser={this.refreshUser.bind(this)}
        ref="changeStateTopBar"
        title={STRINGS.CUSTOMER_CONTACT_PAGE.HEADER_2}/>


    // if (this.state.isCustomerContact) {

    // } else {
    //     renderContact = null
    // }

    return (
      <View style={{flexGrow: 1}}>
        {renderTopBar}

        {renderHeaderContact}
        <View style={{flexDirection: 'row'}}>
          <View style={[styles.indicator, {backgroundColor: this.state.isCustomerContact ? 'black' : '#B2D234'}]}/>
          <View style={[styles.indicator, {backgroundColor: !this.state.isCustomerContact ? 'black' : '#B2D234'}]}/>
        </View>
        {renderContact}
      </View>
    )
  }

  refreshUser() {
    this.refs.UserContact.initUserContact(0, false)
  }

  refreshCustomer() {
    this.refs.CustomerContact.initCustomerContact(0, false)
  }

  addCustomerContact() {
    this.props.navigator.push({
      name: "create_customer_contact",
      title: STRINGS.CUSTOMER_CONTACT_PAGE.CREATE_TITLE,
      component: CreateCustomerContact,
      passProps: {
        navigator: this.props.navigator,
        onBack: this.props.onBack.bind(this),
        // logout: this.logout.bind(this),
        refreshCustomer: this.refreshCustomer.bind(this),
        openMainModal: this.props.openMainModal.bind(this),
        openMainConfirm: this.props.openMainConfirm.bind(this)
      }
    });

    console.log('press add CustomerContact')
  }


  updateLoadView() {
    this.setState({
      isLoadingSuccess: true
    })
  }

  tabPress(type) {
    if (type == 0) {
      this.setState({
        isCustomerContact: true
      })
      this.refs.changeStateTopBar.changeStateCustomerContact()
    } else {
      this.setState({
        isCustomerContact: false
      })
      this.refs.changeStateTopBar.changeStateUserContact()
    }
  }
}

const styles = StyleSheet.create({
  tabbar: {
    height: 45,
    flexDirection: 'row',
    backgroundColor: '#B2D234'
  },
  tabView: {
    flexDirection: 'row',
    flexGrow: 0.5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  indicator: {
    height: 2,
    backgroundColor: 'black',
    flexDirection: 'row',
    flexGrow: 0.5
  },
  navBtnImage: {
    width: 25,
    height: 25
  },
})

export default MainContact;
