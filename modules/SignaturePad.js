import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableHighlight,
  ActivityIndicator,
  ScrollView,
  Keyboard,
  Dimensions,
  Platform,
  NativeModules,
  AsyncStorage,
  Alert,
  NetInfo,
  AppState,
  InteractionManager,
  LayoutAnimation
} from 'react-native';

import SignatureCapture from 'react-native-signature-capture';
import STRINGS from './Util/strings'
import AppText from './Util/AppText';
let self;

class SignaturePad extends Component {
  constructor(props) {
    super(props);
    this.isNotDrag = true;
    this.state = {
      isNotDrag: this.isNotDrag
    };
    self = this;
  }

  render() {
    let btnColor = {backgroundColor: "gray", borderColor: "gray"};
    if (!self.state.isNotDrag) {
      btnColor = {backgroundColor: "#B2D234", borderColor: "#B2D234"};
    }

    return (
      <View style={{flex: 1, flexDirection: "column"}}>
        <SignatureCapture
          style={styles.signature}
          ref="sign"
          onSaveEvent={this._onSaveEvent.bind(this)}
          onDragEvent={this._onDragEvent.bind(this)}
          saveImageFileInExtStorage={true}
          showNativeButtons={false}
          viewMode={"portrait"}/>

        <View style={{flexDirection: "row", height: 70}}>

          <TouchableHighlight
            style={[styles.buttonStyle, {backgroundColor: "#DC6565", borderColor: "#DC6565"}]}
            onPress={() => {
              this.resetSign()
            } }>
            <View>
              <AppText style={{color: '#fff'}}>{STRINGS.COMMON.BTN_RESET}</AppText>
            </View>
          </TouchableHighlight>

          <TouchableHighlight
            style={[styles.buttonStyle, btnColor]}
            onPress={() => {
              this.saveSign()
            } }>
            <View>
              <AppText style={{color: '#fff'}}>{STRINGS.COMMON.BTN_DONE}</AppText>
            </View>
          </TouchableHighlight>

        </View>

      </View>)
  }

  saveSign() {
    this.refs["sign"].saveImage();
  }

  resetSign() {
    self.isNotDrag = true;
    self.setState({
      isNotDrag: self.isNotDrag
    });
    this.refs["sign"].resetImage();
  }

  _onSaveEvent(result) {
    if (!self.isNotDrag) {
      self.isNotDrag = true;
      self.props.setPathUri(result.pathName);
      self.props.navigator.pop();
    }
  }

  _onDragEvent() {
    console.log("dragged");
    self.isNotDrag = false;
    self.setState({
      isNotDrag: self.isNotDrag
    });

  }
}

const styles = StyleSheet.create({
  signature: {
    flexGrow: 1,
    borderColor: '#000033',
    borderWidth: 1,
  },
  buttonStyle: {
    flexGrow: 1, justifyContent: "center", alignItems: "center", height: 50,
    margin: 10
  }
});

export default SignaturePad;
