import React, { Component } from 'react';
import {
  StyleSheet,
  View
} from 'react-native';

import dismissKeyboard from 'react-native-dismiss-keyboard';
import SignaturePad from 'react-native-signature-pad';

export default class SignPage extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={{ flexGrow: 1 }}>
        <SignaturePad onError={this._signaturePadError}
          onChange={this._signaturePadChange}
          style={{ flexGrow: 1, backgroundColor: 'white' }}/>
      </View>
    );
  }

  _signaturePadError(error) {
    console.error(error);
  }

  _signaturePadChange(base64DataUrl) {
    console.log("Got new signature: " + base64DataUrl);
  }
}
