import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableHighlight,
  ListView,
  ActivityIndicator,
  AsyncStorage,
  NativeModules,
  Alert,
  NetInfo,
  Dimensions
} from 'react-native';

import Color from './Util/Color';
import WorkingDetail from './WorkingDetail'
import Topbar from './Topbar'
import DFService from './Service/DFService';
import apiConfig from './Util/Config';
import Util from './Util/Util';
import STRINGS from './Util/strings';
import AppText from './Util/AppText';
import AlertConfirmDialog from './Util/AlertConfirmDialog';
import FCM from 'react-native-fcm';

var {height, width} = Dimensions.get('window');

var Icon = require('react-native-vector-icons/FontAwesome');

let dataSource = new ListView.DataSource({
  rowHasChanged: (r1, r2) => r1 !== r2,
  sectionHeaderHasChanged: (s1, s2) => s1 !== s2
});

class NotiPage extends Component {
  constructor(props) {
    super(props);

    this.formList = [];
    this.formSize = 0;

    this.state = {
      isLoading: true,
      message: '',
      lastUpdate: '',
      dataSource: dataSource.cloneWithRowsAndSections([{}]),
      activeModal: false,
      submitModal: false,
      createWorkData: {},
      isConnected: true,
      emptyList: false
    };

    this.getNotiList();
  }

  componentDidMount() {
    this.notificationUnsubscribe = FCM.on('notification', (notif) => {
      if (notif) {
        this.getNotiList();
      }
    });

    NetInfo.isConnected.addEventListener(
      'change',
      this._handleConnectivityChange
    );
    NetInfo.isConnected.fetch().done(
      (isConnected) => {
        this.setState({isConnected});
      }
    );
  }

  componentWillUnmount() {
    this.notificationUnsubscribe();

    NetInfo.isConnected.removeEventListener(
      'change',
      this._handleConnectivityChange
    );
  }

  _handleConnectivityChange = (isConnected) => {
    this.setState({
      isConnected,
    });
    if (isConnected) {
      this.refreshList();
    }
  };

  rowPressed(formData, sectionID, rowID) {
    this.deleteNoti(formData._id);
    this.props.navigator.push({
      name: formData.form_transaction_id.user_form_id.template_name,
      title: formData.form_transaction_id.user_form_id.template_name,
      component: WorkingDetail,
      passProps: {
        formData: formData.form_transaction_id,
        status: formData.status,
        step: formData.form_transaction_id.step,
        refreshWorkingList: this.props.refreshWorkingList.bind(this),
        refreshNoti: this.refreshList.bind(this),
        logout: this.props.logout.bind(this),
        onBack: this._onBack.bind(this),
        fromNotification: true
      }
    });
  }

  _onBack = (popToTop = false) => {

    let routes = this.props.navigator.getCurrentRoutes();
    let currentRouteName = routes[routes.length - 1].name;

    try {
      this.props.navigator.refs[currentRouteName].onBackPressed(popToTop)
    } catch (error) {
      if (popToTop) {
        this.props.navigator.popToTop(0)
      } else {
        this.props.navigator.pop()
      }
    }

  };

  refreshList() {
    this.setState({
      isLoading: true,
      message: '',
      dataSource: dataSource.cloneWithRowsAndSections([{}])
    });
    this.getNotiList();
  }

  renderRow(rowData, sectionID, rowID) {
    console.log(rowData);
    let createdDate = new Date(rowData.createdDate);
    let createdDateStr = Util.formatDate(createdDate);
    let createdBy_name = this.getName(rowData)

    return (
      <View style={styles.box}>
        <TouchableHighlight
          onPress={() => this.rowPressed(rowData, sectionID, rowID)}
          underlayColor='#dddddd'
          style={[{backgroundColor: '#ffffff'}]}>
          <View>
            <View style={styles.rowContainer}>

              <View
                style={[styles.circle, {backgroundColor: (rowData.form_transaction_id.user_form_id != null) ? rowData.form_transaction_id.user_form_id.category_id.color : 'black'}]}>
                <Image source={require('./img/wl_transaction_icon.png')} style={styles.listImage}/>
              </View>
              <View style={{flexDirection: 'column', paddingLeft: 10, width: width - 135}}>
                <AppText numberOfLines={1} style={{
                  fontSize: 13,
                  color: '#1C1D21',
                  fontWeight: 'bold'
                }}>{rowData.form_transaction_id.template_name}</AppText>
                <AppText style={{fontSize: 10, color: '#B2D234'}}>{STRINGS.COMMON.BY} {createdBy_name}
                  , {rowData.sinceTime}</AppText>
              </View>
              <Image source={this.getImgStatus(rowData.status)}
                     style={[styles.status, {tintColor: this.getColorStatus(rowData.status)}]}>
                <View style={styles.backdropView}>
                  <AppText style={styles.headline}>{this.getStatus(rowData.status)}</AppText>
                </View>
              </Image>
            </View>
          </View>
        </TouchableHighlight>
      </View>
    );
  }

  getName(rowData) {
    if ('createdBy' in rowData && rowData.createdBy != null) {
      return rowData.createdBy.displayName;
    } else if ('createdByCustomerToken' in rowData && rowData.createdByCustomerToken != null) {
      if ('customer_email' in rowData.createdByCustomerToken) {
        return rowData.createdByCustomerToken.customer_email;
      } else {
        return rowData.createdByCustomerToken.customer_id.name;
      }
    } else {
      return '-';
    }
  }

  renderSectionHeader(sectionData, category) {
    return (
      <AppText style={{fontWeight: "700", padding: 4}}>{category}</AppText>
    )
  }

  render() {

    var loadView;

    var loadingView = <View style={styles.spinner}>
      <ActivityIndicator
        hidden='true'
        size='large'/>
      <AppText>{STRINGS.COMMON.LOADING}</AppText>
    </View>

    let empty_list = <View style={{flexGrow: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Image source={require('./img/empty_list_logo.png')} style={{width: 160, resizeMode: 'contain'}}/>
      <Text style={{
        fontSize: 28,
        marginTop: 5,
        fontFamily: 'DB Heavent',
        textAlign: 'center',
        paddingLeft: 10,
        paddingRight: 10
      }}>{STRINGS.NOTIFICATION_PAGE.NO_NOTI}</Text>
    </View>

    var innerView = null

    if (!this.state.isConnected) {
      innerView = <View style={{flexGrow: 1, justifyContent: 'center', alignItems: 'center'}}>
        <View style={{width: 150, height: 150, justifyContent: 'center', alignItems: 'center'}}>
          <Icon
            name='exclamation-triangle'
            size={80}
            color='black'
          />
        </View>
        <AppText style={styles.noInternetText}>{STRINGS.COMMON.NO_INTERNET}</AppText>
      </View>
    } else {
      if (this.state.isLoading) {
        innerView = loadingView
      } else {
        if (this.state.message != '') {
          innerView = <View style={styles.container}>
            <AppText>{this.state.message}</AppText>
          </View>
        } else {
          if (this.state.emptyList) {
            innerView = empty_list
          } else {
            innerView = <ListView
              dataSource={this.state.dataSource}
              renderRow={this.renderRow.bind(this)}
              renderSectionHeader={this.renderSectionHeader.bind(this)}
              enableEmptySections={true}/>
          }
        }
      }
    }

    loadView = <View style={{flexGrow: 1}}>
      <Topbar {...this.props} navigator={this.props.navigator} title={STRINGS.COMMON.NOTI_HEADER}
              onClear={this.clear.bind(this)}/>
      {innerView}
    </View>

    return (<View style={{flexGrow: 1, backgroundColor: '#F7F6F9'}}>
      {loadView}
      {this.state.submitModal ?
        <AlertConfirmDialog confrimMessage={STRINGS.NOTIFICATION_PAGE.CONFIRM_CLEAR}
                            closeModalFail={this.closeModalFail.bind(this)}
                            closeModalSuccess={this.closeModalSuccess.bind(this)}/> : null}

    </View>)
  }


  clear() {
    if (this.formSize > 0) {
      this.setState({submitModal: true});
    }
  }

  closeModalFail() {
    this.setState({submitModal: false});
  }

  closeModalSuccess() {
    this.setState({submitModal: false});
    this.clearNotiList();
  }

  clearNotiList() {
    let self = this;
    let apiData = {};
    DFService.callApiWithHeader(apiData, apiConfig.clearNotiForm, function (error, response) {

      if (response) {
        if (response.resCode == 'DF2000000') {
          // Successfull Response
          self.formList = [];
          self.formSize = 0;

          self.setState({
            isLoading: false,
            message: '',
            emptyList: true,
            dataSource: dataSource.cloneWithRowsAndSections([{}]),
          });
        } else {
          // Failed Response
          self.props.openMainModal(response.resMessage)
        }
      } else {
        // Error from application calling service
        console.log("error = ", error);
      }

    });
  }

  deleteNoti(id) {
    let apiData = {};
    let config = {
      method: apiConfig.deleteNoti.method,
      url: apiConfig.deleteNoti.url
    };
    config.url += id;
    DFService.callApiWithHeader(apiData, config, function (error, response) {
      if (response) {
        console.log("success = ", response);
      } else {
        console.log("error = ", error);
      }

    });
  }

  getNotiList() {
    let self = this;
    let apiData = {};
    DFService.callApiWithHeader(apiData, apiConfig.getNotiForm, function (error, response) {

      if (response) {
        if (response.resCode == 'DF2000000') {
          // Successfull Response
          self.successNoti(response);
        } else {
          // Failed Response
          self.props.openMainModal(response.resMessage)
        }
      } else {
        // Error from application calling service
        console.log("error = ", error);
      }

    });
  }

  getUpdate(name) {
    if (name != null) {
      return name.firstName;
    } else {
      return '-';
    }
  }

  getImgStatus(status) {
    console.log("Status = ", status);
    if (status == "NEW") {
      return require('./img/iconNew.png')
    } else if (status == "REJECTED") {
      return require('./img/wl_rejected_flag.png')
    } else if (status == "INPROGRESS") {
      return require('./img/wl_inprogress_flag.png')
    } else if (status == "EXPIRED") {
      return require('./img/wl_expired_flag.png')
    } else if (status == "COMPLETED") {
      return require('./img/wl_completed_flag.png')
    } else if (status == "CANCELED") {
      return require('./img/wl_cancled_flag.png')
    }
  }

  getColorStatus(status) {
    console.log("Status = ", status);
    if (status == "NEW" && this.activeTab !== 'draftList') {
      return Color.ribbon_new;
    } else if (status == "REJECTED" && this.activeTab !== 'draftList') {
      return Color.ribbon_reject;
    } else if (status == "INPROGRESS" && this.activeTab !== 'draftList') {
      return Color.ribbon_inprogress;
    } else if (status == "EXPIRED" && this.activeTab !== 'draftList') {
      return Color.ribbon_expire;
    } else if (status == "COMPLETED" && this.activeTab !== 'draftList') {
      return Color.ribbon_complete;
    } else if (status == "CANCELED" && this.activeTab !== 'draftList') {
      return Color.ribbon_cancel;
    } else {

    }
  }

  getStatus(status) {
    console.log("Status = ", status);
    if (status == "NEW" && this.activeTab !== 'draftList') {
      return 'New';
    } else if (status == "REJECTED" && this.activeTab !== 'draftList') {
      return 'Reject';
    } else if (status == "INPROGRESS" && this.activeTab !== 'draftList') {
      return 'Inprogress';
    } else if (status == "EXPIRED" && this.activeTab !== 'draftList') {
      return 'Expire';
    } else if (status == "COMPLETED" && this.activeTab !== 'draftList') {
      return 'Complete';
    } else if (status == "CANCELED" && this.activeTab !== 'draftList') {
      return 'Cancel';
    }
  }

  timeSince(date) {

    if (typeof date !== 'object') {
      date = new Date(date);
    }

    let seconds = Math.floor((new Date() - date) / 1000);
    let intervalType;

    let interval = Math.floor(seconds / 31536000);
    if (interval >= 1) {
      intervalType = 'year';
    } else {
      interval = Math.floor(seconds / 2592000);
      if (interval >= 1) {
        intervalType = 'month';
      } else {
        interval = Math.floor(seconds / 86400);
        if (interval >= 1) {
          intervalType = 'day';
        } else {
          interval = Math.floor(seconds / 3600);
          if (interval >= 1) {
            intervalType = "hour";
          } else {
            interval = Math.floor(seconds / 60);
            if (interval >= 1) {
              intervalType = "minute";
            } else {
              interval = seconds;
              intervalType = "second";
            }
          }
        }
      }
    }

    if (interval > 1 || interval === 0) {
      intervalType += 's';
    }

    return interval + ' ' + intervalType;
  }

  days_between(date1, date2) {

    // The number of milliseconds in one day
    let ONE_DAY = 1000 * 60 * 60 * 24;

    // Convert both dates to milliseconds
    let date1_ms = date1.getTime();
    let date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    let difference_ms = Math.abs(date1_ms - date2_ms);

    // Convert back to days and return
    return Math.round(difference_ms / ONE_DAY)

  }

  successNoti(response) {

    if (response.resCode == 'DF2000000') {
      this.formSize = response.data.length;
      this.formList = [];
      for (let i = 0; i < response.data.length; i++) {
        //console.log(response.data[i]);
        let date = new Date(response.data[i].createdDate);
        let cDate = new Date();
        let diff = this.days_between(cDate, date);
        let txDate = STRINGS.COMMON.MORE_TWO_DAYS;
        let obj = response.data[i];
        if (obj.form_transaction_id != null) {
          let arrVariable = obj.form_transaction_id.user_form_id.template_name.match(/\$\{(.*?)\}/g);
          if (arrVariable) {
            for (let x = 0; x < arrVariable.length; x++) {
              let strFormNameVar = arrVariable[x].replace(/\$\{(.*?)\}/g, "$1")
              if (obj.form_transaction_id.form_job_id.form_data_id) {
                if (obj.form_transaction_id.form_job_id.form_data_id.form_name_variable[strFormNameVar]) {
                  obj.form_transaction_id.user_form_id.template_name = obj.form_transaction_id.user_form_id.template_name.replace("${" + strFormNameVar + "}", obj.form_transaction_id.form_job_id.form_data_id.form_name_variable[strFormNameVar]);
                } else {
                  obj.form_transaction_id.user_form_id.template_name = obj.form_transaction_id.user_form_id.template_name.replace("${" + strFormNameVar + "}", '');
                }
              } else {
                obj.form_transaction_id.user_form_id.template_name = obj.form_transaction_id.user_form_id.template_name.replace("${" + strFormNameVar + "}", '');
              }
            }
          }
          obj.form_transaction_id.template_name = obj.form_transaction_id.user_form_id.template_name
        } else {
          obj.form_transaction_id = {};
          obj.form_transaction_id.template_name = "not found"
        }

        obj.sinceTime = Util.formatDate(obj.createdDate);
        if (diff == 0) {
          txDate = STRINGS.COMMON.TODAY;
          obj.sinceTime = this.timeSince(new Date(obj.createdDate)) + ' ' + STRINGS.COMMON.AGO;
        } else if (diff == 1) {
          txDate = STRINGS.COMMON.YESTERDAY;
          obj.sinceTime = Util.formatTime(obj.createdDate);
        }

        if (!this.formList[txDate]) {
          this.formList[txDate] = [];
        }

        this.formList[txDate].push(obj);
      }

      let emptyList = (this.formSize > 0) ? false : true

      this.setState({
        isLoading: false,
        message: '',
        emptyList,
        dataSource: dataSource.cloneWithRowsAndSections(this.formList),
      });

    } else {
      this.setState({isLoading: false, message: response.resMessage});
    }
  }
}


const styles = StyleSheet.create({
  spinner: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  tabbar: {
    height: 45,
    flexDirection: 'row',
    backgroundColor: '#B2D234'
  },
  tabView: {
    flexDirection: 'row',
    flexGrow: 0.3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    flexGrow: 1,
    marginTop: 70,
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowContainer: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center'
  },
  rowContain: {
    flexDirection: 'row'
  },
  colContainer: {
    flexDirection: 'column',
    padding: 10
  },
  separator: {
    height: 1,
    backgroundColor: '#dddddd'
  },
  indicator: {
    height: 2,
    backgroundColor: 'black',
    flexDirection: 'row',
    flexGrow: 0.3
  },
  pipe: {
    width: 1,
    backgroundColor: '#dddddd'
  },
  listView: {
    marginTop: 65
  },
  listImage: {
    width: 16,
    height: 20
  },
  status: {
    alignItems: 'flex-end',
    width: 75,
    height: 20
  },
  backdropView: {
    height: 20,
    width: 75,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0)',
  },
  headline: {
    fontSize: 8,
    textAlign: 'center',
    backgroundColor: 'rgba(0,0,0,0)',
    color: 'white'
  },
  img: {
    width: 25,
    height: 25
  },
  box: {
    paddingBottom: 1,
  },
  circle: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center'
  },
  search: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  navBtnImage: {
    width: 25,
    height: 25
  },
  navBarBtn: {
    height: 35,
    marginTop: 10,
    marginRight: 15,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  rowSearch: {
    marginTop: 5,
    flexDirection: 'row',
    alignItems: 'center'
  },
  containerError: {
    flexGrow: 1,
    marginTop: 70,
    justifyContent: 'center',
    alignItems: 'center'
  },
  noInternetText: {
    marginTop: 20,
    fontSize: 22,
    textAlign: 'center'
  },
  emptyList: {
    marginTop: 20,
    fontSize: 28,
    textAlign: 'center'
  }
});

export default NotiPage;
