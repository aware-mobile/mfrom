import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Modal,
  Dimensions,
  Platform,
  ActivityIndicator
} from 'react-native'


import * as Progress from 'react-native-progress'
import STRINGS from '../Util/strings'
import AppText from '../Util/AppText'

class ProgressSpinner extends Component {

  constructor(props) {
    super(props);

    var spinnerText = 'spinnerText' in this.props ? this.props.spinnerText : STRINGS.COMMON.LOADING
    var setSuccess = 'setSuccess' in this.props ? this.props.setSuccess : false
    var isSuccessfull = 'isSuccessfull' in this.props ? this.props.isSuccessfull : false


    this.state = {
      spinnerText,
      setSuccess,
      isSuccessfull
    }

    // var marginTop = Platform.OS === 'ios' ? 0 : -100

  }

  componentWillReceiveProps(nextProps) {
    var spinnerText = 'spinnerText' in nextProps ? nextProps.spinnerText : STRINGS.COMMON.LOADING
    var setSuccess = 'setSuccess' in nextProps ? nextProps.setSuccess : false
    var isSuccessfull = 'isSuccessfull' in nextProps ? nextProps.isSuccessfull : false
    this.setState({ spinnerText, setSuccess, isSuccessfull });
  }

  render() {

    var isSuccessfull = this.state.isSuccessfull ?
    <Image source={require('../img/success.png')} style={styles.image}/>
    :
    <Image source={require('../img/error.png')} style={styles.image}/>

    var theSpinner;
    if(Platform.OS === 'ios'){
      theSpinner = <Progress.CircleSnail
        indeterminate={true}
        animating={true}
        color='#B2D234'
        size={60}
        thickness={7} />
    } else{
      theSpinner = <View style={styles.spinner}>
        <ActivityIndicator
          hidden='true'
          size='large'/>
      </View>
    }



    var output = this.state.setSuccess ? isSuccessfull : theSpinner
    return (
      <View style={styles.container}>
        <View style={[styles.innerContainer, this.isStyleAndroid() ]}>
          <View style={{alignSelf: 'center', justifyContent: 'center'}}>
            {output}
          </View>
          <View style={{alignSelf: 'center', justifyContent: 'center'}}>
            <AppText style={styles.spinnerText}>{this.state.spinnerText}</AppText>
          </View>
        </View>
      </View>
    )
  }

  isStyleAndroid() {
    if (Platform.OS !== 'ios') {
      return {
        height: 90,
        padding: 15
      }
    }else {
      return {}
    }
  }
}

const styles = StyleSheet.create({
      container: {
        ...StyleSheet.absoluteFillObject,
        zIndex: 9998,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        justifyContent: 'center',
        alignSelf: 'center',
      },
      innerContainer: {
        backgroundColor: 'white',
        borderRadius: 10,
        padding: 10,
        justifyContent: 'center',
        alignSelf: 'center',
      },
      spinnerText: {
        color: 'black',
        alignSelf: 'center',
        justifyContent: 'center'
      },
      image: {
        width: 50,
        height: 50
      },
      spinner: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center'
      },
    })

export default ProgressSpinner;
