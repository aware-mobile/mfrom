import React, { Component } from 'react';
import { Dimensions, Image, ListView, ScrollView, StyleSheet, Text, TouchableHighlight, View } from 'react-native';

import DFService from './Service/DFService';
import apiConfig from './Util/Config';
import STRINGS from './Util/strings'
import AppText from './Util/AppText'

var Ionicons = require('react-native-vector-icons/Ionicons');
var { height, width } = Dimensions.get('window');
var Accordion = require('react-native-accordion');

var dataSource = new ListView.DataSource(
  { rowHasChanged: (r1, r2) => r1._id !== r2._id }
);

var isActionAllowed;

class SelectFormListModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headerTitle: 'Choose a form to',
      dataSource: dataSource.cloneWithRows([{}]),
      selectForm: this.props.dataFormCreate,
      dataList: [],
      createWorkData: {},
      selectSection: []
    };
    isActionAllowed = true;
    this.getWorkingList()
  }

  getWorkingList() {
    var self = this;
    var apiData = {};
    self.props.toggleMainSpinner(true);
    DFService.callApiWithHeader(apiData, apiConfig.getWorkinglistPublished, function (error, response) {
      self.props.toggleMainSpinner(false);
      if (response) {
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          self.successWorking(response);
        } else {
          // Failed Response
          self.props.openMainModal(response.resMessage)
        }
      } else {
        // Error from application calling service
        console.log("error = ", error);
      }
    });
  }

  start() {
    if (!isActionAllowed) {
      return;
    }
    isActionAllowed = false;

    var user_form_id = this.state.selectForm.form_flow_id;
    var id = this.state.selectForm.id;
    var self = this;
    var apiData = {
      form_flow_id: user_form_id,
      form_id: id
    };

    self.props.toggleMainSpinner(true);
    DFService.callApiWithHeader(apiData, apiConfig.createWork, function (error, response) {
      if (response) {
        if (response.resCode === 'DF2000000') {
          // Successfull Response
          //console.log("responseCreateWork = ", response);
          let arrVariable = response.data.user_form_id.template_name.match(/\$\{(.*?)\}/g);
          if (arrVariable) {
            for (let x = 0; x < arrVariable.length; x++) {
              let strFormNameVar = arrVariable[x].replace(/\$\{(.*?)\}/g, "$1")
              response.data.user_form_id.template_name = response.data.user_form_id.template_name.replace("${" + strFormNameVar + "}", '');
            }
          }
          self.setState({ createWorkData: response.data })
          self.props.createWorkModal(self.state.createWorkData, 'creatework')
          self.props.closeModal
        } else {
          // Failed Response
          self.setState({
            isLoading: false
          });
          self.props.openMainModal(response.resMessage)
        }
      } else {
        // Error from application calling service
        self.setState({
          isLoading: false
        });
        console.log("error = ", error);
      }

      isActionAllowed = true;
      self.props.toggleMainSpinner(false);
    });
  }

  successWorking(response) {
    if (response.resCode === 'DF2000000') {
      var resCategories = response.data.categories;
      var categories = [];
      for (var i = 0; i < resCategories.length; i++) {
        resCategories[i].active = false;
        categories.push(resCategories[i]);
      }

      this.setState({
        dataSource: dataSource.cloneWithRows(response.data.categories),
        dataList: categories
      });

    } else {
      this.setState({ message: response.resMessage });
    }
  }

  rowPressed(sectionIndex, rowIndex) {
    var formData = this.state.dataList[sectionIndex].forms[rowIndex];
    formData.color = this.state.dataList[sectionIndex].color;
    if (!(this.state.selectForm)) {
      this.setState({ selectForm: formData })
    } else {
      if (this.state.selectForm.id === formData.id) {
        this.setState({ selectForm: {} })
      } else {
        this.setState({ selectForm: formData })
      }
    }
  }

  pressCreateForm() {
    if (this.props.nextToStep == 3) {
      if (this.state.selectForm.id !== undefined) {
        this.start()
      }
    } else {
      if (this.state.selectForm.id !== undefined) {
        this.props.nextPage(this.state.selectForm, this.props.nextToStep)
      }
    }
  }

  setSelectSection(index) {

    var categories = this.state.dataList
    var catObj = this.state.dataList[index]
    if (catObj.active) {
      catObj.active = false
    } else {
      catObj.active = true
    }
    categories[index] = catObj

    this.setState({ dataList: categories })
  }

  render() {

    var styleBtnNext;
    if (this.state.selectForm.id !== undefined) {
      styleBtnNext = {
        width: 80,
        height: 36,
        backgroundColor: '#B2D234',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#B2D234',
        borderWidth: 1,
        borderRadius: 4,
      }
    } else {
      styleBtnNext = {
        width: 80,
        height: 36,
        backgroundColor: '#90A3AE',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#90A3AE',
        borderWidth: 1,
        borderRadius: 4,
      }
    }

    var sectionHeader = []
    if (this.state.dataList.length > 0) {
      for (let i = 0; i < this.state.dataList.length; i++) {
        var category = this.state.dataList[i]
        var dataInRow = []
        //add data in row
        for (let x = 0; x < category.forms.length; x++) {
          var form = category.forms[x]
          var selectBackground;
          if (this.state.selectForm.id === form.id) {
            selectBackground = {
              backgroundColor: '#dddddd'
            }
          } else {
            selectBackground = {
              backgroundColor: '#ffffff'
            }
          }

          dataInRow.push(
            <View key={form.id} style={styles.box}>
              <TouchableHighlight
                onPress={() => this.rowPressed(i, x)}
                underlayColor='#dddddd'

                style={selectBackground}>
                <View style={[styles.rowContainer]}>
                  <View style={[styles.circle, { backgroundColor: category.color }]}>
                    <Image source={require('./img/wl_transaction_icon.png')} style={styles.listImage} />
                  </View>
                  <View style={{ flexDirection: 'column', paddingLeft: 5 }}>
                    <View style={{ flexDirection: 'row', width: width - 130 }}>
                      <AppText
                        numberOfLines={1}
                        style={{ fontSize: 14, fontWeight: 'bold', color: 'black' }}>{form.template_name}</AppText>
                        {form.isNewForm ? (<Image source={require('./img/ic_new_form.png')} style={{ left: 5, width: 49, height: 24 }} />) : (null)}
                    </View>
                    <View style={{ flexDirection: 'row', width: width - 130 }}>
                      <AppText
                        numberOfLines={2}
                        style={{ fontSize: 13, color: 'grey' }}>{form.description}</AppText>
                    </View>
                  </View>
                </View>
              </TouchableHighlight>
            </View>
          )
        }
        var heightSectionContainerInActive = {
          height: 42,
          justifyContent: 'center',
          backgroundColor: '#ffffff',
          borderColor: '#efeff1',
          borderBottomWidth: 2
        };
        var heightSectionContainerActive = {
          height: 40 + (category.forms.length * 82),
          justifyContent: 'center',
          backgroundColor: '#ffffff',
        };
        //add header
        sectionHeader.push(
          <View
            key={category._id}
            style={category.active ? heightSectionContainerActive : heightSectionContainerInActive}>

            <View style={{ height: 40, backgroundColor: '#ffffff' }}>
              <TouchableHighlight
                onPress={() => this.setSelectSection(i)}
                underlayColor='#dddddd'>
                <View style={{ height: 40, alignItems: 'center', flexDirection: 'row' }}>
                  <View style={[styles.dotSection, { backgroundColor: category.color, width: 10 }]} />
                  <Text numberOfLines={1}
                    style={{ flex: 0.8, fontSize: 20, marginLeft: 10, marginRight: 10, fontFamily: 'DB Heavent' }}>
                    {category.name}
                  </Text>
                  <View style={{ width: 20 }}>
                    {category.active ? <Ionicons name="ios-arrow-down" size={20} color="black"
                      style={{ alignItems: 'center', justifyContent: 'center' }} /> :
                      <Ionicons name="ios-arrow-forward" size={20} color="black"
                        style={{ alignItems: 'center', justifyContent: 'center' }} />}
                  </View>
                </View>
              </TouchableHighlight>
            </View>

            {category.active ? dataInRow : null}
          </View>
        )
      }
    }

    return (
      <View style={styles.container}>
        <ScrollView>
          <View>
            <View>
              {sectionHeader}
            </View>
          </View>
        </ScrollView>
        <View style={styles.bottomContainer}>
          <AppText style={styles.stepCounter}>
            {this.props.nextToStep === 3 ? STRINGS.formatString(STRINGS.COMMON.STEP_COUNTER, 2, 2) : STRINGS.formatString(STRINGS.COMMON.STEP_COUNTER, 2, 3)}
          </AppText>

          <View style={{ alignSelf: "flex-end", flexGrow: undefined }}>
            <TouchableHighlight
              onPress={() => this.pressCreateForm()}
              underlayColor='#dddddd'
              style={styleBtnNext}>
              <View>
                <AppText style={{ color: '#ffffff' }}>
                  {this.props.nextToStep === 3 ? STRINGS.COMMON.BTN_START : STRINGS.COMMON.BTN_NEXT}
                </AppText>
              </View>
            </TouchableHighlight>
          </View>
        </View>

      </View>
    )
  }
}

var styles = StyleSheet.create({
  container: {
    height: height - 180,
    flexGrow: 1,
    paddingTop: 13,
    paddingLeft: 13,
    paddingRight: 13,
    flexDirection: 'column'
  },
  bodyContainer: {
    // flexGrow: 11
  },
  headerText: {
    marginTop: 18,
    fontWeight: 'bold',
    fontSize: 15,
  },
  bodyText: {
    marginTop: 9,
    fontSize: 14,
    color: 'grey'
  },
  sectionHeader: {
    height: 40,
    borderColor: '#efeff1',
    borderBottomWidth: 2,
    justifyContent: 'center',

  },
  containerChoice: {
    marginTop: 80,
    height: 130,
    flexDirection: 'column'
  },
  dotSection: {
    width: 10,
    height: 10,
    borderRadius: 10 / 2,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black',
  },
  circleLeft: {
    width: 85,
    height: 85,
    borderRadius: 85 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
    alignSelf: 'center',
    marginRight: 30
  },
  circleRight: {
    width: 85,
    height: 85,
    borderRadius: 85 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
    alignSelf: 'center',
    marginLeft: 30
  },
  top: {
    // flexGrow: 0.8,
  },
  bottomContainer: {
    // flexGrow: 1,
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'transparent'
  },
  circle: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2,
    backgroundColor: 'red',
    marginRight: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  rowContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    height: 80
  },
  rowContain: {
    flexDirection: 'row'
  },
  colContainer: {
    flexDirection: 'column',
    padding: 10
  },
  listImage: {
    width: 16,
    height: 20
  },
  box: {
    borderColor: '#efeff1',
    borderBottomWidth: 2,
  },
  boxSection: {
    // flexGrow: 1,
    padding: 10,
    backgroundColor: '#527FE4',
    borderColor: '#efeff1',
    borderBottomWidth: 2,
  },
  stepCounter: {
    justifyContent: 'center',
  },
  nextBtn: {
    backgroundColor: "#B2D234",
    borderColor: "#B2D234",
    alignSelf: "flex-end",
    flexGrow: undefined,
    paddingLeft: 30,
    paddingRight: 30
  },
});

export default SelectFormListModal;
