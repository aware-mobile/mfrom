import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    TouchableHighlight,
    ListView,
    ActivityIndicator,
    AsyncStorage,
    Platform
} from 'react-native';

import FormPage from './FormPage'
import DFService from './Service/DFService';
import apiConfig from './Util/Config';
import AppText from './Util/AppText';
import STRINGS from './Util/strings';

var dataSource = new ListView.DataSource(
    { rowHasChanged: (r1, r2) => r1._id !== r2._id }
);

var rows;
var self;
var response;
class FormList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            message: '',
            dataSource: dataSource.cloneWithRows([{}])
        };
        self = this;
        this._loadInitial();
        //this._getFormList();
    }

    _updateDataSource(data) {
        this.setState({
            dataSource: dataSource.cloneWithRows(data)
        })
    }

    async _loadInitial() {
        try {
            var value = await AsyncStorage.getItem("Profile");
            response = await JSON.parse(value);
            if (value !== null) {
                // console.log("FormList value = ", response);
                self._getFormList(response);
            }
        } catch (error) {
            console.log(error)
        }
    }

    rowPressed(formData, sectionID, rowID) {
        for (var i = 0; i < rows.length; i++) {
            rows[i].active = false
        }
        this._updateDataSource(rows)

        this.props.navigator.push({
            title: formData.template_name,
            name: 'submit_form',
            component: FormPage,
            passProps: {
                formData: formData,
                userid: response.user._id
            }
        });
    }

    renderRow(rowData, sectionID, rowID) {

        return (
            <View>
                <TouchableHighlight
                    onPress={() => this.rowPressed(rowData, sectionID, rowID) }
                    underlayColor='#dddddd'
                    style={{ backgroundColor: '#ffffff' }}>
                    <View style={styles.rowContainer}>
                        <Image source={require('./img/form_icon.png') } style={styles.listImage}/>
                        <View style={{ flexDirection: 'column' }}>
                            <AppText>{rowData.template_name}</AppText>
                            <AppText style={{ fontSize: 14, color: 'grey' }}>{STRINGS.formatString(STRINGS.FORM_LIST_PAGE.VERSION, rowData.version)}</AppText>
                        </View>
                    </View>
                </TouchableHighlight>
                <View style={styles.separator}/>
            </View>
        );
    }

    //  set active swipeout item
    _handleSwipeout(sectionID, rowID) {
        for (var i = 0; i < rows.length; i++) {
            if (i != rowID) rows[i].active = false
            else rows[i].active = true
        }
        this._updateDataSource(rows)
    }

    render() {

        var loadView;
        if (this.state.isLoading) {
            loadView =
                <View style={styles.spinner}>
                    <ActivityIndicator
                        hidden='true'
                        size='large'/>
                    <AppText>{STRINGS.COMMON.LOADING}</AppText>
                </View>
        } else {
            if (this.state.message != '') {
                loadView =
                    <View style={styles.container}>
                        <AppText>{this.state.message}</AppText>
                    </View>
            } else {
                if (Platform.OS === 'ios') {
                    loadView =
                        <ListView
                            style={styles.listView}
                            dataSource={this.state.dataSource}
                            renderRow={this.renderRow.bind(this) }/>
                } else {
                    loadView =
                        <ListView
                            dataSource={this.state.dataSource}
                            renderRow={this.renderRow.bind(this) }/>
                }
            }

        }

        return (
            loadView
        )
    }

    _getFormList(profile) {
        DFService.formListApi(profile, function (error, response) {
            console.log("response = ", response);
            if (response) {
                self._handleResponse(response.data)
            } else {
                console.log("error = ", error);
                this.setState({
                    isLoading: false,
                    message: error
                });
            }
        });

    }

    _handleResponse(jsonResponse) {
        rows = jsonResponse
        this.setState({
            isLoading: false,
            message: '',
            dataSource: dataSource.cloneWithRows(jsonResponse)
        });

    }
}

const styles = StyleSheet.create({
    spinner: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        flexGrow: 1,
        marginTop: 70,
    },
    rowContainer: {
        flexDirection: 'row',
        padding: 10
    },
    separator: {
        height: 1,
        backgroundColor: '#dddddd'
    },
    listView: {
        marginTop: 65
    },
    listImage: {
        width: 40,
        height: 40
    }
})

export default FormList;
